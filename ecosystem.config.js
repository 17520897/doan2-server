const REPO = "git@gitlab.com:17520897/doan2-server.git";
module.exports = {
  apps: [
    {
      name: "kltn",
      script: "/root/kltn/kltn-server/server/index.js",
      env: {
        PORT: 3333,
        NODE_ENV: "production",
      },
    },
  ],
  deploy: {
    prod: {
      user: "root",
      host: "171.244.142.146",
      ref: "origin/master",
      repo: REPO,
      ssh_options: "StrictHostKeyChecking=no",
      path: `/root/kltn/kltn-server`,
      "pre-deploy": "git pull",
      "post-deploy":
        "npm install" +
        " && pm2 startOrRestart ecosystem.config.js" +
        " && pm2 save",
    },
  },
};
