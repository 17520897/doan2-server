import {
  ValidatorConstraint,
  ValidatorConstraintInterface,
  ValidationArguments,
  registerDecorator,
  ValidationOptions,
  validate,
  ValidatorOptions,
} from "class-validator";
import { plainToClass } from "class-transformer";
import ResponseService from "../services/responseService";
import { ErrorsCode } from "../const/errorsCode";
import { HTTPCode } from "../const";

export const enum RequestType {
  query,
  body,
  params,
}
function IsObjectHaveProperty(
  props: string[],
  validationOptions?: ValidationOptions
) {
  return function (object: Object, propertyName: string) {
    registerDecorator({
      name: "IsObjectHaveProperty",
      target: object.constructor,
      propertyName: propertyName,
      constraints: [...props],
      options: validationOptions,
      validator: {
        validate(value: any, args: ValidationArguments) {
          let propLength = props.length;
          for (let i = 0; i < propLength; i++) {
            if (value) {
              if (!value && !value[props[i]]) return false;
            } else return false;
          }
          return true;
        },
      },
    });
  };
}

function Valid(
  dtoClass: any,
  requestType: RequestType,
  options: ValidatorOptions = {}
) {
  return async function (req, res, next) {
    let data;
    if (requestType === RequestType.query) data = req.query;
    if (requestType === RequestType.body) data = req.body;
    if (requestType === RequestType.params) data = req.params;
    data = plainToClass(dtoClass, data);
    const err = await validate(data, { ...options });
    const errors = [];
    if (err.length > 0) {
      err.forEach((item) => {
        errors.push({
          field: item.property,
          error: item.constraints[Object.keys(item.constraints)[0]],
          code: ErrorsCode.invalid,
        });
      });
      return ResponseService.send(res, HTTPCode.badRequest, {
        errors: [...errors],
      });
    }
    next();
  };
}

export { Valid, IsObjectHaveProperty };
