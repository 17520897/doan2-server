import { Request, Response, NextFunction } from "express";
import log4j from "../common/log4js";
import l from "../common/logger";
import { ErrorsCode } from "../const/errorsCode";

// eslint-disable-next-line no-unused-vars, no-shadow
export default function errorHandler(
  err,
  req: Request,
  res: Response,
  next: NextFunction
) {
  res.status(err.status || 500).json({
    errors: [
      {
        code: ErrorsCode.serverError,
        error: err.message,
      },
    ],
  });
  l.error(`errorHandler: ${err.message}`);
  log4j("errorHandler: ").error(err);
}
