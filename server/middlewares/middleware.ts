import BaseResponse from "../services/baseResponse";
import { NextFunction, Request, Response } from "express";
import personValidate from "../api/person/person.validate";
import { ErrorsCode } from "../const/errorsCode";
import companiesValidate from "../api/companies/companies.validate";
import accountNumberService from "../api/accountNumber/accountNumber.service";
import companyBankingService from "../api/companyBanking/companyBanking.service";
import stockValidate from "../api/stock/stock.validate";
import companiesService from "../api/companies/companies.service";

const baseResponse = new BaseResponse("ServiceBillDetailMiddleware");

class Middleware {
  async personCreateOrReceiveExisted(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { userId } = req.cookies;
      const { personCreateId, personReceiveId } = req.body;
      const [
        isPersonCreateExisted,
        isPersonReceiveExisted,
      ] = await Promise.all([
        personValidate.isPersonExisted(userId, personCreateId),
        personValidate.isPersonExisted(userId, personReceiveId),
      ]);
      if (personCreateId && !isPersonCreateExisted) {
        return baseResponse.notAccepted({
          res,
          errors: [
            {
              code: ErrorsCode.personCreateNotFound,
              field: "personCreateId",
            },
          ],
        });
      }
      if (personReceiveId && !isPersonReceiveExisted) {
        return baseResponse.notAccepted({
          res,
          errors: [
            {
              code: ErrorsCode.personReceiveNotFound,
              field: "personReceiveId",
            },
          ],
        });
      }
      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async manyPersonIsSame(req: Request, res: Response, next: NextFunction) {
    try {
      const { data } = req.body;
      for (let i = 0; i < data.length; i++) {
        if (data[i].personCreateId === data[i].personReceiveId) {
          return baseResponse.notAccepted({
            res,
            errors: [
              {
                code: ErrorsCode.personIsSameId,
                field: "personReceiveId",
              },
            ],
          });
        }
      }
      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async personIsSame(req: Request, res: Response, next: NextFunction) {
    try {
      const { personCreateId, personReceiveId } = req.body;
      if (personCreateId === personReceiveId) {
        return baseResponse.notAccepted({
          res,
          errors: [
            {
              code: ErrorsCode.personIsSameId,
              field: "personReceiveId",
            },
          ],
        });
      }
      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async manyAccountNumberIsSame(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { data } = req.body;
      for (let i = 0; i < data.length; i++)
        if (data[i].receiveAccountNumber === data[i].debitAccountNumber) {
          return baseResponse.notAccepted({
            res,
            errors: [
              {
                code:
                  ErrorsCode.middlewareReceiveAndDebitAccountNumberCanNotSame,
                field: "receiveAccountNumber",
              },
            ],
          });
        }
      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async accountNumberIsSame(req: Request, res: Response, next: NextFunction) {
    try {
      const { receiveAccountNumber, debitAccountNumber } = req.body;
      if (receiveAccountNumber === debitAccountNumber) {
        return baseResponse.notAccepted({
          res,
          errors: [
            {
              code: ErrorsCode.middlewareReceiveAndDebitAccountNumberCanNotSame,
              field: "receiveAccountNumber",
            },
          ],
        });
      }
      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async userHaveCompanies(req: Request, res: Response, next: NextFunction) {
    try {
      const { userId } = req.cookies;
      const { companyId } = req.body;
      const isUserHaveCompanies = await companiesValidate.isUserHaveCompanies(
        userId,
        companyId
      );
      if (!isUserHaveCompanies)
        return baseResponse.notAccepted({
          res,
          errors: [
            {
              code: ErrorsCode.companiesNotFound,
              field: "companyId",
            },
          ],
        });

      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async existedAccountNumber(req: Request, res: Response, next: NextFunction) {
    try {
      const {
        receiveAccountNumber,
        debitAccountNumber,
        accountNumberId,
      } = req.body;
      const [
        checkReceiveAccountNumber,
        checkDebitAccountNumber,
      ] = await Promise.all([
        accountNumberService.getOne({
          _id: accountNumberId,
          "accountNumbers.accountNumber": receiveAccountNumber,
        }),
        accountNumberService.getOne({
          _id: accountNumberId,
          "accountNumbers.accountNumber": debitAccountNumber,
        }),
      ]);
      if (receiveAccountNumber && !checkReceiveAccountNumber) {
        return baseResponse.notAccepted({
          res,
          errors: [
            {
              code: ErrorsCode.middlewareReceiveAccountNumberNotExisted,
              field: "receiveAccountNumber",
            },
          ],
        });
      }
      if (debitAccountNumber && !checkDebitAccountNumber) {
        return baseResponse.notAccepted({
          res,
          errors: [
            {
              code: ErrorsCode.middlewareDebitAccountNumberNotExisted,
              field: "debitAccountNumber",
            },
          ],
        });
      }
      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async companyHaveAccountNumber(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { accountNumberId, companyId } = req.body;
      if (accountNumberId && companyId) {
        const checkCompaniesAccountNumber = await companiesService.getOne({
          _id: companyId,
          accountNumberId,
        });
        if (!checkCompaniesAccountNumber) {
          return baseResponse.notAccepted({
            res,
            errors: [
              {
                code: ErrorsCode.accountNumberNotExisted,
                field: "accountNumberId",
              },
            ],
          });
        }
      }
      next();
    } catch (error) {
      baseResponse.serverError(res, error);
    }
  }

  async userHaveCompaniesBank(req: Request, res: Response, next: NextFunction) {
    try {
      const { userId } = req.cookies;
      const { companyBankId, companyId } = req.body;
      if (companyBankId && companyId) {
        const checkCompaniesBank = await companyBankingService.getOne({
          userCreateId: userId,
          companyId: companyId,
          _id: companyBankId,
        });
        if (!checkCompaniesBank) {
          return baseResponse.notAccepted({
            res,
            errors: [
              {
                code: ErrorsCode.companyBankingBankNotExisted,
                field: "companyBankId",
                error: "companies bank not existed with user",
              },
            ],
          });
        }
      }
      next();
    } catch (error) {
      baseResponse.serverError(res, error);
    }
  }

  async companyHaveStock(req: Request, res: Response, next: NextFunction) {
    try {
      const { userId } = req.cookies;
      const { companyId, stockId } = req.body;
      const isCompanyHaveStock = await stockValidate.isCompanyHaveStock(
        userId,
        companyId,
        stockId
      );
      if (!isCompanyHaveStock) {
        return baseResponse.notAccepted({
          res,
          errors: [
            {
              code: ErrorsCode.stockCompaniesNotHaveStock,
              field: "stockId",
            },
          ],
        });
      }
      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }
}

export default new Middleware();
