import l from "../common/logger";
import jsonWebToken from "jsonwebtoken";
import { ErrorsCode } from "../const/errorsCode";
import responseService from "../services/responseService";
import rolesService, { RolesService } from "../api/roles/roles.service";
import usersService from "../api/users/users.service";
import {
  AccountType,
  AdminStatus,
  HTTPCode,
  RoleStatus,
  UserRoleStatus,
  UsersStatus,
} from "../const";
import log4j from "../common/log4js";
import { PermissionKeys } from "../const/permissions";
import adminService from "../api/admin/admin.service";
import systemConstService from "../api/systemConst/systemConst.service";
import userRolesService from "../api/userRoles/userRoles.service";
import BaseResponse from "../services/baseResponse";

const baseResponse = new BaseResponse("Validate");

export function usersValidate() {
  return (req, res, next) => {
    try {
      if (!req.headers["authorization"])
        return responseService.send(res, HTTPCode.unauthorize, {
          message: "you are not authorize",
        });
      const headersToken = req.headers["authorization"].split(" ");

      if (headersToken[0] !== "Bearer")
        return responseService.send(res, HTTPCode.unauthorize, {
          message: "you are not authorize",
        });

      if (!headersToken[1]) {
        return responseService.send(res, HTTPCode.unauthorize, {
          message: "you are not authorize",
        });
      }
      const token = jsonWebToken.verify(
        headersToken[1],
        process.env.JWT_SECRET
      ) as any;
      if (!token)
        return responseService.send(res, HTTPCode.unauthorize, {
          message: "you are not authorize",
        });
      if (token.isAccess && token.isUser) {
        req.cookies.userId = token.userId;
        next();
      } else
        return responseService.send(res, HTTPCode.unauthorize, {
          message: "you are not authorize",
        });
    } catch (error) {
      return responseService.send(res, HTTPCode.unauthorize, {
        message: "you are not authorize",
      });
    }
  };
}

export function validateRole(
  key: PermissionKeys = null,
  accountType: AccountType = AccountType.users
) {
  return async function (req, res, next) {
    try {
      if (req.headers["authorization"]) {
        const headersToken = req.headers["authorization"].split(" ");
        if (headersToken[0] !== "Bearer")
          return responseService.send(res, HTTPCode.unauthorize, {
            message: "you are not authorize",
          });

        if (!headersToken[1]) {
          return responseService.send(res, HTTPCode.unauthorize, {
            message: "you are not authorize",
          });
        }
        const token = jsonWebToken.verify(
          headersToken[1],
          process.env.JWT_SECRET
        ) as any;
        if (
          !token ||
          !token.isAccess ||
          (accountType === AccountType.users && token.isUser === false) ||
          (accountType === AccountType.admin && token.isAdmin === false)
        )
          return responseService.send(res, HTTPCode.unauthorize, {
            message: "you are not authorize",
            errors: [
              {
                code: ErrorsCode.usersIsBlocked,
              },
            ],
          });
        if (accountType === AccountType.admin) {
          // check if account type if admin
          const { adminId } = token;
          //check user existed;
          const checkAdmin = await adminService.getOne({
            _id: adminId,
            status: AdminStatus.normal,
          });
          if (!checkAdmin) {
            return responseService.send(res, HTTPCode.unauthorize, {
              message: "you are not authorize",
              errors: [
                {
                  code: ErrorsCode.adminIsBlockedOrDeleted,
                },
              ],
            });
          }
          //check user have permissions;
          if (key) {
            const { roleId } = checkAdmin;
            const checkPermission = await rolesService.getOne({
              _id: roleId,
              "rolePermissions.permissionKey": key,
              status: RoleStatus.enable,
            });
            if (!checkPermission) {
              l.info("Admin not have permission");
              return responseService.send(res, HTTPCode.unauthorize, {
                message: "you are not authorize",
                errors: [
                  {
                    code: ErrorsCode.adminDidNotHavePermissions,
                  },
                ],
              });
            }
          }
          req.cookies.adminId = adminId;
          next();
        } else if (accountType === AccountType.users) {
          // check if account type if users
          const { userId } = token;
          //check user existed;
          const checkUsers = await usersService.getOne({
            _id: userId,
            status: UsersStatus.normal,
          });
          if (!checkUsers) {
            return responseService.send(res, HTTPCode.unauthorize, {
              message: "you are not authorize",
              errors: [
                {
                  code: ErrorsCode.usersIsBlocked,
                },
              ],
            });
          }
          //check user have permissions;
          if (key) {
            const { roleId } = checkUsers;
            const [checkPermission, systemConst] = await Promise.all([
              userRolesService.getOne({
                _id: roleId,
                "permissions.permissionKey": key,
                status: UserRoleStatus.enable,
              }),
              systemConstService.getOne({}),
            ]);
            if (!checkPermission) {
              //check free roles have this permissions
              const userFreeRoles = await userRolesService.getOne({
                _id: systemConst.userRoleFreeId,
                "permissions.permissionKey": key,
                status: UserRoleStatus.enable,
              });
              if (userFreeRoles) {
                req.cookies.userId = userId;
                next();
              }
              return responseService.send(res, HTTPCode.unauthorize, {
                message: "you are not authorize",
                errors: [
                  {
                    code: ErrorsCode.notAuthorize,
                  },
                ],
              });
            }
          }
          req.cookies.userId = userId;
          next();
        } else if (accountType === AccountType.all) {
          req.cookies.userId = token.userId;
          req.cookies.adminId = token.adminId;
          next();
        } else {
          return responseService.send(res, HTTPCode.unauthorize, {
            message: "you are not authorize",
            errors: [
              {
                code: ErrorsCode.notAuthorize,
              },
            ],
          });
        }
      } else {
        return responseService.send(res, HTTPCode.unauthorize, {
          message: "you are not authorize",
          errors: [
            {
              code: ErrorsCode.notAuthorize,
            },
          ],
        });
      }
    } catch (error) {
      return responseService.send(res, HTTPCode.unauthorize, {
        message: "you are not authorize",
      });
    }
  };
}
