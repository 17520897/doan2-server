import { Request, Response } from "express";
import bcrypt from "bcrypt";
import VerifyService from "../verify/verify.service";
import jwt from "../../services/jwt";
import ResponseService from "../../services/responseService";
import randomString from "randomstring";
import {
  VerifyType,
  SMSType,
  VerifyAccountType,
  HTTPCode,
  UsersStatus,
  UserRoleStatus,
} from "../../const/index";
import l from "../../common/logger";
import usersService from "./users.service";
import sendSMS from "../../services/sendSms";
import { ErrorsCode } from "../../const/errorsCode";
import responseService from "../../services/responseService";
import BaseResponse from "../../services/baseResponse";
import mongoose from "mongoose";
import services from "../../services/services";
import userRolesService from "../userRoles/userRoles.service";
import systemConstService from "../systemConst/systemConst.service";
import mailService from "../../services/mailService";

const baseResponse = new BaseResponse("UsersController");
export class UsersController {
  async register(req: Request, res: Response) {
    try {
      const code = randomString.generate({
        length: 6,
        charset: "numeric",
      });
      const { phone, email } = req.body;
      // if (process.env.NODE_ENV !== "dev") {
      //   const sms = await sendSMS.send(phone, code, SMSType.register);
      //   if (sms.errors) {
      //     return ResponseService.send(res, HTTPCode.serverError, {
      //       errors: [
      //         {
      //           code: ErrorsCode.smsCanNotSend,
      //           error: "can not send sms",
      //         },
      //       ],
      //     });
      //   }
      // }
      const mail = await mailService.send({
        to: email,
        subject: "[PD Accounting] Register Code",
        html: `
          <h1>PD Accounting Register Successful</h1>
          <br>Your verify code is: <b>${code}</b>
          <br>Your verify code will be valid for 5 minute
          <br><b>Thank you for using our services!</b>
        `,
      });
      if (mail.errors) {
        return responseService.send(res, HTTPCode.serverError, {
          errors: [
            {
              code: ErrorsCode.emailCanNotSend,
              error: "email can not send",
            },
          ],
        });
      }
      //make verify code
      await VerifyService.create({
        email: email,
        code,
        type: VerifyType.register,
        accountType: VerifyAccountType.user,
      });
      return ResponseService.send(res, HTTPCode.success, {
        message: "register success",
        data: null,
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async login(req: Request, res: Response) {
    try {
      //Get from middleware
      const { userId, roleId } = req.body;
      const [userRoles, systemConst] = await Promise.all([
        userRolesService.getOne({
          _id: roleId,
          status: UserRoleStatus.enable,
        }),
        systemConstService.getOne({}),
      ]);
      let permissions;
      if (!userRoles) {
        const userFreeRoles = await userRolesService.getById(
          systemConst.userRoleFreeId
        );
        permissions = userFreeRoles.rolePermissions;
      } else {
        permissions = userRoles.rolePermissions;
      }
      return ResponseService.send(res, HTTPCode.success, {
        message: "login success",
        data: {
          accessToken: jwt.generateAccessToken({
            userId,
            isUser: true,
          }),
          refreshToken: jwt.generateRefreshToken({
            userId,
            isUser: true,
          }),
          permissions,
        },
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async getUsersInfo(req: Request, res: Response) {
    try {
      const { userId } = req.cookies;
      const user = await usersService.populate({
        query: {
          _id: userId,
          status: UsersStatus.normal,
        },
        populate: [
          {
            path: "roleInfo",
          },
        ],
        select: "-password",
      });
      return ResponseService.send(res, HTTPCode.success, {
        message: "Get user info success",
        data: user,
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async updateUserInfo(req: Request, res: Response) {
    const session = await mongoose.startSession();
    session.startTransaction();
    try {
      const options = { session };
      const { userId } = req.cookies;
      const user = await usersService.updateById(userId, req.body, options);
      user.password = undefined;
      await session.commitTransaction();
      session.endSession();
      return ResponseService.send(res, HTTPCode.success, {
        message: "update user success",
        data: user,
      });
    } catch (error) {
      await session.abortTransaction();
      return baseResponse.serverError(res, error);
    } finally {
      session.endSession();
    }
  }
}
export default new UsersController();
