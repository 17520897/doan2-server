import express from "express";
import { AccountType } from "../../const";
import { RequestType, Valid } from "../../middlewares/Valid";
import { validateRole } from "../../middlewares/validate";
import UsersRegisterDto from "./dto/usesRegister.dto";
import usersController from "./users.controller";
import usersMiddleware from "./users.middleware";

const usersRouter = express.Router();

usersRouter.post(
  "/register",
  Valid(UsersRegisterDto, RequestType.body),
  usersMiddleware.register,
  usersController.register
);

usersRouter.post(
  "/login",
  Valid(UsersRegisterDto, RequestType.body),
  usersMiddleware.login,
  usersController.login
);

usersRouter.put(
  "/",
  validateRole(null, AccountType.users),
  usersMiddleware.updateUsersInfo,
  usersController.updateUserInfo
);

usersRouter.get(
  "/",
  validateRole(null, AccountType.users),
  usersController.getUsersInfo
);
export default usersRouter;
