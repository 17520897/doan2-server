import { Request, Response, NextFunction } from "express";
import ResponseService from "../../services/responseService";
import bcrypt from "bcrypt";
import { ErrorsCode } from "../../const/errorsCode";
import l from "../../common/logger";
import usersService from "./users.service";
import { HTTPCode, UsersStatus, VerifyAccountType } from "../../const";
import P from "pino";
import responseService from "../../services/responseService";
import verifyService from "../verify/verify.service";
import BaseResponse from "../../services/baseResponse";
import mongoose from "mongoose";
import { IUsersModel } from "./users.model";
import locationService from "../location/location.service";
import services from "../../services/services";
const baseResponse = new BaseResponse("UsersMiddleware");
class UsersMiddleware {
  async register(req: Request, res: Response, next: NextFunction) {
    try {
      const { email } = req.body;
      const verify = await verifyService.getByQuery({
        query: {
          email,
          createAt: {
            $gt: Date.now() - parseInt(process.env.TIME_WAIT_SEND_VERIFY_CODE),
          },
          accountType: VerifyAccountType.user,
        },
      });
      if (verify.length > 0) {
        return ResponseService.send(res, HTTPCode.notAccept, {
          message: "verify code has been sent",
          errors: [
            {
              code: ErrorsCode.verifyCodeHasSent,
              field: "createAt",
              error: "only 1 code in 1 minute",
            },
          ],
        });
      }
      const users = await usersService.getOne({ email: req.body.email });
      if (users)
        return ResponseService.send(res, HTTPCode.notAccept, {
          message: "user has been registered",
          errors: [
            {
              code: ErrorsCode.usersEmailRegistered,
              field: "email",
              error: "email has been registered",
            },
          ],
        });
      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }
  async login(req: Request, res: Response, next: NextFunction) {
    try {
      const body = req.body;
      const { email } = req.body;
      const [checkExistedUsers, checkBlockedUsers] = await Promise.all([
        usersService.getOne({ email }),
        usersService.getOne({ email, status: UsersStatus.normal }),
      ]);
      if (!checkExistedUsers)
        return ResponseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.usersNotFound,
              field: "phone",
              error: "user not found",
            },
          ],
        });
      if (!checkBlockedUsers) {
        return ResponseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.usersIsBlocked,
              field: "status",
              error: "user is blocked",
            },
          ],
        });
      }
      const match = await bcrypt.compare(
        body.password,
        checkExistedUsers.password
      );
      if (!match)
        return ResponseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.usersWrongPassword,
              field: "password",
              error: "wrong password",
            },
          ],
          message: "wrong password",
        });
      req.body.userId = checkExistedUsers._id;
      req.body.roleId = checkExistedUsers.roleId;
      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async updateUsersInfo(req: Request, res: Response, next: NextFunction) {
    try {
      const {
        password,
        temporaryAddress,
        permanentAddress,
      } = req.body as IUsersModel;
      const { oldPassword } = req.body;
      if (temporaryAddress) {
        const checkTemporaryAddress = await locationService.getByAddress({
          ward: temporaryAddress.ward,
          district: temporaryAddress.district,
          city: temporaryAddress.city,
        });
        if (
          !checkTemporaryAddress.city ||
          checkTemporaryAddress.district ||
          checkTemporaryAddress.ward
        ) {
          return responseService.send(res, HTTPCode.notAccept, {
            errors: [
              {
                code: ErrorsCode.locationNotFound,
                field: "temporaryAddress",
              },
            ],
          });
        }
      }
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }
}

export default new UsersMiddleware();
