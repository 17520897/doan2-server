import { Users, IUsersModel } from "./users.model";
import BaseDbService from "../../services/baseDb";

export class BorrowerUserService extends BaseDbService<IUsersModel> {}

export default new BorrowerUserService(Users);
