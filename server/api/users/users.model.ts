import mongoose, { Schema } from "mongoose";

export interface IUsersModel extends mongoose.Document {
  phone: string;
  name: string;
  password: string;
  gender: string;
  temporaryAddress: {
    home: string;
    ward: string;
    district: string;
    city: string;
    wardText: {
      type: String;
    };
    districtText: {
      type: String;
    };
    cityText: {
      type: String;
    };
  };
  permanentAddress: {
    home: string;
    ward: string;
    district: string;
    city: string;
    wardText: {
      type: String;
    };
    districtText: {
      type: String;
    };
    cityText: {
      type: String;
    };
  };
  dayOfBirth: Date;
  email: string;
  identityNumber: string;
  dateOfIssue: Date;
  placeOfIssue: string;
  creatAt: Date;
  roleId: string;
}

const schema = new mongoose.Schema(
  {
    email: {
      type: String,
      required: true,
    },
    password: {
      type: String,
      required: true,
    },
    name: {
      type: String,
      index: "text",
    },
    phone: {
      type: String,
      index: "text",
    },
    identityNumber: {
      type: String,
      index: "text",
    },
    temporaryAddress: {
      home: {
        type: String,
      },
      ward: {
        type: Number,
      },
      district: {
        type: Number,
      },
      city: {
        type: Number,
      },
      wardText: {
        type: String,
      },
      districtText: {
        type: String,
      },
      cityText: {
        type: String,
      },
    },
    permanentAddress: {
      home: {
        type: String,
      },
      ward: {
        type: Number,
      },
      district: {
        type: Number,
      },
      city: {
        type: Number,
      },
      wardText: {
        type: Number,
      },
      districtText: {
        type: Number,
      },
      cityText: {
        type: Number,
      },
    },
    gender: {
      type: String,
      enum: ["male", "female", "other"],
    },
    dayOfBirth: {
      type: Date,
    },
    dateOfIssue: {
      type: Date,
    },
    placeOfIssue: {
      type: Number,
    },
    createAt: {
      type: Date,
      default: Date.now,
    },
    status: {
      type: String,
      enum: ["normal", "block"],
      default: "normal",
    },
    roleId: {
      type: mongoose.Types.ObjectId,
    },
  },
  {
    collection: "Users",
  }
);

schema.virtual("roleInfo", {
  ref: "Roles",
  localField: "roleId",
  foreignField: "_id",
  justOne: true,
});
schema.set("toObject", { virtuals: true });
schema.set("toJSON", { virtuals: true });
export const Users = mongoose.model<IUsersModel>("Users", schema);
