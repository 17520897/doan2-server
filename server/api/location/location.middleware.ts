import { Request, Response, NextFunction } from "express";
import BaseResponse from "../../services/baseResponse";
import l from "../../common/logger";
import responseService from "../../services/responseService";
import { ErrorsCode } from "../../const/errorsCode";
import locationService from "./location.service";
import { HTTPCode } from "../../const";

const baseResponse = new BaseResponse("LocationMiddleware");
export class LocationMiddleware {
  //   async getLocation(req: Request, res: Response, next: NextFunction) {
  //     try {
  //       let { parentCode, level } = req.query as any;
  //       parentCode = parseInt(parentCode);
  //       level = parseInt(level);
  //       if (level > 1 && parentCode < 0) {
  //         return responseService.send(res, HTTPCode.success, {
  //           errors: [
  //             {
  //               code: ErrorsCode.locationParentCodeMustBeGreaterThanZero,
  //               error: "parent code must be greater than zero",
  //             },
  //           ],
  //         });
  //       }
  //       next();
  //     } catch (error) {
  //       ;
  //
  //       return responseService.send(res, HTTPCode.serverError, {
  //         errors: [
  //           {
  //             code: ErrorsCode.serverError,
  //             error: error.message,
  //           },
  //         ],
  //       });
  //     }
  //   }

  async getByAddressLocation(req: Request, res: Response, next: NextFunction) {
    try {
      const { district, ward, city } = req.query as any;
      const checkDistrict = locationService.getByCode({
        level: 2,
        code: district,
      });
      if (!checkDistrict) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.locationCodeNotFound,
              error: "district code not found",
              field: "district",
            },
          ],
        });
      }
      if (checkDistrict.parentCode !== parseInt(city)) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.locationDistrictNotInThisCity,
              error: "this district not in this city",
              field: "ward",
            },
          ],
        });
      }
      const checkWard = locationService.getByCode({
        level: 3,
        code: ward,
      });
      if (!checkWard) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.locationCodeNotFound,
              error: "district code not found",
              field: "ward",
            },
          ],
        });
      }
      if (checkWard.parentCode !== parseInt(district)) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.locationWardNotInThisDistrict,
              error: "this ward not in this district",
            },
          ],
        });
      }
      next();
    } catch (error) {
      return responseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    }
  }
}

export default new LocationMiddleware();
