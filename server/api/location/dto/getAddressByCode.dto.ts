import {
  IsString,
  IsEnum,
  IsInt,
  IsOptional,
  Equals,
  IsObject,
  IsArray,
  IsNumber,
  IsNumberString,
  Min,
  Max,
} from "class-validator";
import { IsObjectHaveProperty } from "../../../middlewares/Valid";

export default class GetAddressByCode {
  @IsNumberString()
  code: String;
}
