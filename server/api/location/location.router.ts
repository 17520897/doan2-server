import express from "express";
import locationController from "./location.controller";
import GetLocationDto from "./dto/getLocaion.dto";
import { RequestType, Valid } from "../../middlewares/Valid";
import GetByAddressDto from "./dto/getByAddressLocation.dto";
import locationMiddleware from "./location.middleware";
import GetAddressByCode from "./dto/getAddressByCode.dto";

const locationRouter = express.Router();

locationRouter.get(
  "/",
  Valid(GetLocationDto, RequestType.query),
  locationController.getLocation
);

locationRouter.get(
  "/address",
  Valid(GetByAddressDto, RequestType.query),
  locationMiddleware.getByAddressLocation,
  locationController.getByAddressLocation
);

locationRouter.get(
  "/code",
  Valid(GetAddressByCode, RequestType.query),
  locationController.getAddressByCode
);

locationRouter.get("/child/code", locationController.getAllChildByCodeArray);

export default locationRouter;
