import cities from "../../../config/city.json";
import districts from "../../../config/districts.json";
import wards from "../../../config/wards.json";

interface ILocation {
  name: String;
  fullName: String;
  code: Number;
  prefix: String;
  level: Number;
  parentCode?: Number;
}

interface IAddress {
  city?: ILocation;
  district?: ILocation;
  ward?: ILocation;
}

export class LocationService {
  //lấy toàn bộ dữ liệu từ level và parent code của dữ liệu
  getByLevel({ level, parentCode }): Array<ILocation> {
    level = parseInt(level);
    parentCode = parseInt(parentCode);
    if (level === 2) {
      return districts.filter((item) => {
        return item.parentCode === parentCode;
      });
    } else if (level === 3) {
      return wards.filter((item) => {
        return item.parentCode === parentCode;
      });
    } else {
      return cities;
    }
  }

  getByAddress({ city, district, ward }): IAddress {
    city = parseInt(city);
    district = parseInt(district);
    ward = parseInt(ward);
    let checkDistrict = districts.filter((item) => {
      return item.code === district;
    });
    let checkWard = wards.filter((item) => {
      return item.code === ward;
    });
    if (checkDistrict.length !== 1 || checkDistrict[0].parentCode !== city)
      return null;
    else if (checkWard.length !== 1 || checkWard[0].parentCode !== district)
      return null;
    else
      return {
        city: cities[city],
        district: checkDistrict[0],
        ward: checkWard[0],
      };
  }

  //Lấy record từ level và mã code của record
  getByCode({ level, code }): ILocation {
    level = parseInt(level);
    code = parseInt(code);
    if (level === 2) {
      const district = districts.filter((item) => {
        return item.code === code;
      });
      return district.length === 1 ? district[0] : null;
    } else if (level === 3) {
      const ward = wards.filter((item) => {
        return item.code === code;
      });
      return ward.length === 1 ? ward[0] : null;
    } else {
      return cities[code];
    }
  }

  getAddressByCode(code): IAddress {
    code = parseInt(code);
    let ward = wards.filter((item) => {
      return item.code === code;
    });
    if (ward.length === 1) {
      let district = districts.filter((item) => {
        return item.code === ward[0].parentCode;
      });
      let city = cities[district[0].parentCode];
      return {
        city,
        district: district[0],
        ward: ward[0],
      };
    } else {
      let district = districts.filter((item) => {
        return item.code === code;
      });
      if (district.length === 1) {
        let city = cities[district[0].parentCode];
        return {
          city,
          district: district[0],
        };
      } else {
        let city = cities[code];
        if (city)
          return {
            city,
          };
        else return null;
      }
    }
  }

  getAllChildByCode(code): ILocation[] {
    code = parseInt(code);
    //Tìm tất cả quận của một thành phố nếu code đó là thành phố
    let districtOfCode = districts.filter((item) => {
      return item.parentCode === code;
    });
    if (districtOfCode.length <= 0) {
      //Tìm tất cả phường của một quận nếu code là quận
      let wardOfCode = wards.filter((item) => {
        return item.parentCode === code;
      });
      return wardOfCode;
    } else return districtOfCode;
  }
}

export default new LocationService();
