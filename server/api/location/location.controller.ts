import { Response, Request, response } from "express";
import l from "../../common/logger";
import BaseResponse from "../../services/baseResponse";
import responseService from "../../services/responseService";
import { ErrorsCode } from "../../const/errorsCode";
import locationService from "./location.service";
import { HTTPCode } from "../../const";
import services from "../../services/services";

const baseResponse = new BaseResponse("LocationController");
export class LocationController {
  async getLocation(req: Request, res: Response) {
    try {
      let { level, parentCode } = req.query as any;
      let data = locationService.getByLevel({ level, parentCode });
      return responseService.send(res, HTTPCode.success, {
        message: "get location by level success",
        data,
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async getByAddressLocation(req: Request, res: Response) {
    try {
      let { city, district, ward } = req.query as any;
      return responseService.send(res, HTTPCode.success, {
        message: "get address success",
        data: locationService.getByAddress({ city, district, ward }),
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async getAddressByCode(req: Request, res: Response) {
    try {
      let { code } = req.query;
      return responseService.send(res, HTTPCode.success, {
        message: "get address by code success",
        data: locationService.getAddressByCode(code),
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async getAllChildByCodeArray(req: Request, res: Response) {
    try {
      let { code } = req.query;
      code = code.toString().split(",");
      const locations = {};
      const codeLength = code.length;
      for (let i = 0; i < code.length; i++) {
        const location = locationService.getAllChildByCode(code[i]);
        locations[`${code[i]}`] = location;
      }
      return responseService.send(res, HTTPCode.success, {
        data: locations,
        message: "get child by code success ",
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }
}

export default new LocationController();
