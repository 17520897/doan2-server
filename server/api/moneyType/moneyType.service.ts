import { MoneyType, IMoneyTypeModel } from "./moneyType.model";
import BaseDbService from "../../services/baseDb";

export class MoneyTypeService extends BaseDbService<IMoneyTypeModel> {}

export default new MoneyTypeService(MoneyType);
