import { NextFunction, Request, Response } from "express";
import BaseResponse from "../../services/baseResponse";
import l from "../../common/logger";
import { HTTPCode, MoneyTypeStatus } from "../../const";
import { ErrorsCode } from "../../const/errorsCode";
import responseService from "../../services/responseService";
import services from "../../services/services";
import moneyTypeService from "./moneyType.service";

const baseResponse = new BaseResponse("MoneyTypeMiddleware");
class MoneyTypeMiddleware {
  async existedMoneyType(req: Request, res: Response, next: NextFunction) {
    try {
      const { id } = req.params;
      const moneyType = await moneyTypeService.getById(id);
      if (!moneyType) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.moneyTypeNotExisted,
              error: "money type not existed",
              field: "id",
            },
          ],
        });
      }
      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }
  async createMoneyType(req: Request, res: Response, next: NextFunction) {
    try {
      const { name } = req.body;
      const moneyType = await moneyTypeService.getOne({
        name,
        status: MoneyTypeStatus.normal,
      });
      if (moneyType) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.moneyTypeNameExisted,
              field: "name",
            },
          ],
        });
      }
      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async updateMoneyType(req: Request, res: Response, next: NextFunction) {
    try {
      const { id } = req.params;
      const { name } = req.body;
      const moneyType = await moneyTypeService.getOne({
        _id: { $ne: id },
        name,
        status: MoneyTypeStatus.normal,
      });
      if (moneyType) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.moneyTypeNameExisted,
              field: "name",
            },
          ],
        });
      }
      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }
}

export default new MoneyTypeMiddleware();
