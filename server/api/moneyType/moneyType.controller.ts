import { Request, Response } from "express";
import BaseResponse from "../../services/baseResponse";
import l from "../../common/logger";
import { HTTPCode, MoneyTypeStatus } from "../../const";
import responseService from "../../services/responseService";
import services from "../../services/services";
import moneyTypeService from "./moneyType.service";

const baseResponse = new BaseResponse("MoneyTypeController");
class MoneyTypeController {
  async createMoneyType(req: Request, res: Response) {
    try {
      const { adminId } = req.cookies;
      const moneyType = await moneyTypeService.create({
        ...req.body,
        adminCreateId: adminId,
      });
      return responseService.send(res, HTTPCode.success, {
        data: moneyType,
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async deleteMoneyType(req: Request, res: Response) {
    try {
      const { id } = req.params;
      const { adminId } = req.cookies;
      await moneyTypeService.updateById(id, {
        status: MoneyTypeStatus.deleted,
      });
      return responseService.send(res, HTTPCode.success, {
        message: "delete money type success",
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async updateMoneyType(req: Request, res: Response) {
    try {
      const { id } = req.params;
      const { adminId } = req.cookies;
      const moneyType = await moneyTypeService.updateById(id, {
        ...req.body,
        adminCreateId: adminId,
      });
      return responseService.send(res, HTTPCode.success, {
        message: "update money type success",
        data: moneyType,
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async getMoneyType(req: Request, res: Response) {
    try {
      const { page, limit } = req.query;
      const moneyType = await moneyTypeService.populate({
        query: { status: MoneyTypeStatus.normal },
        populate: [
          {
            path: "adminCreateInfo",
            select: "-password",
          },
        ],
        page,
        limit,
        sort: {
          name: 1,
        },
      });
      return responseService.send(res, HTTPCode.success, {
        data: moneyType,
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }
}

export default new MoneyTypeController();
