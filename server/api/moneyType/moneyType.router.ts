import express from "express";
import { AccountType } from "../../const";
import { PermissionKeys } from "../../const/permissions";
import { validateRole } from "../../middlewares/validate";
import moneyTypeController from "./moneyType.controller";
import moneyTypeMiddleware from "./moneyType.middleware";

const moneyTypeRouter = express.Router();

moneyTypeRouter.get(
  "/",
  validateRole(null, AccountType.all),
  moneyTypeController.getMoneyType
);

moneyTypeRouter.post(
  "/",
  validateRole(PermissionKeys.moneyTypeCreate, AccountType.admin),
  moneyTypeMiddleware.createMoneyType,
  moneyTypeController.createMoneyType
);

moneyTypeRouter.put(
  "/:id",
  validateRole(PermissionKeys.moneyTypeUpdate, AccountType.admin),
  moneyTypeMiddleware.existedMoneyType,
  moneyTypeMiddleware.updateMoneyType,
  moneyTypeController.updateMoneyType
);

moneyTypeRouter.delete(
  "/:id",
  validateRole(PermissionKeys.moneyTypeDelete, AccountType.admin),
  moneyTypeController.deleteMoneyType
);
export default moneyTypeRouter;
