import {
  ValidateNested,
  IsString,
  MaxLength,
  IsEnum,
  IsObject,
  IsDate,
  IsEmail,
  IsNumber,
  IsOptional,
  MinLength,
  IsMongoId,
  Min,
  Length,
  IsUrl,
  IsArray,
  Equals,
} from "class-validator";

export default class UpdateMoneyTypeDto {
  @IsOptional()
  @IsString()
  name: string;

  @IsOptional()
  @IsNumber()
  @Min(1)
  defaultExchangeRate: number;
}
