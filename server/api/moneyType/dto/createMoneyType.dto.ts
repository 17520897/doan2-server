import {
  ValidateNested,
  IsString,
  MaxLength,
  IsEnum,
  IsObject,
  IsDate,
  IsEmail,
  IsNumber,
  IsOptional,
  MinLength,
  IsMongoId,
  Min,
  Length,
  IsUrl,
  IsArray,
  Equals,
} from "class-validator";

export default class CreateMoneyTypeDto {
  @IsString()
  name: string;

  @IsNumber()
  @Min(1)
  defaultExchangeRate: number;
}
