import mongoose, { Schema } from "mongoose";

export interface IMoneyTypeModel extends mongoose.Document {
  name: string;
  defaultExchangeRate: number;
  adminCreateId: string;
  status: string;
}

const schema = new mongoose.Schema(
  {
    name: {
      type: String,
    },
    defaultExchangeRate: {
      type: Number,
    },
    adminCreateId: {
      type: mongoose.SchemaTypes.ObjectId,
    },
    status: {
      type: String,
      enum: ["normal", "deleted"],
      default: "normal",
    },
  },
  {
    collection: "MoneyType",
  }
);

schema.virtual("adminCreateInfo", {
  ref: "Admin",
  localField: "adminCreateId",
  foreignField: "_id",
  justOne: true,
});

schema.set("toObject", { virtuals: true });
schema.set("toJSON", { virtuals: true });

export const MoneyType = mongoose.model<IMoneyTypeModel>("MoneyType", schema);
