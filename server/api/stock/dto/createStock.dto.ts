import {
  ValidateNested,
  IsString,
  MaxLength,
  IsEnum,
  IsObject,
  IsDate,
  IsEmail,
  IsNumber,
  IsOptional,
  MinLength,
  IsMongoId,
  Min,
  Length,
  IsUrl,
  IsArray,
  Equals,
} from "class-validator";
import { StockType } from "../../../const";

export default class CreateStockDto {
  @IsMongoId()
  companyId: string;

  @IsString()
  code: String;

  @IsString()
  name: String;

  @IsEnum(StockType, {
    message: 'type must be: "services", "goods", "fixedAssets", material',
  })
  type: string;

  @IsOptional()
  @IsString()
  unit: String;

  @IsOptional()
  @IsString()
  shortName: String;

  @IsOptional()
  @IsString()
  note: String;
}
