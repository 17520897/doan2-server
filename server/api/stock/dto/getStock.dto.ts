import {
  ValidateNested,
  IsString,
  MaxLength,
  IsEnum,
  IsObject,
  IsDate,
  IsEmail,
  IsNumber,
  IsOptional,
  MinLength,
  IsMongoId,
  Min,
  Length,
  IsUrl,
  IsArray,
  Equals,
} from "class-validator";
import { StockType } from "../../../const";

export default class GetStockDto {
  @IsOptional()
  @IsString()
  search: string;

  @IsMongoId()
  companyId;

  @IsOptional()
  @IsEnum(StockType, {
    message: "type must be: services, goods, fixedAssets",
  })
  type: string;
}
