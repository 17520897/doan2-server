import express from "express";
import { AccountType } from "../../const";
import { RequestType, Valid } from "../../middlewares/Valid";
import { validateRole } from "../../middlewares/validate";
import IdParamsRequestDto from "../../const/IdParamsRequest.dto";
import stockMiddleware from "./stock.middleware";
import stockController from "./stock.controller";
import CheckCodeExistedDto from "./dto/checkCodeExisted.dto";
import CreateStockDto from "./dto/createStock.dto";
import GetStockDto from "./dto/getStock.dto";

const stockRouter = express.Router();

stockRouter.get(
  "/",
  Valid(GetStockDto, RequestType.query),
  validateRole(null, AccountType.users),
  stockController.getStock
);

stockRouter.get(
  "/checkCodeExisted",
  validateRole(null, AccountType.users),
  Valid(CheckCodeExistedDto, RequestType.query),
  stockController.checkCodeExisted
);

stockRouter.post(
  "/",
  validateRole(null, AccountType.users),
  Valid(CreateStockDto, RequestType.body),
  stockMiddleware.userHaveCompanies,
  stockMiddleware.createStock,
  stockController.createStock
);

stockRouter.put(
  "/:id",
  validateRole(null, AccountType.users),
  Valid(IdParamsRequestDto, RequestType.params),
  Valid(CreateStockDto, RequestType.body),
  stockMiddleware.userHaveCompanies,
  stockMiddleware.updateStock,
  stockController.updateStock
);

stockRouter.delete(
  "/:id",
  validateRole(null, AccountType.users),
  Valid(IdParamsRequestDto, RequestType.params),
  stockController.deleteStock
);

export default stockRouter;
