import mongoose, { mongo } from "mongoose";

export interface IStockModel extends mongoose.Document {}

const schema = new mongoose.Schema(
  {
    companyId: {
      type: mongoose.SchemaTypes.ObjectId,
      required: true,
    },
    userCreateId: {
      type: mongoose.SchemaTypes.ObjectId,
      required: true,
    },
    code: {
      type: String,
      index: "text",
    },
    name: {
      type: String,
      index: "text",
    },
    slugName:{
      type: String,
      index: "text",
    },
    shortName: {
      type: String,
      index: "text",
    },
    tax: [
      {
        accountNumber: {
          type: Number,
        },
        percent: {
          type: Number,
        },
      },
    ],
    unit: {
      type: String,
      trim: true,
    },
    type: {
      type: String,
      enum: ["services", "goods", "fixedAssets", "material"],
      default: "goods",
      required: true,
    },
    note: {
      type: String,
    },
    createAt: {
      type: Date,
      default: Date.now,
    },
  },
  {
    collection: "Stock",
  }
);

export const Stock = mongoose.model<IStockModel>("Stock", schema);
