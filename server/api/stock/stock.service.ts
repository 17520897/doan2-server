import { IStockModel, Stock } from "./stock.model";
import { QueryFindOneAndUpdateOptions, SaveOptions } from "mongoose";
import BaseDbService from "../../services/baseDb";

export class StockService extends BaseDbService<IStockModel> {}

export default new StockService(Stock);
