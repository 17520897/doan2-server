import BaseResponse from "../../services/baseResponse";
import accountNumberService from "../accountNumber/accountNumber.service";
import stockService from "./stock.service";

class stockValidate {
  async isCodeExisted(userId, companyId, code, checkId = false, id = "") {
    const stock = checkId
      ? await stockService.getOne({
          code,
          companyId,
          userCreateId: userId,
          _id: { $ne: id },
        })
      : await stockService.getOne({ code, companyId, userCreateId: userId });
    return stock ? true : false;
  }

  async isCompanyHaveStock(userId, companyId, stockId) {
    const stock = await stockService.getOne({
      userCreateId: userId,
      companyId,
      _id: stockId,
    });
    return stock ? true : false;
  }
}

export default new stockValidate();
