import { Request, Response } from "express";
import BaseResponse from "../../services/baseResponse";
import l from "../../common/logger";
import { HTTPCode } from "../../const";
import responseService from "../../services/responseService";
import services from "../../services/services";
import stockService from "./stock.service";
import slugify from "slugify"

const baseResponse = new BaseResponse("StockController");

class StockController {
  async createStock(req: Request, res: Response) {
    try {
      const { userId } = req.cookies;
      const stock = await stockService.create({
        ...req.body,
        userCreateId: userId,
        slugName: slugify(
          req.body.name,
          {
            lower: true,
            replacement: '_',
            trim: true,
            locale: 'vi'
          }
        )
      });
      return responseService.send(res, HTTPCode.success, {
        data: stock,
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async updateStock(req: Request, res: Response) {
    try {
      const { id } = req.params;
      const stock = await stockService.updateById(id, {
        ...req.body,
        createAt: Date.now(),
        slugName: req.body.name ? slugify(
          req.body.name,
          {
            lower: true,
            replacement: '_',
            trim: true,
            locale: 'vi'
          }
        ) : undefined
      });
      return responseService.send(res, HTTPCode.success, {
        data: stock,
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async deleteStock(req: Request, res: Response) {
    try {
      const { id } = req.params;
      await stockService.deleteById(id);
      return responseService.send(res, HTTPCode.success, {
        message: "delete success",
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async getStock(req: Request, res: Response) {
    try {
      const { userId } = req.cookies;
      const { search, page, limit, companyId, type } = req.query;
      let query = {
        userCreateId: userId,
        companyId,
      } as any;
      if (search) {
        query = {
          ...query,
          $or: [
            {
              name: { $regex: search, $options: "i" },
            },
            {
              shortName: { $regex: search, $options: "i" },
            },
            {
              code: { $regex: search, $options: "i" },
            },
          ],
        };
      }
      if (type) {
        query = {
          ...query,
          type,
        };
      }
      const stock = await stockService.getByQuery({
        query,
        page,
        limit,
        sort: {
          name: 1,
        },
      });
      return responseService.send(res, HTTPCode.success, {
        data: stock,
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async checkCodeExisted(req: Request, res: Response) {
    try {
      const { code, companyId } = req.query;
      const { userId } = req.cookies;
      const isCodeExisted = await stockService.getOne({
        code,
        companyId,
        userCreateId: userId,
      });
      return responseService.send(res, HTTPCode.success, {
        data: {
          isExisted: isCodeExisted ? true : false,
        },
      });
    } catch (error) {
      l.error("checkCodeExisted controller: ", error);
      return baseResponse.serverError(res, error);
    }
  }
}

export default new StockController();
