import { NextFunction, Request, Response } from "express";
import BaseResponse from "../../services/baseResponse";
import l from "../../common/logger";
import { CompaniesStatus, HTTPCode, MoneyTypeStatus } from "../../const";
import { ErrorsCode } from "../../const/errorsCode";
import responseService from "../../services/responseService";
import companiesService from "../companies/companies.service";
import stockValidate from "./stock.validate";
import companiesValidate from "../companies/companies.validate";

const baseResponse = new BaseResponse("StockMiddleware");

class StockMiddleware {
  async userHaveCompanies(req: Request, res: Response, next: NextFunction) {
    try {
      const { companyId } = req.body;
      const { userId } = req.cookies;
      if (companyId) {
        const checkUserCompanies = await companiesValidate.isUserHaveCompanies(
          userId,
          companyId
        );
        if (!checkUserCompanies) {
          baseResponse.notAccepted({
            res,
            errors: [
              {
                code: ErrorsCode.companiesNotFound,
                field: "companyId",
                error: "user did not have companies",
              },
            ],
          });
        }
      }
      next();
    } catch (error) {
      baseResponse.serverError(res, error);
    }
  }

  async createStock(req: Request, res: Response, next: NextFunction) {
    try {
      const { userId } = req.cookies;
      const { code, companyId } = req.body;
      const isCodeExisted = await stockValidate.isCodeExisted(
        userId,
        companyId,
        code
      );
      if (isCodeExisted) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.stockCodeExisted,
              field: "code",
            },
          ],
        });
      }
      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async updateStock(req: Request, res: Response, next: NextFunction) {
    try {
      const { id } = req.params;
      const { userId } = req.cookies;
      const { code, companyId } = req.body;
      const isCodeExisted = await stockValidate.isCodeExisted(
        userId,
        companyId,
        code,
        true,
        id
      );
      if (isCodeExisted) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.stockCodeExisted,
              field: "code",
            },
          ],
        });
      }
      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }
}

export default new StockMiddleware();
