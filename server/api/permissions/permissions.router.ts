import express from "express";
import permissionsController from "./permissions.controller";
import { validateRole } from "../../middlewares/validate";
import { PermissionKeys } from "../../const/permissions";

const permissionRouter = express.Router();

permissionRouter.get("/", permissionsController.getListPermissions);
export default permissionRouter;
