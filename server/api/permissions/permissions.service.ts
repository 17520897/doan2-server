import BaseDbService from "../../services/baseDb";
import { Permissions, IPermissionsModel } from "./permissions.model";

export class PermissionsService extends BaseDbService<IPermissionsModel> {}

export default new PermissionsService(Permissions);
