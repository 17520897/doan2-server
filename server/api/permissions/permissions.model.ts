import mongoose, { Schema } from "mongoose";

export interface IPermissionsModel extends mongoose.Document {
  groupName: string,
  groupKey: string,
  permissions: Array<any>
}

const schema = new mongoose.Schema(
  {
    groupName: {
      type: String,
      required: true,
    },
    groupKey: {
      type: String,
      required: true,
      unique: true,
    },
    permissions: [
      {
        name: {
          type: String,
          required: true,
        },
        key: {
          type: String,
          required: true,
          unique: true,
        },
        dependOn: []
      },
    ],
  },
  {
    collection: "Permissions",
  }
);

export const Permissions = mongoose.model<IPermissionsModel>("Permissions", schema);
