import { Request, Response, NextFunction } from "express";
import BaseResponse from "../../services/baseResponse";
import l from "../../common/logger";
import responseService from "../../services/responseService";
import { ErrorsCode } from "../../const/errorsCode";
import jsonWebToken from "jsonwebtoken";
import { HTTPCode } from "../../const";
import services from "../../services/services";
const baseResponse = new BaseResponse("RefreshTokenMiddleware");
export class RefreshTokenMiddleware {
  async getNewAccessToken(req: Request, res: Response, next: NextFunction) {
    try {
      if (req.headers["authorization"]) {
        const headersToken = req.headers["authorization"].split(" ");
        if (headersToken[0] !== "Bearer")
          return responseService.send(res, HTTPCode.unauthorize, {
            message: "you are not authorize",
          });

        if (!headersToken[1]) {
          return responseService.send(res, HTTPCode.unauthorize, {
            message: "you are not authorize",
          });
        }
        const token = jsonWebToken.verify(
          headersToken[1],
          process.env.JWT_SECRET
        ) as any;
        if (!token || !token.isRefresh)
          return responseService.send(res, HTTPCode.unauthorize, {
            message: "you are not authorize",
          });
        delete token.iat;
        delete token.exp;
        req.body.token = token;
        next();
      } else {
        return responseService.send(res, HTTPCode.unauthorize, {
          message: "you are not authorize",
        });
      }
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }
}

export default new RefreshTokenMiddleware();
