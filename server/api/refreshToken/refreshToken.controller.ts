import { Request, Response, NextFunction } from "express";
import BaseResponse from "../../services/baseResponse";
import l from "../../common/logger";
import responseService from "../../services/responseService";
import { ErrorsCode } from "../../const/errorsCode";
import jwt from "../../services/jwt";
import jsonWebToken from "jsonwebtoken";
import { HTTPCode } from "../../const";
import services from "../../services/services";
const baseResponse = new BaseResponse("RefreshTokenController");
export class RefreshTokenController {
  async getNewAccessToken(req: Request, res: Response) {
    try {
      const { token } = req.body;
      const accessToken = jwt.generateAccessToken({
        ...token,
        isRefresh: undefined,
      });
      return responseService.send(res, HTTPCode.success, {
        data: {
          accessToken,
        },
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }
}

export default new RefreshTokenController();
