import express from "express";
import refreshTokenMiddleware from "./refreshToken.middleware";
import refreshTokenController from "./refreshToken.controller";

const router = express.Router();

router.get(
  "/",
  refreshTokenMiddleware.getNewAccessToken,
  refreshTokenController.getNewAccessToken
);

export default router;
