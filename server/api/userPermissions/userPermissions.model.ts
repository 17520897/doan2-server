import mongoose, { Schema } from "mongoose";

export interface IUserPermissionsModel extends mongoose.Document {
  groupName: string;
  groupKey: string;
  UserPermissions: Array<any>;
}

const schema = new mongoose.Schema(
  {
    groupName: {
      type: String,
      required: true,
    },
    groupKey: {
      type: String,
      required: true,
      unique: true,
    },
    permissions: [
      {
        name: {
          type: String,
          required: true,
        },
        key: {
          type: String,
          required: true,
          unique: true,
        },
        dependOn: [],
      },
    ],
  },
  {
    collection: "UserPermissions",
  }
);

export const UserPermissions = mongoose.model<IUserPermissionsModel>(
  "UserPermissions",
  schema
);
