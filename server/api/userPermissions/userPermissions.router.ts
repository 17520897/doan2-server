import express from "express";
import userPermissionsController from "./userPermissions.controller";

const userPermissionsRouter = express.Router();

userPermissionsRouter.get(
  "/",
  userPermissionsController.getListUserPermissions
);
export default userPermissionsRouter;
