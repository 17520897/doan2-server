import { Request, Response } from "express";
import l from "../../common/logger";
import responseService from "../../services/responseService";
import { ErrorsCode } from "../../const/errorsCode";
import permissionsService from "./userPermissions.service";
import BaseResponse from "../../services/baseResponse";
import { HTTPCode } from "../../const";
import services from "../../services/services";

const baseResponse = new BaseResponse("UserPermissionController");
export class UserPermissionController {
  async getListUserPermissions(req: Request, res: Response) {
    try {
      const { page, limit } = req.query;
      const permissions = await permissionsService.getByQuery({
        page,
        limit,
        sort: {
          groupName: 1,
        },
      });
      return responseService.send(res, HTTPCode.success, {
        message: "get all permissions success",
        data: permissions,
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }
}

export default new UserPermissionController();
