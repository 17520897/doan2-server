import BaseDbService from "../../services/baseDb";
import {
  UserPermissions,
  IUserPermissionsModel,
} from "./userPermissions.model";

export class UserPermissionsService extends BaseDbService<
  IUserPermissionsModel
> {}

export default new UserPermissionsService(UserPermissions);
