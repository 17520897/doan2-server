import { NextFunction, Request, Response } from "express";
import BaseResponse from "../../services/baseResponse";
import l from "../../common/logger";
import { HTTPCode, MoneyTypeStatus } from "../../const";
import responseService from "../../services/responseService";
import mongoose from "mongoose";
import serviceBillDetailService from "./serviceBillDetail.service";
import serviceBillService from "../serviceBill/serviceBill.service";

const baseResponse = new BaseResponse("ServiceBillDetailController");

class ServiceBillDetailController {
  async createManyServiceBillDetail(req: Request, res: Response) {
    const session = await mongoose.startSession();
    session.startTransaction();
    try {
      const options = { session };
      const { userId } = req.cookies;
      const { companyId, serviceBillId, accountNumberId, data } = req.body;
      let totalMoney = 0;
      const createData = [];
      for (let i = 0; i < data.length; i++) {
        createData.push({
          ...data[i],
          companyId,
          serviceBillId,
          accountNumberId,
          userCreateId: userId,
        });
        totalMoney += data[i].finalMoney;
      }
      await serviceBillDetailService.deleteMany(
        {
          serviceBillId,
        },
        options
      );
      await serviceBillDetailService.createMany(createData, options);
      await serviceBillService.updateById(
        serviceBillId,
        {
          money: totalMoney,
        },
        options
      );
      await session.commitTransaction();
      return baseResponse.success({
        res,
        message: "create service bill detail success",
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    } finally {
      session.endSession();
    }
  }

  async getServiceBillDetail(req: Request, res: Response) {
    try {
      const { userId } = req.cookies;
      const { companyId, serviceBillId } = req.query;
      const serviceBillDetail = await serviceBillDetailService.populate({
        query: {
          userCreateId: userId,
          companyId,
          serviceBillId,
        },
        sort: {
          stockId: 1,
        },
        populate: [
          {
            path: "stockInfo",
          },
        ],
      });
      return baseResponse.success({
        res,
        data: serviceBillDetail,
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }
}

export default new ServiceBillDetailController();
