import mongoose, { mongo, Schema } from "mongoose";

export interface IServiceBillDetailModel extends mongoose.Document {
  userCreateId: string;
}

const schema = new mongoose.Schema(
  {
    companyId: {
      type: mongoose.SchemaTypes.ObjectId,
      required: true,
      index: true,
    },
    userCreateId: {
      type: mongoose.SchemaTypes.ObjectId,
      required: true,
      index: true,
    },
    serviceBillId: {
      type: mongoose.SchemaTypes.ObjectId,
      required: true,
      index: true,
    },
    accountNumberId: {
      type: mongoose.SchemaTypes.ObjectId,
      required: true,
      index: true,
    },
    stockId: {
      type: mongoose.SchemaTypes.ObjectId,
      required: true,
      index: true,
    },
    receiveAccountNumber: {
      type: Number,
      index: true,
    },
    debitAccountNumber: {
      type: Number,
      index: true,
    },
    money: {
      type: Number,
    },
    moneyType: {
      type: String,
      default: "VND",
    },
    exchangeRate: {
      type: Number,
      default: 1,
    },
    finalMoney: {
      type: Number,
      required: true,
    },
    userCreateAt: {
      type: Date,
      default: Date.now,
    },
  },
  {
    collection: "ServiceBillDetail",
  }
);

schema.virtual("userCreateInfo", {
  ref: "User",
  localField: "userCreateId",
  foreignField: "_id",
  justOne: true,
});

schema.virtual("companyInfo", {
  ref: "Companies",
  localField: "companyId",
  foreignField: "_id",
  justOne: true,
});

schema.virtual("stockInfo", {
  ref: "Stock",
  localField: "stockId",
  foreignField: "_id",
  justOne: true,
});

schema.set("toObject", { virtuals: true });
schema.set("toJSON", { virtuals: true });

export const ServiceBillDetail = mongoose.model<IServiceBillDetailModel>(
  "ServiceBillDetail",
  schema
);
