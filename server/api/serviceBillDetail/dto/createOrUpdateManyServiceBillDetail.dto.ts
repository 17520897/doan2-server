import {
  IsString,
  IsEnum,
  IsInt,
  IsOptional,
  Equals,
  IsObject,
  IsArray,
  IsNumber,
  IsNumberString,
  Min,
  Max,
  IsMongoId,
  IsDateString,
} from "class-validator";
import { IsObjectHaveProperty } from "../../../middlewares/Valid";
import { MoneyBillType } from "../../../const";

export default class CreteOrUpdateManyServiceBillDetailDto {
  @IsMongoId()
  companyId: string;

  @IsMongoId()
  serviceBillId: string;

  @IsMongoId()
  accountNumberId: string;

  @IsArray()
  data: Array<any>;
}
