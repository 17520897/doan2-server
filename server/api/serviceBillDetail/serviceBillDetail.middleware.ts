import { NextFunction, Request, Response } from "express";
import l from "../../common/logger";
import { CompaniesStatus, HTTPCode, MoneyTypeStatus } from "../../const";
import { ErrorsCode } from "../../const/errorsCode";
import BaseResponse from "../../services/baseResponse";
import responseService from "../../services/responseService";
import services from "../../services/services";
import companiesService from "../companies/companies.service";
import companiesValidate from "../companies/companies.validate";
import companyBankingService from "../companyBanking/companyBanking.service";
import moneyBillValidate from "../moneyBill/moneyBill.validate";
import personValidate from "../person/person.validate";
import serviceBillValidate from "../serviceBill/serviceBill.validate";
import stockValidate from "../stock/stock.validate";

const baseResponse = new BaseResponse("ServiceBillDetailMiddleware");
class ServiceBillDetailMiddleware {
  async companyHaveServiceBill(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { userId } = req.cookies;
      const { serviceBillId, companyId } = req.body;
      const isCompanyHaveServiceBill = await serviceBillValidate.isCompaniesHaveServiceBill(
        userId,
        companyId,
        serviceBillId
      );
      if (!isCompanyHaveServiceBill) {
        return baseResponse.notAccepted({
          res,
          errors: [
            {
              code: ErrorsCode.serviceBillCompaniesNotHaveServiceBill,
              field: "serviceBillId",
            },
          ],
        });
      }
      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }
}

export default new ServiceBillDetailMiddleware();
