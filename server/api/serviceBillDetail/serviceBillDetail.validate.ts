import serviceBillDetailService from "./serviceBillDetail.service";

class ServiceBillDetailValidate {
  async isCompaniesHaveServiceBillDetail(
    userId,
    companyId,
    serviceBillDetailId
  ) {
    const serviceBill = await serviceBillDetailService.getOne({
      userCreateId: userId,
      _id: serviceBillDetailId,
      companyId,
    });
    return serviceBill ? true : false;
  }
}

export default new ServiceBillDetailValidate();
