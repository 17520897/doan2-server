import BaseDbService from "../../services/baseDb";
import {
  ServiceBillDetail,
  IServiceBillDetailModel,
} from "./serviceBillDetail.model";

export class ServiceBillService extends BaseDbService<
  IServiceBillDetailModel
> {}

export default new ServiceBillService(ServiceBillDetail);
