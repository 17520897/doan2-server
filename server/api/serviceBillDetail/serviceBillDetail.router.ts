import express from "express";
import { AccountType } from "../../const";
import { RequestType, Valid } from "../../middlewares/Valid";
import { validateRole } from "../../middlewares/validate";
import IdParamsRequestDto from "../../const/IdParamsRequest.dto";
import middleware from "../../middlewares/middleware";
import serviceBillDetailController from "./serviceBillDetail.controller";
import serviceBillDetailMiddleware from "./serviceBillDetail.middleware";
import CreteOrUpdateManyServiceBillDetailDto from "./dto/createOrUpdateManyServiceBillDetail.dto";
import GetServiceBillDetailDto from "./dto/getServiceBillDetail.dto";

const serviceBillDetailRouter = express.Router();

serviceBillDetailRouter.post(
  "/",
  Valid(CreteOrUpdateManyServiceBillDetailDto, RequestType.body),
  validateRole(null, AccountType.users),
  middleware.manyAccountNumberIsSame,
  middleware.userHaveCompanies,
  middleware.companyHaveAccountNumber,
  serviceBillDetailMiddleware.companyHaveServiceBill,
  serviceBillDetailController.createManyServiceBillDetail
);

serviceBillDetailRouter.get(
  "/",
  Valid(GetServiceBillDetailDto, RequestType.query),
  validateRole(null, AccountType.users),
  serviceBillDetailController.getServiceBillDetail
);

export default serviceBillDetailRouter;
