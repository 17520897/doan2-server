import { Request, Response } from "express";
import BaseResponse from "../../services/baseResponse";
import l from "../../common/logger";
import { HTTPCode, MoneyTypeStatus } from "../../const";
import responseService from "../../services/responseService";
import serviceBillService from "./serviceBill.service";

const baseResponse = new BaseResponse("ServiceBillController");
class ServiceBillController {
  async createServiceBill(req: Request, res: Response) {
    try {
      const { userId } = req.cookies;
      const moneyType = await serviceBillService.create({
        ...req.body,
        userCreateId: userId,
      });
      return responseService.send(res, HTTPCode.success, {
        data: moneyType,
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async searchServiceBill(req: Request, res: Response) {
    try {
      const { userId } = req.cookies;
      const {
        page,
        limit,
        companyId,
        beginBillCreateAt,
        endBillCreateAt,
        personCreateId,
        personReceiveId,
        sortType = -1,
        sortProperties,
      } = req.query as any;
      let query = {
        userCreateId: userId,
        companyId,
      } as any;
      let sort = {} as any;
      if (sortProperties) {
        sort[sortProperties] = parseInt(sortType);
      }

      if (beginBillCreateAt && endBillCreateAt)
        query = {
          ...query,
          $and: [
            {
              billCreateAt: { $gte: beginBillCreateAt },
            },
            {
              billCreateAt: { $lte: endBillCreateAt },
            },
          ],
        };
      if (personCreateId) {
        query["personCreateId"] = personCreateId;
      }
      if (personReceiveId) {
        query["personReceiveId"] = personReceiveId;
      }
      const moneyBill = await serviceBillService.populate({
        query,
        page,
        limit,
        sort: {
          ...sort,
        },
        populate: [
          {
            path: "personCreateInfo",
          },
          {
            path: "personReceiveInfo",
          },
        ],
      });
      return responseService.send(res, HTTPCode.success, {
        data: moneyBill,
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async updateServiceBill(req: Request, res: Response) {
    try {
      const { id } = req.params;
      const { userId } = req.cookies;
      const serviceBill = await serviceBillService.updateById(id, {
        ...req.body,
        userCreateId: userId,
        userCreateAt: Date.now(),
      });
      return responseService.send(res, HTTPCode.success, {
        message: "update service bill success",
        data: serviceBill,
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }
}

export default new ServiceBillController();
