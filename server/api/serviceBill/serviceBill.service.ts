import BaseDbService from "../../services/baseDb";
import { ServiceBill, IServiceBillModel } from "./serviceBill.model";

export class ServiceBillService extends BaseDbService<IServiceBillModel> {}

export default new ServiceBillService(ServiceBill);
