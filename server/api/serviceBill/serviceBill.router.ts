import express from "express";
import { AccountType } from "../../const";
import { RequestType, Valid } from "../../middlewares/Valid";
import { validateRole } from "../../middlewares/validate";
import IdParamsRequestDto from "../../const/IdParamsRequest.dto";
import serviceBillMiddleware from "./serviceBill.middleware";
import serviceBillController from "./serviceBill.controller";
import UserCreateOrUpdateServiceBillDto from "./dto/createOrUpdateServiceBill";
import GetServiceBillDto from "./dto/getServiceBill.dto";
import middleware from "../../middlewares/middleware";

const serviceBillRouter = express.Router();

serviceBillRouter.post(
  "/",
  Valid(UserCreateOrUpdateServiceBillDto, RequestType.body),
  validateRole(null, AccountType.users),
  middleware.personIsSame,
  middleware.userHaveCompanies,
  middleware.personCreateOrReceiveExisted,
  middleware.userHaveCompaniesBank,
  middleware.companyHaveAccountNumber,
  middleware.existedAccountNumber,
  serviceBillController.createServiceBill
);

serviceBillRouter.put(
  "/:id",
  Valid(IdParamsRequestDto, RequestType.params),
  Valid(UserCreateOrUpdateServiceBillDto, RequestType.body),
  validateRole(null, AccountType.users),
  middleware.personIsSame,
  middleware.userHaveCompanies,
  middleware.personCreateOrReceiveExisted,
  middleware.userHaveCompaniesBank,
  middleware.companyHaveAccountNumber,
  middleware.existedAccountNumber,
  serviceBillController.updateServiceBill
);

serviceBillRouter.get(
  "/",
  Valid(GetServiceBillDto, RequestType.query),
  validateRole(null, AccountType.users),
  serviceBillController.searchServiceBill
);

export default serviceBillRouter;
