import { NextFunction, Request, Response } from "express";
import BaseResponse from "../../services/baseResponse";
import l from "../../common/logger";
import { CompaniesStatus, HTTPCode, MoneyTypeStatus } from "../../const";
import { ErrorsCode } from "../../const/errorsCode";
import responseService from "../../services/responseService";
import services from "../../services/services";
import companiesService from "../companies/companies.service";
import companyBankingService from "../companyBanking/companyBanking.service";
import personValidate from "../person/person.validate";
import serviceBillService from "./serviceBill.service";
import companiesValidate from "../companies/companies.validate";

const baseResponse = new BaseResponse("ServiceBillMiddleware");
class ServiceBillMiddleware {}

export default new ServiceBillMiddleware();
