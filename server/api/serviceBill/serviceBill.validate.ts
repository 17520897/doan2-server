import serviceBillService from "./serviceBill.service";

class ServiceBillValidate {
  async isCompaniesHaveServiceBill(userId, companyId, serviceBillId) {
    const serviceBill = await serviceBillService.getOne({
      userCreateId: userId,
      _id: serviceBillId,
      companyId,
    });
    return serviceBill ? true : false;
  }
}

export default new ServiceBillValidate();
