import {
  IsString,
  IsEnum,
  IsInt,
  IsOptional,
  Equals,
  IsObject,
  IsArray,
  IsNumber,
  IsNumberString,
  Min,
  Max,
  IsMongoId,
  IsDateString,
} from "class-validator";
import { IsObjectHaveProperty } from "../../../middlewares/Valid";

export default class GetServiceBillDto {
  @IsMongoId()
  companyId;

  @IsOptional()
  @IsMongoId()
  id: string;

  @IsOptional()
  @IsDateString()
  beginBillCreateAt: string;

  @IsOptional()
  @IsDateString()
  endBillCreateAt: string;

  @IsOptional()
  @IsMongoId()
  personCreateId: string;

  @IsOptional()
  @IsMongoId()
  personReceiveId: string;
}
