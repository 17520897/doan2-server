import { NextFunction, Request, Response } from "express";
import BaseResponse from "../../services/baseResponse";
import { HTTPCode } from "../../const";
import { ErrorsCode } from "../../const/errorsCode";
import responseService from "../../services/responseService";
import accountNumberService from "./accountNumber.service";

const baseResponse = new BaseResponse("AccountNumberMiddleware");
class AccountNumberMiddleware {
  async checkFormatAccountNumber(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { accountNumbers } = req.body;
      if (accountNumbers) {
        const accountNumberLength = accountNumbers.length;
        for (let i = 0; i < accountNumberLength; i++) {
          for (let j = 0; j < accountNumberLength; j++) {
            if (
              (accountNumbers[j].accountNumber ===
                accountNumbers[i].accountNumber &&
                j !== i) ||
              !accountNumbers[i].level ||
              !accountNumbers[j].level
            ) {
              return responseService.send(res, HTTPCode.notAccept, {
                errors: [
                  {
                    code:
                      ErrorsCode.accountNumberArrayAccountNumbersWrongFormat,
                    field: "accountNumbers",
                  },
                ],
              });
            }
          }
        }
      }
      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async createAccountNumber(req: Request, res: Response, next: NextFunction) {
    try {
      const { circulars } = req.body;
      const accountNumber = await accountNumberService.getOne({ circulars });
      if (accountNumber) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.accountNumberCircularsExisted,
              field: "circulars",
            },
          ],
        });
      }
      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }
  async updateAccountNumber(req: Request, res: Response, next: NextFunction) {
    try {
      const { id } = req.params;
      const { circulars } = req.body;
      const accountNumber = await accountNumberService.getOne({
        circulars,
        _id: { $ne: id },
      });
      if (accountNumber) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.accountNumberCircularsExisted,
              field: "circulars",
            },
          ],
        });
      }
      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }
}

export default new AccountNumberMiddleware();
