import mongoose, { Schema } from "mongoose";

export interface IAccountNumberModel extends mongoose.Document {
  circulars: String;
  accountNumbers: Array<{
    accountName: string;
    accountNumber: number;
    parentNumber: number;
    level: number;
  }>;
  adminCreateId: string;
}

const schema = new mongoose.Schema(
  {
    circulars: {
      type: String,
    },
    accountNumbers: [
      {
        accountName: {
          type: String,
        },
        accountNumber: {
          type: Number,
        },
        parentNumber: {
          type: Number,
        },
        level: {
          type: Number,
          default: 1,
        },
      },
    ],
    adminCreateId: {
      type: mongoose.SchemaTypes.ObjectId,
    },
  },
  {
    collection: "AccountNumber",
  }
);

schema.virtual("adminCreateInfo", {
  ref: "Admin",
  localField: "adminCreateId",
  foreignField: "_id",
  justOne: true,
});

schema.set("toObject", { virtuals: true });
schema.set("toJSON", { virtuals: true });
export const AccountNumber = mongoose.model<IAccountNumberModel>(
  "AccountNumber",
  schema
);
