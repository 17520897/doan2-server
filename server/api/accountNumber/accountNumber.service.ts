import { AccountNumber, IAccountNumberModel } from "./accountNumber.model";
import BaseDbService from "../../services/baseDb";

export class AccountNumberService extends BaseDbService<IAccountNumberModel> {}

export default new AccountNumberService(AccountNumber);
