import {
  ValidateNested,
  IsString,
  MaxLength,
  IsEnum,
  IsObject,
  IsDate,
  IsEmail,
  IsNumber,
  IsOptional,
  MinLength,
  IsMongoId,
  Min,
  Length,
  IsUrl,
  IsArray,
  Equals,
  Max,
} from "class-validator";

export default class UpdateAccountNumberDto {
  @IsOptional()
  @IsString()
  circulars: string;

  @IsOptional()
  @IsArray()
  accountNumbers: Array<any>;
}
