import { Request, Response } from "express";
import { HTTPCode } from "../../const";
import BaseResponse from "../../services/baseResponse";
import responseService from "../../services/responseService";
import accountNumberService from "./accountNumber.service";

const baseResponse = new BaseResponse("AccountNumberController");
class AccountNumberController {
  async createAccountNumber(req: Request, res: Response) {
    try {
      const { adminId } = req.cookies;
      const accountNumber = await accountNumberService.create({
        ...req.body,
        adminCreateId: adminId,
      });
      return responseService.send(res, HTTPCode.success, {
        data: accountNumber,
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }
  async updateAccountNumber(req: Request, res: Response) {
    try {
      const { id } = req.params;
      const { adminId } = req.cookies;
      const accountNumber = await accountNumberService.updateById(id, {
        ...req.body,
        adminCreateId: adminId,
      });
      return responseService.send(res, HTTPCode.success, {
        data: accountNumber,
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async getAccountNumber(req: Request, res: Response) {
    try {
      const accountNumber = await accountNumberService.getByQuery({});
      return responseService.send(res, HTTPCode.success, {
        data: accountNumber,
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async getByIdAccountNumber(req: Request, res: Response) {
    try {
      const { id } = req.params;
      console.log(id);
      const accountNumber = await accountNumberService.getById(id);
      return baseResponse.success({
        res,
        data: accountNumber,
        message: "get by id account number",
        success: true,
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async deleteAccountNumber(req: Request, res: Response) {
    try {
      const { id } = req.params;
      await accountNumberService.deleteById(id);
      return responseService.send(res, HTTPCode.success, {
        message: "delete account number success",
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }
}

export default new AccountNumberController();
