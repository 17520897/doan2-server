import express from "express";
import { AccountType } from "../../const";
import IdParamsRequestDto from "../../const/IdParamsRequest.dto";
import { PermissionKeys } from "../../const/permissions";
import { RequestType, Valid } from "../../middlewares/Valid";
import { validateRole } from "../../middlewares/validate";
import accountNumberController from "./accountNumber.controller";
import accountNumberMiddleware from "./accountNumber.middleware";
import CreateAccountNumberDto from "./dto/createAccountNumber.dto";
import UpdateAccountNumberDto from "./dto/updateAccountNumber.dto";

const accountNumberRouter = express.Router();

accountNumberRouter.post(
  "/",
  Valid(CreateAccountNumberDto, RequestType.body),
  validateRole(PermissionKeys.accountNumberCreate, AccountType.admin),
  accountNumberMiddleware.checkFormatAccountNumber,
  accountNumberMiddleware.createAccountNumber,
  accountNumberController.createAccountNumber
);

accountNumberRouter.put(
  "/:id",
  Valid(IdParamsRequestDto, RequestType.params),
  Valid(UpdateAccountNumberDto, RequestType.body),
  validateRole(PermissionKeys.accountNumberUpdate, AccountType.admin),
  accountNumberMiddleware.checkFormatAccountNumber,
  accountNumberMiddleware.updateAccountNumber,
  accountNumberController.updateAccountNumber
);

accountNumberRouter.delete(
  "/:id",
  Valid(IdParamsRequestDto, RequestType.params),
  validateRole(PermissionKeys.accountNumberDelete, AccountType.admin),
  accountNumberController.deleteAccountNumber
);

accountNumberRouter.get(
  "/",
  validateRole(null, AccountType.all),
  accountNumberController.getAccountNumber
);

accountNumberRouter.get(
  "/:id",
  Valid(IdParamsRequestDto, RequestType.params),
  validateRole(null, AccountType.all),
  accountNumberController.getByIdAccountNumber
);

export default accountNumberRouter;
