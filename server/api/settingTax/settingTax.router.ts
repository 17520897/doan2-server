import express from "express";
import { AccountType } from "../../const";
import IdParamsRequestDto from "../../const/IdParamsRequest.dto";
import { PermissionKeys } from "../../const/permissions";
import { RequestType, Valid } from "../../middlewares/Valid";
import { validateRole } from "../../middlewares/validate";
import CreateSettingTaxDto from "./dto/createSettingTax.dto";
import GetSettingTaxDto from "./dto/getSettingTax.dto";
import settingTaxController from "./settingTax.controller";
import settingTaxMiddleware from "./settingTax.middleware";

const settingTaxRouter = express.Router();

settingTaxRouter.post(
  "/",
  Valid(CreateSettingTaxDto, RequestType.body),
  validateRole(null, AccountType.admin),
  settingTaxMiddleware.accountNumberExisted,
  settingTaxMiddleware.createSettingTax,
  settingTaxController.createSettingTax
);

settingTaxRouter.put(
  "/:id",
  Valid(IdParamsRequestDto, RequestType.params),
  Valid(CreateSettingTaxDto, RequestType.body),
  validateRole(null, AccountType.admin),
  settingTaxMiddleware.settingTaxExisted,
  settingTaxMiddleware.accountNumberExisted,
  settingTaxMiddleware.updateSettingTax,
  settingTaxController.updateSettingTax
);

settingTaxRouter.delete(
  "/:id",
  Valid(IdParamsRequestDto, RequestType.params),
  validateRole(null, AccountType.admin),
  settingTaxMiddleware.settingTaxExisted,
  settingTaxController.deleteSettingTax
);

settingTaxRouter.get(
  "/",
  Valid(GetSettingTaxDto, RequestType.query),
  validateRole(null, AccountType.all),
  settingTaxController.getAllSettingTax
);
export default settingTaxRouter;
