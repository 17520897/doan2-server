import mongoose, { Schema } from "mongoose";

export interface ISettingTaxModel extends mongoose.Document {
  adminCreateId: string;
  name: string;
  tax: number;
}

const schema = new mongoose.Schema(
  {
    adminCreateId: {
      type: mongoose.SchemaTypes.ObjectId,
      required: true,
    },
    name: {
      type: String,
    },
    percent: {
      type: Number,
    },
    accountNumber: {
      type: Number,
    },
  },
  {
    collection: "SettingTax",
  }
);

schema.virtual("adminCreateInfo", {
  ref: "Admin",
  localField: "adminCreateId",
  foreignField: "_id",
  justOne: true,
});

schema.set("toObject", { virtuals: true });
schema.set("toJSON", { virtuals: true });

export const SettingTax = mongoose.model<ISettingTaxModel>(
  "SettingTax",
  schema
);
