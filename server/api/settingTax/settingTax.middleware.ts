import { NextFunction, Request, Response } from "express";
import BaseResponse from "../../services/baseResponse";
import l from "../../common/logger";
import { HTTPCode } from "../../const";
import { ErrorsCode } from "../../const/errorsCode";
import responseService from "../../services/responseService";
import services from "../../services/services";
import settingTaxValidate from "./settingTax.validate";

const baseResponse = new BaseResponse("SettingTaxMiddleware");

class SettingTaxMiddleware {
  async createSettingTax(req: Request, res: Response, next: NextFunction) {
    try {
      const { name } = req.body;
      const isNameExisted = await settingTaxValidate.isNameExisted(name);
      if (isNameExisted) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.settingTaxNameExisted,
              field: "name",
            },
          ],
        });
      }
      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async updateSettingTax(req: Request, res: Response, next: NextFunction) {
    try {
      const { id } = req.params;
      const { name } = req.body;
      const isNameExisted = await settingTaxValidate.isNameExisted(
        name,
        true,
        id
      );
      if (isNameExisted) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.settingTaxNameExisted,
              field: "name",
            },
          ],
        });
      }

      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async settingTaxExisted(req: Request, res: Response, next: NextFunction) {
    try {
      const { id } = req.params;
      const isSettingTaxExisted = await settingTaxValidate.isSettingTaxExisted(
        id
      );
      if (!isSettingTaxExisted) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.settingTaxNotFound,
              field: "id",
            },
          ],
        });
      }
      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async accountNumberExisted(req: Request, res: Response, next: NextFunction) {
    try {
      const { accountNumber } = req.body;
      if (accountNumber) {
        const isAccountNumberExists = await settingTaxValidate.isAccountNumberExisted(
          accountNumber
        );
        if (!isAccountNumberExists) {
          return responseService.send(res, HTTPCode.notAccept, {
            errors: [
              {
                code: ErrorsCode.settingTaxAccountNumberNotFound,
                field: "accountNumber",
              },
            ],
          });
        }
      }
      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }
}

export default new SettingTaxMiddleware();
