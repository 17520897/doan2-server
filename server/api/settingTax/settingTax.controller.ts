import { Request, Response } from "express";
import BaseResponse from "../../services/baseResponse";
import { HTTPCode } from "../../const";
import responseService from "../../services/responseService";
import settingTaxService from "./settingTax.service";
import mongoose from "mongoose";
import billTaxService from "../billTax/billTax.service";

const baseResponse = new BaseResponse("SettingTaxController");
class SettingTaxController {
  async createSettingTax(req: Request, res: Response) {
    try {
      const { adminId } = req.cookies;
      const settingTax = await settingTaxService.create({
        ...req.body,
        adminCreateId: adminId,
      });
      return responseService.send(res, HTTPCode.success, {
        data: settingTax,
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }
  async updateSettingTax(req: Request, res: Response) {
    try {
      const { adminId } = req.cookies;
      const { id } = req.params;
      const settingTax = await settingTaxService.updateById(id, {
        ...req.body,
        adminCreateId: adminId,
      });
      return responseService.send(res, HTTPCode.success, {
        data: settingTax,
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async getAllSettingTax(req: Request, res: Response) {
    try {
      const { search } = req.query as any;
      const query = {};
      if (search) {
        query["name"] = { $regex: search, $options: "i" };
      }
      const settingTax = await settingTaxService.getByQuery({
        query,
        sort: {
          accountNumber: 1,
          percent: 1,
          name: 1,
        },
      });
      return responseService.send(res, HTTPCode.success, {
        data: settingTax,
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async deleteSettingTax(req: Request, res: Response) {
    const session = await mongoose.startSession();
    session.startTransaction();
    try {
      const options = { session };
      const { id } = req.params;
      await settingTaxService.deleteById(id, options);
      await session.commitTransaction();
      return responseService.send(res, HTTPCode.success, {
        message: "delete setting tax success",
      });
    } catch (error) {
      await session.abortTransaction();
      return baseResponse.serverError(res, error);
    } finally {
      await session.endSession();
    }
  }
}

export default new SettingTaxController();
