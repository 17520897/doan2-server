import BaseResponse from "../../services/baseResponse";
import accountNumberService from "../accountNumber/accountNumber.service";
import settingTaxService from "./settingTax.service";

class SettingTaxValidate {
  async isNameExisted(name, checkId = false, id = "") {
    const settingTax = checkId
      ? await settingTaxService.getOne({
          name,
          _id: { $ne: id },
        })
      : await settingTaxService.getOne({ name });
    return settingTax ? true : false;
  }

  async isSettingTaxExisted(id) {
    const settingTax = await settingTaxService.getById(id);
    return settingTax ? true : false;
  }

  async isAccountNumberExisted(accountNumber) {
    const isExistedAccountNumber = await accountNumberService.getOne({
      "accountNumbers.accountNumber": accountNumber,
    });
    return isExistedAccountNumber ? true : false;
  }
}

export default new SettingTaxValidate();
