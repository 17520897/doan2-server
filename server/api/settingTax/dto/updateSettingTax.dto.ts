import {
  ValidateNested,
  IsString,
  MaxLength,
  IsEnum,
  IsObject,
  IsDate,
  IsEmail,
  IsNumber,
  IsOptional,
  MinLength,
  IsMongoId,
  Min,
  Length,
  IsUrl,
  IsArray,
  Equals,
} from "class-validator";
import { PersonType } from "../../../const";

export default class UpdateSettingTaxDto {
  @IsOptional()
  @IsString()
  name: String;

  @IsOptional()
  @IsNumber()
  percent: Number;

  @IsOptional()
  @IsNumber()
  accountNumber: Number;
}
