import {
  ValidateNested,
  IsString,
  MaxLength,
  IsEnum,
  IsObject,
  IsDate,
  IsEmail,
  IsNumber,
  IsOptional,
  MinLength,
  IsMongoId,
  Min,
  Length,
  IsUrl,
  IsArray,
  Equals,
  IsNumberString,
} from "class-validator";
import { PersonType } from "../../../const";

export default class GetSettingTaxDto {
  @IsOptional()
  @IsString()
  search: string;
}
