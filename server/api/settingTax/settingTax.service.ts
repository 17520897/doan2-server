import { ISettingTaxModel, SettingTax } from "./settingTax.model";
import { QueryFindOneAndUpdateOptions, SaveOptions } from "mongoose";
import BaseDbService from "../../services/baseDb";

export class SettingTaxService extends BaseDbService<ISettingTaxModel> {}

export default new SettingTaxService(SettingTax);
