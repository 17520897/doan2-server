import BaseResponse from "../../services/baseResponse";
import { SettingBankStatus } from "../../const";
import settingBankService from "./settingBank.service";

class SettingBankValidate {
  async isNameExisted(name, checkId = false, id = "") {
    const settingBank = checkId
      ? await settingBankService.getOne({
          name,
          status: SettingBankStatus.normal,
          _id: { $ne: id },
        })
      : await settingBankService.getOne({
          name,
          status: SettingBankStatus.normal,
        });
    return settingBank ? true : false;
  }

  async isSettingBankExisted(id) {
    const settingBank = await settingBankService.getOne({
      _id: id,
      status: SettingBankStatus.normal,
    });
    return settingBank ? true : false;
  }
}

export default new SettingBankValidate();
