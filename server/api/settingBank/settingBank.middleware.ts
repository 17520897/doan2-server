import { NextFunction, Request, Response } from "express";
import BaseResponse from "../../services/baseResponse";
import l from "../../common/logger";
import { HTTPCode } from "../../const";
import { ErrorsCode } from "../../const/errorsCode";
import responseService from "../../services/responseService";
import services from "../../services/services";
import settingBankValidate from "./settingBank.validate";

const baseResponse = new BaseResponse("SettingBankMiddleware");

class SettingBankMiddleware {
  async createSettingBank(req: Request, res: Response, next: NextFunction) {
    try {
      const { name } = req.body;
      const isNameExisted = await settingBankValidate.isNameExisted(name);
      if (isNameExisted) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.settingBankNameExisted,
              field: "name",
            },
          ],
        });
      }
      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async updateSettingBank(req: Request, res: Response, next: NextFunction) {
    try {
      const { id } = req.params;
      const { name } = req.body;
      const isNameExisted = await settingBankValidate.isNameExisted(
        name,
        true,
        id
      );

      if (isNameExisted) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.settingBankNameExisted,
              field: "name",
            },
          ],
        });
      }

      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async settingBankExisted(req: Request, res: Response, next: NextFunction) {
    try {
      const { id } = req.params;
      const isSettingBankExisted = await settingBankValidate.isSettingBankExisted(
        id
      );
      if (!isSettingBankExisted) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.settingBankNotFound,
              field: "id",
            },
          ],
        });
      }
      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }
}

export default new SettingBankMiddleware();
