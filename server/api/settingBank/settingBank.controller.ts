import { Request, Response } from "express";
import BaseResponse from "../../services/baseResponse";
import l from "../../common/logger";
import { HTTPCode, SettingBankStatus } from "../../const";
import responseService from "../../services/responseService";
import services from "../../services/services";
import settingBankService from "./settingBank.service";

const baseResponse = new BaseResponse("SettingBankController");
class SettingBankController {
  async createSettingBank(req: Request, res: Response) {
    try {
      const { adminId } = req.cookies;
      const settingBank = await settingBankService.create({
        ...req.body,
        adminCreateId: adminId,
      });
      return responseService.send(res, HTTPCode.success, {
        data: settingBank,
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async updateSettingBank(req: Request, res: Response) {
    try {
      const { id } = req.params;
      const { adminId } = req.cookies;
      const settingBank = await settingBankService.updateById(id, {
        ...req.body,
        adminCreateId: adminId,
      });
      return responseService.send(res, HTTPCode.success, {
        data: settingBank,
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async deleteSettingBank(req: Request, res: Response) {
    try {
      const { id } = req.params;
      await settingBankService.updateById(id, {
        status: SettingBankStatus.deleted,
      });
      return responseService.send(res, HTTPCode.success, {
        message: "delete setting bank success",
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async getSettingBank(req: Request, res: Response) {
    try {
      const { page, limit, name } = req.query;
      const query = { status: SettingBankStatus.normal };
      if (name) query["name"] = { $regex: name, $options: "i" };
      const settingBank = await settingBankService.getByQuery({
        query,
        page,
        limit,
        sort: {
          name: 1,
        },
      });
      return responseService.send(res, HTTPCode.success, {
        data: settingBank,
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }
}

export default new SettingBankController();
