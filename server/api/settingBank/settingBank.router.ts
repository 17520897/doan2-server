import express from "express";
import { AccountType } from "../../const";
import { PermissionKeys } from "../../const/permissions";
import { validateRole } from "../../middlewares/validate";
import settingBankController from "./settingBank.controller";
import settingBankMiddleware from "./settingBank.middleware";

const settingBankRouter = express.Router();

settingBankRouter.post(
  "/",
  validateRole(null, AccountType.admin),
  settingBankMiddleware.createSettingBank,
  settingBankController.createSettingBank
);

settingBankRouter.put(
  "/:id",
  validateRole(null, AccountType.admin),
  settingBankMiddleware.settingBankExisted,
  settingBankMiddleware.updateSettingBank,
  settingBankController.updateSettingBank
);

settingBankRouter.delete(
  "/:id",
  validateRole(null, AccountType.admin),
  settingBankMiddleware.settingBankExisted,
  settingBankController.deleteSettingBank
);

settingBankRouter.get(
  "/",
  validateRole(null, AccountType.all),
  settingBankController.getSettingBank
);

export default settingBankRouter;
