import { ISettingBankModel, SettingBank } from "./settingBank.model";
import { QueryFindOneAndUpdateOptions, SaveOptions } from "mongoose";
import BaseDbService from "../../services/baseDb";

export class SettingBankService extends BaseDbService<ISettingBankModel> {}

export default new SettingBankService(SettingBank);
