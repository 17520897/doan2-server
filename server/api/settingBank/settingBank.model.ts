import mongoose, { Schema } from "mongoose";

export interface ISettingBankModel extends mongoose.Document {
  adminCreateId: string;
  name: string;
  logo: string;
  status: string;
}

const schema = new mongoose.Schema(
  {
    logo: {
      type: String,
    },
    name: {
      type: String,
    },
    adminCreateId: {
      type: mongoose.SchemaTypes.ObjectId,
    },
    status: {
      type: String,
      enum: ["normal", "deleted"],
      default: "normal",
    },
  },
  {
    collection: "SettingBank",
  }
);

schema.virtual("adminCreateInfo", {
  ref: "Admin",
  localField: "adminCreateId",
  foreignField: "_id",
  justOne: true,
});

schema.set("toObject", { virtuals: true });
schema.set("toJSON", { virtuals: true });

export const SettingBank = mongoose.model<ISettingBankModel>(
  "SettingBank",
  schema
);
