import mongoose from "mongoose";

export interface IAdminModel extends mongoose.Document {
  username: string;
  password: string;
  creatAt: Date;
  name: string;
  roleId: string;
  phone: string;
  status: string;
}

const schema = new mongoose.Schema(
  {
    phone: {
      type: String,
      trim: true,
    },
    username: {
      type: String,
      required: true,
      trim: true,
    },
    password: {
      type: String,
      required: true,
      trim: true,
    },
    name: {
      type: String,
      trim: true,
    },
    roleId: {
      type: mongoose.SchemaTypes.ObjectId,
    },
    createAt: {
      type: Date,
      default: Date.now,
    },
    status: {
      type: String,
      enum: ["normal", "deleted", "block"],
      default: "normal",
    },
    adminCreateId: {
      type: mongoose.SchemaTypes.ObjectId,
    },
  },
  {
    collection: "Admin",
  }
);

schema.virtual("roleInfo", {
  ref: "Roles",
  localField: "roleId",
  foreignField: "_id",
  justOne: true,
});

schema.virtual("adminCreateInfo", {
  ref: "Admin",
  localField: "adminCreateId",
  foreignField: "_id",
  justOne: true,
});
schema.set("toObject", { virtuals: true });
schema.set("toJSON", { virtuals: true });

export const Admin = mongoose.model<IAdminModel>("Admin", schema);
