import {
  ValidateNested,
  IsString,
  MaxLength,
  IsEnum,
  IsObject,
  IsDate,
  IsEmail,
  IsNumber,
  IsOptional,
  MinLength,
  IsMongoId,
  Min,
  Length,
  IsUrl,
  IsArray,
  Equals,
} from "class-validator";

class RolePermissionType {
  @IsMongoId()
  permissionKey: String;
}
export default class CheckUsernameAdminDto {
  @IsString()
  username: string;
}
