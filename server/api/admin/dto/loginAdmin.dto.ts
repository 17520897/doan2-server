import {
  ValidateNested,
  IsString,
  MaxLength,
  IsEnum,
  IsObject,
  IsDate,
  IsEmail,
  IsNumber,
  IsOptional,
  MinLength,
  IsMongoId,
  Min,
  Length,
  IsUrl,
  IsArray,
  Equals,
} from "class-validator";

class RolePermissionType {
  @IsMongoId()
  permissionKey: String;
}
export default class LoginAdminDto {
  @IsString()
  username: string;

  @IsString()
  @MinLength(8)
  password: string;
}
