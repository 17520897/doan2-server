import { Admin, IAdminModel } from "./admin.model";
import BaseDbService from "../../services/baseDb";

export class AdminService extends BaseDbService<IAdminModel> {}

export default new AdminService(Admin);
