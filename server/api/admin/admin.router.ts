import express from "express";
import { AccountType } from "../../const";
import { PermissionKeys } from "../../const/permissions";
import { RequestType, Valid } from "../../middlewares/Valid";
import { validateRole } from "../../middlewares/validate";
import adminController from "./admin.controller";
import adminMiddleware from "./admin.middleware";
import CheckUsernameAdminDto from "./dto/checkUsernameAdmin.dto";
import CreateAdminDto from "./dto/createAdmin.dto";
import LoginAdminDto from "./dto/loginAdmin.dto";

const adminRouter = express.Router();

adminRouter.get(
  "/",
  validateRole(null, AccountType.admin),
  adminController.getAdminInfo
);

adminRouter.get(
  "/checkUsername",
  Valid(CheckUsernameAdminDto, RequestType.query),
  validateRole(null, AccountType.admin),
  adminController.checkExistedUsernameAdmin
);

adminRouter.post(
  "/",
  Valid(CreateAdminDto, RequestType.body),
  validateRole(PermissionKeys.adminCreate, AccountType.admin),
  adminMiddleware.createAdmin,
  adminController.createAdmin
);

adminRouter.post(
  "/login",
  Valid(LoginAdminDto, RequestType.body),
  adminMiddleware.loginAdmin,
  adminController.loginAdmin
);
export default adminRouter;
