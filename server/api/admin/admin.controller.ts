import { Request, Response } from "express";
import { AdminStatus, HTTPCode, RoleStatus } from "../../const";
import BaseResponse from "../../services/baseResponse";
import jwt from "../../services/jwt";
import responseService from "../../services/responseService";
import rolesService from "../roles/roles.service";
import adminService from "./admin.service";

const baseResponse = new BaseResponse("AdminController");
class AdminController {
  async createAdmin(req: Request, res: Response) {
    try {
      const { adminId } = req.cookies;
      const admin = await adminService.create({
        ...req.body,
        adminCreateId: adminId,
      }); //hash password from middleware;
      admin.password = undefined;
      return responseService.send(res, HTTPCode.success, {
        message: "create admin success",
        data: admin,
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async getAdminInfo(req: Request, res: Response) {
    try {
      const { adminId } = req.cookies;
      const admin = await adminService.populate({
        query: {
          _id: adminId,
        },
        populate: [
          {
            path: "roleInfo",
          },
          {
            path: "adminCreateInfo",
            select: "-password",
          },
        ],
      });
      return responseService.send(res, HTTPCode.success, {
        data: admin,
        message: "get admin info success",
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async loginAdmin(req: Request, res: Response) {
    try {
      const { adminId, roleId } = req.body; //get from middleware
      const accessToken = await jwt.generateAccessToken({
        adminId,
        isAdmin: true,
      });
      const refreshToken = await jwt.generateRefreshToken({
        adminId,
        isAdmin: true,
      });
      const roles = await rolesService.getOne({
        _id: roleId,
        status: RoleStatus.enable,
      });
      return responseService.send(res, HTTPCode.success, {
        data: {
          accessToken,
          refreshToken,
          permissions: roles ? roles.rolePermissions : [],
        },
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async checkExistedUsernameAdmin(req: Request, res: Response) {
    try {
      const { username } = req.query;
      const admin = await adminService.getOne({
        username,
        status: { $ne: AdminStatus.deleted },
      });
      return responseService.send(res, HTTPCode.success, {
        data: { existed: admin ? true : false },
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }
}

export default new AdminController();
