import { NextFunction, Request, Response } from "express";
import { HTTPCode } from "../../const";
import { ErrorsCode } from "../../const/errorsCode";
import BaseResponse from "../../services/baseResponse";
import responseService from "../../services/responseService";
import services from "../../services/services";
import adminService from "./admin.service";

const baseResponse = new BaseResponse("AdminMiddleware");
class AdminMiddleware {
  async createAdmin(req: Request, res: Response, next: NextFunction) {
    try {
      const { password, username } = req.body;
      const checkExistedUsername = await adminService.getOne({
        username,
      });
      if (checkExistedUsername) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.adminUserNameExisted,
              error: "user name existed",
              field: "username",
            },
          ],
        });
      }
      req.body.password = await services.hashPassword(
        password,
        parseInt(process.env.HASH_PASSWORD_SALT)
      );
      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async loginAdmin(req: Request, res: Response, next: NextFunction) {
    try {
      const { username, password } = req.body;
      const checkExistedAdmin = await adminService.getOne({
        username,
      });
      if (!checkExistedAdmin) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.adminNotFound,
              error: "user name not found",
              field: "username",
            },
          ],
        });
      }
      const comparePassword = await services.verifyPassword(
        password,
        checkExistedAdmin.password
      );
      if (!comparePassword) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.adminWrongPassword,
              error: "wrong password",
              field: "password",
            },
          ],
        });
      }
      req.body.adminId = checkExistedAdmin._id;
      req.body.roleId = checkExistedAdmin.roleId;
      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }
}

export default new AdminMiddleware();
