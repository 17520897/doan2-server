import { Request, Response, NextFunction, response } from "express";
import l from "../../common/logger";
import responseService from "../../services/responseService";
import { ErrorsCode } from "../../const/errorsCode";
import rolesService from "./userRoles.service";
import { HTTPCode, UserRoleStatus } from "../../const";
import BaseResponse from "../../services/baseResponse";
import services from "../../services/services";
import adminService from "../admin/admin.service";
const baseResponse = new BaseResponse("RolesMiddleware");
export class RolesMiddleware {
  async createRoles(req: Request, res: Response, next: NextFunction) {
    try {
      const { roleName } = req.body;
      const role = await rolesService.getOne({
        roleName,
        status: UserRoleStatus.enable,
      });
      if (role)
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.userRolesHasBeenExisted,
              error: "roles name has been existed",
              field: "roleName",
            },
          ],
        });
      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async checkIsUserRolesExisted(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }
}

export default new RolesMiddleware();
