import { Request, Response } from "express";
import l from "../../common/logger";
import responseService from "../../services/responseService";
import { ErrorsCode } from "../../const/errorsCode";
import rolesService from "./userRoles.service";
import { HTTPCode, UserRoleStatus } from "../../const";
import BaseResponse from "../../services/baseResponse";
import services from "../../services/services";

const baseResponse = new BaseResponse("RolesController");
export class RolesController {
  async createRoles(req: Request, res: Response) {
    try {
      const { adminId } = req.cookies;
      const { roleName, rolePermissions } = req.body;
      const role = await rolesService.create({
        roleName,
        rolePermissions,
        adminCreateId: adminId,
      });
      return responseService.send(res, HTTPCode.success, {
        message: "create roles success",
        data: role,
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async getRoles(req: Request, res: Response) {
    try {
      const { page, limit } = req.query;
      const roles = await rolesService.populate({
        page,
        limit,
        sort: {
          roleName: 1,
        },
        populate: [
          {
            path: "adminCreateInfo",
            select: "-password",
          },
        ],
      });
      return responseService.send(res, HTTPCode.success, {
        message: "get roles success",
        data: roles,
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async disableRoles(req: Request, res: Response) {
    try {
      const { adminId } = req.cookies;
      const { id } = req.params;
      await rolesService.updateById(id, {
        status: UserRoleStatus.disable,
        adminCreateId: adminId,
      });
      return responseService.send(res, HTTPCode.success, {
        message: "disable role success",
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async enableRoles(req: Request, res: Response) {
    try {
      const { adminId } = req.cookies;
      const { id } = req.params;
      await rolesService.updateById(id, {
        status: UserRoleStatus.enable,
        adminCreateId: adminId,
      });
      return responseService.send(res, HTTPCode.success, {
        message: "enable roles success",
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async updateRoles(req: Request, res: Response) {
    try {
      const { id } = req.params;
      const { adminId } = req.cookies;
      const roles = await rolesService.updateById(id, {
        ...req.body,
        adminCreateId: adminId,
      });
      return responseService.send(res, HTTPCode.success, {
        message: "update roles success",
        data: roles,
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async deleteRoles(req: Request, res: Response) {
    try {
      const { id } = req.params;
      await rolesService.deleteById(id);
      return responseService.send(res, HTTPCode.success, {
        message: "delete roles success",
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }
}

export default new RolesController();
