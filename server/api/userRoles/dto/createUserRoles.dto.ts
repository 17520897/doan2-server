import {
  ValidateNested,
  IsString,
  MaxLength,
  IsEnum,
  IsObject,
  IsDate,
  IsEmail,
  IsNumber,
  IsOptional,
  MinLength,
  IsMongoId,
  Min,
  Length,
  IsUrl,
  IsArray,
  Equals,
} from "class-validator";

class UserRolePermissionType {
  @IsMongoId()
  permissionKey: String;
}
export default class CreateUserRolesDto {
  @IsString()
  roleName: String;

  @IsOptional()
  @IsArray()
  rolePermissions: Array<UserRolePermissionType>;

  @Equals(undefined)
  status: String;
}
