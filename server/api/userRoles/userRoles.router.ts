import express from "express";
import userRolesMiddleware from "./userRoles.middleware";
import userRolesController from "./userRoles.controller";
import { Valid, RequestType } from "../../middlewares/Valid";
import { validateRole } from "../../middlewares/validate";
import { PermissionKeys } from "../../const/permissions";

import CreateUserRolesDto from "./dto/createUserRoles.dto";
import GetUserRolesDto from "./dto/getUserRoles.dto";
import ParamsUserRolesDto from "./dto/paramsUserRoles.dto";
import UpdateUserRoleDto from "./dto/updateUserRoles.dto";
import { AccountType } from "../../const";
const userRolesRouter = express.Router();

userRolesRouter.post(
  "/",
  validateRole(PermissionKeys.userRoleCreate, AccountType.admin),
  Valid(CreateUserRolesDto, RequestType.body),
  userRolesMiddleware.createRoles,
  userRolesController.createRoles
);

userRolesRouter.get(
  "/",
  Valid(GetUserRolesDto, RequestType.query),
  validateRole(PermissionKeys.userRoleGetList, AccountType.admin),
  userRolesController.getRoles
);

userRolesRouter.put(
  "/disable/:id",
  validateRole(PermissionKeys.userRoleDisableEnable, AccountType.admin),
  Valid(ParamsUserRolesDto, RequestType.params),
  userRolesController.disableRoles
);

userRolesRouter.put(
  "/enable/:id",
  validateRole(PermissionKeys.userRoleDisableEnable, AccountType.admin),
  Valid(ParamsUserRolesDto, RequestType.params),
  userRolesController.enableRoles
);

userRolesRouter.put(
  "/:id",
  validateRole(PermissionKeys.userRoleDelete, AccountType.admin),
  Valid(ParamsUserRolesDto, RequestType.params),
  Valid(UpdateUserRoleDto, RequestType.body),
  userRolesController.updateRoles
);

userRolesRouter.delete(
  "/:id",
  validateRole(PermissionKeys.roleDelete, AccountType.admin),
  Valid(ParamsUserRolesDto, RequestType.params),
  userRolesController.deleteRoles
);
export default userRolesRouter;
