import { UserRoles, IUserRolesModel } from "./userRoles.model";
import { QueryFindOneAndUpdateOptions } from "mongoose";
import BaseDbService from "../../services/baseDb";

export class UserRolesService extends BaseDbService<IUserRolesModel> {}

export default new UserRolesService(UserRoles);
