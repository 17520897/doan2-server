import billService from "./bill.service";

class BillValidate {
  async isCompaniesHaveBill(userId, companyId, billId) {
    const serviceBill = await billService.getOne({
      userCreateId: userId,
      _id: billId,
      companyId,
    });
    return serviceBill ? true : false;
  }
}

export default new BillValidate();
