import { Request, Response } from "express";
import BaseResponse from "../../services/baseResponse";
import { HTTPCode, MoneyTypeStatus, PersonStatus } from "../../const";
import responseService from "../../services/responseService";
import billService from "./bill.service";
import mongoose from "mongoose";
import billDetailService from "../billDetail/billDetail.service";
import billTaxService from "../billTax/billTax.service";
import path from "path";
import xlsx from "xlsx";
import personService from "../person/person.service";
import slugify from "slugify";
import stockService from "../stock/stock.service";
import { ErrorsCode } from "../../const/errorsCode";
import fs from "fs";
const baseResponse = new BaseResponse("ServiceBillController");

function slugName(name) {
  return slugify(name, {
    lower: true,
    replacement: "_",
    trim: true,
    locale: "vi",
  });
}

function formatDate(data) {
  try {
    const dateSplit = data.split("/");
    const date = new Date(`${dateSplit[2]}-${dateSplit[1]}-${dateSplit[0]}`);
    return date.toISOString();
  } catch (error) {
    return new Date().toISOString();
  }
}

async function getBillData(sheetData, companyId = "", userId = "") {
  const billNumber = "SỐ HÓA ĐƠN";
  const billPersonCreate = "NGƯỜI TẠO";
  const billPersonReceive = "NGƯỜI NHẬN";
  const billCreateAt = "NGÀY TẠO";
  const billAddress = "ĐỊA CHỈ";
  const returnData = [];
  const newPersonData = {};
  for (let i = 0; i < sheetData.length; i++) {
    //init return object
    const returnObject = {
      billNumber: sheetData[i][billNumber],
      billCreateAt: formatDate(sheetData[i][billCreateAt]),
      address: sheetData[i][billAddress],
    };
    //check person create existed
    const personCreate = await personService.getOne({
      userCreateId: userId,
      companyId,
      status: PersonStatus.normal,
      $or: [
        {
          name: sheetData[i][billPersonCreate],
        },
        {
          slugName: slugName(sheetData[i][billPersonCreate]),
        },
      ],
    });
    //check person receive existed
    const personReceive = await personService.getOne({
      userCreateId: userId,
      companyId,
      status: PersonStatus.normal,
      $or: [
        {
          name: sheetData[i][billPersonReceive],
        },
        {
          slugName: slugName(sheetData[i][billPersonReceive]),
        },
      ],
    });
    if (personCreate) {
      returnObject["personCreateId"] = personCreate._id;
      returnObject["personCreateInfo"] = {
        ...personCreate,
      };
    } else {
      if (newPersonData[sheetData[i][billPersonCreate]]) {
        newPersonData[sheetData[i][billPersonCreate]].push(i + 2);
      } else {
        newPersonData[sheetData[i][billPersonCreate]] = [i + 2];
      }
    }
    if (personReceive) {
      returnObject["personReceiveId"] = personReceive._id;
      returnObject["personReceiveInfo"] = {
        ...personReceive,
      };
    } else {
      if (newPersonData[sheetData[i][billPersonReceive]]) {
        newPersonData[sheetData[i][billPersonReceive]].push(i + 2);
      } else {
        newPersonData[sheetData[i][billPersonReceive]] = [i + 2];
      }
    }
    returnData.push(returnObject);
  }
  return {
    billData: returnData,
    newPersonData,
  };
}

async function getBillDetailData(
  sheetData,
  companyId = "",
  userId = "",
  stockType = "services"
) {
  const billNumber = "SỐ HÓA ĐƠN";
  const stock = "HÀNG HÓA";
  const stockPrice = "ĐƠN GIÁ";
  const stockQuantity = "SỐ LƯỢNG";
  const receiveNumber = "TK CÓ";
  const debitNumber = "TK NỢ";
  const returnData = [];
  const newStockData = {};
  for (let i = 0; i < sheetData.length; i++) {
    const returnObject = {
      billNumber: sheetData[i][billNumber],
      receiveAccountNumber: sheetData[i][receiveNumber],
      debitAccountNumber: sheetData[i][debitNumber],
      money: sheetData[i][stockPrice],
      moneyType: "VND",
      exchangeRate: 1,
      quantity: sheetData[i][stockQuantity],
      finalMoney: sheetData[i][stockPrice] * sheetData[i][stockQuantity],
    };
    const stockName = sheetData[i][stock];
    const checkStock = await stockService.getOne({
      type: stockType,
      userCreateId: userId,
      companyId,
      $or: [
        {
          name: stockName,
        },
        {
          slugName: slugName(stockName),
        },
      ],
    });
    if (checkStock) {
      returnObject["stockId"] = checkStock._id;
      returnObject["stockInfo"] = {
        ...checkStock,
      };
      returnData.push(returnObject);
    } else {
      if (newStockData[stockName]) {
        newStockData[stockName].push(i + 2);
      } else {
        newStockData[stockName] = [i + 2];
      }
    }
  }
  return {
    newStockData,
    billDetailData: returnData,
  };
}

async function getBillTaxData(
  sheetData,
  companyId = "",
  userId = "",
  stockType = "service"
) {
  const billNumber = "SỐ HÓA ĐƠN";
  const stock = "HÀNG HÓA";
  const stockMoney = "SỐ TIỀN";
  const taxPercent = "PHẦN TRĂM";
  const receiveNumber = "TK CÓ";
  const returnData = [];
  for (let i = 0; i < sheetData.length; i++) {
    const returnObject = {
      billNumber: sheetData[i][billNumber],
      accountNumber: sheetData[i][receiveNumber],
      stockMoney: sheetData[i][stockMoney],
      percent: sheetData[i][taxPercent],
      finalMoney:
        parseFloat(sheetData[i][stockMoney]) *
        parseFloat(sheetData[i][taxPercent]) / 100,
    };
    const stockName = sheetData[i][stock];
    const checkStock = await stockService.getOne({
      type: stockType,
      userCreateId: userId,
      companyId,
      $or: [
        {
          name: stockName,
        },
        {
          slugName: slugName(stockName),
        },
      ],
    });
    if (checkStock) {
      returnObject["stockId"] = checkStock._id;
      returnObject["stockInfo"] = {
        ...checkStock,
      };
      returnData.push(returnObject);
    }
  }
  return returnData;
}

function makeBillArrayForMany(
  billData,
  userId,
  companyId,
  accountNumberId,
  billType,
  billFunction,
  billDetailData = [],
  billTaxData = []
) {
  const moneyData = {}
  const taxMoneyData = {}
  for (let i = 0; i < billDetailData.length; i++) {
    if(moneyData.hasOwnProperty(billDetailData[i]['billNumber']))
    {
      moneyData[billDetailData[i]['billNumber']] += billDetailData[i]['finalMoney']
    }else{
      moneyData[billDetailData[i]['billNumber']] = billDetailData[i]['finalMoney']
    }
  }
  for (let i = 0; i < billTaxData.length; i++) {
    if(taxMoneyData.hasOwnProperty(billTaxData[i]['billNumber']))
    {
      taxMoneyData[billTaxData[i]['billNumber']] += billTaxData[i]['finalMoney']
    }else{
      taxMoneyData[billTaxData[i]['billNumber']] = billTaxData[i]['finalMoney']
    }
  }
  const returnData = [];
  console.log(moneyData, taxMoneyData)
  for (let i = 0; i < billData.length; i++) {
    returnData.push({
      billNumber: billData[i].billNumber,
      billCreateAt: billData[i].billCreateAt,
      address: billData[i].address,
      personCreateId: billData[i].personCreateId,
      personReceiveId: billData[i].personReceiveId,
      companyId,
      accountNumberId,
      billType,
      billFunction,
      userCreateId: userId,
      paidType: "cash",
      money: moneyData.hasOwnProperty(billData[i]['billNumber']) ? moneyData[billData[i]['billNumber']] : 0,
      taxMoney: taxMoneyData.hasOwnProperty(billData[i]['billNumber']) ? taxMoneyData[billData[i]['billNumber']] : 0,
    });
  }
  return returnData;
}

async function createManyBill(billArray, options = {}) {
  const returnData = {};
  for (let i = 0; i < billArray.length; i++) {
    const bill = await billService.create(billArray[i], options);
    returnData[billArray[i]["billNumber"]] = {
      _id: bill._id,
      billCreateAt: billArray[i]["billCreateAt"],
    };
  }
  return returnData;
}

async function createManyBillDetail(
  billDetailData,
  billCreateArray,
  companyId,
  accountNumberId,
  userId,
  options = {}
) {
  const createData = [];
  for (let i = 0; i < billDetailData.length; i++) {
    const billId = billCreateArray[billDetailData[i]["billNumber"]]._id
      ? billCreateArray[billDetailData[i]["billNumber"]]
      : undefined;
    if (billId) {
      createData.push({
        companyId,
        userCreateId: userId,
        billId,
        accountNumberId,
        stockId: billDetailData[i]['stockId'],
        receiveAccountNumber: billDetailData[i]['receiveAccountNumber'],
        debitAccountNumber: billDetailData[i]['debitAccountNumber'],
        money: billDetailData[i]['money'],
        moneyType: billDetailData[i]['moneyType'],
        quantity: billDetailData[i]['quantity'],
        exchangeRate: billDetailData[i]['exchangeRate'],
        finalMoney: billDetailData[i]['finalMoney'],
        billCreateAt: billCreateArray[billDetailData[i]['billNumber']]["billCreateAt"],
      });
    }
  }
  await billDetailService.createMany(createData, options)
  return
}

async function createManyBillTax(
  billTaxData,
  billCreateArray,
  companyId,
  accountNumberId,
  userId,
  options = {}
) {
  const createData = [];
  for (let i = 0; i < billTaxData.length; i++) {
    const billId = billCreateArray[billTaxData[i]["billNumber"]]._id
      ? billCreateArray[billTaxData[i]["billNumber"]]
      : undefined;
    if (billId) {
      createData.push({
        companyId,
        userCreateId: userId,
        billId,
        accountNumberId,
        stockId: billTaxData[i]['stockId'],
        accountNumber: billTaxData[i]['accountNumber'],
        stockMoney: billTaxData[i]['stockMoney'],
        percent: billTaxData[i]['percent'],
        finalMoney: billTaxData[i]['finalMoney'],
        billCreateAt: billCreateArray[billTaxData[i]['billNumber']]["billCreateAt"],
      })
    }
  }
  await billTaxService.createMany(createData, options)
  return
}
class ServiceBillController {
  async createServiceBill(req: Request, res: Response) {
    try {
      const { userId } = req.cookies;
      const moneyType = await billService.create({
        ...req.body,
        userCreateId: userId,
      });
      return responseService.send(res, HTTPCode.success, {
        data: moneyType,
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async searchServiceBill(req: Request, res: Response) {
    try {
      const { userId } = req.cookies;
      const {
        page,
        limit,
        companyId,
        beginBillCreateAt,
        endBillCreateAt,
        personCreateId,
        personReceiveId,
        sortType = -1,
        sortProperties,
        billFunction,
        receiveAccountNumber,
        debitAccountNumber,
        billType,
      } = req.query as any;
      let query = {
        userCreateId: userId,
        billFunction,
        companyId,
      } as any;
      let sort = {} as any;
      if (sortProperties) {
        sort[sortProperties] = parseInt(sortType);
      }

      if (beginBillCreateAt && endBillCreateAt)
        query = {
          ...query,
          $and: [
            {
              billCreateAt: { $gte: beginBillCreateAt },
            },
            {
              billCreateAt: { $lte: endBillCreateAt },
            },
          ],
        };

      if (receiveAccountNumber) {
        const billArrayIds = await billDetailService.distinct("billId", {
          receiveAccountNumber: parseInt(receiveAccountNumber),
        });
        query = {
          ...query,
          _id: { $in: billArrayIds },
        };
      }

      if (debitAccountNumber) {
        const billArrayIds = await billDetailService.distinct("billId", {
          debitAccountNumber: parseInt(debitAccountNumber),
        });
        query = {
          ...query,
          _id: { $in: billArrayIds },
        };
      }
      if (personCreateId) {
        query["personCreateId"] = personCreateId;
      }
      if (personReceiveId) {
        query["personReceiveId"] = personReceiveId;
      }

      if (billType) {
        query["billType"] = billType;
      }

      const bill = await billService.populate({
        query,
        page,
        limit,
        sort: {
          ...sort,
        },
        populate: [
          {
            path: "personCreateInfo",
          },
          {
            path: "personReceiveInfo",
          },
        ],
      });
      return responseService.send(res, HTTPCode.success, {
        data: bill,
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async updateServiceBill(req: Request, res: Response) {
    try {
      const { id } = req.params;
      const { userId } = req.cookies;
      const serviceBill = await billService.updateById(id, {
        ...req.body,
        userCreateId: userId,
        userCreateAt: Date.now(),
      });
      return responseService.send(res, HTTPCode.success, {
        message: "update bill success",
        data: serviceBill,
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async deleteBill(req: Request, res: Response) {
    const session = await mongoose.startSession();
    session.startTransaction();
    try {
      const options = { session };
      const { id } = req.params;
      const idObject = new mongoose.Types.ObjectId(id);
      await billDetailService.deleteMany(
        {
          billId: idObject,
        },
        options
      );
      await billTaxService.deleteMany(
        {
          billId: idObject,
        },
        options
      );
      await billService.deleteById(id, options);
      await session.commitTransaction();
      return baseResponse.success({
        res,
        message: "delete bill success",
      });
    } catch (error) {
      await session.abortTransaction;
    } finally {
      await session.endSession();
    }
  }

  async getInputFromFileBill(req: Request, res: Response) {
    try {
      const root = process.env.FILE_UPLOAD_FOLDER;
      const { userId } = req.cookies;
      const { fileUrl, companyId, stockType } = req.body;
      const excelFile = `${root}/${fileUrl}`;
      const billNumber = "SỐ HÓA ĐƠN";
      const billPersonCreate = "NGƯỜI TẠO";
      const billPersonReceive = "NGƯỜI NHẬN";
      const billCreateAt = "NGÀY TẠO";
      const billAddress = "ĐỊA CHỈ";
      const stockMoney = "SỐ TIỀN";
      const stockPrice = "ĐƠN GIÁ";
      const taxPercent = "PHẦN TRĂM";
      const receiveNumber = "TK CÓ";
      const debitNumber = "TK NỢ";
      const stock = "HÀNG HÓA";
      const stockQuantity = "SỐ LƯỢNG";
      if (!fs.existsSync(excelFile)) {
        return baseResponse.notAccepted({
          res,
          errors: [
            {
              code: ErrorsCode.filesNotFound,
              field: "fileUrl",
            },
          ],
        });
      }
      //read data
      const workbook = xlsx.readFile(excelFile);
      const sheetNameList = workbook.SheetNames;
      //bill data
      const billData = xlsx.utils.sheet_to_json(
        workbook.Sheets[sheetNameList[0]]
      );
      //bill detail data
      const billDetailData = xlsx.utils.sheet_to_json(
        workbook.Sheets[sheetNameList[1]]
      );
      //bill tax data
      const billTaxData = xlsx.utils.sheet_to_json(
        workbook.Sheets[sheetNameList[2]]
      );
      // validate data
      if (
        billData.length <= 0 ||
        billDetailData.length <= 0 ||
        billTaxData.length <= 0
      ) {
        return baseResponse.notAccepted({
          res,
          errors: [
            {
              code: ErrorsCode.filesNotHaveData,
            },
          ],
        });
      }
      if (
        !billData[0].hasOwnProperty(billNumber) ||
        !billData[0].hasOwnProperty(billPersonCreate) ||
        !billData[0].hasOwnProperty(billPersonReceive) ||
        !billData[0].hasOwnProperty(billCreateAt) ||
        !billData[0].hasOwnProperty(billAddress)
      ) {
        console.log("billData");
        return baseResponse.notAccepted({
          res,
          errors: [
            {
              code: ErrorsCode.filesWrongFormat,
            },
          ],
        });
      }

      if (
        !billTaxData[0].hasOwnProperty(stock) ||
        !billTaxData[0].hasOwnProperty(billNumber) ||
        !billTaxData[0].hasOwnProperty(stockMoney) ||
        !billTaxData[0].hasOwnProperty(taxPercent) ||
        !billTaxData[0].hasOwnProperty(receiveNumber)
      ) {
        console.log("billTaxData");
        return baseResponse.notAccepted({
          res,
          errors: [
            {
              code: ErrorsCode.filesWrongFormat,
            },
          ],
        });
      }

      if (
        !billDetailData[0].hasOwnProperty(billNumber) ||
        !billDetailData[0].hasOwnProperty(stock) ||
        !billDetailData[0].hasOwnProperty(stockPrice) ||
        !billDetailData[0].hasOwnProperty(stockQuantity) ||
        !billDetailData[0].hasOwnProperty(receiveNumber) ||
        !billDetailData[0].hasOwnProperty(debitNumber)
      ) {
        console.log("billDetailData", billDetailData[0]);
        return baseResponse.notAccepted({
          res,
          errors: [
            {
              code: ErrorsCode.filesWrongFormat,
            },
          ],
        });
      }
      return baseResponse.success({
        res,
        data: {
          billData: await getBillData(billData, companyId, userId),
          billDetailData: await getBillDetailData(
            billDetailData,
            companyId,
            userId,
            stockType
          ),
          billTaxData: await getBillTaxData(
            billTaxData,
            companyId,
            userId,
            stockType
          ),
        },
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async createManyBill(req: Request, res: Response) {
    const session = await mongoose.startSession();
    session.startTransaction();
    try {
      const options = { session };
      const { userId } = req.cookies;
      const {
        companyId,
        accountNumberId,
        billType,
        billFunction,
        billData,
        billDetailData,
        billTaxData,
      } = req.body;
      // bill array for create bill info
      const billArray = makeBillArrayForMany(
        billData,
        userId,
        companyId,
        accountNumberId,
        billType,
        billFunction,
        billDetailData,
        billTaxData
      );
      const billCreateData = await createManyBill(billArray, options);
      await createManyBillDetail(billDetailData, billCreateData, companyId, accountNumberId, userId, options)
      await createManyBillTax(billTaxData, billCreateData, companyId, accountNumberId, userId, options)
      await session.commitTransaction();
      return baseResponse.success({
        res,
        message: "create data success"
      });
    } catch (error) {
      await session.abortTransaction();
      return baseResponse.serverError(res, error);
    } finally {
      session.endSession();
    }
  }
}

export default new ServiceBillController();
