import express from "express";
import { AccountType } from "../../const";
import { RequestType, Valid } from "../../middlewares/Valid";
import { validateRole } from "../../middlewares/validate";
import IdParamsRequestDto from "../../const/IdParamsRequest.dto";
import serviceBillController from "./bill.controller";
import UserCreateOrUpdateBillDto from "./dto/createOrUpdateBill";
import GetBillDto from "./dto/getBill.dto";
import middleware from "../../middlewares/middleware";
import billMiddleware from "./bill.middleware";
import CreateManyBillDto from "./dto/createManyBill";
import GetBillFromFile from "./dto/getFromFile"

const billRouter = express.Router();

billRouter.post(
  "/",
  Valid(UserCreateOrUpdateBillDto, RequestType.body),
  validateRole(null, AccountType.users),
  middleware.personIsSame,
  middleware.userHaveCompanies,
  middleware.personCreateOrReceiveExisted,
  middleware.userHaveCompaniesBank,
  middleware.companyHaveAccountNumber,
  middleware.existedAccountNumber,
  serviceBillController.createServiceBill
);

billRouter.post(
  "/file",
  Valid(GetBillFromFile, RequestType.body),
  validateRole(null, AccountType.users),
  middleware.userHaveCompanies,
  serviceBillController.getInputFromFileBill
)
billRouter.post(
  "/many",
  Valid(CreateManyBillDto, RequestType.body),
  validateRole(null, AccountType.users),
  middleware.userHaveCompanies,
  middleware.companyHaveAccountNumber,
  billMiddleware.createManyBill,
  serviceBillController.createManyBill
)

billRouter.put(
  "/:id",
  Valid(IdParamsRequestDto, RequestType.params),
  Valid(UserCreateOrUpdateBillDto, RequestType.body),
  validateRole(null, AccountType.users),
  middleware.personIsSame,
  middleware.userHaveCompanies,
  middleware.personCreateOrReceiveExisted,
  middleware.userHaveCompaniesBank,
  middleware.companyHaveAccountNumber,
  middleware.existedAccountNumber,
  serviceBillController.updateServiceBill
);

billRouter.get(
  "/",
  Valid(GetBillDto, RequestType.query),
  validateRole(null, AccountType.users),
  serviceBillController.searchServiceBill
);

billRouter.delete(
  "/:id",
  Valid(IdParamsRequestDto, RequestType.params),
  validateRole(null, AccountType.users),
  serviceBillController.deleteBill
);

export default billRouter;
