import {
  IsString,
  IsEnum,
  IsInt,
  IsOptional,
  Equals,
  IsObject,
  IsArray,
  IsNumber,
  IsNumberString,
  Min,
  Max,
  IsMongoId,
  IsDateString,
} from "class-validator";
import { IsObjectHaveProperty } from "../../../middlewares/Valid";
import { BillFunction, MoneyBillType } from "../../../const";

export default class UserCreateOrUpdateBillDto {
  @IsMongoId()
  companyId: string;

  @IsEnum(BillFunction, {
    message: "bill function must be: services, fixedAssets",
  })
  billFunction: string;

  @IsOptional()
  @IsMongoId()
  personCreateId: string;

  @IsMongoId()
  personReceiveId: string;

  @IsEnum(MoneyBillType, {
    message: "bill type must be: receipt or expenses",
  })
  billType: string;

  @IsOptional()
  @IsString()
  address: string;

  @IsDateString()
  billCreateAt: Date;

  @IsString()
  billNumber: String;
}
