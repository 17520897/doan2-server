import {
  IsString,
  IsEnum,
  IsInt,
  IsOptional,
  Equals,
  IsObject,
  IsArray,
  IsNumber,
  IsNumberString,
  Min,
  Max,
  IsMongoId,
  IsDateString,
} from "class-validator";
import { IsObjectHaveProperty } from "../../../middlewares/Valid";
import { BillFunction, MoneyBillType } from "../../../const";

export default class CreateManyBillDto {
  @IsMongoId()
  companyId: string;

  @IsMongoId()
  accountNumberId: string;

  @IsEnum(BillFunction, {
    message: "bill function must be: services, fixedAssets",
  })
  billFunction: string;

  @IsEnum(MoneyBillType, {
    message: "bill type must be: receipt or expenses",
  })
  billType: string;

  @IsArray()
  billData: any;

  @IsArray()
  billDetailData: any;

  @IsArray()
  billTaxData: any;
}
