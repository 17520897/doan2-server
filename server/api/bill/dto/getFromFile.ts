import {
  IsString,
  IsEnum,
  IsInt,
  IsOptional,
  Equals,
  IsObject,
  IsArray,
  IsNumber,
  IsNumberString,
  Min,
  Max,
  IsMongoId,
  IsDateString,
} from "class-validator";
import { IsObjectHaveProperty } from "../../../middlewares/Valid";
import { BillFunction, MoneyBillType, StockType } from "../../../const";

export default class GetBillFromFileDto {
  @IsMongoId()
  companyId: string;

  @IsEnum(StockType, {
    message: 'type must be: "services", "goods", "fixedAssets", material',
  })
  stockType: string;

  @IsString()
  fileUrl: any;
}
