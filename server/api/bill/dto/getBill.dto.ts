import {
  IsString,
  IsEnum,
  IsInt,
  IsOptional,
  Equals,
  IsObject,
  IsArray,
  IsNumber,
  IsNumberString,
  Min,
  Max,
  IsMongoId,
  IsDateString,
} from "class-validator";
import { BillFunction } from "../../../const";
import { IsObjectHaveProperty } from "../../../middlewares/Valid";

export default class GetServiceBillDto {
  @IsMongoId()
  companyId;

  @IsEnum(BillFunction, {
    message: "bill function must be: services, fixedAssets",
  })
  billFunction: string;

  @IsOptional()
  @IsDateString()
  beginBillCreateAt: string;

  @IsOptional()
  @IsDateString()
  endBillCreateAt: string;

  @IsOptional()
  @IsMongoId()
  personCreateId: string;

  @IsOptional()
  @IsMongoId()
  personReceiveId: string;
}
