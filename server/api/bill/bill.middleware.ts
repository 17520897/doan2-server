import { NextFunction, Request, Response } from "express";
import BaseResponse from "../../services/baseResponse";
import { HTTPCode, MoneyTypeStatus } from "../../const";
import responseService from "../../services/responseService";
import billService from "./bill.service";
import mongoose from "mongoose";
import billDetailService from "../billDetail/billDetail.service";
import billTaxService from "../billTax/billTax.service";
import path from "path";
import xlsx from "xlsx";
import personService from "../person/person.service";
import slugify from "slugify";
import stockService from "../stock/stock.service";
import { ErrorsCode } from "../../const/errorsCode";
import fs from "fs";
const baseResponse = new BaseResponse("ServiceBillMiddleware");

class ServiceBillMiddleware {
    async createManyBill(req: Request, res: Response, next: NextFunction)
    {
        try {
            const {userId} = req.cookies
            const {billData, companyId, billFunction} = req.body
            const returnData = []
            for(let i=0; i<billData.length;i++)
            {
                const bill = await billService.getOne({
                    companyId,
                    userCreateId: userId,
                    billNumber: billData[i]['billNumber'],
                    billFunction
                })
                if(bill)
                {
                    returnData.push(bill['billNumber'])
                }
            }
            if(returnData.length > 0)
            {
                return baseResponse.notAccepted({
                    res,
                    message: `Số hóa đơn đã có trong hệ thống ${returnData.join(',')}`,
                    errors: [
                        {
                            code: ErrorsCode.billExisted,
                            field: 'billNumber'
                        }
                    ]
                })
            }
            next()
        } catch (error) {
            return baseResponse.serverError(res, error)
        }
    }
}

export default new ServiceBillMiddleware()