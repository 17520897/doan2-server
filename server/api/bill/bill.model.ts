import mongoose, { mongo, Schema } from "mongoose";

export interface IBillModel extends mongoose.Document {
  userCreateId: string;
}

const schema = new mongoose.Schema(
  {
    companyId: {
      type: mongoose.SchemaTypes.ObjectId,
      required: true,
    },
    userCreateId: {
      type: mongoose.SchemaTypes.ObjectId,
      required: true,
    },
    billType: {
      type: String,
      enum: ["receipt", "expenses"],
      required: true,
    },
    billFunction: {
      type: String,
      enum: ["services", "goods", "fixedAssets", "material"],
      required: true,
    },
    paidType: {
      type: String,
      enum: ["bank", "cash"],
      default: "cash",
    },
    billNumber: {
      type: String,
    },
    companyBankId: {
      type: mongoose.SchemaTypes.ObjectId,
    },
    // customerBankId: {
    //   type: mongoose.SchemaTypes.ObjectId,
    // },
    accountNumberId: {
      type: mongoose.SchemaTypes.ObjectId,
    },
    personCreateId: {
      type: mongoose.SchemaTypes.ObjectId,
    },
    personReceiveId: {
      type: mongoose.SchemaTypes.ObjectId,
    },
    address: {
      type: String,
    },
    note: {
      type: String,
    },
    taxMoney: {
      type: Number,
      default: 0,
      index: true,
    },
    money: {
      type: Number,
      index: true,
      default: 0,
    },
    billCreateAt: {
      type: Date,
    },
    userCreateAt: {
      type: Date,
      default: Date.now,
    },
  },
  {
    collection: "Bill",
  }
);

schema.virtual("userCreateInfo", {
  ref: "User",
  localField: "userCreateId",
  foreignField: "_id",
  justOne: true,
});

schema.virtual("companyInfo", {
  ref: "Companies",
  localField: "companyId",
  foreignField: "_id",
  justOne: true,
});

schema.virtual("personCreateInfo", {
  ref: "Person",
  localField: "personCreateId",
  foreignField: "_id",
  justOne: true,
});

schema.virtual("personReceiveInfo", {
  ref: "Person",
  localField: "personReceiveId",
  foreignField: "_id",
  justOne: true,
});

schema.set("toObject", { virtuals: true });
schema.set("toJSON", { virtuals: true });

export const Bill = mongoose.model<IBillModel>("Bill", schema);
