import BaseDbService from "../../services/baseDb";
import { Bill, IBillModel } from "./bill.model";

export class BillService extends BaseDbService<IBillModel> {}

export default new BillService(Bill);
