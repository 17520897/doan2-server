import { IsString, MaxLength, IsEnum, IsObject, IsDate, IsEmail, IsNumber, IsOptional, MinLength, IsMongoId, Min, Length, IsUrl } from "class-validator";



export default class ParamsRolesDto {
  @IsMongoId()
  id: string
}