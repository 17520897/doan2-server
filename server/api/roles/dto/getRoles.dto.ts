import { IsString, MaxLength, IsEnum, IsObject, IsDate, IsEmail, IsNumber, IsOptional, MinLength, IsMongoId, Min, Length } from "class-validator";

export default class GetRolesDto {
    @IsString()
    page: Number

    @IsString()
    limit: Number
}