import mongoose, { Schema } from "mongoose";

export interface IRolesModel extends mongoose.Document {
  roleName: string;
  rolePermissions: Array<any>;
  status: string;
  adminCreateId: string;
}

const schema = new mongoose.Schema(
  {
    roleName: {
      type: String,
      required: true,
    },
    rolePermissions: [
      {
        permissionKey: {
          type: String,
          required: true,
        },
      },
    ],
    status: {
      type: String,
      enum: ["enable", "disable"],
      default: "enable",
    },
    adminCreateId: {
      type: mongoose.SchemaTypes.ObjectId,
    },
  },
  {
    collection: "Roles",
  }
);

schema.virtual("adminCreateInfo", {
  ref: "Admin",
  localField: "adminCreateId",
  foreignField: "_id",
  justOne: true,
});

schema.set("toObject", { virtuals: true });
schema.set("toJSON", { virtuals: true });

export const Roles = mongoose.model<IRolesModel>("Roles", schema);
