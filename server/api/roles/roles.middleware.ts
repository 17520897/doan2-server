import { Request, Response, NextFunction, response } from "express";
import l from "../../common/logger";
import responseService from "../../services/responseService";
import { ErrorsCode } from "../../const/errorsCode";
import rolesService from "./roles.service";
import { HTTPCode, RoleStatus } from "../../const";
import BaseResponse from "../../services/baseResponse";
import services from "../../services/services";
import adminService from "../admin/admin.service";
const baseResponse = new BaseResponse("RolesMiddleware");
export class RolesMiddleware {
  async createRoles(req: Request, res: Response, next: NextFunction) {
    try {
      const { roleName } = req.body;
      const role = await rolesService.getOne({
        roleName,
        status: RoleStatus.enable,
      });
      if (role)
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.rolesHasBeenExisted,
              error: "roles name has been existed",
              field: "roleName",
            },
          ],
        });
      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }
  async updateRoles(req: Request, res: Response, next: NextFunction) {
    try {
      const { adminId } = req.cookies;
      const { id } = req.params;
      const admin = await adminService.getById(adminId);
      if (admin.roleId.toString() === id.toString()) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.roleNotAcceptUpdate,
              error: "not accept update own role",
              field: "_id",
            },
          ],
        });
      }
      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }
  async disableRoles(req: Request, res: Response, next: NextFunction) {
    try {
      const { adminId } = req.cookies;
      const { id } = req.params;
      const admin = await adminService.getById(adminId);
      if (admin.roleId.toString() === id.toString()) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.roleNotAcceptDisable,
              error: "not accept disable own roles",
              field: "_id",
            },
          ],
        });
      }
      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }
  async deleteRoles(req: Request, res: Response, next: NextFunction) {
    try {
      const { adminId } = req.cookies;
      const { id } = req.params;
      const admin = await adminService.getById(adminId);
      if (admin.roleId.toString() === id.toString()) {
        return responseService.send(res, HTTPCode.serverError, {
          errors: [
            {
              code: ErrorsCode.roleNotAcceptDelete,
              error: "not accept delete own roles",
              field: "_id",
            },
          ],
        });
      }
      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }
}

export default new RolesMiddleware();
