import { Roles, IRolesModel } from "./roles.model";
import { QueryFindOneAndUpdateOptions } from "mongoose";
import BaseDbService from "../../services/baseDb";

export class RolesService extends BaseDbService<IRolesModel> {}

export default new RolesService(Roles);
