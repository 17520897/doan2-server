import express from "express";
import rolesMiddleware from "./roles.middleware";
import rolesController from "./roles.controller";
import { Valid, RequestType } from "../../middlewares/Valid";
import CreateRolesDto from "./dto/createRoles.dto";
import ParamsRolesDto from "./dto/paramsRoles.dto";
import { validateRole } from "../../middlewares/validate";
import { PermissionKeys } from "../../const/permissions";
import UpdateRoleDto from "./dto/updateRoles.dto";
import GetRolesDto from "./dto/getRoles.dto";
import { AccountType } from "../../const";
const rolesRouter = express.Router();

rolesRouter.post(
  "/",
  Valid(CreateRolesDto, RequestType.body),
  validateRole(PermissionKeys.roleCreate, AccountType.admin),
  rolesMiddleware.createRoles,
  rolesController.createRoles
);

rolesRouter.get(
  "/",
  Valid(GetRolesDto, RequestType.query),
  validateRole(PermissionKeys.roleGetList, AccountType.admin),
  rolesController.getRoles
);

rolesRouter.put(
  "/disable/:id",
  Valid(ParamsRolesDto, RequestType.params),
  validateRole(PermissionKeys.roleDisableEnable, AccountType.admin),
  rolesMiddleware.disableRoles,
  rolesController.disableRoles
);

rolesRouter.put(
  "/enable/:id",
  Valid(ParamsRolesDto, RequestType.params),
  validateRole(PermissionKeys.roleDisableEnable, AccountType.admin),
  rolesController.enableRoles
);

rolesRouter.put(
  "/:id",
  validateRole(PermissionKeys.roleUpdate, AccountType.admin),
  Valid(ParamsRolesDto, RequestType.params),
  Valid(UpdateRoleDto, RequestType.body),
  rolesMiddleware.updateRoles,
  rolesController.updateRoles
);

rolesRouter.delete(
  "/:id",
  validateRole(PermissionKeys.roleDelete, AccountType.admin),
  Valid(ParamsRolesDto, RequestType.params),
  rolesMiddleware.deleteRoles,
  rolesController.deleteRoles
);
export default rolesRouter;
