import mongoose, { Schema } from "mongoose";

export interface IReportModel extends mongoose.Document {}

const schema = new mongoose.Schema(
  {
    companyId: {
      type: mongoose.SchemaTypes.ObjectId,
      required: true,
    },
    userCreateId: {
      type: mongoose.SchemaTypes.ObjectId,
      required: true,
    },
    accountNumberId: {
      type: mongoose.SchemaTypes.ObjectId,
      required: true,
    },
    debitReports: [
      {
        accountNumber: {
          type: Number,
        },
        parentNumber: {
          type: Number,
        },
        level: {
          type: Number,
          default: 1,
        },
        money: {
          type: Number,
        },
      },
    ],
    receiveReports: [
      {
        accountNumber: {
          type: Number,
        },
        parentNumber: {
          type: Number,
        },
        level: {
          type: Number,
          default: 1,
        },
        money: {
          type: Number,
        },
      },
    ],
    time: {
      type: Date,
      required: true,
    },
  },
  {
    collection: "Report",
  }
);

schema.virtual("adminCreateInfo", {
  ref: "Admin",
  localField: "adminCreateId",
  foreignField: "_id",
  justOne: true,
});

schema.set("toObject", { virtuals: true });
schema.set("toJSON", { virtuals: true });
export const Report = mongoose.model<IReportModel>("Report", schema);
