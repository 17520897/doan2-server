import { Report, IReportModel } from "./report.model";
import BaseDbService from "../../services/baseDb";

export class ReportService extends BaseDbService<IReportModel> {}

export default new ReportService(Report);
