import express from "express";
import { validateRole } from "../../middlewares/validate";
import { AccountType } from "../../const";
import { RequestType, Valid } from "../../middlewares/Valid";
import IdParamsRequestDto from "../../const/IdParamsRequest.dto";
import middleware from "../../middlewares/middleware";
import reportController from "./report.controller";
import reportMiddleware from "./report.middleware";

const reportRouter = express.Router();

reportRouter.post(
  "/",
  validateRole(null, AccountType.users),
  middleware.userHaveCompanies,
  middleware.companyHaveAccountNumber,
  reportController.inputReport
);

reportRouter.post(
  "/file",
  validateRole(null, AccountType.users),
  reportController.getInputFromFileReport
)

reportRouter.get(
  "/",
  validateRole(null, AccountType.users),
  reportMiddleware.getReportMiddleware,
  reportController.getReport
);

reportRouter.get(
  "/firstInput",
  validateRole(null, AccountType.users),
  reportMiddleware.getReportMiddleware,
  reportController.getFirstInputReport
);

export default reportRouter;
