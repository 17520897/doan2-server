import { NextFunction, Request, Response } from "express";
import { ErrorsCode } from "../../const/errorsCode";
import BaseResponse from "../../services/baseResponse";
import companiesService from "../companies/companies.service";
import companiesValidate from "../companies/companies.validate";

const baseResponse = new BaseResponse("ReportMiddleware");
class ReportMiddleware {
  async getReportMiddleware(req: Request, res: Response, next: NextFunction) {
    try {
      const { userId } = req.cookies;
      const { companyId, accountNumberId } = req.query;
      const isUserHaveCompanies = await companiesValidate.isUserHaveCompanies(
        userId,
        companyId
      );
      if (!isUserHaveCompanies) {
        return baseResponse.notAccepted({
          res,
          errors: [
            {
              code: ErrorsCode.companiesNotFound,
              field: "companyId",
            },
          ],
        });
      }
      const checkCompaniesAccountNumber = await companiesService.getOne({
        _id: companyId,
        accountNumberId,
      });
      if (!checkCompaniesAccountNumber) {
        return baseResponse.notAccepted({
          res,
          errors: [
            {
              code: ErrorsCode.accountNumberNotExisted,
              field: "accountNumberId",
            },
          ],
        });
      }
      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }
}

export default new ReportMiddleware();
