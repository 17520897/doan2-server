import express from "express";
import { AccountType } from "../../const";
import { RequestType, Valid } from "../../middlewares/Valid";
import { validateRole } from "../../middlewares/validate";
import IdParamsRequestDto from "../../const/IdParamsRequest.dto";
import middleware from "../../middlewares/middleware";
import CreteOrUpdateManyServiceBillTaxDto from "./dto/createOrUpdateManyServiceBillTax.dto";
import serviceBillTaxController from "./serviceBillTax.controller";
import GetServiceBillTaxDto from "./dto/getServiceBillTax.dto";
import serviceBillTaxMiddleware from "./serviceBillTax.middleware";

const serviceBillTaxRouter = express.Router();

serviceBillTaxRouter.post(
  "/",
  Valid(CreteOrUpdateManyServiceBillTaxDto, RequestType.body),
  validateRole(null, AccountType.users),
  middleware.userHaveCompanies,
  middleware.companyHaveAccountNumber,
  serviceBillTaxMiddleware.companyHaveServiceBill,
  serviceBillTaxController.createManyServiceBillTax
);

serviceBillTaxRouter.get(
  "/",
  Valid(GetServiceBillTaxDto, RequestType.query),
  validateRole(null, AccountType.users),
  serviceBillTaxController.getServiceBillTax
);

export default serviceBillTaxRouter;
