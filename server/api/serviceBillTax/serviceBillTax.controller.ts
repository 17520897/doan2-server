import { NextFunction, Request, Response } from "express";
import BaseResponse from "../../services/baseResponse";
import l from "../../common/logger";
import { HTTPCode, MoneyTypeStatus } from "../../const";
import responseService from "../../services/responseService";
import mongoose from "mongoose";
import serviceBillService from "../serviceBill/serviceBill.service";
import serviceBillTaxService from "./serviceBillTax.service";

const baseResponse = new BaseResponse("ServiceBillTaxController");

class ServiceBillTaxController {
  async createManyServiceBillTax(req: Request, res: Response) {
    const session = await mongoose.startSession();
    session.startTransaction();
    try {
      const options = { session };
      const { userId } = req.cookies;
      const {
        companyId,
        serviceBillId,
        serviceBillDetailId,
        accountNumberId,
        data,
      } = req.body;
      let totalTax = 0;
      const createData = [];
      for (let i = 0; i < data.length; i++) {
        createData.push({
          ...data[i],
          companyId,
          serviceBillId,
          serviceBillDetailId,
          accountNumberId,
          userCreateId: userId,
          finalMoney: (data[i].stockMoney * data[i].percent) / 100,
        });
        totalTax += data[i].stockMoney;
      }
      await serviceBillTaxService.deleteMany(
        {
          serviceBillId,
        },
        options
      );
      await serviceBillTaxService.createMany(createData, options);
      await serviceBillService.updateById(
        serviceBillId,
        {
          taxMoney: totalTax,
        },
        options
      );
      await session.commitTransaction();
      return baseResponse.success({
        res,
        message: "create service bill tax success",
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    } finally {
      session.endSession();
    }
  }

  async getServiceBillTax(req: Request, res: Response) {
    try {
      const { userId } = req.cookies;
      const { companyId, serviceBillId } = req.query;
      const serviceBillTax = await serviceBillTaxService.populate({
        query: {
          userCreateId: userId,
          companyId,
          serviceBillId,
        },
        sort: {
          serviceBillDetailId: 1,
        },
        populate: [
          {
            path: "stockInfo",
          },
        ],
      });
      return baseResponse.success({
        res,
        data: serviceBillTax,
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }
}

export default new ServiceBillTaxController();
