import BaseDbService from "../../services/baseDb";
import { ServiceBillTax, IServiceBillTaxModel } from "./serviceBillTax.model";

export class ServiceBillTaxService extends BaseDbService<
  IServiceBillTaxModel
> {}

export default new ServiceBillTaxService(ServiceBillTax);
