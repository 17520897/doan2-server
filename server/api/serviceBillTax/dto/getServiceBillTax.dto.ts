import {
  IsString,
  IsEnum,
  IsInt,
  IsOptional,
  Equals,
  IsObject,
  IsArray,
  IsNumber,
  IsNumberString,
  Min,
  Max,
  IsMongoId,
  IsDateString,
} from "class-validator";
import { IsObjectHaveProperty } from "../../../middlewares/Valid";
import { MoneyBillType } from "../../../const";

export default class GetServiceBillTaxDto {
  @IsMongoId()
  companyId: string;

  @IsMongoId()
  serviceBillId: string;
}
