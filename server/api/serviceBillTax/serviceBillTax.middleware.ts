import { NextFunction, Request, Response } from "express";
import { ErrorsCode } from "../../const/errorsCode";
import BaseResponse from "../../services/baseResponse";
import serviceBillValidate from "../serviceBill/serviceBill.validate";
import serviceBillDetailValidate from "../serviceBillDetail/serviceBillDetail.validate";

const baseResponse = new BaseResponse("ServiceBillTaxMiddleware");
class ServiceBillTaxMiddleware {
  async companyHaveServiceBill(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { userId } = req.cookies;
      const { serviceBillId, companyId } = req.body;
      const isCompanyHaveServiceBill = await serviceBillValidate.isCompaniesHaveServiceBill(
        userId,
        companyId,
        serviceBillId
      );
      if (!isCompanyHaveServiceBill) {
        return baseResponse.notAccepted({
          res,
          errors: [
            {
              code: ErrorsCode.serviceBillCompaniesNotHaveServiceBill,
              field: "serviceBillId",
            },
          ],
        });
      }
      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }
}

export default new ServiceBillTaxMiddleware();
