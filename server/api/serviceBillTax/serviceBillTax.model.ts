import mongoose, { mongo, Schema } from "mongoose";

export interface IServiceBillTaxModel extends mongoose.Document {
  userCreateId: string;
}

const schema = new mongoose.Schema(
  {
    companyId: {
      type: mongoose.SchemaTypes.ObjectId,
      required: true,
      index: true,
    },
    userCreateId: {
      type: mongoose.SchemaTypes.ObjectId,
      required: true,
      index: true,
    },
    serviceBillId: {
      type: mongoose.SchemaTypes.ObjectId,
      required: true,
      index: true,
    },
    stockId: {
      type: mongoose.SchemaTypes.ObjectId,
      required: true,
      index: true,
    },
    accountNumber: {
      type: Number,
      index: true,
    },
    stockMoney: {
      type: Number,
      required: true,
    },
    percent: {
      type: String,
      required: true,
    },
    finalMoney: {
      type: Number,
      required: true,
    },
    userCreateAt: {
      type: Date,
      default: Date.now,
    },
  },
  {
    collection: "ServiceBillTax",
  }
);

schema.virtual("userCreateInfo", {
  ref: "User",
  localField: "userCreateId",
  foreignField: "_id",
  justOne: true,
});

schema.virtual("companyInfo", {
  ref: "Companies",
  localField: "companyId",
  foreignField: "_id",
  justOne: true,
});

schema.virtual("stockInfo", {
  ref: "Stock",
  localField: "stockId",
  foreignField: "_id",
  justOne: true,
});

schema.set("toObject", { virtuals: true });
schema.set("toJSON", { virtuals: true });

export const ServiceBillTax = mongoose.model<IServiceBillTaxModel>(
  "ServiceBillTax",
  schema
);
