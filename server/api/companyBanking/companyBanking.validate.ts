import { CompanyBankingStatus, SettingBankStatus } from "../../const";
import settingBankService from "../settingBank/settingBank.service";
import companyBankingService from "./companyBanking.service";

class companyBankingValidate {
  async isBankExisted(bankId) {
    const bank = await settingBankService.getOne({
      _id: bankId,
      status: SettingBankStatus.normal,
    });
    return bank ? true : false;
  }

  async isBankAccountExisted(userId, bankAccount, checkId = false, id = "") {
    const companyBanking = checkId
      ? await companyBankingService.getOne({
          userCreateId: userId,
          status: CompanyBankingStatus.normal,
          bankAccount,
          _id: { $ne: id },
        })
      : await companyBankingService.getOne({
          userCreateId: userId,
          bankAccount,
          status: CompanyBankingStatus.normal,
        });
    return companyBanking ? true : false;
  }

  async isCompanyBankingExisted(userId, id) {
    const companyBanking = await companyBankingService.getOne({
      _id: id,
      userCreateId: userId,
      status: CompanyBankingStatus.normal,
    });
    return companyBanking ? true : false;
  }
}

export default new companyBankingValidate();
