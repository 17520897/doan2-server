import {
  ValidateNested,
  IsString,
  MaxLength,
  IsEnum,
  IsObject,
  IsDate,
  IsEmail,
  IsNumber,
  IsOptional,
  MinLength,
  IsMongoId,
  Min,
  Length,
  IsUrl,
  IsArray,
  Equals,
} from "class-validator";

export default class CreateCompanyBankingDto {
  @Equals(undefined)
  userCreateId: string;

  @IsMongoId()
  companyId: string;
  @IsMongoId()
  bankId: string;

  @IsString()
  bankAccount: string;

  @IsString()
  bankAccountName: string;
}
