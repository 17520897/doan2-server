import {
  ValidateNested,
  IsString,
  MaxLength,
  IsEnum,
  IsObject,
  IsDate,
  IsEmail,
  IsNumber,
  IsOptional,
  MinLength,
  IsMongoId,
  Min,
  Length,
  IsUrl,
  IsArray,
  Equals,
} from "class-validator";

export default class UpdateCompanyBankingDto {
  @Equals(undefined)
  userCreateId: string;

  @Equals(undefined)
  companyId: string;

  @IsOptional()
  @IsMongoId()
  bankId: string;

  @IsOptional()
  @IsString()
  bankAccount: string;

  @IsOptional()
  @IsString()
  bankAccountName: string;
}
