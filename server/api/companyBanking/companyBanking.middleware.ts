import { NextFunction, Request, Response } from "express";
import BaseResponse from "../../services/baseResponse";
import l from "../../common/logger";
import { HTTPCode } from "../../const";
import { ErrorsCode } from "../../const/errorsCode";
import responseService from "../../services/responseService";
import services from "../../services/services";
import companyBankingValidate from "./companyBanking.validate";

const baseResponse = new BaseResponse("CompanyBankingMiddleware");

class CompanyBankingMiddleware {
  async createCompanyBanking(req: Request, res: Response, next: NextFunction) {
    try {
      const { userId } = req.cookies;
      const { bankAccount } = req.body;
      const isBankAccountExisted = await companyBankingValidate.isBankAccountExisted(
        userId,
        bankAccount
      );
      if (isBankAccountExisted) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.companyBankingBankAccountExisted,
              field: "bankAccount",
            },
          ],
        });
      }
      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async companyBankingExisted(req: Request, res: Response, next: NextFunction) {
    try {
      const { id } = req.params;
      const { userId } = req.cookies;
      const isCompanyBankingExisted = await companyBankingValidate.isCompanyBankingExisted(
        userId,
        id
      );
      if (!isCompanyBankingExisted) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.companyBankingNotFound,
              field: "id",
            },
          ],
        });
      }
      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async bankExisted(req: Request, res: Response, next: NextFunction) {
    try {
      const { bankId } = req.body;
      if (bankId) {
        const isBankExisted = await companyBankingValidate.isBankExisted(
          bankId
        );
        if (!isBankExisted) {
          return responseService.send(res, HTTPCode.notAccept, {
            errors: [
              {
                code: ErrorsCode.companyBankingBankNotExisted,
                field: "bankId",
              },
            ],
          });
        }
      }
      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async updateCompanyBanking(req: Request, res: Response, next: NextFunction) {
    try {
      const { id } = req.params;
      const { userId } = req.cookies;
      const { bankId, bankAccount } = req.body;
      const isBankAccountExisted = await companyBankingValidate.isBankAccountExisted(
        userId,
        bankAccount,
        true,
        id
      );
      if (isBankAccountExisted) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.companyBankingBankAccountExisted,
              field: "bankAccount",
            },
          ],
        });
      }
      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }
}

export default new CompanyBankingMiddleware();
