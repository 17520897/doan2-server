import BaseDbService from "../../services/baseDb";
import { CompanyBanking, ICompanyBankingModel } from "./companyBanking.model";

export class CompanyBankingService extends BaseDbService<
  ICompanyBankingModel
> {}

export default new CompanyBankingService(CompanyBanking);
