import express from "express";
import { AccountType } from "../../const";
import IdParamsRequestDto from "../../const/IdParamsRequest.dto";
import { RequestType, Valid } from "../../middlewares/Valid";
import { validateRole } from "../../middlewares/validate";
import companyBankingController from "./companyBanking.controller";
import companyBankingMiddleware from "./companyBanking.middleware";
import CreateCompanyBankingDto from "./dto/createCompanyBanking.dto";
import GetCompanyBankingDto from "./dto/getCompanyBanking.dto";

const companyBankingRouter = express.Router();

companyBankingRouter.post(
  "/",
  Valid(CreateCompanyBankingDto, RequestType.body),
  validateRole(null, AccountType.users),
  companyBankingMiddleware.bankExisted,
  companyBankingMiddleware.createCompanyBanking,
  companyBankingController.createCompanyBanking
);

companyBankingRouter.put(
  "/:id",
  Valid(IdParamsRequestDto, RequestType.params),
  Valid(CreateCompanyBankingDto, RequestType.body),
  validateRole(null, AccountType.users),
  companyBankingMiddleware.companyBankingExisted,
  companyBankingMiddleware.bankExisted,
  companyBankingMiddleware.updateCompanyBanking,
  companyBankingController.updateCompanyBanking
);

companyBankingRouter.delete(
  "/:id",
  Valid(IdParamsRequestDto, RequestType.params),
  validateRole(null, AccountType.users),
  companyBankingMiddleware.companyBankingExisted,
  companyBankingController.deleteCompanyBanking
);

companyBankingRouter.get(
  "/",
  Valid(GetCompanyBankingDto, RequestType.query),
  validateRole(null, AccountType.users),
  companyBankingController.getAllCompanyBanking
);

export default companyBankingRouter;
