import { Request, Response } from "express";
import { HTTPCode } from "../../const";
import BaseResponse from "../../services/baseResponse";
import responseService from "../../services/responseService";
import services from "../../services/services";
import settingBankService from "../settingBank/settingBank.service";
import companyBankingService from "./companyBanking.service";

const baseResponse = new BaseResponse("CompanyBankingController");
class CompanyBankingController {
  async createCompanyBanking(req: Request, res: Response) {
    try {
      const { userId } = req.cookies;
      const companyBanking = await companyBankingService.create({
        ...req.body,
        userCreateId: userId,
      });
      return responseService.send(res, HTTPCode.success, {
        data: companyBanking,
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async updateCompanyBanking(req: Request, res: Response) {
    try {
      const { id } = req.params;
      const companyBanking = await companyBankingService.updateById(id, {
        ...req.body,
      });
      return responseService.send(res, HTTPCode.success, {
        data: companyBanking,
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async deleteCompanyBanking(req: Request, res: Response) {
    try {
      const { id } = req.params;
      await companyBankingService.deleteById(id);
      return responseService.send(res, HTTPCode.success, {
        message: "delete company banking success",
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async getAllCompanyBanking(req: Request, res: Response) {
    try {
      const { userId } = req.cookies;
      const { search, page, limit, companyId } = await req.query;
      let query = {
        userCreateId: userId,
        companyId,
      } as any;
      if (search) {
        const bank = (await settingBankService.getByQuery({
          query: { name: { $regex: search, $options: "i" } },
        })) as any;
        const bankId = [];
        bank.pop();
        for (let i = 0; i < bank.length; i++) {
          bankId.push(bank[i]._id);
        }
        query = {
          ...query,
          $or: [
            {
              bankAccount: { $regex: search, $options: "i" },
            },
            {
              bankAccountName: { $regex: search, $options: "i" },
            },
            {
              bankId: bankId,
            },
          ],
        };
      }
      const companyBanking = await companyBankingService.populate({
        query,
        page,
        limit,
        sort: {
          bankAccount: 1,
          bankAccountName: 1,
        },
        populate: [
          {
            path: "bankInfo",
          },
        ],
      });
      return responseService.send(res, HTTPCode.success, {
        data: companyBanking,
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }
}

export default new CompanyBankingController();
