import mongoose, { Schema } from "mongoose";

export interface ICompanyBankingModel extends mongoose.Document {
  userCreateId: string;
  companyId: string;
  bankId: string;
  bankAccount: string;
  bankAccountName: string;
}

const schema = new mongoose.Schema(
  {
    companyId: {
      type: mongoose.SchemaTypes.ObjectId,
      required: true,
    },
    userCreateId: {
      type: mongoose.SchemaTypes.ObjectId,
      required: true,
    },
    bankId: {
      type: String,
    },
    bankAccount: {
      type: String,
    },
    bankAccountName: {
      type: String,
    },
    usedAt: {
      type: Date,
      default: Date.now,
    },
    status: {
      type: String,
      enum: ["normal", "deleted"],
      default: "normal",
    },
  },
  {
    collection: "CompanyBanking",
  }
);
schema.virtual("userCreateInfo", {
  ref: "User",
  localField: "userCreateId",
  foreignField: "_id",
  justOne: true,
});

schema.virtual("bankInfo", {
  ref: "SettingBank",
  localField: "bankId",
  foreignField: "_id",
  justOne: true,
});

schema.set("toObject", { virtuals: true });
schema.set("toJSON", { virtuals: true });

export const CompanyBanking = mongoose.model<ICompanyBankingModel>(
  "CompanyBanking",
  schema
);
