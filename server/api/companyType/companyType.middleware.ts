import { NextFunction, Request, Response } from "express";
import BaseResponse from "../../services/baseResponse";
import l from "../../common/logger";
import { CompanyTypeStatus, HTTPCode } from "../../const";
import { ErrorsCode } from "../../const/errorsCode";
import responseService from "../../services/responseService";
import services from "../../services/services";
import companyTypeService from "./companyType.service";

const baseResponse = new BaseResponse("CompanyTypeMiddleware");

class CompanyTypeMiddleware {
  async createCompanyType(req: Request, res: Response, next: NextFunction) {
    try {
      const { name } = req.body;
      const isNameExisted = await companyTypeService.getOne({
        name,
        status: CompanyTypeStatus.normal,
      });
      if (isNameExisted) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.companyTypeNameExisted,
              field: "name",
            },
          ],
        });
      }
      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async updateCompanyType(req: Request, res: Response, next: NextFunction) {
    try {
      const { name } = req.body;
      const { id } = req.params;
      const isNameExisted = await companyTypeService.getOne({
        name,
        status: CompanyTypeStatus.normal,
        _id: { $ne: id },
      });
      if (isNameExisted) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.companyTypeNameExisted,
              field: "name",
            },
          ],
        });
      }
      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }
}

export default new CompanyTypeMiddleware();
