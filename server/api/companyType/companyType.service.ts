import { CompanyType, ICompanyTypeModel } from "./companyType.model";
import BaseDbService from "../../services/baseDb";

export class CompanyTypeService extends BaseDbService<ICompanyTypeModel> {}

export default new CompanyTypeService(CompanyType);
