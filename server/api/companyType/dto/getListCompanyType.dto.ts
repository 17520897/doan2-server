import {
  ValidateNested,
  IsString,
  MaxLength,
  IsEnum,
  IsObject,
  IsDate,
  IsEmail,
  IsNumber,
  IsOptional,
  MinLength,
  IsMongoId,
  Min,
  Length,
  IsUrl,
  IsArray,
  Equals,
  IsNumberString,
} from "class-validator";

export default class GetListCompanyTypeDto {
  @IsNumberString()
  page: string;

  @IsNumberString()
  limit: string;
}
