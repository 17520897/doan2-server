import { Request, Response } from "express";
import BaseResponse from "../../services/baseResponse";
import l from "../../common/logger";
import { CompanyTypeStatus, HTTPCode } from "../../const";
import responseService from "../../services/responseService";
import services from "../../services/services";
import companyTypeService from "./companyType.service";

const baseResponse = new BaseResponse("CompanyTypeController");
class CompanyTypeController {
  async createCompanyType(req: Request, res: Response) {
    try {
      const { adminId } = req.cookies;
      const companyType = await companyTypeService.create({
        ...req.body,
        adminCreateId: adminId,
      });
      return responseService.send(res, HTTPCode.success, {
        data: companyType,
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async getListCompanyType(req: Request, res: Response) {
    try {
      const { page, limit } = req.cookies;
      const listCompanyType = await companyTypeService.populate({
        query: {
          status: CompanyTypeStatus.normal,
        },
        populate: [
          {
            path: "adminCreateInfo",
            select: "-password",
          },
        ],
        sort: {
          numberOfCompany: -1,
          createAt: -1,
        },
        page,
        limit,
      });
      return responseService.send(res, HTTPCode.success, {
        data: listCompanyType,
        message: "get list company type success",
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async updateCompanyType(req: Request, res: Response) {
    try {
      const { adminId } = req.cookies;

      const { id } = req.params;
      const companyType = await companyTypeService.updateById(id, {
        ...req.body,
        adminCreateId: adminId,
      });
      return responseService.send(res, HTTPCode.success, {
        data: companyType,
        message: "update company type success",
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async deleteCompanyType(req: Request, res: Response) {
    try {
      const { id } = req.params;
      await companyTypeService.updateById(id, {
        status: CompanyTypeStatus.deleted,
      });
      return responseService.send(res, HTTPCode.success, {
        message: "delete company type success",
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }
}

export default new CompanyTypeController();
