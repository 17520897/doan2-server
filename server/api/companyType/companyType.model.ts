import mongoose, { Schema } from "mongoose";

export interface ICompanyTypeModel extends mongoose.Document {
  name: string;
  numberOfCompany: number;
  status: string;
  createAt: Date;
  adminCreateId: string;
}

const schema = new mongoose.Schema(
  {
    name: {
      type: String,
      trim: true,
    },
    numberOfCompany: {
      type: Number,
      default: 0,
    },
    status: {
      type: String,
      default: "normal",
      enum: ["normal", "deleted"],
    },
    createAt: {
      type: Date,
      default: Date.now,
    },
    adminCreateId: {
      type: mongoose.SchemaTypes.ObjectId,
    },
  },
  {
    collection: "CompanyType",
  }
);

schema.virtual("adminCreateInfo", {
  ref: "Admin",
  localField: "adminCreateId",
  foreignField: "_id",
  justOne: true,
});

schema.set("toObject", { virtuals: true });
schema.set("toJSON", { virtuals: true });
export const CompanyType = mongoose.model<ICompanyTypeModel>(
  "CompanyType",
  schema
);
