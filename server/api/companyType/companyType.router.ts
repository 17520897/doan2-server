import express from "express";
import { AccountType } from "../../const";
import IdParamsRequestDto from "../../const/IdParamsRequest.dto";
import { PermissionKeys } from "../../const/permissions";
import { RequestType, Valid } from "../../middlewares/Valid";
import { validateRole } from "../../middlewares/validate";
import companyTypeController from "./companyType.controller";
import companyTypeMiddleware from "./companyType.middleware";
import CreateCompanyTypeDto from "./dto/createCompanyType.dto";
import GetListCompanyTypeDto from "./dto/getListCompanyType.dto";
import UpdateCompanyTypeDto from "./dto/updateCompanyType.dto";

const companyTypeRouter = express.Router();

companyTypeRouter.get(
  "/",
  Valid(GetListCompanyTypeDto, RequestType.query),
  validateRole(null, AccountType.all),
  companyTypeController.getListCompanyType
);

companyTypeRouter.post(
  "/",
  validateRole(PermissionKeys.companyTypeCreate, AccountType.admin),
  Valid(CreateCompanyTypeDto, RequestType.body),
  companyTypeMiddleware.createCompanyType,
  companyTypeController.createCompanyType
);
companyTypeRouter.put(
  "/:id",
  Valid(IdParamsRequestDto, RequestType.params),
  Valid(UpdateCompanyTypeDto, RequestType.params),
  validateRole(PermissionKeys.companyTypeUpdate, AccountType.admin),
  companyTypeMiddleware.updateCompanyType,
  companyTypeController.updateCompanyType
);

companyTypeRouter.delete(
  "/:id",
  validateRole(PermissionKeys.companyTypeDelete, AccountType.admin),
  companyTypeController.deleteCompanyType
);

export default companyTypeRouter;
