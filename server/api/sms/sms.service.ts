import { ISMSModel, SMS } from "./sms.model";
export class SMSService {
  async create(data: any) {
    const doc = new SMS(data);
    const sms = (await doc.save()) as ISMSModel;
    return sms;
  }
}

export default new SMSService();
