import mongoose from "mongoose";

export interface ISMSModel extends mongoose.Document {
  phone: string,
  code: string,
  type: string
}

const schema = new mongoose.Schema(
  {
    phone: {
      type: String,
      required: true
    },
    code: {
      type: String
    },
    type: {
      type: String,
      enum: ["register", "forgotPassword"]
    },
    createAt: {
      type: Date,
      default: Date.now
    }
  },
  {
    collection: "SMS"
  }
);

export const SMS = mongoose.model<ISMSModel>("SMS", schema);
