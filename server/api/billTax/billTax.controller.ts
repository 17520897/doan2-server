import { NextFunction, Request, Response } from "express";
import BaseResponse from "../../services/baseResponse";
import l from "../../common/logger";
import { HTTPCode, MoneyTypeStatus } from "../../const";
import responseService from "../../services/responseService";
import mongoose from "mongoose";
import billService from "../bill/bill.service";
import billTaxService from "./billTax.service";

const baseResponse = new BaseResponse("BillTaxController");

class BillTaxController {
  async createManyBillTax(req: Request, res: Response) {
    const session = await mongoose.startSession();
    session.startTransaction();
    try {
      const options = { session };
      const { userId } = req.cookies;
      const {
        companyId,
        billId,
        serviceBillDetailId,
        accountNumberId,
        data,
        billCreateAt,
      } = req.body;
      let totalTax = 0;
      const createData = [];
      for (let i = 0; i < data.length; i++) {
        createData.push({
          ...data[i],
          companyId,
          billId,
          serviceBillDetailId,
          accountNumberId,
          userCreateId: userId,
          billCreateAt,
          finalMoney: data[i].finalMoney,
        });
        totalTax += data[i].finalMoney;
      }
      await billTaxService.deleteMany(
        {
          billId,
        },
        options
      );
      await billTaxService.createMany(createData, options);
      await billService.updateById(
        billId,
        {
          taxMoney: totalTax,
        },
        options
      );
      await session.commitTransaction();
      return baseResponse.success({
        res,
        message: "create bill tax success",
      });
    } catch (error) {
      await session.abortTransaction();
      return baseResponse.serverError(res, error);
    } finally {
      session.endSession();
    }
  }

  async getBillTax(req: Request, res: Response) {
    try {
      const { userId } = req.cookies;
      const { companyId, billId } = req.query;
      const serviceBillTax = await billTaxService.populate({
        query: {
          userCreateId: userId,
          companyId,
          billId,
        },
        sort: {
          stockId: 1,
        },
        populate: [
          {
            path: "stockInfo",
          },
        ],
      });
      return baseResponse.success({
        res,
        data: serviceBillTax,
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }
}

export default new BillTaxController();
