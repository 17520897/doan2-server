import mongoose, { mongo, Schema } from "mongoose";

export interface IBillTaxModel extends mongoose.Document {
  userCreateId: string;
  debitAccountNumber: number;
  receiveAccountNumber: number;
  finalMoney: number;
  accountNumber;
}

const schema = new mongoose.Schema(
  {
    companyId: {
      type: mongoose.SchemaTypes.ObjectId,
      required: true,
      index: true,
    },
    userCreateId: {
      type: mongoose.SchemaTypes.ObjectId,
      required: true,
      index: true,
    },
    accountNumberId: {
      type: mongoose.SchemaTypes.ObjectId,
      index: true,
    },
    billId: {
      type: mongoose.SchemaTypes.ObjectId,
      required: true,
      index: true,
    },
    stockId: {
      type: mongoose.SchemaTypes.ObjectId,
      required: true,
      index: true,
    },
    accountNumber: {
      type: Number,
      index: true,
    },
    stockMoney: {
      type: Number,
      required: true,
    },
    percent: {
      type: String,
      required: true,
    },
    finalMoney: {
      type: Number,
      required: true,
    },
    billCreateAt: {
      type: Date,
      required: true,
    },
    userCreateAt: {
      type: Date,
      default: Date.now,
    },
  },
  {
    collection: "BillTax",
  }
);

schema.virtual("userCreateInfo", {
  ref: "User",
  localField: "userCreateId",
  foreignField: "_id",
  justOne: true,
});

schema.virtual("companyInfo", {
  ref: "Companies",
  localField: "companyId",
  foreignField: "_id",
  justOne: true,
});

schema.virtual("stockInfo", {
  ref: "Stock",
  localField: "stockId",
  foreignField: "_id",
  justOne: true,
});

schema.set("toObject", { virtuals: true });
schema.set("toJSON", { virtuals: true });

export const BillTax = mongoose.model<IBillTaxModel>("BillTax", schema);
