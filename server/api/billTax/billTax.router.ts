import express from "express";
import { AccountType } from "../../const";
import { RequestType, Valid } from "../../middlewares/Valid";
import { validateRole } from "../../middlewares/validate";
import IdParamsRequestDto from "../../const/IdParamsRequest.dto";
import middleware from "../../middlewares/middleware";
import CreteOrUpdateManyServiceBillTaxDto from "./dto/createOrUpdateManyBillTax.dto";
import billTaxController from "./billTax.controller";
import GetBillTaxDto from "./dto/getBillTax.dto";
import billTaxMiddleware from "./billTax.middleware";

const billTaxRouter = express.Router();

billTaxRouter.post(
  "/",
  Valid(CreteOrUpdateManyServiceBillTaxDto, RequestType.body),
  validateRole(null, AccountType.users),
  middleware.userHaveCompanies,
  middleware.companyHaveAccountNumber,
  billTaxMiddleware.companyHaveBill,
  billTaxController.createManyBillTax
);

billTaxRouter.get(
  "/",
  Valid(GetBillTaxDto, RequestType.query),
  validateRole(null, AccountType.users),
  billTaxController.getBillTax
);

export default billTaxRouter;
