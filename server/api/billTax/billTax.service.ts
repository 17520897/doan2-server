import BaseDbService from "../../services/baseDb";
import { BillTax, IBillTaxModel } from "./billTax.model";

export class BillTaxService extends BaseDbService<IBillTaxModel> {}

export default new BillTaxService(BillTax);
