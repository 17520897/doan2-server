import mongoose, { Schema } from "mongoose";

export interface ISystemConstModel extends mongoose.Document {
  userTotalFreeCompany: number;
  userRoleFreeId: string;
  userTotalFreeBill: number;
}

const schema = new mongoose.Schema(
  {
    userTotalFreeCompany: {
      type: Number,
      default: 0,
    },
    userRoleFreeId: {
      type: mongoose.SchemaTypes.ObjectId,
    },
    userTotalFreeBill: {
      type: Number,
      default: 0,
    },
  },
  {
    collection: "SystemConst",
  }
);

schema.virtual("userRoleFreeInfo", {
  ref: "UserRoles",
  localField: "userRoleFreeId",
  foreignField: "_id",
  justOne: true,
});
schema.set("toObject", { virtuals: true });
schema.set("toJSON", { virtuals: true });
export const SystemConst = mongoose.model<ISystemConstModel>(
  "SystemConst",
  schema
);
