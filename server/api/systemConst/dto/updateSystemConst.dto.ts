import {
  ValidateNested,
  IsString,
  MaxLength,
  IsEnum,
  IsObject,
  IsDate,
  IsEmail,
  IsNumber,
  IsOptional,
  MinLength,
  IsMongoId,
  Min,
  Length,
  IsUrl,
  IsArray,
  Equals,
} from "class-validator";

export default class UpdateSystemConstDto {
  @IsOptional()
  @IsNumber()
  @Min(1)
  userTotalFreeCompany: number;

  @IsOptional()
  @IsMongoId()
  userRoleFreeId: string;
}
