import express from "express";
import { AccountType } from "../../const";
import { PermissionKeys } from "../../const/permissions";
import { RequestType, Valid } from "../../middlewares/Valid";
import { validateRole } from "../../middlewares/validate";
import UpdateSystemConstDto from "./dto/updateSystemConst.dto";
import systemConstController from "./systemConst.controller";
import systemConstMiddleware from "./systemConst.middleware";

const systemConstRouter = express.Router();

systemConstRouter.get(
  "/",
  validateRole(PermissionKeys.systemConstGetList, AccountType.admin),
  systemConstController.getSystemConst
);

systemConstRouter.put(
  "/",
  Valid(UpdateSystemConstDto, RequestType.body),
  validateRole(PermissionKeys.systemConstUpdate, AccountType.admin),
  systemConstMiddleware.updateSystemConst,
  systemConstController.updateSystemConst
);

export default systemConstRouter;
