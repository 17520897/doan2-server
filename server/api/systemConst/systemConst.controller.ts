import { Request, Response } from "express";
import BaseResponse from "../../services/baseResponse";
import l from "../../common/logger";
import { HTTPCode } from "../../const";
import responseService from "../../services/responseService";
import services from "../../services/services";
import systemConstService from "./systemConst.service";

const baseResponse = new BaseResponse("SystemConstController");
class SystemConstController {
  async getSystemConst(req: Request, res: Response) {
    try {
      const systemConst = await systemConstService.getOne({});
      return responseService.send(res, HTTPCode.success, {
        data: systemConst,
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async updateSystemConst(req: Request, res: Response) {
    try {
      const systemConst = await systemConstService.updateOne({}, req.body);
      return responseService.send(res, HTTPCode.success, {
        data: systemConst,
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }
}

export default new SystemConstController();
