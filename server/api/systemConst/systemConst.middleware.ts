import { NextFunction, Request, Response } from "express";
import BaseResponse from "../../services/baseResponse";
import l from "../../common/logger";
import { HTTPCode, UserRoleStatus } from "../../const";
import { ErrorsCode } from "../../const/errorsCode";
import responseService from "../../services/responseService";
import services from "../../services/services";
import userRolesService from "../userRoles/userRoles.service";
import systemConstService from "./systemConst.service";

const baseResponse = new BaseResponse("SystemConstMiddleware");
class SystemConstMiddleware {
  async updateSystemConst(req: Request, res: Response, next: NextFunction) {
    try {
      const { userRoleFreeId } = req.body;
      if (userRoleFreeId) {
        const userRoles = await userRolesService.getOne({
          _id: userRoleFreeId,
          status: UserRoleStatus.enable,
        });
        if (!userRoles) {
          return responseService.send(res, HTTPCode.notAccept, {
            errors: [
              {
                code: ErrorsCode.systemConstUserRolesNotExisted,
                error: "user roles not existed",
                field: "userRoleFreeId",
              },
            ],
          });
        }
      }
      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }
}

export default new SystemConstMiddleware();
