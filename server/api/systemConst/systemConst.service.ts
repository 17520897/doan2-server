import { QueryFindOneAndUpdateOptions } from "mongoose";
import BaseDbService from "../../services/baseDb";
import { SystemConst, ISystemConstModel } from "./systemConst.model";

export class SystemConstService extends BaseDbService<ISystemConstModel> {}

export default new SystemConstService(SystemConst);
