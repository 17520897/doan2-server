import {
  ValidateNested,
  IsString,
  MaxLength,
  IsEnum,
  IsObject,
  IsDate,
  IsEmail,
  IsNumber,
  IsOptional,
  MinLength,
  IsMongoId,
  Min,
  Length,
  IsUrl,
  IsArray,
  Equals,
} from "class-validator";

export default class CreateCompaniesDto {
  @IsString()
  name: String;

  @IsString()
  taxCode: String;

  @IsMongoId()
  companyTypeId: String;

  @Equals(undefined)
  userCreateId: String;

  @IsOptional()
  @IsString()
  phone: String;

  @Equals(undefined)
  createAt: String;

  @IsOptional()
  address: String;

  @IsMongoId()
  accountNumberId: String;
}
