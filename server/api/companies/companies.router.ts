import express from "express";
import { AccountType } from "../../const";
import { PermissionKeys } from "../../const/permissions";
import { RequestType, Valid } from "../../middlewares/Valid";
import { validateRole } from "../../middlewares/validate";
import companiesController from "./companies.controller";
import companiesMiddleware from "./companies.middleware";
import CreateCompaniesDto from "./dto/createCompanies.dto";
import GetByIdCompaniesDto from "./dto/getByIdCompanies.dto";

const companiesRouter = express.Router();

companiesRouter.get(
  "/",
  validateRole(null, AccountType.users),
  companiesController.userGetListCompanies
);

companiesRouter.get(
  "/:id",
  Valid(GetByIdCompaniesDto, RequestType.params),
  validateRole(null, AccountType.users),
  companiesController.userGetByIdCompanies
);

companiesRouter.post(
  "/",
  Valid(CreateCompaniesDto, RequestType.body),
  validateRole(null, AccountType.users),
  companiesMiddleware.checkAccountNumberIdExisted,
  companiesMiddleware.checkRightAddressCompanies,
  companiesMiddleware.userCreateCompanies,
  companiesController.userCreateCompanies
);
companiesRouter.put(
  "/:id",
  validateRole(null, AccountType.users),
  companiesMiddleware.userHaveCompanies,
  companiesMiddleware.checkRightAddressCompanies,
  companiesMiddleware.userUpdateCompanies,
  companiesController.userUpdateCompanies
);

companiesRouter.delete(
  "/:id",
  validateRole(null, AccountType.users),
  companiesMiddleware.userHaveCompanies,
  companiesController.userDeleteCompanies
);

companiesRouter.put(
  "/used/:id",
  validateRole(null, AccountType.users),
  companiesMiddleware.userHaveCompanies,
  companiesController.userCheckIsUsedCompanies
);

export default companiesRouter;
