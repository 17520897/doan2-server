import { CompaniesStatus } from "../../const";
import companiesService from "./companies.service";

class CompaniesValidate {
  async isUserHaveCompanies(userId, companiesId) {
    const companies = await companiesService.getOne({
      userCreateId: userId,
      status: CompaniesStatus.normal,
      _id: companiesId,
    });
    return companies ? true : false;
  }
}

export default new CompaniesValidate();
