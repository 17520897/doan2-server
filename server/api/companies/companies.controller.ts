import { Request, Response } from "express";
import { CompaniesStatus, HTTPCode } from "../../const";
import responseService from "../../services/responseService";
import companiesService from "./companies.service";
import mongoose from "mongoose";
import companyTypeService from "../companyType/companyType.service";
import BaseResponse from "../../services/baseResponse";

const baseResponse = new BaseResponse("CompaniesController");
class CompaniesController {
  async userCreateCompanies(req: Request, res: Response) {
    const session = await mongoose.startSession();
    session.startTransaction();
    try {
      const options = { session };
      const { userId } = req.cookies;
      const { companyTypeId } = req.body;
      const companies = await companiesService.create(
        {
          ...req.body,
          userCreateId: userId,
        },
        options
      );
      await companyTypeService.updateById(
        companyTypeId,
        {
          $inc: {
            numberOfCompany: 1,
          },
        },
        options
      );
      await session.commitTransaction();
      return responseService.send(res, HTTPCode.success, {
        data: companies,
      });
    } catch (error) {
      await session.abortTransaction();
      return baseResponse.serverError(res, error);
    } finally {
      session.endSession();
    }
  }

  async userGetListCompanies(req: Request, res: Response) {
    try {
      const { userId } = req.cookies;
      const { page, limit, search } = req.query;
      let query = {
        status: CompaniesStatus.normal,
        userCreateId: userId,
      } as any;
      if (search) {
        query = {
          ...query,
          $or: [
            {
              name: { $regex: search, $options: "i" },
            },
            {
              taxCode: { $regex: search, $options: "i" },
            },
            {
              phone: { $regex: search, $options: "i" },
            },
          ],
        };
      }
      const listCompanies = await companiesService.populate({
        query,
        populate: [
          {
            path: "userCreateInfo",
            select: "-password",
          },
          {
            path: "companyTypeInfo",
            select: "name",
          },
        ],
        sort: {
          name: 1,
          usedAt: -1,
          createAt: -1,
        },
        page,
        limit,
      });
      return responseService.send(res, HTTPCode.success, {
        data: listCompanies,
        message: "get list company type success",
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async userGetByIdCompanies(req: Request, res: Response) {
    try {
      const { id } = req.params;
      const { userId } = req.cookies;
      const companies = await companiesService.getOne({
        _id: id,
        userCreateId: userId,
        status: CompaniesStatus.normal,
      });
      return baseResponse.success({
        res,
        data: companies,
        message: "get companies by id success",
        success: true,
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async userUpdateCompanies(req: Request, res: Response) {
    try {
      const { userId } = req.cookies;
      const { id } = req.params;
      const companies = await companiesService.updateById(id, {
        ...req.body,
        userCreateId: userId,
        usedAt: Date.now(),
      });
      return responseService.send(res, HTTPCode.success, {
        data: companies,
        message: "update company success",
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async userDeleteCompanies(req: Request, res: Response) {
    try {
      const { id } = req.params;
      await companiesService.updateById(id, {
        status: CompaniesStatus.deleted,
      });
      return responseService.send(res, HTTPCode.success, {
        message: "delete company type success",
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async userCheckIsUsedCompanies(req: Request, res: Response) {
    try {
      const { id } = req.params;
      await companiesService.updateById(id, {
        usedAt: Date.now(),
      });
      return responseService.send(res, HTTPCode.success, {
        message: "check used company success",
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }
}

export default new CompaniesController();
