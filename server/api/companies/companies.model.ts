import mongoose, { Schema } from "mongoose";

export interface ICompaniesModel extends mongoose.Document {
  companyTypeId: string;
  name: string;
  taxCode: string;
  phone: string;
  address: {
    home: string;
    ward: number;
    district: number;
    city: number;
    wardText: string;
    districtText: string;
    cityText: string;
  };
  userCreateId: string;
  createAt: Date;
  accountNumberId: string;
}

const schema = new mongoose.Schema(
  {
    companyTypeId: {
      type: mongoose.SchemaTypes.ObjectId,
    },
    name: {
      type: String,
    },
    taxCode: {
      type: String,
    },
    phone: {
      type: String,
    },
    address: {
      home: {
        type: String,
      },
      ward: {
        type: Number,
      },
      district: {
        type: Number,
      },
      city: {
        type: Number,
      },
      wardText: {
        type: String,
      },
      districtText: {
        type: String,
      },
      cityText: {
        type: String,
      },
    },
    firstInput: {
      type: Date,
    },
    accountNumberId: {
      type: String,
      index: "text",
    },
    status: {
      type: String,
      default: "normal",
      enum: ["normal", "deleted"],
    },
    userCreateId: {
      type: mongoose.SchemaTypes.ObjectId,
    },
    createAt: {
      type: Date,
      default: Date.now,
    },
    usedAt: {
      type: Date,
      default: Date.now,
    },
  },
  {
    collection: "Companies",
  }
);

schema.virtual("userCreateInfo", {
  ref: "Users",
  localField: "userCreateId",
  foreignField: "_id",
  justOne: true,
});

schema.virtual("companyTypeInfo", {
  ref: "CompanyType",
  localField: "companyTypeId",
  foreignField: "_id",
  justOne: true,
});

schema.set("toObject", { virtuals: true });
schema.set("toJSON", { virtuals: true });
export const Companies = mongoose.model<ICompaniesModel>("Companies", schema);
