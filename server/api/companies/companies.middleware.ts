import { NextFunction, Request, Response } from "express";
import { CompaniesStatus, HTTPCode } from "../../const";
import { ErrorsCode } from "../../const/errorsCode";
import BaseResponse from "../../services/baseResponse";
import responseService from "../../services/responseService";
import services from "../../services/services";
import accountNumberService from "../accountNumber/accountNumber.service";
import systemConstService from "../systemConst/systemConst.service";
import userUsedHistoryService from "../userUsedHistory/userUsedHistory.service";
import companiesService from "./companies.service";

const baseResponse = new BaseResponse("CompaniesMiddleware");
class CompaniesMiddleware {
  async checkRightAddressCompanies(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { address } = req.body;
      const isRightAddress = services.isRightAddress(address);
      if (address && !isRightAddress.success) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.companiesIsWrongAddress,
              field: "address",
            },
          ],
        });
      }
      req.body.address = {
        ...req.body.address,
        cityText: isRightAddress.address.city.fullName,
        districtText: isRightAddress.address.district.fullName,
        wardText: isRightAddress.address.ward.fullName,
      };
      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async checkAccountNumberIdExisted(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { accountNumberId } = req.body;
      if (accountNumberId) {
        const accountNumberExisted = await accountNumberService.getById(
          accountNumberId
        );
        if (!accountNumberExisted) {
          return baseResponse.notAccepted({
            message: "account number not found",
            res,
            errors: [
              {
                code: ErrorsCode.accountNumberNotExisted,
                field: "accountNumberId",
              },
            ],
          });
        }
      }
      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async userCreateCompanies(req: Request, res: Response, next: NextFunction) {
    try {
      const { name, taxCode } = req.body;
      const { userId } = req.cookies;
      const [isNameExisted, isTaxCodeExisted, systemConst] = await Promise.all([
        companiesService.getOne({
          name,
          status: CompaniesStatus.normal,
          userCreateId: userId,
        }),
        companiesService.getOne({
          taxCode,
          status: CompaniesStatus.normal,
          userCreateId: userId,
        }),
        systemConstService.getOne({}),
      ]);
      if (isNameExisted) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              field: "name",
              error: ErrorsCode.companiesNameExisted,
            },
          ],
        });
      }
      if (isTaxCodeExisted) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              field: "taxCode",
              error: ErrorsCode.companiesTaxCodeExisted,
            },
          ],
        });
      }
      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async userUpdateCompanies(req: Request, res: Response, next: NextFunction) {
    try {
      const { id } = req.params;
      const { name, taxCode } = req.body;
      const { userId } = req.cookies;
      const [isNameExisted, isTaxCodeExisted] = await Promise.all([
        companiesService.getOne({
          _id: { $ne: id },
          name,
          status: CompaniesStatus.normal,
          userCreateId: userId,
        }),
        companiesService.getOne({
          _id: { $ne: id },
          taxCode,
          status: CompaniesStatus.normal,
          userCreateId: userId,
        }),
      ]);
      if (isNameExisted) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              field: "name",
              error: ErrorsCode.companiesNameExisted,
            },
          ],
        });
      }
      if (isTaxCodeExisted) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              field: "taxCode",
              error: ErrorsCode.companiesTaxCodeExisted,
            },
          ],
        });
      }
      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async userHaveCompanies(req: Request, res: Response, next: NextFunction) {
    try {
      const { id } = req.params;
      const { userId } = req.cookies;
      const checkUsedHaveCompany = await companiesService.getOne({
        userCreateId: userId,
        _id: id,
        status: CompaniesStatus.normal,
      });
      if (!checkUsedHaveCompany) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.companiesNotFound,
              field: "id",
            },
          ],
        });
      }
      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }
}

export default new CompaniesMiddleware();
