import { Companies, ICompaniesModel } from "./companies.model";
import BaseDbService from "../../services/baseDb";

export class CompaniesService extends BaseDbService<ICompaniesModel> {}

export default new CompaniesService(Companies);
