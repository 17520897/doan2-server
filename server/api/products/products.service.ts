import BaseDbService from "../../services/baseDb";
import { IProductsModel, Products } from "./products.model";

class ProductsService extends BaseDbService<IProductsModel> {}

export default new ProductsService(Products);
