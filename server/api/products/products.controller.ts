import { Request, Response } from "express";
import { ProductsStatus } from "../../const";
import BaseResponse from "../../services/baseResponse";
import productsService from "./products.service";

const baseResponse = new BaseResponse("ProductsController");
class ProductsController {
  async createProducts(req: Request, res: Response) {
    try {
      const { userId } = req.cookies;
      const products = await productsService.create({
        ...req.body,
        userCreateId: userId,
      });
      return baseResponse.success({
        res,
        data: products,
        message: "create products success",
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async updateProducts(req: Request, res: Response) {
    try {
      const { id } = req.params;
      const products = await productsService.updateById(id, req.body);
      return baseResponse.success({
        res,
        data: products,
        message: "update products success",
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async deleteProducts(req: Request, res: Response) {
    try {
      const { id } = req.params;
      await productsService.updateById(id, {
        status: ProductsStatus.deleted,
      });
      return baseResponse.success({
        res,
        message: "delete products success",
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async getProducts(req: Request, res: Response) {
    try {
      const { page, limit, search, companyId } = req.query;
      const { userId } = req.cookies;
      let query = {
        userCreateId: userId,
        companyId,
      } as any;
      if (search) {
        query = {
          ...query,
          $or: [
            {
              code: { $regex: search, $options: "i" },
            },
            {
              name: { $regex: search, $options: "i" },
            },
          ],
        };
      }
      const products = await productsService.getByQuery({
        query,
        page,
        limit,
        sort: {
          code: 1,
          name: 1,
        },
      });
      return baseResponse.success({
        res,
        data: products,
        message: "get products success",
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }
}

export default new ProductsController();
