import mongoose, { Schema } from "mongoose";

export interface IProductsModel extends mongoose.Document {
  userCreateId: string;
  companyId: string;
  code: number;
  name: string;
  taxConfig: Array<{
    name: string;
    percent: number;
    accountNumber: number;
  }>;
  status: String;
}

const schema = new mongoose.Schema(
  {
    userCreateId: {
      type: mongoose.SchemaTypes.ObjectId,
      required: true,
    },
    companyId: {
      type: mongoose.SchemaTypes.ObjectId,
      required: true,
    },
    code: {
      type: String,
      index: "text",
    },
    name: {
      type: String,
      index: "text",
    },
    taxConfig: [
      {
        name: {
          type: String,
        },
        percent: {
          type: Number,
        },
        accountNumber: {
          type: Number,
        },
      },
    ],
    status: {
      type: String,
      default: "normal",
      enum: ["normal", "deleted"],
    },
  },
  {
    collection: "Products",
  }
);

schema.virtual("adminCreateInfo", {
  ref: "Admin",
  localField: "adminCreateId",
  foreignField: "_id",
  justOne: true,
});

schema.set("toObject", { virtuals: true });
schema.set("toJSON", { virtuals: true });

export const Products = mongoose.model<IProductsModel>("Products", schema);
