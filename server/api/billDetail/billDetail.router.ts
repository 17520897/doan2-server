import express from "express";
import { AccountType } from "../../const";
import { RequestType, Valid } from "../../middlewares/Valid";
import { validateRole } from "../../middlewares/validate";
import middleware from "../../middlewares/middleware";
import billDetailDetailController from "./billDetail.controller";
import billDetailDetailMiddleware from "./billDetail.middleware";
import CreteOrUpdateManyBillDetailDto from "./dto/createOrUpdateManyBillDetail.dto";
import GetBillDetailDto from "./dto/getBillDetail.dto";

const billDetailRouter = express.Router();

billDetailRouter.post(
  "/",
  Valid(CreteOrUpdateManyBillDetailDto, RequestType.body),
  validateRole(null, AccountType.users),
  middleware.manyAccountNumberIsSame,
  middleware.userHaveCompanies,
  middleware.companyHaveAccountNumber,
  billDetailDetailMiddleware.companyHaveBill,
  billDetailDetailController.createManyBillDetail
);

billDetailRouter.get(
  "/",
  Valid(GetBillDetailDto, RequestType.query),
  validateRole(null, AccountType.users),
  billDetailDetailController.getBillDetail
);

export default billDetailRouter;
