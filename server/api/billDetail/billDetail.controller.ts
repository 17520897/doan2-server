import { NextFunction, Request, Response } from "express";
import BaseResponse from "../../services/baseResponse";
import l from "../../common/logger";
import { HTTPCode, MoneyTypeStatus } from "../../const";
import responseService from "../../services/responseService";
import mongoose from "mongoose";
import billDetailService from "./billDetail.service";
import billService from "../bill/bill.service";

const baseResponse = new BaseResponse("billDetailController");

class billDetailController {
  async createManyBillDetail(req: Request, res: Response) {
    const session = await mongoose.startSession();
    session.startTransaction();
    try {
      const options = { session };
      const { userId } = req.cookies;
      const {
        companyId,
        billId,
        accountNumberId,
        data,
        billCreateAt,
      } = req.body;
      let totalMoney = 0;
      const createData = [];
      for (let i = 0; i < data.length; i++) {
        createData.push({
          ...data[i],
          companyId,
          billId,
          accountNumberId,
          billCreateAt,
          userCreateId: userId,
        });
        totalMoney += data[i].finalMoney;
      }
      await billDetailService.deleteMany(
        {
          billId,
        },
        options
      );
      await billService.updateById(
        billId,
        {
          money: totalMoney,
        },
        options
      );
      await billDetailService.createMany(createData, options);
      await session.commitTransaction();
      return baseResponse.success({
        res,
        message: "create bill detail success",
      });
    } catch (error) {
      await session.abortTransaction();
      return baseResponse.serverError(res, error);
    } finally {
      session.endSession();
    }
  }

  async getBillDetail(req: Request, res: Response) {
    try {
      const { userId } = req.cookies;
      const { companyId, billId } = req.query;
      const billDetail = await billDetailService.populate({
        query: {
          userCreateId: userId,
          companyId,
          billId,
        },
        sort: {
          stockId: 1,
        },
        populate: [
          {
            path: "stockInfo",
          },
        ],
      });
      return baseResponse.success({
        res,
        data: billDetail,
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }
}

export default new billDetailController();
