import BaseDbService from "../../services/baseDb";
import { BillDetail, IBillDetailModel } from "./billDetail.model";

export class BillDetailService extends BaseDbService<IBillDetailModel> {}

export default new BillDetailService(BillDetail);
