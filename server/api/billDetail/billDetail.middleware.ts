import { NextFunction, Request, Response } from "express";
import { ErrorsCode } from "../../const/errorsCode";
import BaseResponse from "../../services/baseResponse";
import billValidate from "../bill/bill.validate";

const baseResponse = new BaseResponse("BillDetailMiddleware");
class BillDetailMiddleware {
  async companyHaveBill(req: Request, res: Response, next: NextFunction) {
    try {
      const { userId } = req.cookies;
      const { billId, companyId } = req.body;
      const isCompanyHaveServiceBill = await billValidate.isCompaniesHaveBill(
        userId,
        companyId,
        billId
      );
      if (!isCompanyHaveServiceBill) {
        return baseResponse.notAccepted({
          res,
          errors: [
            {
              code: ErrorsCode.billCompaniesNotHaveBill,
              field: "billId",
            },
          ],
        });
      }
      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }
}

export default new BillDetailMiddleware();
