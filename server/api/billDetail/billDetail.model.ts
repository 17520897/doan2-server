import mongoose, { mongo, Schema } from "mongoose";

export interface IBillDetailModel extends mongoose.Document {
  userCreateId: string;
  debitAccountNumber: number;
  receiveAccountNumber: number;
  finalMoney: number;
}

const schema = new mongoose.Schema(
  {
    companyId: {
      type: mongoose.SchemaTypes.ObjectId,
      required: true,
      index: true,
    },
    userCreateId: {
      type: mongoose.SchemaTypes.ObjectId,
      required: true,
      index: true,
    },
    billId: {
      type: mongoose.SchemaTypes.ObjectId,
      required: true,
      index: true,
    },
    accountNumberId: {
      type: mongoose.SchemaTypes.ObjectId,
      required: true,
      index: true,
    },
    stockId: {
      type: mongoose.SchemaTypes.ObjectId,
      required: true,
      index: true,
    },
    receiveAccountNumber: {
      type: Number,
      index: true,
    },
    debitAccountNumber: {
      type: Number,
      index: true,
    },
    money: {
      type: Number,
    },
    quantity: {
      type: Number,
      min: 1,
      default: 1,
      required: true,
    },
    moneyType: {
      type: String,
      default: "VND",
    },
    exchangeRate: {
      type: Number,
      default: 1,
    },
    finalMoney: {
      type: Number,
      required: true,
    },
    billCreateAt: {
      type: Date,
      required: true,
    },
    userCreateAt: {
      type: Date,
      default: Date.now,
    },
  },
  {
    collection: "BillDetail",
  }
);

schema.virtual("userCreateInfo", {
  ref: "User",
  localField: "userCreateId",
  foreignField: "_id",
  justOne: true,
});

schema.virtual("companyInfo", {
  ref: "Companies",
  localField: "companyId",
  foreignField: "_id",
  justOne: true,
});

schema.virtual("stockInfo", {
  ref: "Stock",
  localField: "stockId",
  foreignField: "_id",
  justOne: true,
});

schema.set("toObject", { virtuals: true });
schema.set("toJSON", { virtuals: true });

export const BillDetail = mongoose.model<IBillDetailModel>(
  "BillDetail",
  schema
);
