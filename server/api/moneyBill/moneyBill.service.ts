import { MoneyBill, IMoneyBillModel } from "./moneyBill.model";
import { QueryFindOneAndUpdateOptions, SaveOptions } from "mongoose";
import BaseDbService from "../../services/baseDb";

export class MoneyBillService extends BaseDbService<IMoneyBillModel> {}

export default new MoneyBillService(MoneyBill);
