import { PersonStatus } from "../../const";
import accountNumberService from "../accountNumber/accountNumber.service";
import personService from "../person/person.service";

class MoneyBillValidate {
  isAccountNumberIsSame(receiveAccountNumber, debitAccountNumber) {
    return parseInt(receiveAccountNumber) === parseInt(debitAccountNumber)
      ? true
      : false;
  }

  async isPersonExisted(userId, personId) {
    const person = await personService.getOne({
      userCreateId: userId,
      _id: personId,
      status: PersonStatus.normal,
    });
    return person ? true : false;
  }

  async isExistedAccountNumber(accountNumberId, accountNumber) {
    const checkAccountNumber = await accountNumberService.getOne({
      _id: accountNumberId,
      "accountNumbers.accountNumber": parseInt(accountNumber),
    });
    if (!checkAccountNumber) {
      return false;
    }

    return true;
  }

  isMoneyGreaterThenZero(money) {
    return parseInt(money) > 0 ? true : false;
  }

  isCanConvertDateFromFile(date: string) {
    const splitDate = date.split("/");
    if (splitDate.length !== 3) return { success: false };
    const newDate = new Date(`${splitDate[2]}-${splitDate[1]}-${splitDate[0]}`);
    return isNaN(newDate.getDay()) === false
      ? { success: true, date: newDate }
      : { success: false };
  }
}

export default new MoneyBillValidate();
