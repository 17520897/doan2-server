import {
  IsString,
  IsEnum,
  IsInt,
  IsOptional,
  Equals,
  IsObject,
  IsArray,
  IsNumber,
  IsNumberString,
  Min,
  Max,
  IsMongoId,
  IsDateString,
} from "class-validator";
import { IsObjectHaveProperty } from "../../../middlewares/Valid";

export default class GetMoneyBillDto {
  @IsMongoId()
  companyId: string;

  @IsOptional()
  @IsMongoId()
  id: string;

  @IsOptional()
  @IsNumberString()
  receiveAccountNumber: string;

  @IsOptional()
  @IsNumberString()
  debitAccountNumber: string;

  @IsOptional()
  @IsDateString()
  beginBillCreateAt: string;

  @IsOptional()
  @IsDateString()
  endBillCreateAt: string;

  @IsOptional()
  @IsMongoId()
  personCreateId: string;

  @IsOptional()
  @IsMongoId()
  personReceiveId: string;

  @IsOptional()
  @IsNumberString()
  minMoney: string;

  @IsOptional()
  @IsNumberString()
  maxMoney: string;
}
