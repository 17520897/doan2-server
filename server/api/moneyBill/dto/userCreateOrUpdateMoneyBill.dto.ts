import {
  IsString,
  IsEnum,
  IsInt,
  IsOptional,
  Equals,
  IsObject,
  IsArray,
  IsNumber,
  IsNumberString,
  Min,
  Max,
  IsMongoId,
  IsDateString,
} from "class-validator";
import { IsObjectHaveProperty } from "../../../middlewares/Valid";
import { MoneyBillType } from "../../../const";

export default class UserCreateOrUpdateMoneyBillDto {
  @IsMongoId()
  companyId: string;

  @IsOptional()
  @IsMongoId()
  personCreateId: string;

  @IsMongoId()
  personReceiveId: string;

  @IsEnum(MoneyBillType, {
    message: "bill type must be: receipt or expenses",
  })
  billType: string;

  @IsNumber()
  @Min(0)
  receiveAccountNumber: Number;

  @IsNumber()
  @Min(0)
  debitAccountNumber: Number;

  @IsOptional()
  @IsString()
  address: string;

  @IsOptional()
  @IsString()
  purport: string;

  @IsOptional()
  @IsString()
  moneyType: string;

  @IsNumber()
  @Min(0)
  money: number;

  @IsNumber()
  @Min(0)
  exchangeRate: String;

  @IsNumber()
  @Min(0)
  finalMoney: Number;

  @IsDateString()
  billCreateAt: Date;

  @IsString()
  billNumber: String;
}
