import { NextFunction, Request, Response } from "express";
import BaseResponse from "../../services/baseResponse";
import l from "../../common/logger";
import { CompaniesStatus, HTTPCode } from "../../const";
import { ErrorsCode } from "../../const/errorsCode";
import responseService from "../../services/responseService";
import services from "../../services/services";
import accountNumberService from "../accountNumber/accountNumber.service";
import companiesService from "../companies/companies.service";
import moneyBillService from "./moneyBill.service";
import xlsx from "node-xlsx";
import moneyBillValidate from "./moneyBill.validate";

const baseResponse = new BaseResponse("MoneyBillMiddleware");
class MoneyBillMiddleware {
  async userHaveBill(req: Request, res: Response, next: NextFunction) {
    try {
      const { id } = req.params;
      const { userId } = req.cookies;
      const moneyBill = await moneyBillService.getOne({
        _id: id,
        userCreateId: userId,
      });
      if (!moneyBill) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.moneyBillNotFound,
              field: "id",
            },
          ],
        });
      }
      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async userGetDataFromFileMoneyBill(
    req: Request,
    res: Response,
    next: NextFunction
  ) {
    try {
      const { userId } = req.cookies;
      const { accountNumberId, companyId, billType } = req.body;
      const { moneyType, exchangeRate } = { moneyType: "VND", exchangeRate: 1 };
      const fileUrl = services.fileUploadDir(req);
      if (fileUrl === "")
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.filesOnlyAcceptXlsxFiles,
              error: "not accept this file - only support: xlsx",
              field: "file",
            },
          ],
        });
      const data = xlsx.parse(`${fileUrl}`) as any;
      if (data[0].data.length <= 1) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.moneyBillFromFileNotHaveData,
              field: "file",
              error: `Không có dữ liệu từ file`,
            },
          ],
        });
      }
      const moneyData = [];
      const xlsxData = data[0].data;
      for (let i = 1; i < xlsxData.length; i++) {
        const createName = xlsxData[i][0];
        const receiveName = xlsxData[i][1];
        const billCreateAt = moneyBillValidate.isCanConvertDateFromFile(
          xlsxData[i][2]
        );
        const receiveAccountNumber = xlsxData[i][3];
        const debitAccountNumber = xlsxData[i][4];
        const money = xlsxData[i][5];
        const address = xlsxData[i][6];
        const purport = xlsxData[i][7];
        if (!billCreateAt.success) {
          return responseService.send(res, HTTPCode.notAccept, {
            errors: [
              {
                code: ErrorsCode.moneyBillFromFileWrongDateFormat,
                field: "file",
                error: `Dòng ${i + 1} - Sai dữ liệu ngày`,
              },
            ],
          });
        }
        if (
          moneyBillValidate.isAccountNumberIsSame(
            receiveAccountNumber,
            debitAccountNumber
          )
        ) {
          return responseService.send(res, HTTPCode.notAccept, {
            errors: [
              {
                code: ErrorsCode.moneyBillFromFileNotAccountNumberIsSame,
                field: "file",
                error: `Dòng ${i + 1} - Tài khoản nợ và có không được trùng`,
              },
            ],
          });
        }
        if (
          !moneyBillValidate.isExistedAccountNumber(
            accountNumberId,
            receiveAccountNumber
          )
        ) {
          return responseService.send(res, HTTPCode.notAccept, {
            errors: [
              {
                code: ErrorsCode.moneyBillFromFileNotExistedAccountNumber,
                field: "file",
                error: `Dòng ${i + 1} - Tài khoản có không tồn tại`,
              },
            ],
          });
        }
        if (
          !moneyBillValidate.isExistedAccountNumber(
            accountNumberId,
            debitAccountNumber
          )
        ) {
          return responseService.send(res, HTTPCode.notAccept, {
            errors: [
              {
                code: ErrorsCode.moneyBillFromFileNotExistedAccountNumber,
                field: "file",
                error: `Dòng ${i + 1} - Tài khoản nợ không tồn tại`,
              },
            ],
          });
        }
        if (!moneyBillValidate.isMoneyGreaterThenZero(money)) {
          return responseService.send(res, HTTPCode.notAccept, {
            errors: [
              {
                code: ErrorsCode.moneyBillFromFileMoneyMustGreaterThanZero,
                field: "file",
                error: `Dòng ${i + 1} - Số tiền phải lớn hơn 0`,
              },
            ],
          });
        }
        moneyData.push({
          userCreateId: userId,
          companyId,
          billType,
          accountNumberId,
          receiveAccountNumber: parseInt(receiveAccountNumber),
          debitAccountNumber: parseInt(debitAccountNumber),
          createName,
          receiveName,
          address,
          purport,
          money: parseInt(money),
          moneyType,
          exchangeRate,
          finalMoney: money,
          billCreateAt: billCreateAt.date,
        });
      }
      req.body.moneyData = moneyData;
      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }
}

export default new MoneyBillMiddleware();
