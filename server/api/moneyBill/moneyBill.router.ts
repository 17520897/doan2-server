import express from "express";
import { validateRole } from "../../middlewares/validate";
import { AccountType } from "../../const";
import MoneyBillMiddleware from "./moneyBill.middleware";
import moneyBillController from "./moneyBill.controller";
import { RequestType, Valid } from "../../middlewares/Valid";
import UserCreateOrUpdateMoneyBillDto from "./dto/userCreateOrUpdateMoneyBill.dto";
import GetMoneyBillDto from "./dto/getMoneyBill.dto";
import IdParamsRequestDto from "../../const/IdParamsRequest.dto";
import middleware from "../../middlewares/middleware";

const moneyBillRouter = express.Router();

moneyBillRouter.post(
  "/",
  Valid(UserCreateOrUpdateMoneyBillDto, RequestType.body),
  validateRole(null, AccountType.users),
  middleware.personIsSame,
  middleware.accountNumberIsSame,
  middleware.userHaveCompanies,
  middleware.existedAccountNumber,
  middleware.personCreateOrReceiveExisted,
  moneyBillController.userCreateMoneyBill
);

moneyBillRouter.get(
  "/",
  Valid(GetMoneyBillDto, RequestType.query),
  validateRole(null, AccountType.users),
  moneyBillController.userSearchMoneyBill
);

moneyBillRouter.put(
  "/:id",
  Valid(IdParamsRequestDto, RequestType.params),
  Valid(UserCreateOrUpdateMoneyBillDto, RequestType.body),
  validateRole(null, AccountType.users),
  MoneyBillMiddleware.userHaveBill,
  middleware.personIsSame,
  middleware.accountNumberIsSame,
  middleware.userHaveCompanies,
  middleware.existedAccountNumber,
  middleware.personCreateOrReceiveExisted,
  moneyBillController.userUpdateMoneyBill
);

moneyBillRouter.delete(
  "/:id",
  Valid(IdParamsRequestDto, RequestType.params),
  validateRole(null, AccountType.users),
  MoneyBillMiddleware.userHaveBill,
  moneyBillController.userDeleteMoneyBill
);
// moneyBillRouter.post(
//   "/info/file",
//   validateRole(null, AccountType.users),
//   uploadFile.uploadFile(["xlsx"], false, "HDTM").single("file"),
//   MoneyBillMiddleware.userGetDataFromFileMoneyBill,
//   moneyBillController.userGetDataFromFileMoneyBill
// );

// moneyBillRouter.post(
//   "/file",
//   validateRole(null, AccountType.users),
//   uploadFile.uploadFile(["xlsx"], false, "HDTM").single("file"),
//   MoneyBillMiddleware.userGetDataFromFileMoneyBill,
//   moneyBillController.userCreateFromFileMoneyBill
// );

export default moneyBillRouter;
