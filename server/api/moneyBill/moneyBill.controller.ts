import { NextFunction, Request, Response } from "express";
import BaseResponse from "../../services/baseResponse";
import l from "../../common/logger";
import { HTTPCode } from "../../const";
import responseService from "../../services/responseService";
import services from "../../services/services";
import moneyBillService from "./moneyBill.service";
import xlsx from "node-xlsx";
import personService from "../person/person.service";
const baseResponse = new BaseResponse("MoneyBillController");
class MoneyBillController {
  async userCreateMoneyBill(req: Request, res: Response) {
    try {
      const { userId } = req.cookies;
      const moneyBill = await moneyBillService.create({
        ...req.body,
        userCreateId: userId,
      });
      return responseService.send(res, HTTPCode.success, {
        data: moneyBill,
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async userUpdateMoneyBill(req: Request, res: Response) {
    try {
      const { id } = req.params;
      const { userId } = req.cookies;
      const moneyBill = await moneyBillService.updateById(id, {
        ...req.body,
        userCreateId: userId,
        userCreateAt: Date.now(),
      });
      return responseService.send(res, HTTPCode.success, {
        message: "update money bill success",
        data: moneyBill,
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async userSearchMoneyBill(req: Request, res: Response) {
    try {
      const { userId } = req.cookies;
      const {
        page,
        limit,
        receiveAccountNumber,
        debitAccountNumber,
        beginBillCreateAt,
        endBillCreateAt,
        personCreateId,
        personReceiveId,
        minMoney,
        maxMoney,
        sortType = -1,
        sortProperties,
        companyId,
        billType,
      } = req.query as any;
      let query = {
        userCreateId: userId,
        companyId,
      } as any;
      let sort = {} as any;
      if (sortProperties) {
        sort[sortProperties] = parseInt(sortType);
      }
      console.log("sort: ", sort);
      if (receiveAccountNumber)
        query["receiveAccountNumber"] = parseInt(receiveAccountNumber);
      if (debitAccountNumber)
        query["debitAccountNumber"] = parseInt(debitAccountNumber);
      if (beginBillCreateAt && endBillCreateAt)
        query = {
          ...query,
          $and: [
            {
              billCreateAt: { $gte: beginBillCreateAt },
            },
            {
              billCreateAt: { $lte: endBillCreateAt },
            },
          ],
        };
      if (personCreateId) {
        query["personCreateId"] = personCreateId;
      }
      if (personReceiveId) {
        query["personReceiveId"] = personReceiveId;
      }
      if (minMoney || maxMoney) {
        query = {
          ...query,
          $or: [
            {
              $and: [
                {
                  money: { $gte: minMoney ? minMoney : 0 },
                },
                {
                  money: { $lte: maxMoney ? maxMoney : Infinity },
                },
              ],
            },
            {
              $and: [
                {
                  finalMoney: { $gte: minMoney ? minMoney : 0 },
                },
                {
                  finalMoney: { $lte: maxMoney ? maxMoney : Infinity },
                },
              ],
            },
          ],
        };
      }
      if (billType) {
        query["billType"] = billType;
      }
      const moneyBill = await moneyBillService.populate({
        query,
        page,
        limit,
        sort: {
          ...sort,
        },
        populate: [
          {
            path: "companyInfo",
          },
          {
            path: "personCreateInfo",
          },
          {
            path: "personReceiveInfo",
          },
        ],
      });
      return responseService.send(res, HTTPCode.success, {
        data: moneyBill,
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async userGetDataFromFileMoneyBill(req: Request, res: Response) {
    try {
      const { moneyData } = req.body;
      return responseService.send(res, HTTPCode.success, {
        data: moneyData,
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async userCreateFromFileMoneyBill(req: Request, res: Response) {
    try {
      const { moneyData } = req.body;
      await moneyBillService.createMany(moneyData);
      return responseService.send(res, HTTPCode.success, {
        data: {
          count: moneyData.length,
        },
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async userDeleteMoneyBill(req: Request, res: Response) {
    try {
      const { id } = req.params;
      await moneyBillService.deleteById(id);
      return responseService.send(res, HTTPCode.success, {
        message: "delete money bill success",
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }
}

export default new MoneyBillController();
