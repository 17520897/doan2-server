import mongoose, { Schema } from "mongoose";

export interface IMoneyBillModel extends mongoose.Document {
  companyId: string;
  userCreateId: string;
  billType: string;
  receiveAccountNumber: number;
  debitAccountNumber: number;
  personCreateId: string;
  personReceiveId: string;
  address: string;
  purport: string;
  money: number;
  moneyType: string;
  exchangeRate: number;
  finalMoney: number;
  billCreatAt: Date;
  userCreatAt: Date;
  accountNumberId: String;
  billNumber: String;
}

const schema = new mongoose.Schema(
  {
    companyId: {
      type: mongoose.SchemaTypes.ObjectId,
      required: true,
    },
    userCreateId: {
      type: mongoose.SchemaTypes.ObjectId,
      required: true,
    },
    billType: {
      type: String,
      enum: ["receipt", "expenses"],
      required: true,
    },
    billNumber: {
      type: String,
    },
    accountNumberId: {
      type: mongoose.SchemaTypes.ObjectId,
    },
    receiveAccountNumber: {
      type: Number,
      index: true,
    },
    debitAccountNumber: {
      type: Number,
      index: true,
    },
    personCreateId: {
      type: mongoose.SchemaTypes.ObjectId,
    },
    personReceiveId: {
      type: mongoose.SchemaTypes.ObjectId,
    },
    address: {
      type: String,
    },
    purport: {
      type: String,
      index: "text",
    },
    billCreateAt: {
      type: Date,
    },
    money: {
      type: Number,
    },
    moneyType: {
      type: String,
      default: "VND",
    },
    exchangeRate: {
      type: Number,
      default: 1,
    },
    finalMoney: {
      type: Number,
      required: true,
    },
    billCreatAt: {
      type: Date,
    },
    userCreateAt: {
      type: Date,
      default: Date.now,
    },
  },
  {
    collection: "MoneyBill",
  }
);

schema.virtual("userInfo", {
  ref: "Users",
  localField: "userCreateId",
  foreignField: "_id",
  justOne: true,
});

schema.virtual("companyInfo", {
  ref: "Companies",
  localField: "companyId",
  foreignField: "_id",
  justOne: true,
});

schema.virtual("personCreateInfo", {
  ref: "Person",
  localField: "personCreateId",
  foreignField: "_id",
  justOne: true,
});

schema.virtual("personReceiveInfo", {
  ref: "Person",
  localField: "personReceiveId",
  foreignField: "_id",
  justOne: true,
});

schema.set("toObject", { virtuals: true });
schema.set("toJSON", { virtuals: true });
export const MoneyBill = mongoose.model<IMoneyBillModel>("MoneyBill", schema);
