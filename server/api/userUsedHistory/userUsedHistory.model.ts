import mongoose, { Schema } from "mongoose";

export interface IUserUsedHistoryModel extends mongoose.Document {
  userId: string;
  totalCompany: number;
  totalBill: number;
}

const schema = new mongoose.Schema(
  {
    userId: {
      type: mongoose.SchemaTypes.ObjectId,
    },
    companies: [],
  },
  {
    collection: "UserUsedHistory",
  }
);

schema.virtual("userInfo", {
  ref: "Users",
  localField: "userId",
  foreignField: "_id",
  justOne: true,
});

schema.set("toObject", { virtuals: true });
schema.set("toJSON", { virtuals: true });
export const UserUsedHistory = mongoose.model<IUserUsedHistoryModel>(
  "UserUsedHistory",
  schema
);
