import {
  UserUsedHistory,
  IUserUsedHistoryModel,
} from "./userUsedHistory.model";
import BaseDbService from "../../services/baseDb";

export class UserUsedHistoryService extends BaseDbService<
  IUserUsedHistoryModel
> {}

export default new UserUsedHistoryService(UserUsedHistory);
