import express from "express";
import { AccountType } from "../../const";
import { validateRole } from "../../middlewares/validate";
import userUsedHistoryController from "./userUsedHistory.controller";
const userUsedHistoryRouter = express.Router();

userUsedHistoryRouter.get(
  "/",
  validateRole(null, AccountType.users),
  userUsedHistoryController.userGetUserUsedHistory
);

userUsedHistoryRouter.put(
  "/",
  validateRole(null, AccountType.users),
  userUsedHistoryController.updateUserUsedHistory
);

export default userUsedHistoryRouter;
