import { Request, Response } from "express";
import BaseResponse from "../../services/baseResponse";
import { HTTPCode } from "../../const";
import responseService from "../../services/responseService";
import userUsedHistoryService from "./userUsedHistory.service";

const baseResponse = new BaseResponse("UserUsedHistoryController");
class UserUsedHistoryController {
  async userGetUserUsedHistory(req: Request, res: Response) {
    try {
      const { userId } = req.cookies;
      let userUsedHistory = await userUsedHistoryService.getOne({
        userId: userId,
      });
      if (!userUsedHistory) {
        userUsedHistory = await userUsedHistoryService.create({
          userId: userId,
        });
      }
      return responseService.send(res, HTTPCode.success, {
        data: userUsedHistory,
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async updateUserUsedHistory(req: Request, res: Response) {
    try {
      const { userId } = req.cookies;
      const { companies } = req.body;
      const userUsedHistory = await userUsedHistoryService.updateOne(
        {
          userId: userId,
        },
        {
          companies,
        },
        {
          upsert: true,
          new: true,
        }
      );
      return responseService.send(res, HTTPCode.success, {
        data: userUsedHistory,
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }
}

export default new UserUsedHistoryController();
