import mongoose, { Schema } from "mongoose";

export interface IUserTypingHistoryModel extends mongoose.Document {
  userCreateId: string;
}

const schema = new mongoose.Schema(
  {
    userCreateId: {
      type: mongoose.SchemaTypes.ObjectId,
      required: true,
    },
    address: [],
    money: [],
  },
  {
    collection: "UserTypingHistory",
  }
);

schema.virtual("userCreateInfo", {
  ref: "User",
  localField: "userCreateId",
  foreignField: "_id",
  justOne: true,
});

schema.set("toObject", { virtuals: true });
schema.set("toJSON", { virtuals: true });

export const UserTypingHistory = mongoose.model<IUserTypingHistoryModel>(
  "UserTypingHistory",
  schema
);
