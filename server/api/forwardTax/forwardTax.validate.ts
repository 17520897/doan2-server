import forwardTaxService from "./forwardTax.service";

class ForwardTaxService {
  async isUserHaveForwardTax(userId, forwardTaxId) {
    const serviceBill = await forwardTaxService.getOne({
      userCreateId: userId,
      _id: forwardTaxId,
    });
    return serviceBill ? true : false;
  }
}

export default new ForwardTaxService();
