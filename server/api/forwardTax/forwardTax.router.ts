import express from "express";
import { AccountType } from "../../const";
import { RequestType, Valid } from "../../middlewares/Valid";
import { validateRole } from "../../middlewares/validate";
import IdParamsRequestDto from "../../const/IdParamsRequest.dto";
import UserCreateOrUpdateForwardTaxDto from "./dto/createOrUpdateForwardTax.dto";
import forwardTaxMiddleware from "./forwardTax.middleware";
import middleware from "../../middlewares/middleware";
import forwardTaxController from "./forwardTax.controller";
import UserGetForwardTaxDto from "./dto/getForwardTax.dto";

const forwardTaxRouter = express.Router();

forwardTaxRouter.post(
  "/",
  Valid(UserCreateOrUpdateForwardTaxDto, RequestType.body),
  validateRole(null, AccountType.users),
  middleware.accountNumberIsSame,
  middleware.companyHaveAccountNumber,
  middleware.existedAccountNumber,
  forwardTaxController.createForwardTax
);

forwardTaxRouter.put(
  "/:id",
  Valid(IdParamsRequestDto, RequestType.params),
  Valid(UserCreateOrUpdateForwardTaxDto, RequestType.body),
  validateRole(null, AccountType.users),
  middleware.accountNumberIsSame,
  middleware.companyHaveAccountNumber,
  middleware.existedAccountNumber,
  forwardTaxMiddleware.userHaveForwardTax,
  forwardTaxController.updateForwardTax
);

forwardTaxRouter.delete(
  "/:id",
  Valid(IdParamsRequestDto, RequestType.params),
  validateRole(null, AccountType.users),
  forwardTaxMiddleware.userHaveForwardTax,
  forwardTaxController.deleteForwardTax
);

forwardTaxRouter.get(
  "/",
  Valid(UserGetForwardTaxDto, RequestType.query),
  validateRole(null, AccountType.users),
  forwardTaxController.searchForwardTaxBill
);

export default forwardTaxRouter;
