import {
  IsString,
  IsEnum,
  IsInt,
  IsOptional,
  Equals,
  IsObject,
  IsArray,
  IsNumber,
  IsNumberString,
  Min,
  Max,
  IsMongoId,
  IsDateString,
} from "class-validator";
import { IsObjectHaveProperty } from "../../../middlewares/Valid";

export default class UserGetForwardTaxDto {
  @IsMongoId()
  companyId: string;
}
