import {
  IsString,
  IsEnum,
  IsInt,
  IsOptional,
  Equals,
  IsObject,
  IsArray,
  IsNumber,
  IsNumberString,
  Min,
  Max,
  IsMongoId,
  IsDateString,
} from "class-validator";
import { IsObjectHaveProperty } from "../../../middlewares/Valid";

export default class UserCreateOrUpdateForwardTaxDto {
  @IsMongoId()
  companyId: string;

  @IsDateString()
  billCreateAt: Date;

  @IsMongoId()
  accountNumberId: String;

  @IsNumber()
  @Min(0)
  receiveAccountNumber: Number;

  @IsNumber()
  @Min(0)
  debitAccountNumber: Number;

  @IsNumber()
  finalMoney: number;

  @IsOptional()
  @IsString()
  note: String;
}
