import { Request, Response } from "express";
import BaseResponse from "../../services/baseResponse";
import forwardTaxService from "./forwardTax.service";

const baseResponse = new BaseResponse("ForwardTaxController");
class ForwardTaxController {
  async createForwardTax(req: Request, res: Response) {
    try {
      const { userId } = req.cookies;
      const forwardTax = await forwardTaxService.create({
        ...req.body,
        userCreateId: userId,
      });
      return baseResponse.success({
        res,
        data: forwardTax,
        message: "create forward tax success",
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async updateForwardTax(req: Request, res: Response) {
    try {
      const { id } = req.params;
      const forwardTax = await forwardTaxService.updateById(id, {
        ...req.body,
        userCreateAt: Date.now(),
      });
      return baseResponse.success({
        res,
        data: forwardTax,
        message: "update forward tax success",
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async deleteForwardTax(req: Request, res: Response) {
    try {
      const { id } = req.params;
      await forwardTaxService.deleteById(id);
      return baseResponse.success({
        res,
        message: "delete forward tax success",
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async searchForwardTaxBill(req: Request, res: Response) {
    try {
      const { userId } = req.cookies;
      const {
        page,
        limit,
        companyId,
        beginBillCreateAt,
        endBillCreateAt,
        sortType = -1,
        sortProperties,
        receiveAccountNumber,
        debitAccountNumber,
      } = req.query as any;
      let query = {
        userCreateId: userId,
        companyId,
      } as any;
      let sort = {} as any;

      if (sortProperties) {
        sort[sortProperties] = parseInt(sortType);
      }

      if (receiveAccountNumber)
        query["receiveAccountNumber"] = parseInt(receiveAccountNumber);
      if (debitAccountNumber)
        query["debitAccountNumber"] = parseInt(debitAccountNumber);

      if (beginBillCreateAt && endBillCreateAt)
        query = {
          ...query,
          $and: [
            {
              billCreateAt: { $gte: beginBillCreateAt },
            },
            {
              billCreateAt: { $lte: endBillCreateAt },
            },
          ],
        };
      const forwardTax = await forwardTaxService.populate({
        query,
        page,
        limit,
        sort: {
          ...sort,
        },
        populate: [
          {
            path: "personCreateInfo",
          },
          {
            path: "personReceiveInfo",
          },
        ],
      });
      return baseResponse.success({
        res,
        data: forwardTax,
        message: "get forward tax success",
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }
}

export default new ForwardTaxController();
