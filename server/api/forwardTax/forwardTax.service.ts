import { ForwardTax, IForwardTaxModel } from "./forwardTax.model";
import BaseDbService from "../../services/baseDb";

export class ForwardTaxService extends BaseDbService<IForwardTaxModel> {}

export default new ForwardTaxService(ForwardTax);
