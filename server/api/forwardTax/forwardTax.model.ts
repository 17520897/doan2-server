import mongoose, { mongo, Schema } from "mongoose";

export interface IForwardTaxModel extends mongoose.Document {
  userCreateId: string;
  debitAccountNumber: number;
  receiveAccountNumber: number;
  finalMoney: number;
}

const schema = new mongoose.Schema(
  {
    companyId: {
      type: mongoose.SchemaTypes.ObjectId,
      required: true,
      index: true,
    },
    userCreateId: {
      type: mongoose.SchemaTypes.ObjectId,
      required: true,
      index: true,
    },
    accountNumberId: {
      type: mongoose.SchemaTypes.ObjectId,
      index: true,
    },
    receiveAccountNumber: {
      type: Number,
      index: true,
    },
    debitAccountNumber: {
      type: Number,
      index: true,
    },
    note: {
      type: String,
    },
    finalMoney: {
      type: Number,
      required: true,
    },
    billCreateAt: {
      type: Date,
      required: true,
    },
    userCreateAt: {
      type: Date,
      default: Date.now,
    },
  },
  {
    collection: "ForwardTax",
  }
);

schema.virtual("userCreateInfo", {
  ref: "User",
  localField: "userCreateId",
  foreignField: "_id",
  justOne: true,
});

schema.virtual("companyInfo", {
  ref: "Companies",
  localField: "companyId",
  foreignField: "_id",
  justOne: true,
});

schema.virtual("stockInfo", {
  ref: "Stock",
  localField: "stockId",
  foreignField: "_id",
  justOne: true,
});

schema.set("toObject", { virtuals: true });
schema.set("toJSON", { virtuals: true });

export const ForwardTax = mongoose.model<IForwardTaxModel>(
  "ForwardTax",
  schema
);
