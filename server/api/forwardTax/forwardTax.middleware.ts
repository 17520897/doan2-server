import { NextFunction, Request, Response } from "express";
import { ErrorsCode } from "../../const/errorsCode";
import BaseResponse from "../../services/baseResponse";
import forwardTaxValidate from "./forwardTax.validate";

const baseResponse = new BaseResponse("ForwardTaxMiddleware");
class ForwardTaxMiddleware {
  async userHaveForwardTax(req: Request, res: Response, next: NextFunction) {
    try {
      const { id } = req.params;
      const { userId } = req.cookies;
      const isUserHaveForwardTax = await forwardTaxValidate.isUserHaveForwardTax(
        userId,
        id
      );
      if (!isUserHaveForwardTax) {
        return baseResponse.notAccepted({
          res,
          errors: [
            {
              code: ErrorsCode.forwardTaxNotFound,
              field: "id",
            },
          ],
        });
      }
      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }
}

export default new ForwardTaxMiddleware();
