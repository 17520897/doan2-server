import mongoose, { mongo } from "mongoose";

export interface IVerifyModel extends mongoose.Document {
  phone: string;
  code: string;
  createAt: Date;
  type: string;
  password: string;
}

const schema = new mongoose.Schema(
  {
    code: {
      type: String,
      required: true,
    },
    email: {
      type: String,
      required: true,
    },
    phone: {
      type: String,
    },
    name: {
      type: String,
    },
    createAt: {
      type: Date,
      default: Date.now,
      index: {
        expires: "5m",
      },
    },
    accountType: {
      type: String,
      enum: ["user", "shop", "inspector", "shopEmployee"],
    },
    type: {
      type: String,
      enum: ["register", "forgotPassword"],
    },
  },
  {
    collection: "Verify",
  }
);

export const Verify = mongoose.model<IVerifyModel>("Verify", schema);
