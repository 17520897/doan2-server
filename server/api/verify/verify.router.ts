import express from "express";
import { Valid, RequestType } from "../../middlewares/Valid";
import VerifyRegisterDto from "./dto/verifyRegister.dto";
import VerifyForgotPasswordDto from "./dto/verifyForgotPassword.dto";
import VerifyGetRegisterDto from "./dto/verifyGetRegister.dto";
import verifyController from "./verify.controller";
import verifyMiddleware from "./verify.middleware";

const verifyRouter = express.Router();

verifyRouter.post(
  "/register",
  Valid(VerifyRegisterDto, RequestType.body),
  verifyMiddleware.verifyRegister,
  verifyController.verifyRegister
);

verifyRouter.get(
  "/register",
  Valid(VerifyGetRegisterDto, RequestType.query),
  verifyMiddleware.getRegisterVerifyCode,
  verifyController.getRegisterVerifyCode
);

verifyRouter.get(
  "/forgotPassword",
  verifyMiddleware.getForgotPasswordCode,
  verifyController.getForgotPasswordVerifyCode
);

verifyRouter.post(
  "/forgotPassword",
  Valid(VerifyForgotPasswordDto, RequestType.body),
  verifyMiddleware.resetPassword,
  verifyController.resetPassword
);

export default verifyRouter;
