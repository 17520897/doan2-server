import { Request, Response } from "express";
import ResponseService from "../../services/responseService";
import VerifyDto from "./dto/verifyRegister.dto";
import {
  VerifyType,
  SMSType,
  VerifyAccountType,
  HTTPCode,
} from "../../const/index";
import UsersService from "../users/users.service";
import VerifyService from "./verify.service";
import randomString from "randomstring";
import l from "../../common/logger";
import bcrypt from "bcrypt";
import sendSMS from "../../services/sendSms";
import { ErrorsCode } from "../../const/errorsCode";
import BaseResponse from "../../services/baseResponse";
import mongoose from "mongoose";
import services from "../../services/services";
import userUsedHistoryService from "../userUsedHistory/userUsedHistory.service";
const baseResponse = new BaseResponse("VerifyController");
export class VerifyController {
  async verifyRegister(req: Request, res: Response) {
    const session = await mongoose.startSession();
    session.startTransaction();
    try {
      const options = { session };
      const { email, password, name } = req.body;
      const hash = await services.hashPassword(
        password,
        parseInt(process.env.HASH_PASSWORD_SALT)
      );
      const user = await UsersService.create(
        {
          email,
          password: hash,
          name,
        },
        options
      );
      await VerifyService.deleteMany(
        {
          email,
          type: VerifyType.register,
          accountType: VerifyAccountType.user,
        },
        options
      );
      // await userUsedHistoryService.create(
      //   {
      //     userId: user._id,
      //   },
      //   options
      // );
      user.password = undefined;
      await session.commitTransaction();
      session.endSession();
      return ResponseService.send(res, HTTPCode.success, {
        message: "verify new user success",
        data: user,
      });
    } catch (error) {
      session.abortTransaction();
      return baseResponse.serverError(res, error);
    } finally {
      session.endSession();
    }
  }
  async getRegisterVerifyCode(req: Request, res: Response) {
    try {
      const { phone } = req.body;
      const code = randomString.generate({
        length: 6,
        charset: "numeric",
      });
      if (process.env.NODE_ENV !== "dev") {
        const sms = await sendSMS.send(phone, code, SMSType.register);
        if (sms.errors) {
          return ResponseService.send(res, HTTPCode.serverError, {
            errors: [
              ...sms.errors,
              {
                code: ErrorsCode.smsCanNotSend,
                error: "can not send sms",
              },
            ],
          });
        }
      }
      await VerifyService.create({
        code,
        phone: phone,
        type: VerifyType.register,
        accountType: VerifyAccountType.user,
      });
      return ResponseService.send(res, HTTPCode.success, {
        message: "get new verify code success",
        data: [
          {
            phone: req.query.phone,
          },
        ],
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async getForgotPasswordVerifyCode(req: Request, res: Response) {
    try {
      const { userId, phone } = req.body;
      const code = randomString.generate({ length: 6, charset: "numeric" });
      const sms = await sendSMS.send(phone, code, SMSType.forgotPassword);
      if (sms.errors) {
        return ResponseService.send(res, HTTPCode.serverError, {
          errors: [
            ...sms.errors,
            {
              code: ErrorsCode.smsCanNotSend,
              error: "can not send sms",
            },
          ],
        });
      }
      await VerifyService.create({
        userId,
        code,
        phone,
        type: VerifyType.forgotPassword,
        accountType: VerifyAccountType.user,
      });
      return ResponseService.send(res, HTTPCode.success, {
        message: "get forgot password verify code success",
        data: [
          {
            phone,
          },
        ],
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async resetPassword(req: Request, res: Response) {
    const session = await mongoose.startSession();
    session.startTransaction();
    try {
      const options = { session };
      const { password, phone } = req.body;
      const salt = await bcrypt.genSalt(10);
      const hash = await bcrypt.hash(password, salt);
      const user = await UsersService.updateOne(
        { phone },
        { password: hash },
        options
      );
      await VerifyService.deleteMany(
        {
          phone,
          accountType: VerifyAccountType.user,
        },
        options
      );
      await session.commitTransaction();
      session.endSession();
      return ResponseService.send(res, HTTPCode.success, {
        message: "reset password success",
        data: {
          userId: user._id,
          phone: user.phone,
        },
      });
    } catch (error) {
      session.abortTransaction();
      return baseResponse.serverError(res, error);
    } finally {
      session.endSession();
    }
  }
}
export default new VerifyController();
