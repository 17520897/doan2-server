import { Request, Response, NextFunction } from "express";
import ResponseService from "../../services/responseService";
import { VerifyType, VerifyAccountType, HTTPCode } from "../../const/index";
import usersService from "../users/users.service";
import VerifyService from "./verify.service";
import l from "../../common/logger";
import { ErrorsCode } from "../../const/errorsCode";
import BaseResponse from "../../services/baseResponse";

function allowSendCode(createAt: Date) {
  const now = Date.now();
  const createTime = createAt.getTime();
  return now - createTime > parseInt(process.env.TIME_WAIT_SEND_VERIFY_CODE)
    ? true
    : false;
}
const baseResponse = new BaseResponse("VerifyMiddleware");
class VerifyMiddleware {
  async verifyRegister(req: Request, res: Response, next: NextFunction) {
    try {
      const { email, code } = req.body;
      const verify = await VerifyService.getOne({
        email,
        code,
        type: VerifyType.register,
        accountType: VerifyAccountType.user,
      });
      if (!verify)
        return ResponseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.verifyCodeNotFound,
              field: "code",
              error: "verify not found",
            },
          ],
        });
      const users = await usersService.getOne({ email: req.body.email });
      if (users)
        return ResponseService.send(res, HTTPCode.notAccept, {
          message: "user has been registered",
          errors: [
            {
              code: ErrorsCode.usersEmailRegistered,
              field: "email",
              error: "email number has been registered",
            },
          ],
        });
      req.body.verifyId = verify._id;
      next();
    } catch (error) {
      return ResponseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    }
  }

  async getRegisterVerifyCode(req: Request, res: Response, next: NextFunction) {
    try {
      const { email } = req.query;
      const user = await usersService.getOne({ email });
      if (user)
        return ResponseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.verifyEmailHasBeenRegistered,
              field: "phone",
            },
          ],
        });
      const verify = await VerifyService.getSort(
        { email, type: VerifyType.register },
        { createAt: -1 }
      );
      if (verify.length > 0 && !allowSendCode(verify[0].createAt))
        return ResponseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.verifyCodeHasSent,
              error: "code has sent, can get code after 1 min",
              field: "createAt",
            },
          ],
        });
      req.body.email = email;
      next();
    } catch (error) {
      return ResponseService.send(res, HTTPCode.serverError, {
        errors: [{ code: ErrorsCode.serverError, error: error.message }],
      });
    }
  }

  async getForgotPasswordCode(req: Request, res: Response, next: NextFunction) {
    try {
      const { email } = req.query;
      const user = await usersService.getOne({ email });
      if (!user)
        return ResponseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.verifyCantForgotPassword,
              error: "user not found",
              field: "phone",
            },
          ],
        });
      const verify = await VerifyService.getSort(
        {
          email,
          type: VerifyType.forgotPassword,
          accountType: VerifyAccountType.user,
        },
        { createAt: -1 }
      );
      if (verify.length > 0 && !allowSendCode(verify[0].createAt))
        return ResponseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.verifyCodeHasSent,
              error: "code has sent, can get code after 1 min",
              field: "createAt",
            },
          ],
        });
      req.body.userId = user._id;
      req.body.email = user.email;
      next();
    } catch (error) {
      return ResponseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    }
  }

  async resetPassword(req: Request, res: Response, next: NextFunction) {
    try {
      const { email, code } = req.body;
      const verify = await VerifyService.getOne({
        email,
        code,
        type: VerifyType.forgotPassword,
        accountType: VerifyAccountType.user,
      });
      if (!verify)
        return ResponseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.verifyCodeNotFound,
              error: "verify code not found",
              field: "code",
            },
          ],
        });
      next();
    } catch (error) {
      l.info(`middleware reset password: ${error.message}`);
      return ResponseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    }
  }
}

export default new VerifyMiddleware();
