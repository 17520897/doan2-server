import { Verify, IVerifyModel } from "./verify.model";
import BaseDbService from "../../services/baseDb";

export class VerifyService extends BaseDbService<IVerifyModel> {}

export default new VerifyService(Verify);
