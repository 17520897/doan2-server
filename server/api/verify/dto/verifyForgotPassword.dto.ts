import {
  IsString,
  MaxLength,
  IsEnum,
  IsObject,
  IsDate,
  IsEmail,
  IsNumber,
  IsOptional,
  MinLength,
  IsMongoId,
  Min,
  Length,
} from "class-validator";

export default class VerifyForgotPasswordDto {
  @IsOptional()
  @IsMongoId()
  userId: String;

  @IsString()
  @Length(6)
  code: String;

  @IsEmail()
  email: String;

  @IsString()
  @MinLength(8)
  password: String;
}
