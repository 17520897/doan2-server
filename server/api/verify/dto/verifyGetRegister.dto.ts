import {
  IsString,
  MaxLength,
  IsEnum,
  IsObject,
  IsDate,
  IsEmail,
  IsNumber,
  IsOptional,
  MinLength,
  IsMongoId,
  Min,
  Length,
  Equals,
  IsNumberString,
} from "class-validator";

export enum VerifyType {
  register = "register",
  forgotPassword = "forgotPassword",
}

export default class VerifyGetRegisterDto {
  @IsEmail()
  email: string;
}
