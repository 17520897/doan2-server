import {
  IsString,
  MaxLength,
  IsEnum,
  IsObject,
  IsDate,
  IsEmail,
  IsNumber,
  IsOptional,
  MinLength,
  IsMongoId,
  Min,
  Length,
  Equals,
} from "class-validator";

export enum VerifyType {
  register = "register",
  forgotPassword = "forgotPassword",
}

export default class VerifyRegisterDto {
  @Equals(undefined)
  userId: String;

  @IsString()
  @Length(6)
  code: String;

  @IsEmail()
  email: String;

  @IsString()
  name: String;

  @IsString()
  @MinLength(8)
  password: String;
}
