import { Person, IPersonModel } from "./person.model";
import { QueryFindOneAndUpdateOptions, SaveOptions } from "mongoose";
import BaseDbService from "../../services/baseDb";

export class PersonService extends BaseDbService<IPersonModel> {}

export default new PersonService(Person);
