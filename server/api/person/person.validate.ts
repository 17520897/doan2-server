import { CompaniesStatus, PersonStatus } from "../../const";
import companiesService from "../companies/companies.service";
import personService from "./person.service";

class PersonValidate {
  async isCodeExisted(userId, code, companyId, checkId = false, id = "") {
    const person = checkId
      ? await personService.getOne({
          code,
          userCreateId: userId,
          companyId,
          _id: { $ne: id },
          status: PersonStatus.normal,
        })
      : await personService.getOne({
          code,
          userCreateId: userId,
          companyId,
          status: PersonStatus.normal,
        });
    return person ? true : false;
  }

  async isUserHavePerson(userId, id) {
    const person = await personService.getOne({
      _id: id,
      userCreateId: userId,
      status: PersonStatus.normal,
    });
    return person ? true : false;
  }

  async isUserHaveCompany(userId, companyId) {
    const company = await companiesService.getOne({
      _id: companyId,
      userCreateId: userId,
      status: CompaniesStatus.normal,
    });
    return company ? true : false;
  }

  async isPersonExisted(userId, personId) {
    const person = await personService.getOne({
      userCreateId: userId,
      _id: personId,
      status: PersonStatus.normal,
    });
    return person ? true : false;
  }
}

export default new PersonValidate();
