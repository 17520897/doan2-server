import express from "express";
import { AccountType } from "../../const";
import { RequestType, Valid } from "../../middlewares/Valid";
import { validateRole } from "../../middlewares/validate";
import CreatePersonDto from "./dto/createPerson.dto";
import UpdatePersonDto from "./dto/updatePerson.dto";
import personController from "./person.controller";
import personMiddleware from "./person.middleware";
import GetPersonDto from "./dto/getPerson.dto";
import CountPersonDto from "./dto/countPerson.dto";
import IdParamsRequestDto from "../../const/IdParamsRequest.dto";
import CheckCodeExistedDto from "./dto/checkCodeExisted.dto";
import middleware from "../../middlewares/middleware";

const personRouter = express.Router();

personRouter.post(
  "/",
  Valid(CreatePersonDto, RequestType.body),
  validateRole(null, AccountType.users),
  middleware.userHaveCompanies,
  personMiddleware.createPerson,
  personController.createPerson
);

personRouter.put(
  "/:id",
  Valid(IdParamsRequestDto, RequestType.params),
  Valid(UpdatePersonDto, RequestType.body),
  validateRole(null, AccountType.users),
  middleware.userHaveCompanies,
  personMiddleware.userHavePerson,
  personMiddleware.updatePerson,
  personController.updatePerson
);

personRouter.delete(
  "/:id",
  Valid(IdParamsRequestDto, RequestType.params),
  validateRole(null, AccountType.users),
  personMiddleware.userHavePerson,
  personController.deletePerson
);

personRouter.get(
  "/",
  validateRole(null, AccountType.users),
  Valid(GetPersonDto, RequestType.query),
  personController.searchPerson
);

personRouter.put(
  "/used/:id",
  validateRole(null, AccountType.users),
  personController.usedPerson
);

personRouter.get(
  "/count",
  Valid(CountPersonDto, RequestType.query),
  validateRole(null, AccountType.users),
  personController.countPerson
);

personRouter.get(
  "/checkCodeExisted",
  Valid(CheckCodeExistedDto, RequestType.query),
  validateRole(null, AccountType.users),
  personController.checkCodeExisted
);

export default personRouter;
