import { Request, Response } from "express";
import { HttpError } from "express-openapi-validator/dist/framework/types";
import BaseResponse from "../../services/baseResponse";
import l from "../../common/logger";
import { HTTPCode, PersonStatus } from "../../const";
import responseService from "../../services/responseService";
import services from "../../services/services";
import personService from "./person.service";
import personValidate from "./person.validate";
import slugify from "slugify"

const baseResponse = new BaseResponse("PersonController");
class PersonController {
  async createPerson(req: Request, res: Response) {
    try {
      const { userId } = req.cookies;
      console.log(userId);
      const person = await personService.create({
        ...req.body,
        userCreateId: userId,
        slugName: slugify(
          req.body.name,
          {
            lower: true,
            replacement: '_',
            trim: true,
            locale: 'vi'
          }
        )
      });
      return responseService.send(res, HTTPCode.success, {
        data: person,
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async updatePerson(req: Request, res: Response) {
    try {
      const { id } = req.params;
      const person = await personService.updateById(id, {
        ...req.body,
        usedAt: Date.now(),
        slugName: req.body.name ? slugify(
          req.body.name,
          {
            lower: true,
            replacement: '_',
            trim: true,
            locale: 'vi'
          }
        ) : undefined
      });
      return responseService.send(res, HTTPCode.success, {
        data: person,
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async deletePerson(req: Request, res: Response) {
    try {
      const { id } = req.params;
      await personService.deleteById(id)
      return responseService.send(res, HTTPCode.success, {
        message: "delete success",
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async searchPerson(req: Request, res: Response) {
    try {
      const { userId } = req.cookies;
      const { search, page, limit, type, companyId } = req.query;
      let query = {
        userCreateId: userId,
        status: PersonStatus.normal,
        companyId,
      } as any;
      if (search) {
        query = {
          ...query,
          $or: [
            {
              name: { $regex: search, $options: "i" },
            },
            {
              shortName: { $regex: search, $options: "i" },
            },
            {
              code: { $regex: search, $options: "i" },
            },
            {
              address: { $regex: search, $options: "i" },
            },
            {
              taxNumber: { $regex: search, $options: "i" },
            },
          ],
        };
      }
      if (type) {
        query = {
          ...query,
          personType: type,
        };
      }
      const person = await personService.getByQuery({
        query,
        page,
        limit,
        sort: {
          name: -1,
        },
      });
      return responseService.send(res, HTTPCode.success, {
        data: person,
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async usedPerson(req: Request, res: Response) {
    try {
      const { id } = req.params;
      await personService.updateById(id, {
        usedAt: Date.now(),
      });
      return responseService.send(res, HTTPCode.success, {
        message: "used success",
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async countPerson(req: Request, res: Response) {
    try {
      const { userId } = req.cookies;
      const { companyId } = req.query;
      const personCount = await personService.count({
        userCreateId: userId,
        companyId,
      });
      return responseService.send(res, HTTPCode.success, {
        data: {
          count: personCount,
        },
      });
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async checkCodeExisted(req: Request, res: Response) {
    try {
      const { code, companyId } = req.query;
      const { userId } = req.cookies;
      const isCodeExisted = await personValidate.isCodeExisted(
        userId,
        code,
        companyId
      );
      return responseService.send(res, HTTPCode.success, {
        data: {
          isExisted: isCodeExisted ? true : false,
        },
      });
    } catch (error) {
      l.error("checkCodeExisted controller: ", error);
      return baseResponse.serverError(res, error);
    }
  }
}

export default new PersonController();
