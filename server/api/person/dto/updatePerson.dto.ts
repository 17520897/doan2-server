import {
  ValidateNested,
  IsString,
  MaxLength,
  IsEnum,
  IsObject,
  IsDate,
  IsEmail,
  IsNumber,
  IsOptional,
  MinLength,
  IsMongoId,
  Min,
  Length,
  IsUrl,
  IsArray,
  Equals,
} from "class-validator";
import { PersonType } from "../../../const";

export default class UpdatePersonDto {
  @IsMongoId()
  companyId: string;

  @IsString()
  code: String;

  @IsString()
  name: String;

  @IsOptional()
  @IsString()
  shortName: String;

  @IsOptional()
  @IsString()
  taxNumber: String;

  @IsOptional()
  @IsString()
  phone: String;

  @IsEnum(PersonType, {
    message: "person must be: people or company or employee",
  })
  personType: String;

  @IsOptional()
  @IsString()
  address: String;

  @IsOptional()
  @IsString()
  note: String;
}
