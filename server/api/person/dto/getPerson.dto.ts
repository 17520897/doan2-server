import {
  ValidateNested,
  IsString,
  MaxLength,
  IsEnum,
  IsObject,
  IsDate,
  IsEmail,
  IsNumber,
  IsOptional,
  MinLength,
  IsMongoId,
  Min,
  Length,
  IsUrl,
  IsArray,
  Equals,
} from "class-validator";
import { PersonType } from "../../../const";

export default class GetPersonDto {
  @IsOptional()
  @IsString()
  search: string;

  @IsOptional()
  @IsEnum(PersonType, {
    message: "person must be: people or company or employee",
  })
  type: string;

  @IsMongoId()
  companyId;
}
