import mongoose, { Schema } from "mongoose";

export interface IPersonModel extends mongoose.Document {
  code: string;
  name: string;
  userCreateId: string;
  tax: string;
  address: string;
  personType: string;
  note: string;
  companyId: string;
}

const schema = new mongoose.Schema(
  {
    companyId: {
      type: mongoose.SchemaTypes.ObjectId,
      required: true,
    },
    userCreateId: {
      type: mongoose.SchemaTypes.ObjectId,
      required: true,
    },
    code: {
      type: String,
      index: "text",
    },
    name: {
      type: String,
      index: "text",
    },
    shortName: {
      type: String,
      index: "text",
    },
    slugName: {
      type: String,
      index: "text",
    },
    taxNumber: {
      type: String,
      index: "text",
    },
    phone: {
      type: String,
      index: "text",
    },
    address: {
      type: String,
      index: "text",
    },
    usedAt: {
      type: Date,
      default: Date.now(),
    },
    status: {
      enum: ["normal", "deleted"],
      type: String,
      default: "normal",
    },
    personType: {
      enum: ["people", "company", "employee"],
      type: String,
      default: "people",
      index: "text",
    },
    note: {
      type: String,
    },
  },
  {
    collection: "Person",
  }
);

schema.virtual("userCreateInfo", {
  ref: "User",
  localField: "userCreateId",
  foreignField: "_id",
  justOne: true,
});

schema.set("toObject", { virtuals: true });
schema.set("toJSON", { virtuals: true });

export const Person = mongoose.model<IPersonModel>("Person", schema);
