import { NextFunction, Request, Response } from "express";
import BaseResponse from "../../services/baseResponse";
import l from "../../common/logger";
import { HTTPCode } from "../../const";
import { ErrorsCode } from "../../const/errorsCode";
import responseService from "../../services/responseService";
import services from "../../services/services";
import personValidate from "./person.validate";

const baseResponse = new BaseResponse("PersonMiddleware");
class PersonMiddleware {
  async createPerson(req: Request, res: Response, next: NextFunction) {
    try {
      const { userId } = req.cookies;
      const { code, companyId } = req.body;
      const isCodeExisted = await personValidate.isCodeExisted(
        userId,
        code,
        companyId
      );
      if (isCodeExisted) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.personCodeExisted,
              field: "code",
            },
          ],
        });
      }
      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }

  async updatePerson(req: Request, res: Response, next: NextFunction) {
    try {
      const { id } = req.params;
      const { userId } = req.cookies;
      const { code, companyId } = req.body;
      const isCodeExisted = await personValidate.isCodeExisted(
        userId,
        code,
        companyId,
        true,
        id
      );
      if (isCodeExisted) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.personCodeExisted,
              field: "code",
            },
          ],
        });
      }
      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }
  async userHavePerson(req: Request, res: Response, next: NextFunction) {
    try {
      const { userId } = req.cookies;
      const { id } = req.params;
      const isUserHavePerson = await personValidate.isUserHavePerson(
        userId,
        id
      );
      if (!isUserHavePerson) {
        return responseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.personNotFound,
              field: "code",
            },
          ],
        });
      }
      next();
    } catch (error) {
      return baseResponse.serverError(res, error);
    }
  }
}

export default new PersonMiddleware();
