import express from "express";
import filesController from "./files.controller";
import uploadFile from "../../services/uploadFile";
import filesMiddleware from "./files.middleware";

const fileRouter = express.Router();

fileRouter.post(
  "/images",
  uploadFile.uploadFile(["jpg", "jpeg", "png"]).single("file"),
  filesMiddleware.uploadImages,
  filesController.uploadImages
);

fileRouter.post(
  "/xlsx",
  uploadFile.uploadFile(["xls", "xlsx"]).single("file"),
  filesMiddleware.uploadXlsx,
  filesController.uploadImages
);

export default fileRouter;
