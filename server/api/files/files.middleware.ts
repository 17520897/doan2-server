import { Request, Response, NextFunction } from "express";
import ResponseService from "../../services/responseService";
import l from "../../common/logger";
import { ErrorsCode } from "../../const/errorsCode";
import BaseResponse from "../../services/baseResponse";
import { HTTPCode } from "../../const";

const baseResponse = new BaseResponse(`FilesMiddleware`);
export class Middleware {
  async uploadImages(req: Request, res: Response, next: NextFunction) {
    try {
      const { filename, originalname } = req.file;
      if (filename === "null") {
        return ResponseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.filesOnlyAcceptImageFiles,
              error: "not accept this file - only support: jpg, jpeg, png",
              field: "file",
            },
          ],
        });
      }
      req.body.url = `${process.env.UPLOAD_FOLDER}/${filename}`;

      req.body.originalName = req.file.originalname;
      next();
    } catch (error) {
      return ResponseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    }
  }

  async uploadXlsx(req: Request, res: Response, next: NextFunction) {
    try {
      const { filename, originalname } = req.file;
      if (filename === "null") {
        return ResponseService.send(res, HTTPCode.notAccept, {
          errors: [
            {
              code: ErrorsCode.filesOnlyAcceptImageFiles,
              error: "not accept this file - only support: xls, xlsx",
              field: "file",
            },
          ],
        });
      }
      req.body.url = `${process.env.UPLOAD_FOLDER}/${filename}`;

      req.body.originalName = req.file.originalname;
      next();
    } catch (error) {
      return ResponseService.send(res, HTTPCode.serverError, {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      });
    }
  }
}

export default new Middleware();
