import { Request, Response } from "express";
import { HTTPCode } from "../../const";
import ResponseService from "../../services/responseService";

export class Controller {
  async uploadImages(req: Request, res: Response) {
    const { url, originalName } = req.body;
    return ResponseService.send(res, HTTPCode.success, {
      data: { url, originalName },
      message: "upload file success",
    });
  }
}

export default new Controller();
