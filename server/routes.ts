import { Application } from "express";
import homepage from "./api/routes";
import path from "path";
import fs from "fs"

import swaggerUi from "swagger-ui-express";

export default function routes(app: Application): void {
  app.get("/healthCheck", (req, res) => {
    const {url} = req.query
    const existed = fs.existsSync(`${process.env.FILE_UPLOAD_FOLDER}/${url}`)
    return existed ? res.send('yay') : res.send('f*ck')
  });

  app.use("/api", homepage);
  app.use("/docs", (req, res) => {
    const renderPath = __dirname.split("/");
    renderPath.pop();
    res.sendFile(path.join(renderPath.join("/") + "/public/index.html"));
  });
}
