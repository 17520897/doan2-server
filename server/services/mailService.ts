import nodemailer from "nodemailer";
import log4j from "../common/log4js";
import l from "../common/logger";
import { ErrorsCode } from "../const/errorsCode";
import BaseResponse from "./baseResponse";

const baseResponse = new BaseResponse("MailService");

interface ISENDMAIL {
  to: string;
  subject: string;
  text?: string;
  html?: string;
}
export class SendMail {
  async send(iSendMail: ISENDMAIL) {
    try {
      const { to, subject, text, html } = iSendMail;
      const transporter = nodemailer.createTransport({
        host: "smtp.gmail.com",
        port: 587,
        secure: false,
        requireTLS: true,
        auth: {
          user:
            process.env.NODE_ENV === "dev"
              ? process.env.DEV_EMAIL_USERNAME
              : process.env.EMAIL_USERNAME,
          pass:
            process.env.NODE_ENV === "dev"
              ? process.env.DEV_EMAIL_PASSWORD
              : process.env.EMAIL_PASSWORD,
        },
      });
      let response = await transporter.sendMail({
        from: `PD Accounting <${
          process.env.NODE_ENV === "dev"
            ? process.env.DEV_EMAIL_USERNAME
            : process.env.EMAIL_USERNAME
        }>`,
        to,
        subject,
        text,
        html,
      });

      return {
        message: "send mail success",
        data: response,
      };
    } catch (error) {
      l.error(`Mail Service: ${error.message}`);
      log4j("MailService: ").error(error);
      return {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      };
    }
  }
}

export default new SendMail();
