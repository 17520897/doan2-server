import {
  Document,
  QueryFindOneAndUpdateOptions,
  SaveOptions,
  QueryFindOneAndRemoveOptions,
  Mongoose,
} from "mongoose";

interface IBaseDbQuery {
  query?: any;
  page?: any;
  limit?: any;
  sort?: any;
  options?: any;
}
interface IBaseDbPopulate {
  query?: any;
  populate?: Array<{
    match?: any;
    path: string;
    select?: string;
    options?: any;
  }>;
  sort?: any;
  select?: string;
  page?: any;
  limit?: any;
}
export class BaseDbService<T> {
  private db: any;

  constructor(db) {
    this.db = db;
  }

  /**
   *
   * @param query
   * @type T
   * @param selected
   * @returns
   */
  async getOne(query: any, selected = ""): Promise<T> {
    const doc = await this.db.findOne(query, selected).lean();
    return doc;
  }

  async getAllByQuery(query): Promise<T[]> {
    const doc = await this.db.find(query).lean();
    return doc;
  }
  async getByQuery(iQuery: IBaseDbQuery): Promise<T[]> {
    const { sort, query, options } = iQuery;
    let { page, limit } = iQuery;
    if (page && limit) {
      page = parseInt(page);
      limit = parseInt(limit);
      const doc = await this.db
        .find(query, options)
        .sort(sort)
        .skip(page * limit)
        .limit(limit)
        .lean();
      const count = await this.db.find(query).countDocuments();
      return [
        ...doc,
        {
          totalPage: Math.ceil(count / limit),
          totalRecord: count,
        },
      ] as T[];
    } else {
      const doc = await this.db.find(query, options).sort(sort).lean();
      return doc;
    }
  }
  async getById(id: any, selected: string = ""): Promise<T> {
    const doc = (await this.db.findById(id, selected).lean()) as any;
    return doc;
  }

  async getSort(query: any, sort: any = {}): Promise<T[]> {
    const doc = await this.db.find(query).sort(sort);
    return doc;
  }

  async create(data: any, options: SaveOptions = {}): Promise<T> {
    const newDoc = new this.db(data);
    const doc = (await newDoc.save(options)) as T;
    return doc;
  }

  async createMany(data: any, options: SaveOptions = {}) {
    const doc = await this.db.create(data, options);
    return doc;
  }

  async updateOne(
    query: any,
    data: any,
    options: QueryFindOneAndUpdateOptions = {}
  ): Promise<T> {
    const doc = (await this.db
      .findOneAndUpdate(query, data, {
        new: true,
        ...options,
      })
      .lean()) as T;
    return doc;
  }

  async updateById(
    id: any,
    data: any,
    options: QueryFindOneAndUpdateOptions = {}
  ): Promise<T> {
    const doc = (await this.db
      .findByIdAndUpdate(id, data, {
        new: true,
        ...options,
      })
      .lean()) as T;
    return doc;
  }

  async populate(iPopulate: IBaseDbPopulate) {
    const { query, sort, select, populate } = iPopulate;
    let { page, limit } = iPopulate;
    if (page && limit) {
      page = parseInt(page);
      limit = parseInt(limit);
      const doc = await this.db
        .find(query)
        .populate(populate)
        .select(select)
        .sort(sort)
        .skip(page * limit)
        .limit(limit)
        .exec();
      const count = await this.db.find(query).countDocuments();
      return [
        ...doc,
        {
          totalPage: Math.ceil(count / limit),
          totalRecord: count,
        },
      ];
    } else {
      const doc = await this.db
        .find(query)
        .populate(populate)
        .select(select)
        .sort(sort)
        .exec();
      return doc;
    }
  }

  async deleteById(id, options: QueryFindOneAndRemoveOptions = {}): Promise<T> {
    const doc = await this.db.findByIdAndDelete(id, options);
    return doc;
  }

  async deleteMany(
    query: any,
    options: QueryFindOneAndRemoveOptions
  ): Promise<T> {
    const doc = await this.db.deleteMany(query, options);
    return doc;
  }

  async deleteByQuery(
    query: any,
    options: QueryFindOneAndRemoveOptions = {}
  ): Promise<T> {
    const doc = await this.db.findOneAndDelete(query, options);
    return doc;
  }

  async count(query) {
    const doc = await this.db.find(query).count();
    return doc;
  }

  async distinct(unique: string, query: any = {}) {
    const doc = await this.db.distinct(unique, query);
    return doc;
  }
}

export default BaseDbService;
