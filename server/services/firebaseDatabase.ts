import firebase from "firebase-admin";

var serviceAccount = require("../../config/firebaseCloudFirestore.json");

firebase.initializeApp({
  credential: firebase.credential.cert(serviceAccount),
  databaseURL: "https://pawn-codosa.firebaseio.com",
});

const fireBaseDbService = firebase.firestore();

export default fireBaseDbService;
