const Sentry = require("@sentry/node");

Sentry.init({
  dsn:
    "https://5cb54ae8a3644214abd9dca8a0ea1da4@o652734.ingest.sentry.io/5761618",

  // Set tracesSampleRate to 1.0 to capture 100%
  // of transactions for performance monitoring.
  // We recommend adjusting this value in production
  tracesSampleRate: 1.0,
});

export default Sentry;
