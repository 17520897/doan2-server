import { Request, Response } from "express";
import { ErrorsCode } from "../const/errorsCode";
import { HTTPCode } from "../const";

export interface IErrors {
  code?: ErrorsCode;
  error?: any;
  field?: any;
}

interface IResponse {
  message?: String;
  data?: any;
  errors?: Array<IErrors>;
  success?: boolean;
}

export class ResponseService {
  send(res: Response, statusCode: HTTPCode, response: IResponse = {}) {
    return res.status(statusCode).send({
      message: response.message,
      data: response.data ? response.data : null,
      errors: response.errors,
      timestamp: Date.now(),
      success: statusCode == HTTPCode.success ? true : false,
    });
  }
}

export default new ResponseService();
