import { Response } from "express";
import log4j from "../common/log4js";
import l from "../common/logger";
import { HTTPCode } from "../const";
import { ErrorsCode } from "../const/errorsCode";
import responseService from "./responseService";
import { IErrors } from "./responseService";
import Sentry from "./sentry";

interface IResponse {
  res: Response;
  data?: any;
  message?: string;
  errors?: Array<IErrors>;
  success?: boolean;
}
export default class BaseResponse {
  private module: string;
  private log: any;

  constructor(module: string) {
    this.module = module;
    this.log = log4j(this.module);
  }

  async serverError(res: Response, error) {
    l.error(`${this.module} server error: ${error.message}`);
    this.log.error(`server error: `, error);
    Sentry.captureException(error);
    return responseService.send(res, HTTPCode.serverError, {
      errors: [
        {
          code: ErrorsCode.serverError,
          error: error.message,
        },
      ],
      success: false,
    });
  }

  async success(response: IResponse) {
    return responseService.send(response.res, HTTPCode.success, {
      message: response.message,
      data: response.data,
      success: true,
    });
  }

  async notAccepted(response: IResponse) {
    return responseService.send(response.res, HTTPCode.notAccept, {
      message: response.message,
      errors: response.errors,
      success: false,
    });
  }
}
