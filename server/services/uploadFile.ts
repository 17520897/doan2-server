import multer from "multer";
import randomString from "randomstring";

export class UploadFile {
  uploadFile(checkType = [], changeName = true, prefix = "") {
    var placeStore = process.env.UPLOAD_DIR;
    const storage = multer.diskStorage({
      destination: function (req, _file: any, cb: any) {
        cb(null, placeStore);
      },
      filename: function (req, file: any, cb: any) {
        const fileName = file.originalname.split(".");
        const newFileName = `${randomString.generate(
          10
        )}-${new Date().getTime()}`;
        if (checkType) {
          if (checkType.indexOf(fileName[fileName.length - 1]) > -1) {
            if (changeName)
              cb(null, `${newFileName}.${fileName[fileName.length - 1]}`);
            else {
              const { userId, adminId } = req.cookies;
              cb(
                null,
                userId
                  ? `${prefix}-${userId}.${fileName[fileName.length - 1]}`
                  : `${prefix}-${adminId}.${fileName[fileName.length - 1]}`
              );
            }
          } else {
            cb(null, `null`);
          }
        } else {
          cb(null, `${newFileName}.${fileName[fileName.length - 1]}`);
        }
      },
    });
    return multer({ storage: storage });
  }
}

export default new UploadFile();
