import request from "request";
import smsService from "../api/sms/sms.service";
import { SMSType } from "../const";
import { ErrorsCode } from "../const/errorsCode";
import log4j from "../common/log4js";
import BaseResponse from "./baseResponse";

function doRequest(options) {
  return new Promise((resolve, reject) => {
    request({ ...options, timeout: 3000 }, (error, response, body) => {
      if (error) {
        reject(error);
      }
      resolve(body);
    });
  });
}

const baseResponse = new BaseResponse("SMS");

export class SendSMS {
  async send(phone: string, code: string, type: SMSType = SMSType.register) {
    try {
      if (process.env.NODE_ENV !== "dev") {
        let message = `Chao mung ban den voi ung dung PAWN P2P. Vui long nhap ma ${code} de dang ky tai khoan.`;
        if (type === SMSType.forgotPassword) {
          message = `Khong chia se OTP voi bat ky ai. Nhap ma ${code} de dat lai mat khau`;
        }
        let messageBase64Encode = Buffer.from(message).toString("base64");
        let body =
          '<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance\\" xmlns:xsd="http://www.w3.org/2001/XMLSchema\\" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/\\" xmlns:mts="MTService\\">\r\n\t<soapenv:Header/>\r\n\t<soapenv:Body>\r\n\t<mts:sendMT soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/\\">\r\n\t<string xsi:type="xsd:string\\">' +
          phone +
          '</string>\r\n\t<string0 xsi:type="xsd:string\\">' +
          messageBase64Encode +
          '</string0>\r\n\t<string1 xsi:type="xsd:string\\">CODOSA</string1>\r\n\t<string2 xsi:type="xsd:string\\">CODOSA</string2>\r\n\t<string3 xsi:type="xsd:string\\">0</string3>\r\n\t<string4 xsi:type="xsd:string\\">0</string4>\r\n\t<string5 xsi:type="xsd:string\\">0</string5>\r\n\t<string6 xsi:type="xsd:string\\">0</string6>\r\n\t<string7 xsi:type="xsd:string\\">0</string7>\r\n\t<string8 xsi:type="xsd:string\\">0</string8>\r\n\t</mts:sendMT>\r\n\t</soapenv:Body>\r\n</soapenv:Envelope>';

        var options = {
          method: "POST",
          url: "http://sms.8x77.vn:8077/mt-services/MTService",
          headers: {
            "Postman-Token": "d98732b6-67c6-4e4a-85ab-252e56813fe7",
            "cache-control": "no-cache",
            Authorization: "Basic Y29kb3NhOkNPRE9TQUAjKCkxMjM=",
            "Content-Type": "text/xml;charset=utf-8",
          },
          body,
        };
        let response = await doRequest(options);
        smsService.create({
          phone,
          code,
          type:
            type === SMSType.register
              ? SMSType.register
              : SMSType.forgotPassword,
        });
        console.log("sms mes: ", message);
        console.log("sms res: ", response);
        return {
          message: "send sms success",
          data: response,
        };
      } else {
        return {
          message: "send sms success",
        };
      }
    } catch (error) {
      return {
        errors: [
          {
            code: ErrorsCode.serverError,
            error: error.message,
          },
        ],
      };
    }
  }
}

export default new SendSMS();
