import bcrypt from "bcrypt";
import { Request } from "express";
import accountNumberService from "../api/accountNumber/accountNumber.service";
import companiesService from "../api/companies/companies.service";
import locationService from "../api/location/location.service";
import { CompaniesStatus, HTTPCode } from "../const";
import { ErrorsCode } from "../const/errorsCode";
import responseService from "./responseService";
class Services {
  fileUploadDir(req: Request) {
    const { filename } = req.file;
    if (filename === "null") {
      return "";
    }
    return filename === "null" ? "" : `${process.env.UPLOAD_DIR}/${filename}`;
  }

  async hashPassword(password: string, salt: number = 10) {
    const hashSalt = await bcrypt.genSalt(salt);
    const hashPassword = await bcrypt.hash(password, hashSalt);
    return hashPassword;
  }

  async verifyPassword(password: string, hashPassword: string) {
    const compare = await bcrypt.compare(password, hashPassword);
    return compare;
  }

  isRightAddress(address) {
    const checkAddress = locationService.getByAddress({
      city: address.city,
      district: address.district,
      ward: address.ward,
    });
    return checkAddress
      ? { success: true, address: checkAddress }
      : { success: false };
  }

  async isRightAccountNumber(accountNumber, accountNumberId): Promise<Boolean> {
    const checkAccountNumber = await accountNumberService.getOne({
      _id: accountNumberId,
      "accountNumbers.accountNumber": accountNumber,
    });
    return checkAccountNumber ? true : false;
  }

  async isUserHaveCompany(userId, companyId) {
    const company = await companiesService.getOne({
      userCreateId: userId,
      status: CompaniesStatus.normal,
      _id: companyId,
    });
    return company ? true : false;
  }
}

export default new Services();
