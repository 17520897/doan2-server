import { HTTPCode } from "../const";
import { ErrorsCode } from "../const/errorsCode";
import BaseResponse from "./baseResponse";

function errorHandler(err, res) {
  const baseResponse = new BaseResponse("ErrorHandler");
  baseResponse.serverError(res, err);
}

class ErrorHandler extends Error {
  errors: any;
  constructor(message) {
    super();
    this.errors = [
      {
        code: ErrorsCode.serverError,
        error: message,
      },
    ];
  }
}

export { errorHandler, ErrorHandler };
