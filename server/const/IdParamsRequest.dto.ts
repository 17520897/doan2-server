import { IsMongoId } from "class-validator";

export default class IdParamsRequestDto {
  @IsMongoId()
  id: String;
}
