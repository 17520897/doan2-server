export enum ErrorsCode {
  invalid = "invalid",
  serverError = "serverError",
  locationNotFound = "locationNotFound",
  notAuthorize = "notAuthorize",

  //admin
  adminUserNameExisted = "adminUserNameExisted",
  adminDidNotHavePermissions = "adminDidNotHavePermissions",
  adminNotFound = "adminNotFound",
  adminWrongPassword = "adminWrongPassword",
  adminIsBlockedOrDeleted = "adminIsBlockedOrDeleted",

  //account number
  accountNumberCircularsExisted = "accountNumberCircularsExisted",
  accountNumberArrayAccountNumbersWrongFormat = "accountNumberArrayAccountNumbersWrongFormat",
  accountNumberNotExisted = "accountNumberNotExisted",

  //serviceBill
  billCompaniesNotHaveBill = "billCompaniesNotHaveBill",
  billExisted = 'billExisted',

  //company type
  companyTypeNameExisted = "companyTypeNameExisted",

  //companies
  companiesNameExisted = "companiesNameExisted",
  companiesTaxCodeExisted = "companiesTaxCodeExisted",
  companiesNotFound = "companiesNotFound",
  companiesUserNotEnoughCompany = "companiesUserNotEnoughCompany",
  companiesIsWrongAddress = "companiesIsWrongAddress",

  //companyBanking
  companyBankingBankNotExisted = "companyBankingBankNotExisted",
  companyBankingBankAccountExisted = "companyBankingBankAccountExisted",
  companyBankingNotFound = "companyBankingNotFound",

  //email
  emailCanNotSend = "emailCanNotSend",

  //forwardTax
  forwardTaxNotFound = "forwardTaxNotFound",

  //location
  locationWardNotInThisDistrict = "locationWardNotInThisDistrict",
  locationCodeNotFound = "locationCodeNotFound",
  locationDistrictNotInThisCity = "locationDistrictNotInThisCity",

  //money type
  moneyTypeNotExisted = "moneyTypeNotExisted",
  moneyTypeNameExisted = "moneyTypeNameExisted",

  //money bill
  moneyBillUserNotHaveThisCompany = "moneyBillUserNotHaveThisCompany",
  moneyBillAccountNumberNotExisted = "moneyBillAccountNumberNotExisted",
  moneyBillReceiveAccountNumberNotExisted = "moneyBillReceiveAccountNumberNotExisted",
  moneyBillDebitAccountNumberNotExisted = "moneyBillDebitAccountNumberNotExisted",
  moneyBillReceiveAndDebitAccountNumberCanNotSame = "moneyBillReceiveAndDebitAccountNumberCanNotSame",
  moneyBillNotFound = "moneyBillNotFound",
  moneyBillFromFileNotHaveData = "moneyBillFromFileNotHaveData",
  moneyBillFromFileWrongDateFormat = "moneyBillFromFileWrongDateFormat",
  moneyBillFromFileNotExistedAccountNumber = "moneyBillFromFileNotExistedAccountNumber",
  moneyBillFromFileNotAccountNumberIsSame = "moneyBillFromFileNotAccountNumberIsSame",
  moneyBillFromFileMoneyMustGreaterThanZero = "moneyBillFromFileMoneyMustGreaterThanZero",

  //person
  personNameExisted = "personNameExisted",
  personCodeExisted = "personCodeExisted",
  personNotFound = "personNotFound",
  personCompanyNotFound = "personCompanyNotFound",
  personCreateNotFound = "personCreateNotFound",
  personReceiveNotFound = "personReceiveNotFound",
  personIsSameId = "personIsSameId",

  //personBanking
  personBankingBankNotExisted = "personBankingBankNotExisted",
  personBankingBankAccountExisted = "personBankingBankAccountExisted",
  personBankingNotFound = "personBankingNotFound",
  //sms
  smsCanNotSend = "smsCanNotSend",

  //users
  usersWrongPassword = "userWrongPassword",
  usersEmailRegistered = "usersEmailRegistered",
  usersNotFound = "usersNotFound",
  usersIsBlocked = "usersIsBlocked",
  usersDidNotHavePermission = "usersDidNotHavePermission",

  //verify
  verifyCodeHasSent = "verifyCodeHasSent",
  verifyCodeNotFound = "verifyCodeNotFound",
  verifyCantForgotPassword = "verifyCantForgotPassword",
  verifyEmailHasBeenRegistered = "verifyEmailHasBeenRegistered",

  //admin roles
  rolesHasBeenExisted = "rolesHasBeenExisted",
  roleNotAcceptUpdate = "roleNotAcceptUpdate",
  roleNotAcceptDisable = "roleNotAcceptDisable",
  roleNotAcceptDelete = "roleNotAcceptDelete",

  //systemConst
  systemConstUserRolesNotExisted = "systemConstUserRolesNotExisted",

  //settingTax
  settingTaxNameExisted = "settingTaxNameExisted",
  settingTaxNotFound = "settingTaxNotFound",
  settingTaxAccountNumberNotFound = "settingTaxAccountNumberNotFound",

  //serviceBill
  serviceBillCompaniesNotHaveServiceBill = "serviceBillCompaniesNotHaveServiceBill",

  //settingBank
  settingBankNameExisted = "settingBankNameExisted",
  settingBankNotFound = "settingBankNotFound",

  //stock
  stockCodeExisted = "stockCodeExisted",
  stockCompaniesNotHaveStock = "stockCompaniesNotHaveStock",

  //user roles
  userRolesHasBeenExisted = "userRolesHasBeenExisted",
  userRoleNotAcceptUpdate = "userRoleNotAcceptUpdate",
  userRoleNotAcceptDisable = "userRoleNotAcceptDisable",
  userRoleNotAcceptDelete = "userRoleNotAcceptDelete",
  //files
  filesOnlyAcceptImageFiles = "filesOnlyAcceptImageFiles",
  filesOnlyAcceptXlsxFiles = "filesOnlyAcceptXlsxFiles",
  filesNotFound = 'filesNotFound',
  filesWrongFormat = 'filesWrongFormat',
  filesNotHaveData = 'filesNotHaveData',

  //middleware
  middlewareReceiveAndDebitAccountNumberCanNotSame = "middlewareReceiveAndDebitAccountNumberCanNotSame",
  middlewareReceiveAccountNumberNotExisted = "middlewareReceiveAccountNumberNotExisted",
  middlewareDebitAccountNumberNotExisted = "middlewareDebitAccountNumberNotExisted",
}
