export enum AccountType {
  users = "users",
  admin = "admin",
  all = "all",
}

export enum AdminStatus {
  normal = "normal",
  deleted = "deleted",
  block = "block",
}

export enum BillFunction {
  services = "services",
  fixedAssets = "fixedAssets",
  goods = "goods",
  material = "material",
}

export enum CompanyTypeStatus {
  normal = "normal",
  deleted = "deleted",
}

export enum CompaniesStatus {
  normal = "normal",
  deleted = "deleted",
}

export enum CompanyBankingStatus {
  normal = "normal",
  deleted = "deleted",
}

export enum HTTPCode {
  success = 200,
  badRequest = 400,
  notAccept = 406,
  unauthorize = 401,
  serverError = 500,
}

export enum MoneyTypeStatus {
  normal = "normal",
  deleted = "deleted",
}

export enum MoneyBillType {
  receipt = "receipt",
  expenses = "expenses",
}

export enum UsersStatus {
  normal = "normal",
  block = "block",
}

export enum UserRoleStatus {
  enable = "enable",
  disable = "disable",
  delete = "delete",
}

export enum RoleStatus {
  enable = "enable",
  disable = "disable",
  delete = "delete",
}

export enum PersonStatus {
  normal = "normal",
  deleted = "deleted",
}

export enum PersonType {
  people = "people",
  company = "company",
  employee = "employee",
}

export enum PersonBankingStatus {
  normal = "normal",
  deleted = "deleted",
}

export enum ProductsStatus {
  normal = "normal",
  deleted = "deleted",
}

export enum SMSType {
  register = "register",
  forgotPassword = "forgotPassword",
}

export enum SettingBankStatus {
  normal = "normal",
  deleted = "deleted",
}

export enum StockType {
  services = "services",
  fixedAssets = "fixedAssets",
  goods = "goods",
  material = "material",
}

export enum VerifyAccountType {
  user = "user",
}

export enum VerifyType {
  register = "register",
  forgotPassword = "forgotPassword",
}
