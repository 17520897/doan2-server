import express from "express";
import { Application } from "express";
import path from "path";
import http from "http";
import os from "os";
import cookieParser from "cookie-parser";
import installValidator from "./openapi";
import l from "./logger";
import morgan from "morgan";
import { IDatabase } from "./database";
import helmet from "helmet";
import cors from "cors";
import cron, { CronCommand } from "cron";
import log4js from "log4js";
const app = express();
import dotenv from "dotenv";
dotenv.config();

const CronJob = cron.CronJob;

export default class ExpressServer {
  constructor() {
    const root = path.normalize(__dirname + "/../../..");
    const configRoot = path.normalize(__dirname + "/../..");
    console.log("TEST ENV: ", process.env.NODE_ENV)
    app.set("appPath", root + "client");
    app.use(morgan("dev"));
    app.use(express.json({ limit: process.env.REQUEST_LIMIT || "300kb" }));
    app.use(helmet());
    app.use(
      express.urlencoded({
        extended: true,
        limit: process.env.REQUEST_LIMIT || "300kb",
      })
    );
    app.use(cors());
    app.use(cookieParser(process.env.SESSION_SECRET));
    app.use(express.static(`public`));
    
    try {
      require("fs").mkdirSync("./log");
    } catch (e) {
      if (e.code != "EEXIST") {
        console.error("Could not set up log directory, error was: ", e);
        process.exit(1);
      }
    }
    log4js.configure(`${configRoot}/config/log4js.json`);
  }

  router(routes: (app: Application) => void): ExpressServer {
    installValidator(app, routes);
    return this;
  }

  database(db: IDatabase): ExpressServer {
    db.init();
    return this;
  }

  cronJob(
    time: string,
    job: CronCommand = () => {
      console.log("cron jobs");
    }
  ): ExpressServer {
    const task = new CronJob(time, job, null, true, "Asia/Ho_Chi_Minh");
    task.start();
    return this;
  }

  listen(p: string | number = process.env.PORT): Application {
    const welcome = (port) => () =>
      l.info(
        `up and running in ${
          process.env.NODE_ENV || "development"
        } @: ${os.hostname()} on port: ${port}`
      );
    http.createServer(app).listen(p, welcome(p));
    return app;
  }
}
