import "./common/env";
import Database from "./common/database";
import Server from "./common/server";
import routes from "./routes";
import BaseResponse from "./services/baseResponse";
import { Request, Response } from "express";
import l from "./common/logger";
import log4j from "./common/log4js";
import responseService from "./services/responseService";
import { HTTPCode } from "./const";
import { ErrorsCode } from "./const/errorsCode";
import { errorHandler, ErrorHandler } from "./services/errorHandler";

const port = parseInt(process.env.PORT || "3000");

const connectionString =
  process.env.NODE_ENV === "production"
    ? process.env.MONGODB_URI
    : process.env.NODE_ENV === "staging"
    ? process.env.MONGODB_URI_STAGING ||
      "mongodb://localhost:27017/pawn-staging"
    : process.env.MONGODB_URI_DEV || "mongodb://localhost:27017/pawn-dev";
const db = new Database(connectionString);

export default new Server().database(db).router(routes).listen(port);
