const mongoose = require("mongoose");

const permissionSchema = new mongoose.Schema(
  {
    groupName: {
      type: String,
      required: true,
    },
    groupKey: {
      type: String,
      required: true,
      unique: true,
    },

    permissions: [
      {
        name: {
          type: String,
          required: true,
        },
        key: {
          type: String,
          required: true,
          unique: true,
        },
        dependOn: [],
      },
    ],
  },
  {
    collection: "Permissions",
  }
);

mongoose.model("Permissions", permissionSchema);

const employeePermissionsSchema = new mongoose.Schema(
  {
    groupName: {
      type: String,
      required: true,
    },
    groupKey: {
      type: String,
      required: true,
      unique: true,
    },
    permissions: [
      {
        name: {
          type: String,
          required: true,
        },
        key: {
          type: String,
          required: true,
          unique: true,
        },
        dependOn: [],
      },
    ],
  },
  {
    collection: "EmployeePermissions",
  }
);

mongoose.model("EmployeePermissions", employeePermissionsSchema);

const roleSchema = new mongoose.Schema(
  {
    roleName: {
      type: String,
      required: true,
    },
    rolePermissions: [
      {
        permissionKey: {
          type: String,
          required: true,
        },
      },
    ],
    status: {
      type: String,
      enum: ["enable", "disable"],
      default: "enable",
    },
    adminCreateId: {
      type: mongoose.SchemaTypes.ObjectId,
    },
  },
  {
    collection: "Roles",
  }
);

mongoose.model("Roles", roleSchema);

const province = new mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      index: "text",
    },

    districts: [
      {
        name: {
          type: String,
          required: true,
          index: "text",
        },
        wards: [
          {
            name: {
              type: String,
              required: true,
              index: "text",
            },
          },
        ],
      },
    ],
  },
  {
    collection: "Province",
  }
);

mongoose.model("Province", province);

const adminSchema = new mongoose.Schema(
  {
    phone: {
      type: String,
      trim: true,
    },
    username: {
      type: String,
      required: true,
      trim: true,
    },
    password: {
      type: String,
      required: true,
      trim: true,
    },
    name: {
      type: String,
      trim: true,
    },
    roleId: {
      type: mongoose.SchemaTypes.ObjectId,
    },
    createAt: {
      type: Date,
      default: Date.now,
    },
    status: {
      type: String,
      enum: ["normal", "deleted", "block"],
      default: "normal",
    },
    adminCreateId: {
      type: mongoose.SchemaTypes.ObjectId,
    },
  },
  {
    collection: "Admin",
  }
);

mongoose.model("Admin", adminSchema);

const userPermissionsSchema = new mongoose.Schema(
  {
    groupName: {
      type: String,
      required: true,
    },
    groupKey: {
      type: String,
      required: true,
      unique: true,
    },
    permissions: [
      {
        name: {
          type: String,
          required: true,
        },
        key: {
          type: String,
          required: true,
          unique: true,
        },
        dependOn: [],
      },
    ],
  },
  {
    collection: "UserPermissions",
  }
);

mongoose.model("UserPermissions", userPermissionsSchema);

const userRolesSchema = new mongoose.Schema(
  {
    roleName: {
      type: String,
      required: true,
    },
    rolePermissions: [
      {
        permissionKey: {
          type: String,
          required: true,
        },
      },
    ],
    status: {
      type: String,
      enum: ["enable", "disable"],
      default: "enable",
    },
    adminCreateId: {
      type: mongoose.SchemaTypes.ObjectId,
    },
  },
  {
    collection: "UserRoles",
  }
);

mongoose.model("UserRoles", userRolesSchema);

const systemConstSchema = new mongoose.Schema(
  {
    userTotalFreeCompany: {
      type: Number,
      default: 0,
    },
    userRoleFreeId: {
      type: mongoose.SchemaTypes.ObjectId,
    },
    userTotalFreeBill: {
      type: Number,
    },
  },
  {
    collection: "SystemConst",
  }
);

mongoose.model("SystemConst", systemConstSchema);
