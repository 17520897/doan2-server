const mongoose = require("mongoose");
const fs = require("fs");
const bcrypt = require("bcrypt");
require("./model");
require("dotenv").config();
const data = require("./permission.json").data;
const userPermissionsData = require("./userPermissions.json").data;
//setup data
const setupSystemAdminRole = {
  roleName: "System Admins",
};
const setupSystemUsersRole = {
  roleName: "System Users",
};
const setupAdminInfo = {
  username: "admin",
  password: "admin123@",
  phone: "0327888662",
};

async function connectMongoose() {
  if (process.argv.length <= 2) {
    console.log("must have params: [dev, staging, production]");
    return;
  }
  try {
    const env = process.argv[2];
    console.log(env);
    let dbString = process.env.MONGODB_URI_DEV;
    if (env.toString() === "production") {
      dbString = process.env.MONGODB_URI;
    } else if (env.toString() === "staging") {
      dbString = process.env.MONGODB_URI_STAGING;
    }
    console.log(`Begin connect to: ${dbString}`);
    mongoose.connect(
      dbString,
      {
        useUnifiedTopology: true,
        useNewUrlParser: true,
        useFindAndModify: false,
        useCreateIndex: true,
      },
      () => {
        console.log("Connect to mongoose: \n", dbString);
      }
    );
    return;
  } catch (error) {
    console.log("[Error] Connect Mongodb Fail: \n", error);
    process.exit(1);
  }
}

async function setupPermissions() {
  try {
    console.log("=== SETUP PERMISSIONS ===");
    const adminRole = {
      roleName: setupSystemAdminRole.roleName,
      rolePermissions: [],
    };
    let permissionKeys = `export enum PermissionKeys {
    `;
    await mongoose.model("Permissions").remove({});
    for (let i = 0; i < data.length; i++) {
      const newData = await mongoose.model("Permissions").findOneAndUpdate(
        {
          groupKey: data[i].groupKey,
        },
        data[i],
        {
          new: true,
          upsert: true,
        }
      );
      for (let j = 0; j < newData.permissions.length; j++) {
        permissionKeys += `${newData.permissions[j].key} = "${newData.permissions[j].key}",\n`;
        adminRole.rolePermissions.push({
          permissionKey: newData.permissions[j].key,
        });
      }
    }
    permissionKeys += `}`;
    const systemAdminRole = await mongoose.model("Roles").findOneAndUpdate(
      {
        roleName: adminRole.roleName,
        status: "enable",
      },
      adminRole,
      {
        upsert: true,
        new: true,
      }
    );
    console.log("=== SETUP SYSTEM ADMIN ===");
    const checkExistedAdmin = await mongoose.model("Admin").findOne({
      username: "admin",
    });
    let admin = null;
    if (checkExistedAdmin) {
      admin = await mongoose
        .model("Admin")
        .findByIdAndUpdate(checkExistedAdmin._id, {
          roleId: systemAdminRole._id,
        });
    } else {
      const salt = await bcrypt.genSalt(
        parseInt(process.env.HASH_PASSWORD_SALT)
      );
      const password = await bcrypt.hash(setupAdminInfo.password, salt);
      admin = await mongoose.model("Admin").create({
        username: setupAdminInfo.username,
        password: password,
        phone: setupAdminInfo.phone,
        roleId: systemAdminRole._id,
      });
    }
    await mongoose.model("Roles").findByIdAndUpdate(systemAdminRole._id, {
      adminCreateId: admin._id,
    });
    console.log("[Success] Setup Admin Success\n");
    fs.writeFile(
      `server/const/permissions.ts`,
      permissionKeys,
      { encoding: "utf-8" },
      (error) => {
        if (error) console.log("Setup permission error: ", error);
        console.log("[Success]Setup Permissions Success\n");
        return;
      }
    );
  } catch (error) {
    console.log("[Error] Setup Permissions Error: \n", error);
    process.exit(1);
  }
}

async function setupUsersPermissionsAndSystemConst() {
  try {
    console.log("=== SETUP USERS PERMISSIONS ===");
    const userRoles = {
      roleName: setupSystemUsersRole.roleName,
      rolePermissions: [],
    };
    let permissionKeys = `export enum UserPermissionsKey {
    `;
    await mongoose.model("UserPermissions").remove({});
    for (let i = 0; i < userPermissionsData.length; i++) {
      const newData = await mongoose.model("UserPermissions").findOneAndUpdate(
        {
          groupKey: userPermissionsData[i].groupKey,
        },
        userPermissionsData[i],
        {
          new: true,
          upsert: true,
        }
      );
      for (let j = 0; j < newData.permissions.length; j++) {
        permissionKeys += `${newData.permissions[j].key} = "${newData.permissions[j].key}",\n`;
        userRoles.rolePermissions.push({
          permissionKey: newData.permissions[j].key,
        });
      }
    }
    permissionKeys += `}`;
    const admin = await mongoose.model("Admin").findOne({
      username: setupAdminInfo.username,
    });
    const newUserRoles = await mongoose.model("UserRoles").findOneAndUpdate(
      {
        roleName: userRoles.roleName,
        status: "enable",
      },
      { ...userRoles, adminCreateId: admin._id },
      {
        upsert: true,
        new: true,
      }
    );
    await mongoose.model("SystemConst").findOneAndUpdate(
      {},
      {
        userRoleFreeId: newUserRoles._id,
        userTotalFreeCompany: parseInt(process.env.USER_TOTAL_FREE_COMPANY),
        userTotalFreeBill: parseInt(process.env.USER_TOTAL_FREE_BILL),
      },
      {
        new: true,
        upsert: true,
      }
    );
    fs.writeFile(
      `server/const/userPermissions.ts`,
      permissionKeys,
      { encoding: "utf-8" },
      (error) => {
        if (error) console.log("Setup user permission error: ", error);
        console.log("[Success]Setup Permissions Success\n");
        return;
      }
    );
  } catch (error) {
    console.log("[Error] Setup Permissions Error: \n", error);
    process.exit(1);
  }
}

async function setupAdmin() {
  try {
    return;
  } catch (error) {
    console.log("[Error] Setup Admin Error: \n", error);
    process.exit(1);
  }
}

async function finish() {
  console.log("=== Finish ===");
  process.exit();
}

function setupUploadFolder() {
  console.log("=== SETUP UPLOAD FOLDER ===");
  const checkPublic = fs.existsSync("./public");
  console.log(checkPublic);
  if (!checkPublic) {
    console.log("create public upload folder");
    fs.mkdirSync("./public");
  }
  const checkUploads = fs.existsSync("./public/uploads");
  if (!checkUploads) {
    console.log("create uploads upload folder\n");
    fs.mkdirSync("./public/uploads");
  }
  console.log("[Success] Setup Folder Success\n");
}

async function main() {
  await connectMongoose();
  setupUploadFolder();

  //1: system role
  await setupPermissions();
  //2: add role to admin
  await setupAdmin();

  //3: system user role
  await setupUsersPermissionsAndSystemConst();

  await finish();
}

main();
