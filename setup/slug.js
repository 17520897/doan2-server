const bcrypt = require("bcrypt");
require("dotenv").config();
const salt = bcrypt.genSaltSync(
  parseInt(process.env.HASH_PASSWORD_SALT)
);
const password = bcrypt.hashSync("admin123@", salt);

console.log(password)