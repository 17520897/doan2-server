const data = require("./newLocal.json");
const mongoose = require("mongoose");
const fs = require("fs");
require("./model");
require("dotenv").config();
async function main() {
  if (process.argv.length <= 2) {
    console.log("must have params: [dev, staging, production]");
    return;
  }
  try {
    const env = process.argv[2];
    console.log(env);
    let dbString = process.env.MONGODB_URI_DEV;
    if (env.toString() === "production") {
      dbString = process.env.MONGODB_URI;
    } else if (env.toString() === "staging") {
      dbString = process.env.MONGODB_URI_STAGING;
    }
    console.log(`Begin connect to: ${dbString}`);
    mongoose.connect(
      dbString,
      {
        useUnifiedTopology: true,
        useNewUrlParser: true,
        useFindAndModify: false,
        useCreateIndex: true,
      },
      () => {
        console.log("Create Province script connect to: ", dbString);
      }
    );
    let newData = [];
    for (let i = 0; i < data.length; i++) {
      let city = {
        name: data[i].name,
        districts: [...data[i].districts],
      };
      for (let j = 0; j < city.districts.length; j++) {
        city.districts[j] = {
          ...city.districts[j],
          wards: city.districts[j].wards,
        };
      }
      newData.push(city);
      await mongoose.model("Province").findOneAndUpdate(
        {
          name: city.name,
        },
        city,
        {
          new: true,
          upsert: true,
        }
      );
    }
    process.exit();
  } catch (error) {
    console.log(error);
  }
}

main();
