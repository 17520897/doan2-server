let province = require("./newLocal.json");
const fs = require("fs");
function main() {
  let code = 0;
  let city = [];
  let districts = [];
  let wards = [];
  for (let i = 0; i < province.length; i++) {
    let name = "";
    let splitName = province[i].name.split(" ");
    let x = splitName[0] === "Tỉnh" ? 1 : 2;
    for (x; x < splitName.length; x++) {
      name += " " + splitName[x];
    }
    city.push({
      name: name.trim(),
      fullName: province[i].name,
      code: i,
      prefix: splitName[0] === "Tỉnh" ? splitName[0] : `Thành phố`,
      level: 1,
    });
    code++;
  }
  for (let i = 0; i < province.length; i++) {
    for (let j = 0; j < province[i].districts.length; j++) {
      let districtCode = code;

      let name = "";
      let splitName = province[i].districts[j].name.split(" ");
      let x = 1;
      if (splitName[0] === "Thị" || splitName[0] === "Thành") x = 2;
      for (x; x < splitName.length; x++) {
        name += " " + splitName[x];
      }
      districts.push({
        fullName: province[i].districts[j].name,
        name: name.trim(),
        code,
        prefix:
          splitName[0] === "Thị"
            ? "Thị xã"
            : splitName[0] === "Thành"
            ? "Thành phố"
            : splitName[0],
        parentCode: i,
        level: 2,
      });
      code++;
      for (let k = 0; k < province[i].districts[j].wards.length; k++) {
        let name = "";
        let splitName = province[i].districts[j].wards[k].name.split(" ");
        let x = splitName[0] === "Thị" ? 2 : 1;
        for (x; x < splitName.length; x++) {
          name += " " + splitName[x];
        }

        wards.push({
          fullName: province[i].districts[j].wards[k].name,
          name: name.trim(),
          code: code,
          parentCode: districtCode,
          prefix: splitName[0] === "Thị" ? "Thị trấn" : splitName[0],
          level: 3,
        });
        code++;
      }
    }
  }
  fs.writeFileSync("config/city.json", JSON.stringify(city), {
    encoding: "utf-8",
  });
  fs.writeFileSync("config/districts.json", JSON.stringify(districts), {
    encoding: "utf-8",
  });
  fs.writeFileSync("config/wards.json", JSON.stringify(wards), {
    encoding: "utf-8",
  });
}

main();
