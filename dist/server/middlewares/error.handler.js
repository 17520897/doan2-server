"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const log4js_1 = __importDefault(require("../common/log4js"));
const logger_1 = __importDefault(require("../common/logger"));
const errorsCode_1 = require("../const/errorsCode");
// eslint-disable-next-line no-unused-vars, no-shadow
function errorHandler(err, req, res, next) {
    res.status(err.status || 500).json({
        errors: [
            {
                code: errorsCode_1.ErrorsCode.serverError,
                error: err.message,
            },
        ],
    });
    logger_1.default.error(`errorHandler: ${err.message}`);
    log4js_1.default("errorHandler: ").error(err);
}
exports.default = errorHandler;
//# sourceMappingURL=error.handler.js.map