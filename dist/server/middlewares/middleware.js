"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const baseResponse_1 = __importDefault(require("../services/baseResponse"));
const person_validate_1 = __importDefault(require("../api/person/person.validate"));
const errorsCode_1 = require("../const/errorsCode");
const companies_validate_1 = __importDefault(require("../api/companies/companies.validate"));
const accountNumber_service_1 = __importDefault(require("../api/accountNumber/accountNumber.service"));
const companyBanking_service_1 = __importDefault(require("../api/companyBanking/companyBanking.service"));
const stock_validate_1 = __importDefault(require("../api/stock/stock.validate"));
const companies_service_1 = __importDefault(require("../api/companies/companies.service"));
const baseResponse = new baseResponse_1.default("ServiceBillDetailMiddleware");
class Middleware {
    personCreateOrReceiveExisted(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { userId } = req.cookies;
                const { personCreateId, personReceiveId } = req.body;
                const [isPersonCreateExisted, isPersonReceiveExisted,] = yield Promise.all([
                    person_validate_1.default.isPersonExisted(userId, personCreateId),
                    person_validate_1.default.isPersonExisted(userId, personReceiveId),
                ]);
                if (personCreateId && !isPersonCreateExisted) {
                    return baseResponse.notAccepted({
                        res,
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.personCreateNotFound,
                                field: "personCreateId",
                            },
                        ],
                    });
                }
                if (personReceiveId && !isPersonReceiveExisted) {
                    return baseResponse.notAccepted({
                        res,
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.personReceiveNotFound,
                                field: "personReceiveId",
                            },
                        ],
                    });
                }
                next();
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    manyPersonIsSame(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { data } = req.body;
                for (let i = 0; i < data.length; i++) {
                    if (data[i].personCreateId === data[i].personReceiveId) {
                        return baseResponse.notAccepted({
                            res,
                            errors: [
                                {
                                    code: errorsCode_1.ErrorsCode.personIsSameId,
                                    field: "personReceiveId",
                                },
                            ],
                        });
                    }
                }
                next();
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    personIsSame(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { personCreateId, personReceiveId } = req.body;
                if (personCreateId === personReceiveId) {
                    return baseResponse.notAccepted({
                        res,
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.personIsSameId,
                                field: "personReceiveId",
                            },
                        ],
                    });
                }
                next();
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    manyAccountNumberIsSame(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { data } = req.body;
                for (let i = 0; i < data.length; i++)
                    if (data[i].receiveAccountNumber === data[i].debitAccountNumber) {
                        return baseResponse.notAccepted({
                            res,
                            errors: [
                                {
                                    code: errorsCode_1.ErrorsCode.middlewareReceiveAndDebitAccountNumberCanNotSame,
                                    field: "receiveAccountNumber",
                                },
                            ],
                        });
                    }
                next();
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    accountNumberIsSame(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { receiveAccountNumber, debitAccountNumber } = req.body;
                if (receiveAccountNumber === debitAccountNumber) {
                    return baseResponse.notAccepted({
                        res,
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.middlewareReceiveAndDebitAccountNumberCanNotSame,
                                field: "receiveAccountNumber",
                            },
                        ],
                    });
                }
                next();
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    userHaveCompanies(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { userId } = req.cookies;
                const { companyId } = req.body;
                const isUserHaveCompanies = yield companies_validate_1.default.isUserHaveCompanies(userId, companyId);
                if (!isUserHaveCompanies)
                    return baseResponse.notAccepted({
                        res,
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.companiesNotFound,
                                field: "companyId",
                            },
                        ],
                    });
                next();
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    existedAccountNumber(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { receiveAccountNumber, debitAccountNumber, accountNumberId, } = req.body;
                const [checkReceiveAccountNumber, checkDebitAccountNumber,] = yield Promise.all([
                    accountNumber_service_1.default.getOne({
                        _id: accountNumberId,
                        "accountNumbers.accountNumber": receiveAccountNumber,
                    }),
                    accountNumber_service_1.default.getOne({
                        _id: accountNumberId,
                        "accountNumbers.accountNumber": debitAccountNumber,
                    }),
                ]);
                if (receiveAccountNumber && !checkReceiveAccountNumber) {
                    return baseResponse.notAccepted({
                        res,
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.middlewareReceiveAccountNumberNotExisted,
                                field: "receiveAccountNumber",
                            },
                        ],
                    });
                }
                if (debitAccountNumber && !checkDebitAccountNumber) {
                    return baseResponse.notAccepted({
                        res,
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.middlewareDebitAccountNumberNotExisted,
                                field: "debitAccountNumber",
                            },
                        ],
                    });
                }
                next();
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    companyHaveAccountNumber(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { accountNumberId, companyId } = req.body;
                if (accountNumberId && companyId) {
                    const checkCompaniesAccountNumber = yield companies_service_1.default.getOne({
                        _id: companyId,
                        accountNumberId,
                    });
                    if (!checkCompaniesAccountNumber) {
                        return baseResponse.notAccepted({
                            res,
                            errors: [
                                {
                                    code: errorsCode_1.ErrorsCode.accountNumberNotExisted,
                                    field: "accountNumberId",
                                },
                            ],
                        });
                    }
                }
                next();
            }
            catch (error) {
                baseResponse.serverError(res, error);
            }
        });
    }
    userHaveCompaniesBank(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { userId } = req.cookies;
                const { companyBankId, companyId } = req.body;
                if (companyBankId && companyId) {
                    const checkCompaniesBank = yield companyBanking_service_1.default.getOne({
                        userCreateId: userId,
                        companyId: companyId,
                        _id: companyBankId,
                    });
                    if (!checkCompaniesBank) {
                        return baseResponse.notAccepted({
                            res,
                            errors: [
                                {
                                    code: errorsCode_1.ErrorsCode.companyBankingBankNotExisted,
                                    field: "companyBankId",
                                    error: "companies bank not existed with user",
                                },
                            ],
                        });
                    }
                }
                next();
            }
            catch (error) {
                baseResponse.serverError(res, error);
            }
        });
    }
    companyHaveStock(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { userId } = req.cookies;
                const { companyId, stockId } = req.body;
                const isCompanyHaveStock = yield stock_validate_1.default.isCompanyHaveStock(userId, companyId, stockId);
                if (!isCompanyHaveStock) {
                    return baseResponse.notAccepted({
                        res,
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.stockCompaniesNotHaveStock,
                                field: "stockId",
                            },
                        ],
                    });
                }
                next();
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
}
exports.default = new Middleware();
//# sourceMappingURL=middleware.js.map