"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.IsObjectHaveProperty = exports.Valid = void 0;
const class_validator_1 = require("class-validator");
const class_transformer_1 = require("class-transformer");
const responseService_1 = __importDefault(require("../services/responseService"));
const errorsCode_1 = require("../const/errorsCode");
const const_1 = require("../const");
function IsObjectHaveProperty(props, validationOptions) {
    return function (object, propertyName) {
        class_validator_1.registerDecorator({
            name: "IsObjectHaveProperty",
            target: object.constructor,
            propertyName: propertyName,
            constraints: [...props],
            options: validationOptions,
            validator: {
                validate(value, args) {
                    let propLength = props.length;
                    for (let i = 0; i < propLength; i++) {
                        if (value) {
                            if (!value && !value[props[i]])
                                return false;
                        }
                        else
                            return false;
                    }
                    return true;
                },
            },
        });
    };
}
exports.IsObjectHaveProperty = IsObjectHaveProperty;
function Valid(dtoClass, requestType, options = {}) {
    return function (req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            let data;
            if (requestType === 0 /* query */)
                data = req.query;
            if (requestType === 1 /* body */)
                data = req.body;
            if (requestType === 2 /* params */)
                data = req.params;
            data = class_transformer_1.plainToClass(dtoClass, data);
            const err = yield class_validator_1.validate(data, Object.assign({}, options));
            const errors = [];
            if (err.length > 0) {
                err.forEach((item) => {
                    errors.push({
                        field: item.property,
                        error: item.constraints[Object.keys(item.constraints)[0]],
                        code: errorsCode_1.ErrorsCode.invalid,
                    });
                });
                return responseService_1.default.send(res, const_1.HTTPCode.badRequest, {
                    errors: [...errors],
                });
            }
            next();
        });
    };
}
exports.Valid = Valid;
//# sourceMappingURL=Valid.js.map