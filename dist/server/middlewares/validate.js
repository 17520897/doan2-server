"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.validateRole = exports.usersValidate = void 0;
const logger_1 = __importDefault(require("../common/logger"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const errorsCode_1 = require("../const/errorsCode");
const responseService_1 = __importDefault(require("../services/responseService"));
const roles_service_1 = __importDefault(require("../api/roles/roles.service"));
const users_service_1 = __importDefault(require("../api/users/users.service"));
const const_1 = require("../const");
const admin_service_1 = __importDefault(require("../api/admin/admin.service"));
const systemConst_service_1 = __importDefault(require("../api/systemConst/systemConst.service"));
const userRoles_service_1 = __importDefault(require("../api/userRoles/userRoles.service"));
const baseResponse_1 = __importDefault(require("../services/baseResponse"));
const baseResponse = new baseResponse_1.default("Validate");
function usersValidate() {
    return (req, res, next) => {
        try {
            if (!req.headers["authorization"])
                return responseService_1.default.send(res, const_1.HTTPCode.unauthorize, {
                    message: "you are not authorize",
                });
            const headersToken = req.headers["authorization"].split(" ");
            if (headersToken[0] !== "Bearer")
                return responseService_1.default.send(res, const_1.HTTPCode.unauthorize, {
                    message: "you are not authorize",
                });
            if (!headersToken[1]) {
                return responseService_1.default.send(res, const_1.HTTPCode.unauthorize, {
                    message: "you are not authorize",
                });
            }
            const token = jsonwebtoken_1.default.verify(headersToken[1], process.env.JWT_SECRET);
            if (!token)
                return responseService_1.default.send(res, const_1.HTTPCode.unauthorize, {
                    message: "you are not authorize",
                });
            if (token.isAccess && token.isUser) {
                req.cookies.userId = token.userId;
                next();
            }
            else
                return responseService_1.default.send(res, const_1.HTTPCode.unauthorize, {
                    message: "you are not authorize",
                });
        }
        catch (error) {
            return responseService_1.default.send(res, const_1.HTTPCode.unauthorize, {
                message: "you are not authorize",
            });
        }
    };
}
exports.usersValidate = usersValidate;
function validateRole(key = null, accountType = const_1.AccountType.users) {
    return function (req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                if (req.headers["authorization"]) {
                    const headersToken = req.headers["authorization"].split(" ");
                    if (headersToken[0] !== "Bearer")
                        return responseService_1.default.send(res, const_1.HTTPCode.unauthorize, {
                            message: "you are not authorize",
                        });
                    if (!headersToken[1]) {
                        return responseService_1.default.send(res, const_1.HTTPCode.unauthorize, {
                            message: "you are not authorize",
                        });
                    }
                    const token = jsonwebtoken_1.default.verify(headersToken[1], process.env.JWT_SECRET);
                    if (!token ||
                        !token.isAccess ||
                        (accountType === const_1.AccountType.users && token.isUser === false) ||
                        (accountType === const_1.AccountType.admin && token.isAdmin === false))
                        return responseService_1.default.send(res, const_1.HTTPCode.unauthorize, {
                            message: "you are not authorize",
                            errors: [
                                {
                                    code: errorsCode_1.ErrorsCode.usersIsBlocked,
                                },
                            ],
                        });
                    if (accountType === const_1.AccountType.admin) {
                        // check if account type if admin
                        const { adminId } = token;
                        //check user existed;
                        const checkAdmin = yield admin_service_1.default.getOne({
                            _id: adminId,
                            status: const_1.AdminStatus.normal,
                        });
                        if (!checkAdmin) {
                            return responseService_1.default.send(res, const_1.HTTPCode.unauthorize, {
                                message: "you are not authorize",
                                errors: [
                                    {
                                        code: errorsCode_1.ErrorsCode.adminIsBlockedOrDeleted,
                                    },
                                ],
                            });
                        }
                        //check user have permissions;
                        if (key) {
                            const { roleId } = checkAdmin;
                            const checkPermission = yield roles_service_1.default.getOne({
                                _id: roleId,
                                "rolePermissions.permissionKey": key,
                                status: const_1.RoleStatus.enable,
                            });
                            if (!checkPermission) {
                                logger_1.default.info("Admin not have permission");
                                return responseService_1.default.send(res, const_1.HTTPCode.unauthorize, {
                                    message: "you are not authorize",
                                    errors: [
                                        {
                                            code: errorsCode_1.ErrorsCode.adminDidNotHavePermissions,
                                        },
                                    ],
                                });
                            }
                        }
                        req.cookies.adminId = adminId;
                        next();
                    }
                    else if (accountType === const_1.AccountType.users) {
                        // check if account type if users
                        const { userId } = token;
                        //check user existed;
                        const checkUsers = yield users_service_1.default.getOne({
                            _id: userId,
                            status: const_1.UsersStatus.normal,
                        });
                        if (!checkUsers) {
                            return responseService_1.default.send(res, const_1.HTTPCode.unauthorize, {
                                message: "you are not authorize",
                                errors: [
                                    {
                                        code: errorsCode_1.ErrorsCode.usersIsBlocked,
                                    },
                                ],
                            });
                        }
                        //check user have permissions;
                        if (key) {
                            const { roleId } = checkUsers;
                            const [checkPermission, systemConst] = yield Promise.all([
                                userRoles_service_1.default.getOne({
                                    _id: roleId,
                                    "permissions.permissionKey": key,
                                    status: const_1.UserRoleStatus.enable,
                                }),
                                systemConst_service_1.default.getOne({}),
                            ]);
                            if (!checkPermission) {
                                //check free roles have this permissions
                                const userFreeRoles = yield userRoles_service_1.default.getOne({
                                    _id: systemConst.userRoleFreeId,
                                    "permissions.permissionKey": key,
                                    status: const_1.UserRoleStatus.enable,
                                });
                                if (userFreeRoles) {
                                    req.cookies.userId = userId;
                                    next();
                                }
                                return responseService_1.default.send(res, const_1.HTTPCode.unauthorize, {
                                    message: "you are not authorize",
                                    errors: [
                                        {
                                            code: errorsCode_1.ErrorsCode.notAuthorize,
                                        },
                                    ],
                                });
                            }
                        }
                        req.cookies.userId = userId;
                        next();
                    }
                    else if (accountType === const_1.AccountType.all) {
                        req.cookies.userId = token.userId;
                        req.cookies.adminId = token.adminId;
                        next();
                    }
                    else {
                        return responseService_1.default.send(res, const_1.HTTPCode.unauthorize, {
                            message: "you are not authorize",
                            errors: [
                                {
                                    code: errorsCode_1.ErrorsCode.notAuthorize,
                                },
                            ],
                        });
                    }
                }
                else {
                    return responseService_1.default.send(res, const_1.HTTPCode.unauthorize, {
                        message: "you are not authorize",
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.notAuthorize,
                            },
                        ],
                    });
                }
            }
            catch (error) {
                return responseService_1.default.send(res, const_1.HTTPCode.unauthorize, {
                    message: "you are not authorize",
                });
            }
        });
    };
}
exports.validateRole = validateRole;
//# sourceMappingURL=validate.js.map