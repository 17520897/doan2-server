"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ConfigFeesActionCode = void 0;
var ConfigFeesActionCode;
(function (ConfigFeesActionCode) {
    ConfigFeesActionCode["configFeesActionRequestPawn"] = "configFeesActionRequestPawn";
    ConfigFeesActionCode["configFeesActionApprovePawn"] = "configFeesActionApprovePawn";
    ConfigFeesActionCode["configFeesActionInspectPawn"] = "configFeesActionInspectPawn";
})(ConfigFeesActionCode = exports.ConfigFeesActionCode || (exports.ConfigFeesActionCode = {}));
//# sourceMappingURL=configFeesActionCode.js.map