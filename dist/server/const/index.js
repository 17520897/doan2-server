"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.VerifyType = exports.VerifyAccountType = exports.StockType = exports.SettingBankStatus = exports.SMSType = exports.ProductsStatus = exports.PersonBankingStatus = exports.PersonType = exports.PersonStatus = exports.RoleStatus = exports.UserRoleStatus = exports.UsersStatus = exports.MoneyBillType = exports.MoneyTypeStatus = exports.HTTPCode = exports.CompanyBankingStatus = exports.CompaniesStatus = exports.CompanyTypeStatus = exports.BillFunction = exports.AdminStatus = exports.AccountType = void 0;
var AccountType;
(function (AccountType) {
    AccountType["users"] = "users";
    AccountType["admin"] = "admin";
    AccountType["all"] = "all";
})(AccountType = exports.AccountType || (exports.AccountType = {}));
var AdminStatus;
(function (AdminStatus) {
    AdminStatus["normal"] = "normal";
    AdminStatus["deleted"] = "deleted";
    AdminStatus["block"] = "block";
})(AdminStatus = exports.AdminStatus || (exports.AdminStatus = {}));
var BillFunction;
(function (BillFunction) {
    BillFunction["services"] = "services";
    BillFunction["fixedAssets"] = "fixedAssets";
    BillFunction["goods"] = "goods";
    BillFunction["material"] = "material";
})(BillFunction = exports.BillFunction || (exports.BillFunction = {}));
var CompanyTypeStatus;
(function (CompanyTypeStatus) {
    CompanyTypeStatus["normal"] = "normal";
    CompanyTypeStatus["deleted"] = "deleted";
})(CompanyTypeStatus = exports.CompanyTypeStatus || (exports.CompanyTypeStatus = {}));
var CompaniesStatus;
(function (CompaniesStatus) {
    CompaniesStatus["normal"] = "normal";
    CompaniesStatus["deleted"] = "deleted";
})(CompaniesStatus = exports.CompaniesStatus || (exports.CompaniesStatus = {}));
var CompanyBankingStatus;
(function (CompanyBankingStatus) {
    CompanyBankingStatus["normal"] = "normal";
    CompanyBankingStatus["deleted"] = "deleted";
})(CompanyBankingStatus = exports.CompanyBankingStatus || (exports.CompanyBankingStatus = {}));
var HTTPCode;
(function (HTTPCode) {
    HTTPCode[HTTPCode["success"] = 200] = "success";
    HTTPCode[HTTPCode["badRequest"] = 400] = "badRequest";
    HTTPCode[HTTPCode["notAccept"] = 406] = "notAccept";
    HTTPCode[HTTPCode["unauthorize"] = 401] = "unauthorize";
    HTTPCode[HTTPCode["serverError"] = 500] = "serverError";
})(HTTPCode = exports.HTTPCode || (exports.HTTPCode = {}));
var MoneyTypeStatus;
(function (MoneyTypeStatus) {
    MoneyTypeStatus["normal"] = "normal";
    MoneyTypeStatus["deleted"] = "deleted";
})(MoneyTypeStatus = exports.MoneyTypeStatus || (exports.MoneyTypeStatus = {}));
var MoneyBillType;
(function (MoneyBillType) {
    MoneyBillType["receipt"] = "receipt";
    MoneyBillType["expenses"] = "expenses";
})(MoneyBillType = exports.MoneyBillType || (exports.MoneyBillType = {}));
var UsersStatus;
(function (UsersStatus) {
    UsersStatus["normal"] = "normal";
    UsersStatus["block"] = "block";
})(UsersStatus = exports.UsersStatus || (exports.UsersStatus = {}));
var UserRoleStatus;
(function (UserRoleStatus) {
    UserRoleStatus["enable"] = "enable";
    UserRoleStatus["disable"] = "disable";
    UserRoleStatus["delete"] = "delete";
})(UserRoleStatus = exports.UserRoleStatus || (exports.UserRoleStatus = {}));
var RoleStatus;
(function (RoleStatus) {
    RoleStatus["enable"] = "enable";
    RoleStatus["disable"] = "disable";
    RoleStatus["delete"] = "delete";
})(RoleStatus = exports.RoleStatus || (exports.RoleStatus = {}));
var PersonStatus;
(function (PersonStatus) {
    PersonStatus["normal"] = "normal";
    PersonStatus["deleted"] = "deleted";
})(PersonStatus = exports.PersonStatus || (exports.PersonStatus = {}));
var PersonType;
(function (PersonType) {
    PersonType["people"] = "people";
    PersonType["company"] = "company";
    PersonType["employee"] = "employee";
})(PersonType = exports.PersonType || (exports.PersonType = {}));
var PersonBankingStatus;
(function (PersonBankingStatus) {
    PersonBankingStatus["normal"] = "normal";
    PersonBankingStatus["deleted"] = "deleted";
})(PersonBankingStatus = exports.PersonBankingStatus || (exports.PersonBankingStatus = {}));
var ProductsStatus;
(function (ProductsStatus) {
    ProductsStatus["normal"] = "normal";
    ProductsStatus["deleted"] = "deleted";
})(ProductsStatus = exports.ProductsStatus || (exports.ProductsStatus = {}));
var SMSType;
(function (SMSType) {
    SMSType["register"] = "register";
    SMSType["forgotPassword"] = "forgotPassword";
})(SMSType = exports.SMSType || (exports.SMSType = {}));
var SettingBankStatus;
(function (SettingBankStatus) {
    SettingBankStatus["normal"] = "normal";
    SettingBankStatus["deleted"] = "deleted";
})(SettingBankStatus = exports.SettingBankStatus || (exports.SettingBankStatus = {}));
var StockType;
(function (StockType) {
    StockType["services"] = "services";
    StockType["fixedAssets"] = "fixedAssets";
    StockType["goods"] = "goods";
    StockType["material"] = "material";
})(StockType = exports.StockType || (exports.StockType = {}));
var VerifyAccountType;
(function (VerifyAccountType) {
    VerifyAccountType["user"] = "user";
})(VerifyAccountType = exports.VerifyAccountType || (exports.VerifyAccountType = {}));
var VerifyType;
(function (VerifyType) {
    VerifyType["register"] = "register";
    VerifyType["forgotPassword"] = "forgotPassword";
})(VerifyType = exports.VerifyType || (exports.VerifyType = {}));
//# sourceMappingURL=index.js.map