"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.PermissionKeys = void 0;
var PermissionKeys;
(function (PermissionKeys) {
    PermissionKeys["adminGetList"] = "adminGetList";
    PermissionKeys["adminCreate"] = "adminCreate";
    PermissionKeys["adminUpdate"] = "adminUpdate";
    PermissionKeys["adminDelete"] = "adminDelete";
    PermissionKeys["companyTypeGetList"] = "companyTypeGetList";
    PermissionKeys["companyTypeCreate"] = "companyTypeCreate";
    PermissionKeys["companyTypeUpdate"] = "companyTypeUpdate";
    PermissionKeys["companyTypeDelete"] = "companyTypeDelete";
    PermissionKeys["roleGetList"] = "roleGetList";
    PermissionKeys["roleCreate"] = "roleCreate";
    PermissionKeys["roleDisableEnable"] = "roleDisableEnable";
    PermissionKeys["roleUpdate"] = "roleUpdate";
    PermissionKeys["roleDelete"] = "roleDelete";
    PermissionKeys["systemConstGetList"] = "systemConstGetList";
    PermissionKeys["systemConstUpdate"] = "systemConstUpdate";
    PermissionKeys["userRoleGetList"] = "userRoleGetList";
    PermissionKeys["userRoleCreate"] = "userRoleCreate";
    PermissionKeys["userRoleDisableEnable"] = "userRoleDisableEnable";
    PermissionKeys["userRoleUpdate"] = "userRoleUpdate";
    PermissionKeys["userRoleDelete"] = "userRoleDelete";
    PermissionKeys["moneyTypeGetList"] = "moneyTypeGetList";
    PermissionKeys["moneyTypeCreate"] = "moneyTypeCreate";
    PermissionKeys["moneyTypeUpdate"] = "moneyTypeUpdate";
    PermissionKeys["moneyTypeDelete"] = "moneyTypeDelete";
    PermissionKeys["accountNumberCreate"] = "accountNumberCreate";
    PermissionKeys["accountNumberUpdate"] = "accountNumberUpdate";
    PermissionKeys["accountNumberDelete"] = "accountNumberDelete";
    PermissionKeys["settingTaxCreate"] = "settingTaxCreate";
    PermissionKeys["settingTaxUpdate"] = "settingTaxUpdate";
    PermissionKeys["settingTaxDelete"] = "settingTaxDelete";
    PermissionKeys["settingBankCreate"] = "settingBankCreate";
    PermissionKeys["settingBankUpdate"] = "settingBankUpdate";
    PermissionKeys["settingBankDelete"] = "settingBankDelete";
})(PermissionKeys = exports.PermissionKeys || (exports.PermissionKeys = {}));
//# sourceMappingURL=permissions.js.map