"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ErrorsCode = void 0;
var ErrorsCode;
(function (ErrorsCode) {
    ErrorsCode["invalid"] = "invalid";
    ErrorsCode["serverError"] = "serverError";
    ErrorsCode["locationNotFound"] = "locationNotFound";
    ErrorsCode["notAuthorize"] = "notAuthorize";
    //admin
    ErrorsCode["adminUserNameExisted"] = "adminUserNameExisted";
    ErrorsCode["adminDidNotHavePermissions"] = "adminDidNotHavePermissions";
    ErrorsCode["adminNotFound"] = "adminNotFound";
    ErrorsCode["adminWrongPassword"] = "adminWrongPassword";
    ErrorsCode["adminIsBlockedOrDeleted"] = "adminIsBlockedOrDeleted";
    //account number
    ErrorsCode["accountNumberCircularsExisted"] = "accountNumberCircularsExisted";
    ErrorsCode["accountNumberArrayAccountNumbersWrongFormat"] = "accountNumberArrayAccountNumbersWrongFormat";
    ErrorsCode["accountNumberNotExisted"] = "accountNumberNotExisted";
    //serviceBill
    ErrorsCode["billCompaniesNotHaveBill"] = "billCompaniesNotHaveBill";
    ErrorsCode["billExisted"] = "billExisted";
    //company type
    ErrorsCode["companyTypeNameExisted"] = "companyTypeNameExisted";
    //companies
    ErrorsCode["companiesNameExisted"] = "companiesNameExisted";
    ErrorsCode["companiesTaxCodeExisted"] = "companiesTaxCodeExisted";
    ErrorsCode["companiesNotFound"] = "companiesNotFound";
    ErrorsCode["companiesUserNotEnoughCompany"] = "companiesUserNotEnoughCompany";
    ErrorsCode["companiesIsWrongAddress"] = "companiesIsWrongAddress";
    //companyBanking
    ErrorsCode["companyBankingBankNotExisted"] = "companyBankingBankNotExisted";
    ErrorsCode["companyBankingBankAccountExisted"] = "companyBankingBankAccountExisted";
    ErrorsCode["companyBankingNotFound"] = "companyBankingNotFound";
    //email
    ErrorsCode["emailCanNotSend"] = "emailCanNotSend";
    //forwardTax
    ErrorsCode["forwardTaxNotFound"] = "forwardTaxNotFound";
    //location
    ErrorsCode["locationWardNotInThisDistrict"] = "locationWardNotInThisDistrict";
    ErrorsCode["locationCodeNotFound"] = "locationCodeNotFound";
    ErrorsCode["locationDistrictNotInThisCity"] = "locationDistrictNotInThisCity";
    //money type
    ErrorsCode["moneyTypeNotExisted"] = "moneyTypeNotExisted";
    ErrorsCode["moneyTypeNameExisted"] = "moneyTypeNameExisted";
    //money bill
    ErrorsCode["moneyBillUserNotHaveThisCompany"] = "moneyBillUserNotHaveThisCompany";
    ErrorsCode["moneyBillAccountNumberNotExisted"] = "moneyBillAccountNumberNotExisted";
    ErrorsCode["moneyBillReceiveAccountNumberNotExisted"] = "moneyBillReceiveAccountNumberNotExisted";
    ErrorsCode["moneyBillDebitAccountNumberNotExisted"] = "moneyBillDebitAccountNumberNotExisted";
    ErrorsCode["moneyBillReceiveAndDebitAccountNumberCanNotSame"] = "moneyBillReceiveAndDebitAccountNumberCanNotSame";
    ErrorsCode["moneyBillNotFound"] = "moneyBillNotFound";
    ErrorsCode["moneyBillFromFileNotHaveData"] = "moneyBillFromFileNotHaveData";
    ErrorsCode["moneyBillFromFileWrongDateFormat"] = "moneyBillFromFileWrongDateFormat";
    ErrorsCode["moneyBillFromFileNotExistedAccountNumber"] = "moneyBillFromFileNotExistedAccountNumber";
    ErrorsCode["moneyBillFromFileNotAccountNumberIsSame"] = "moneyBillFromFileNotAccountNumberIsSame";
    ErrorsCode["moneyBillFromFileMoneyMustGreaterThanZero"] = "moneyBillFromFileMoneyMustGreaterThanZero";
    //person
    ErrorsCode["personNameExisted"] = "personNameExisted";
    ErrorsCode["personCodeExisted"] = "personCodeExisted";
    ErrorsCode["personNotFound"] = "personNotFound";
    ErrorsCode["personCompanyNotFound"] = "personCompanyNotFound";
    ErrorsCode["personCreateNotFound"] = "personCreateNotFound";
    ErrorsCode["personReceiveNotFound"] = "personReceiveNotFound";
    ErrorsCode["personIsSameId"] = "personIsSameId";
    //personBanking
    ErrorsCode["personBankingBankNotExisted"] = "personBankingBankNotExisted";
    ErrorsCode["personBankingBankAccountExisted"] = "personBankingBankAccountExisted";
    ErrorsCode["personBankingNotFound"] = "personBankingNotFound";
    //sms
    ErrorsCode["smsCanNotSend"] = "smsCanNotSend";
    //users
    ErrorsCode["usersWrongPassword"] = "userWrongPassword";
    ErrorsCode["usersEmailRegistered"] = "usersEmailRegistered";
    ErrorsCode["usersNotFound"] = "usersNotFound";
    ErrorsCode["usersIsBlocked"] = "usersIsBlocked";
    ErrorsCode["usersDidNotHavePermission"] = "usersDidNotHavePermission";
    //verify
    ErrorsCode["verifyCodeHasSent"] = "verifyCodeHasSent";
    ErrorsCode["verifyCodeNotFound"] = "verifyCodeNotFound";
    ErrorsCode["verifyCantForgotPassword"] = "verifyCantForgotPassword";
    ErrorsCode["verifyEmailHasBeenRegistered"] = "verifyEmailHasBeenRegistered";
    //admin roles
    ErrorsCode["rolesHasBeenExisted"] = "rolesHasBeenExisted";
    ErrorsCode["roleNotAcceptUpdate"] = "roleNotAcceptUpdate";
    ErrorsCode["roleNotAcceptDisable"] = "roleNotAcceptDisable";
    ErrorsCode["roleNotAcceptDelete"] = "roleNotAcceptDelete";
    //systemConst
    ErrorsCode["systemConstUserRolesNotExisted"] = "systemConstUserRolesNotExisted";
    //settingTax
    ErrorsCode["settingTaxNameExisted"] = "settingTaxNameExisted";
    ErrorsCode["settingTaxNotFound"] = "settingTaxNotFound";
    ErrorsCode["settingTaxAccountNumberNotFound"] = "settingTaxAccountNumberNotFound";
    //serviceBill
    ErrorsCode["serviceBillCompaniesNotHaveServiceBill"] = "serviceBillCompaniesNotHaveServiceBill";
    //settingBank
    ErrorsCode["settingBankNameExisted"] = "settingBankNameExisted";
    ErrorsCode["settingBankNotFound"] = "settingBankNotFound";
    //stock
    ErrorsCode["stockCodeExisted"] = "stockCodeExisted";
    ErrorsCode["stockCompaniesNotHaveStock"] = "stockCompaniesNotHaveStock";
    //user roles
    ErrorsCode["userRolesHasBeenExisted"] = "userRolesHasBeenExisted";
    ErrorsCode["userRoleNotAcceptUpdate"] = "userRoleNotAcceptUpdate";
    ErrorsCode["userRoleNotAcceptDisable"] = "userRoleNotAcceptDisable";
    ErrorsCode["userRoleNotAcceptDelete"] = "userRoleNotAcceptDelete";
    //files
    ErrorsCode["filesOnlyAcceptImageFiles"] = "filesOnlyAcceptImageFiles";
    ErrorsCode["filesOnlyAcceptXlsxFiles"] = "filesOnlyAcceptXlsxFiles";
    ErrorsCode["filesNotFound"] = "filesNotFound";
    ErrorsCode["filesWrongFormat"] = "filesWrongFormat";
    ErrorsCode["filesNotHaveData"] = "filesNotHaveData";
    //middleware
    ErrorsCode["middlewareReceiveAndDebitAccountNumberCanNotSame"] = "middlewareReceiveAndDebitAccountNumberCanNotSame";
    ErrorsCode["middlewareReceiveAccountNumberNotExisted"] = "middlewareReceiveAccountNumberNotExisted";
    ErrorsCode["middlewareDebitAccountNumberNotExisted"] = "middlewareDebitAccountNumberNotExisted";
})(ErrorsCode = exports.ErrorsCode || (exports.ErrorsCode = {}));
//# sourceMappingURL=errorsCode.js.map