"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const bcrypt_1 = __importDefault(require("bcrypt"));
const accountNumber_service_1 = __importDefault(require("../api/accountNumber/accountNumber.service"));
const companies_service_1 = __importDefault(require("../api/companies/companies.service"));
const location_service_1 = __importDefault(require("../api/location/location.service"));
const const_1 = require("../const");
class Services {
    fileUploadDir(req) {
        const { filename } = req.file;
        if (filename === "null") {
            return "";
        }
        return filename === "null" ? "" : `${process.env.UPLOAD_DIR}/${filename}`;
    }
    hashPassword(password, salt = 10) {
        return __awaiter(this, void 0, void 0, function* () {
            const hashSalt = yield bcrypt_1.default.genSalt(salt);
            const hashPassword = yield bcrypt_1.default.hash(password, hashSalt);
            return hashPassword;
        });
    }
    verifyPassword(password, hashPassword) {
        return __awaiter(this, void 0, void 0, function* () {
            const compare = yield bcrypt_1.default.compare(password, hashPassword);
            return compare;
        });
    }
    isRightAddress(address) {
        const checkAddress = location_service_1.default.getByAddress({
            city: address.city,
            district: address.district,
            ward: address.ward,
        });
        return checkAddress
            ? { success: true, address: checkAddress }
            : { success: false };
    }
    isRightAccountNumber(accountNumber, accountNumberId) {
        return __awaiter(this, void 0, void 0, function* () {
            const checkAccountNumber = yield accountNumber_service_1.default.getOne({
                _id: accountNumberId,
                "accountNumbers.accountNumber": accountNumber,
            });
            return checkAccountNumber ? true : false;
        });
    }
    isUserHaveCompany(userId, companyId) {
        return __awaiter(this, void 0, void 0, function* () {
            const company = yield companies_service_1.default.getOne({
                userCreateId: userId,
                status: const_1.CompaniesStatus.normal,
                _id: companyId,
            });
            return company ? true : false;
        });
    }
}
exports.default = new Services();
//# sourceMappingURL=services.js.map