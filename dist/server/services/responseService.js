"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ResponseService = void 0;
const const_1 = require("../const");
class ResponseService {
    send(res, statusCode, response = {}) {
        return res.status(statusCode).send({
            message: response.message,
            data: response.data ? response.data : null,
            errors: response.errors,
            timestamp: Date.now(),
            success: statusCode == const_1.HTTPCode.success ? true : false,
        });
    }
}
exports.ResponseService = ResponseService;
exports.default = new ResponseService();
//# sourceMappingURL=responseService.js.map