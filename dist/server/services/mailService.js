"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SendMail = void 0;
const nodemailer_1 = __importDefault(require("nodemailer"));
const log4js_1 = __importDefault(require("../common/log4js"));
const logger_1 = __importDefault(require("../common/logger"));
const errorsCode_1 = require("../const/errorsCode");
const baseResponse_1 = __importDefault(require("./baseResponse"));
const baseResponse = new baseResponse_1.default("MailService");
class SendMail {
    send(iSendMail) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { to, subject, text, html } = iSendMail;
                const transporter = nodemailer_1.default.createTransport({
                    host: "smtp.gmail.com",
                    port: 587,
                    secure: false,
                    requireTLS: true,
                    auth: {
                        user: process.env.NODE_ENV === "dev"
                            ? process.env.DEV_EMAIL_USERNAME
                            : process.env.EMAIL_USERNAME,
                        pass: process.env.NODE_ENV === "dev"
                            ? process.env.DEV_EMAIL_PASSWORD
                            : process.env.EMAIL_PASSWORD,
                    },
                });
                let response = yield transporter.sendMail({
                    from: `PD Accounting <${process.env.NODE_ENV === "dev"
                        ? process.env.DEV_EMAIL_USERNAME
                        : process.env.EMAIL_USERNAME}>`,
                    to,
                    subject,
                    text,
                    html,
                });
                return {
                    message: "send mail success",
                    data: response,
                };
            }
            catch (error) {
                logger_1.default.error(`Mail Service: ${error.message}`);
                log4js_1.default("MailService: ").error(error);
                return {
                    errors: [
                        {
                            code: errorsCode_1.ErrorsCode.serverError,
                            error: error.message,
                        },
                    ],
                };
            }
        });
    }
}
exports.SendMail = SendMail;
exports.default = new SendMail();
//# sourceMappingURL=mailService.js.map