"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ErrorHandler = exports.errorHandler = void 0;
const errorsCode_1 = require("../const/errorsCode");
const baseResponse_1 = __importDefault(require("./baseResponse"));
function errorHandler(err, res) {
    const baseResponse = new baseResponse_1.default("ErrorHandler");
    baseResponse.serverError(res, err);
}
exports.errorHandler = errorHandler;
class ErrorHandler extends Error {
    constructor(message) {
        super();
        this.errors = [
            {
                code: errorsCode_1.ErrorsCode.serverError,
                error: message,
            },
        ];
    }
}
exports.ErrorHandler = ErrorHandler;
//# sourceMappingURL=errorHandler.js.map