"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SendSMS = void 0;
const request_1 = __importDefault(require("request"));
const sms_service_1 = __importDefault(require("../api/sms/sms.service"));
const const_1 = require("../const");
const errorsCode_1 = require("../const/errorsCode");
const baseResponse_1 = __importDefault(require("./baseResponse"));
function doRequest(options) {
    return new Promise((resolve, reject) => {
        request_1.default(Object.assign(Object.assign({}, options), { timeout: 3000 }), (error, response, body) => {
            if (error) {
                reject(error);
            }
            resolve(body);
        });
    });
}
const baseResponse = new baseResponse_1.default("SMS");
class SendSMS {
    send(phone, code, type = const_1.SMSType.register) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                if (process.env.NODE_ENV !== "dev") {
                    let message = `Chao mung ban den voi ung dung PAWN P2P. Vui long nhap ma ${code} de dang ky tai khoan.`;
                    if (type === const_1.SMSType.forgotPassword) {
                        message = `Khong chia se OTP voi bat ky ai. Nhap ma ${code} de dat lai mat khau`;
                    }
                    let messageBase64Encode = Buffer.from(message).toString("base64");
                    let body = '<soapenv:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance\\" xmlns:xsd="http://www.w3.org/2001/XMLSchema\\" xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/\\" xmlns:mts="MTService\\">\r\n\t<soapenv:Header/>\r\n\t<soapenv:Body>\r\n\t<mts:sendMT soapenv:encodingStyle="http://schemas.xmlsoap.org/soap/encoding/\\">\r\n\t<string xsi:type="xsd:string\\">' +
                        phone +
                        '</string>\r\n\t<string0 xsi:type="xsd:string\\">' +
                        messageBase64Encode +
                        '</string0>\r\n\t<string1 xsi:type="xsd:string\\">CODOSA</string1>\r\n\t<string2 xsi:type="xsd:string\\">CODOSA</string2>\r\n\t<string3 xsi:type="xsd:string\\">0</string3>\r\n\t<string4 xsi:type="xsd:string\\">0</string4>\r\n\t<string5 xsi:type="xsd:string\\">0</string5>\r\n\t<string6 xsi:type="xsd:string\\">0</string6>\r\n\t<string7 xsi:type="xsd:string\\">0</string7>\r\n\t<string8 xsi:type="xsd:string\\">0</string8>\r\n\t</mts:sendMT>\r\n\t</soapenv:Body>\r\n</soapenv:Envelope>';
                    var options = {
                        method: "POST",
                        url: "http://sms.8x77.vn:8077/mt-services/MTService",
                        headers: {
                            "Postman-Token": "d98732b6-67c6-4e4a-85ab-252e56813fe7",
                            "cache-control": "no-cache",
                            Authorization: "Basic Y29kb3NhOkNPRE9TQUAjKCkxMjM=",
                            "Content-Type": "text/xml;charset=utf-8",
                        },
                        body,
                    };
                    let response = yield doRequest(options);
                    sms_service_1.default.create({
                        phone,
                        code,
                        type: type === const_1.SMSType.register
                            ? const_1.SMSType.register
                            : const_1.SMSType.forgotPassword,
                    });
                    console.log("sms mes: ", message);
                    console.log("sms res: ", response);
                    return {
                        message: "send sms success",
                        data: response,
                    };
                }
                else {
                    return {
                        message: "send sms success",
                    };
                }
            }
            catch (error) {
                return {
                    errors: [
                        {
                            code: errorsCode_1.ErrorsCode.serverError,
                            error: error.message,
                        },
                    ],
                };
            }
        });
    }
}
exports.SendSMS = SendSMS;
exports.default = new SendSMS();
//# sourceMappingURL=sendSms.js.map