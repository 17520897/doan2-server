"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BaseDbService = void 0;
class BaseDbService {
    constructor(db) {
        this.db = db;
    }
    /**
     *
     * @param query
     * @type T
     * @param selected
     * @returns
     */
    getOne(query, selected = "") {
        return __awaiter(this, void 0, void 0, function* () {
            const doc = yield this.db.findOne(query, selected).lean();
            return doc;
        });
    }
    getAllByQuery(query) {
        return __awaiter(this, void 0, void 0, function* () {
            const doc = yield this.db.find(query).lean();
            return doc;
        });
    }
    getByQuery(iQuery) {
        return __awaiter(this, void 0, void 0, function* () {
            const { sort, query, options } = iQuery;
            let { page, limit } = iQuery;
            if (page && limit) {
                page = parseInt(page);
                limit = parseInt(limit);
                const doc = yield this.db
                    .find(query, options)
                    .sort(sort)
                    .skip(page * limit)
                    .limit(limit)
                    .lean();
                const count = yield this.db.find(query).countDocuments();
                return [
                    ...doc,
                    {
                        totalPage: Math.ceil(count / limit),
                        totalRecord: count,
                    },
                ];
            }
            else {
                const doc = yield this.db.find(query, options).sort(sort).lean();
                return doc;
            }
        });
    }
    getById(id, selected = "") {
        return __awaiter(this, void 0, void 0, function* () {
            const doc = (yield this.db.findById(id, selected).lean());
            return doc;
        });
    }
    getSort(query, sort = {}) {
        return __awaiter(this, void 0, void 0, function* () {
            const doc = yield this.db.find(query).sort(sort);
            return doc;
        });
    }
    create(data, options = {}) {
        return __awaiter(this, void 0, void 0, function* () {
            const newDoc = new this.db(data);
            const doc = (yield newDoc.save(options));
            return doc;
        });
    }
    createMany(data, options = {}) {
        return __awaiter(this, void 0, void 0, function* () {
            const doc = yield this.db.create(data, options);
            return doc;
        });
    }
    updateOne(query, data, options = {}) {
        return __awaiter(this, void 0, void 0, function* () {
            const doc = (yield this.db
                .findOneAndUpdate(query, data, Object.assign({ new: true }, options))
                .lean());
            return doc;
        });
    }
    updateById(id, data, options = {}) {
        return __awaiter(this, void 0, void 0, function* () {
            const doc = (yield this.db
                .findByIdAndUpdate(id, data, Object.assign({ new: true }, options))
                .lean());
            return doc;
        });
    }
    populate(iPopulate) {
        return __awaiter(this, void 0, void 0, function* () {
            const { query, sort, select, populate } = iPopulate;
            let { page, limit } = iPopulate;
            if (page && limit) {
                page = parseInt(page);
                limit = parseInt(limit);
                const doc = yield this.db
                    .find(query)
                    .populate(populate)
                    .select(select)
                    .sort(sort)
                    .skip(page * limit)
                    .limit(limit)
                    .exec();
                const count = yield this.db.find(query).countDocuments();
                return [
                    ...doc,
                    {
                        totalPage: Math.ceil(count / limit),
                        totalRecord: count,
                    },
                ];
            }
            else {
                const doc = yield this.db
                    .find(query)
                    .populate(populate)
                    .select(select)
                    .sort(sort)
                    .exec();
                return doc;
            }
        });
    }
    deleteById(id, options = {}) {
        return __awaiter(this, void 0, void 0, function* () {
            const doc = yield this.db.findByIdAndDelete(id, options);
            return doc;
        });
    }
    deleteMany(query, options) {
        return __awaiter(this, void 0, void 0, function* () {
            const doc = yield this.db.deleteMany(query, options);
            return doc;
        });
    }
    deleteByQuery(query, options = {}) {
        return __awaiter(this, void 0, void 0, function* () {
            const doc = yield this.db.findOneAndDelete(query, options);
            return doc;
        });
    }
    count(query) {
        return __awaiter(this, void 0, void 0, function* () {
            const doc = yield this.db.find(query).count();
            return doc;
        });
    }
    distinct(unique, query = {}) {
        return __awaiter(this, void 0, void 0, function* () {
            const doc = yield this.db.distinct(unique, query);
            return doc;
        });
    }
}
exports.BaseDbService = BaseDbService;
exports.default = BaseDbService;
//# sourceMappingURL=baseDb.js.map