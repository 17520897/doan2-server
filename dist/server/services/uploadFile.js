"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UploadFile = void 0;
const multer_1 = __importDefault(require("multer"));
const randomstring_1 = __importDefault(require("randomstring"));
class UploadFile {
    uploadFile(checkType = [], changeName = true, prefix = "") {
        var placeStore = process.env.UPLOAD_DIR;
        const storage = multer_1.default.diskStorage({
            destination: function (req, _file, cb) {
                cb(null, placeStore);
            },
            filename: function (req, file, cb) {
                const fileName = file.originalname.split(".");
                const newFileName = `${randomstring_1.default.generate(10)}-${new Date().getTime()}`;
                if (checkType) {
                    if (checkType.indexOf(fileName[fileName.length - 1]) > -1) {
                        if (changeName)
                            cb(null, `${newFileName}.${fileName[fileName.length - 1]}`);
                        else {
                            const { userId, adminId } = req.cookies;
                            cb(null, userId
                                ? `${prefix}-${userId}.${fileName[fileName.length - 1]}`
                                : `${prefix}-${adminId}.${fileName[fileName.length - 1]}`);
                        }
                    }
                    else {
                        cb(null, `null`);
                    }
                }
                else {
                    cb(null, `${newFileName}.${fileName[fileName.length - 1]}`);
                }
            },
        });
        return multer_1.default({ storage: storage });
    }
}
exports.UploadFile = UploadFile;
exports.default = new UploadFile();
//# sourceMappingURL=uploadFile.js.map