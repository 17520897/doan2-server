// import l from "../common/logger";
// import log4j from "../common/log4js";
// import { FireStoreAccountType, FireStoreNotifyType } from "../const/index";
// import fireBaseDbService from "./fireBaseDbService";
// const baseResponse = new BaseResponse(`fireStoreNotify`);
// interface IFireStoreData {
//   id?: String;
//   //approve - reject deposit withdraw
//   isApprove?: Boolean;
//   money?: Number;
//   approveAt?: Number;
//   //update data
//   isUpdate?: Boolean;
//   updateAt?: Number;
// }
// interface ISendNotify {
//   accountType: FireStoreAccountType;
//   data: IFireStoreData;
//   notifyType: FireStoreNotifyType;
//   id: string;
// }
// export class FireStoreService {
//   async sendNotify(iSendNotify: ISendNotify) {
//     try {
//       const { accountType, data, notifyType, id } = iSendNotify;
//       const docRef = fireBaseDbService
//         .collection(process.env.NODE_ENV === "dev" ? "devNotify" : "notify")
//         .doc(accountType.toString())
//         .collection(id.toString())
//         .doc();
//       await docRef.set({
//         timestamp: Date.now(),
//         data,
//         notifyType,
//       });
//     } catch (error) {
//
//
//     }
//   }
// }
// export default new FireStoreService();
//# sourceMappingURL=fireStoreService.js.map