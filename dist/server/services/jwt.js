"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const generateAccessToken = function (data, options = {}) {
    return jsonwebtoken_1.default.sign(Object.assign(Object.assign({}, data), { isAccess: true }), process.env.JWT_SECRET, Object.assign({ expiresIn: process.env.ACCESS_TOKEN_EXPIRE }, options));
};
const generateRefreshToken = function (data, options = {}) {
    return jsonwebtoken_1.default.sign(Object.assign(Object.assign({}, data), { isRefresh: true }), process.env.JWT_SECRET, Object.assign({ expiresIn: process.env.REFRESH_TOKEN_EXPIRE }, options));
};
exports.default = {
    generateAccessToken,
    generateRefreshToken
};
//# sourceMappingURL=jwt.js.map