"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const log4js_1 = __importDefault(require("../common/log4js"));
const logger_1 = __importDefault(require("../common/logger"));
const const_1 = require("../const");
const errorsCode_1 = require("../const/errorsCode");
const responseService_1 = __importDefault(require("./responseService"));
const sentry_1 = __importDefault(require("./sentry"));
class BaseResponse {
    constructor(module) {
        this.module = module;
        this.log = log4js_1.default(this.module);
    }
    serverError(res, error) {
        return __awaiter(this, void 0, void 0, function* () {
            logger_1.default.error(`${this.module} server error: ${error.message}`);
            this.log.error(`server error: `, error);
            sentry_1.default.captureException(error);
            return responseService_1.default.send(res, const_1.HTTPCode.serverError, {
                errors: [
                    {
                        code: errorsCode_1.ErrorsCode.serverError,
                        error: error.message,
                    },
                ],
                success: false,
            });
        });
    }
    success(response) {
        return __awaiter(this, void 0, void 0, function* () {
            return responseService_1.default.send(response.res, const_1.HTTPCode.success, {
                message: response.message,
                data: response.data,
                success: true,
            });
        });
    }
    notAccepted(response) {
        return __awaiter(this, void 0, void 0, function* () {
            return responseService_1.default.send(response.res, const_1.HTTPCode.notAccept, {
                message: response.message,
                errors: response.errors,
                success: false,
            });
        });
    }
}
exports.default = BaseResponse;
//# sourceMappingURL=baseResponse.js.map