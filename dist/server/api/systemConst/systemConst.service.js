"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SystemConstService = void 0;
const baseDb_1 = __importDefault(require("../../services/baseDb"));
const systemConst_model_1 = require("./systemConst.model");
class SystemConstService extends baseDb_1.default {
}
exports.SystemConstService = SystemConstService;
exports.default = new SystemConstService(systemConst_model_1.SystemConst);
//# sourceMappingURL=systemConst.service.js.map