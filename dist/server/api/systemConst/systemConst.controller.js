"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const baseResponse_1 = __importDefault(require("../../services/baseResponse"));
const const_1 = require("../../const");
const responseService_1 = __importDefault(require("../../services/responseService"));
const systemConst_service_1 = __importDefault(require("./systemConst.service"));
const baseResponse = new baseResponse_1.default("SystemConstController");
class SystemConstController {
    getSystemConst(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const systemConst = yield systemConst_service_1.default.getOne({});
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    data: systemConst,
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    updateSystemConst(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const systemConst = yield systemConst_service_1.default.updateOne({}, req.body);
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    data: systemConst,
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
}
exports.default = new SystemConstController();
//# sourceMappingURL=systemConst.controller.js.map