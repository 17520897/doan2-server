"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const const_1 = require("../../const");
const permissions_1 = require("../../const/permissions");
const Valid_1 = require("../../middlewares/Valid");
const validate_1 = require("../../middlewares/validate");
const updateSystemConst_dto_1 = __importDefault(require("./dto/updateSystemConst.dto"));
const systemConst_controller_1 = __importDefault(require("./systemConst.controller"));
const systemConst_middleware_1 = __importDefault(require("./systemConst.middleware"));
const systemConstRouter = express_1.default.Router();
systemConstRouter.get("/", validate_1.validateRole(permissions_1.PermissionKeys.systemConstGetList, const_1.AccountType.admin), systemConst_controller_1.default.getSystemConst);
systemConstRouter.put("/", Valid_1.Valid(updateSystemConst_dto_1.default, 1 /* body */), validate_1.validateRole(permissions_1.PermissionKeys.systemConstUpdate, const_1.AccountType.admin), systemConst_middleware_1.default.updateSystemConst, systemConst_controller_1.default.updateSystemConst);
exports.default = systemConstRouter;
//# sourceMappingURL=systemConst.router.js.map