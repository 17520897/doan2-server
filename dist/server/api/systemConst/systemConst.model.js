"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SystemConst = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
const schema = new mongoose_1.default.Schema({
    userTotalFreeCompany: {
        type: Number,
        default: 0,
    },
    userRoleFreeId: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
    },
    userTotalFreeBill: {
        type: Number,
        default: 0,
    },
}, {
    collection: "SystemConst",
});
schema.virtual("userRoleFreeInfo", {
    ref: "UserRoles",
    localField: "userRoleFreeId",
    foreignField: "_id",
    justOne: true,
});
schema.set("toObject", { virtuals: true });
schema.set("toJSON", { virtuals: true });
exports.SystemConst = mongoose_1.default.model("SystemConst", schema);
//# sourceMappingURL=systemConst.model.js.map