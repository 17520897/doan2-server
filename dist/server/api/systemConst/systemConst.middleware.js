"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const baseResponse_1 = __importDefault(require("../../services/baseResponse"));
const const_1 = require("../../const");
const errorsCode_1 = require("../../const/errorsCode");
const responseService_1 = __importDefault(require("../../services/responseService"));
const userRoles_service_1 = __importDefault(require("../userRoles/userRoles.service"));
const baseResponse = new baseResponse_1.default("SystemConstMiddleware");
class SystemConstMiddleware {
    updateSystemConst(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { userRoleFreeId } = req.body;
                if (userRoleFreeId) {
                    const userRoles = yield userRoles_service_1.default.getOne({
                        _id: userRoleFreeId,
                        status: const_1.UserRoleStatus.enable,
                    });
                    if (!userRoles) {
                        return responseService_1.default.send(res, const_1.HTTPCode.notAccept, {
                            errors: [
                                {
                                    code: errorsCode_1.ErrorsCode.systemConstUserRolesNotExisted,
                                    error: "user roles not existed",
                                    field: "userRoleFreeId",
                                },
                            ],
                        });
                    }
                }
                next();
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
}
exports.default = new SystemConstMiddleware();
//# sourceMappingURL=systemConst.middleware.js.map