"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RolesMiddleware = void 0;
const responseService_1 = __importDefault(require("../../services/responseService"));
const errorsCode_1 = require("../../const/errorsCode");
const roles_service_1 = __importDefault(require("./roles.service"));
const const_1 = require("../../const");
const baseResponse_1 = __importDefault(require("../../services/baseResponse"));
const admin_service_1 = __importDefault(require("../admin/admin.service"));
const baseResponse = new baseResponse_1.default("RolesMiddleware");
class RolesMiddleware {
    createRoles(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { roleName } = req.body;
                const role = yield roles_service_1.default.getOne({
                    roleName,
                    status: const_1.RoleStatus.enable,
                });
                if (role)
                    return responseService_1.default.send(res, const_1.HTTPCode.notAccept, {
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.rolesHasBeenExisted,
                                error: "roles name has been existed",
                                field: "roleName",
                            },
                        ],
                    });
                next();
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    updateRoles(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { adminId } = req.cookies;
                const { id } = req.params;
                const admin = yield admin_service_1.default.getById(adminId);
                if (admin.roleId.toString() === id.toString()) {
                    return responseService_1.default.send(res, const_1.HTTPCode.notAccept, {
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.roleNotAcceptUpdate,
                                error: "not accept update own role",
                                field: "_id",
                            },
                        ],
                    });
                }
                next();
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    disableRoles(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { adminId } = req.cookies;
                const { id } = req.params;
                const admin = yield admin_service_1.default.getById(adminId);
                if (admin.roleId.toString() === id.toString()) {
                    return responseService_1.default.send(res, const_1.HTTPCode.notAccept, {
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.roleNotAcceptDisable,
                                error: "not accept disable own roles",
                                field: "_id",
                            },
                        ],
                    });
                }
                next();
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    deleteRoles(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { adminId } = req.cookies;
                const { id } = req.params;
                const admin = yield admin_service_1.default.getById(adminId);
                if (admin.roleId.toString() === id.toString()) {
                    return responseService_1.default.send(res, const_1.HTTPCode.serverError, {
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.roleNotAcceptDelete,
                                error: "not accept delete own roles",
                                field: "_id",
                            },
                        ],
                    });
                }
                next();
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
}
exports.RolesMiddleware = RolesMiddleware;
exports.default = new RolesMiddleware();
//# sourceMappingURL=roles.middleware.js.map