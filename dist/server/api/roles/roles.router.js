"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const roles_middleware_1 = __importDefault(require("./roles.middleware"));
const roles_controller_1 = __importDefault(require("./roles.controller"));
const Valid_1 = require("../../middlewares/Valid");
const createRoles_dto_1 = __importDefault(require("./dto/createRoles.dto"));
const paramsRoles_dto_1 = __importDefault(require("./dto/paramsRoles.dto"));
const validate_1 = require("../../middlewares/validate");
const permissions_1 = require("../../const/permissions");
const updateRoles_dto_1 = __importDefault(require("./dto/updateRoles.dto"));
const getRoles_dto_1 = __importDefault(require("./dto/getRoles.dto"));
const const_1 = require("../../const");
const rolesRouter = express_1.default.Router();
rolesRouter.post("/", Valid_1.Valid(createRoles_dto_1.default, 1 /* body */), validate_1.validateRole(permissions_1.PermissionKeys.roleCreate, const_1.AccountType.admin), roles_middleware_1.default.createRoles, roles_controller_1.default.createRoles);
rolesRouter.get("/", Valid_1.Valid(getRoles_dto_1.default, 0 /* query */), validate_1.validateRole(permissions_1.PermissionKeys.roleGetList, const_1.AccountType.admin), roles_controller_1.default.getRoles);
rolesRouter.put("/disable/:id", Valid_1.Valid(paramsRoles_dto_1.default, 2 /* params */), validate_1.validateRole(permissions_1.PermissionKeys.roleDisableEnable, const_1.AccountType.admin), roles_middleware_1.default.disableRoles, roles_controller_1.default.disableRoles);
rolesRouter.put("/enable/:id", Valid_1.Valid(paramsRoles_dto_1.default, 2 /* params */), validate_1.validateRole(permissions_1.PermissionKeys.roleDisableEnable, const_1.AccountType.admin), roles_controller_1.default.enableRoles);
rolesRouter.put("/:id", validate_1.validateRole(permissions_1.PermissionKeys.roleUpdate, const_1.AccountType.admin), Valid_1.Valid(paramsRoles_dto_1.default, 2 /* params */), Valid_1.Valid(updateRoles_dto_1.default, 1 /* body */), roles_middleware_1.default.updateRoles, roles_controller_1.default.updateRoles);
rolesRouter.delete("/:id", validate_1.validateRole(permissions_1.PermissionKeys.roleDelete, const_1.AccountType.admin), Valid_1.Valid(paramsRoles_dto_1.default, 2 /* params */), roles_middleware_1.default.deleteRoles, roles_controller_1.default.deleteRoles);
exports.default = rolesRouter;
//# sourceMappingURL=roles.router.js.map