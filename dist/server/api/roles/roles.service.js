"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RolesService = void 0;
const roles_model_1 = require("./roles.model");
const baseDb_1 = __importDefault(require("../../services/baseDb"));
class RolesService extends baseDb_1.default {
}
exports.RolesService = RolesService;
exports.default = new RolesService(roles_model_1.Roles);
//# sourceMappingURL=roles.service.js.map