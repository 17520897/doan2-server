"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RolesController = void 0;
const responseService_1 = __importDefault(require("../../services/responseService"));
const roles_service_1 = __importDefault(require("./roles.service"));
const const_1 = require("../../const");
const baseResponse_1 = __importDefault(require("../../services/baseResponse"));
const baseResponse = new baseResponse_1.default("RolesController");
class RolesController {
    createRoles(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { adminId } = req.cookies;
                const { roleName, rolePermissions } = req.body;
                const role = yield roles_service_1.default.create({
                    roleName,
                    rolePermissions,
                    adminCreateId: adminId,
                });
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    message: "create roles success",
                    data: role,
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    getRoles(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { page, limit } = req.query;
                const roles = yield roles_service_1.default.populate({
                    page,
                    limit,
                    sort: {
                        roleName: 1,
                    },
                    populate: [
                        {
                            path: "adminCreateInfo",
                            select: "-password",
                        },
                    ],
                });
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    message: "get roles success",
                    data: roles,
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    disableRoles(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { id } = req.params;
                const { adminId } = req.cookies;
                yield roles_service_1.default.updateById(id, {
                    status: const_1.RoleStatus.disable,
                    adminCreateId: adminId,
                });
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    message: "disable role success",
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    enableRoles(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { id } = req.params;
                const { adminId } = req.cookies;
                yield roles_service_1.default.updateById(id, {
                    status: const_1.RoleStatus.enable,
                    adminCreateId: adminId,
                });
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    message: "enable roles success",
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    updateRoles(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { id } = req.params;
                const { adminId } = req.cookies;
                const roles = yield roles_service_1.default.updateById(id, Object.assign(Object.assign({}, req.body), { adminCreateId: adminId }));
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    message: "update roles success",
                    data: roles,
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    deleteRoles(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { id } = req.params;
                yield roles_service_1.default.deleteById(id);
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    message: "delete roles success",
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
}
exports.RolesController = RolesController;
exports.default = new RolesController();
//# sourceMappingURL=roles.controller.js.map