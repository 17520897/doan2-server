"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Roles = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
const schema = new mongoose_1.default.Schema({
    roleName: {
        type: String,
        required: true,
    },
    rolePermissions: [
        {
            permissionKey: {
                type: String,
                required: true,
            },
        },
    ],
    status: {
        type: String,
        enum: ["enable", "disable"],
        default: "enable",
    },
    adminCreateId: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
    },
}, {
    collection: "Roles",
});
schema.virtual("adminCreateInfo", {
    ref: "Admin",
    localField: "adminCreateId",
    foreignField: "_id",
    justOne: true,
});
schema.set("toObject", { virtuals: true });
schema.set("toJSON", { virtuals: true });
exports.Roles = mongoose_1.default.model("Roles", schema);
//# sourceMappingURL=roles.model.js.map