"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const validate_1 = require("../../middlewares/validate");
const const_1 = require("../../const");
const middleware_1 = __importDefault(require("../../middlewares/middleware"));
const report_controller_1 = __importDefault(require("./report.controller"));
const report_middleware_1 = __importDefault(require("./report.middleware"));
const reportRouter = express_1.default.Router();
reportRouter.post("/", validate_1.validateRole(null, const_1.AccountType.users), middleware_1.default.userHaveCompanies, middleware_1.default.companyHaveAccountNumber, report_controller_1.default.inputReport);
reportRouter.post("/file", validate_1.validateRole(null, const_1.AccountType.users), report_controller_1.default.getInputFromFileReport);
reportRouter.get("/", validate_1.validateRole(null, const_1.AccountType.users), report_middleware_1.default.getReportMiddleware, report_controller_1.default.getReport);
reportRouter.get("/firstInput", validate_1.validateRole(null, const_1.AccountType.users), report_middleware_1.default.getReportMiddleware, report_controller_1.default.getFirstInputReport);
exports.default = reportRouter;
//# sourceMappingURL=report.router.js.map