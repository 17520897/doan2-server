"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const baseResponse_1 = __importDefault(require("../../services/baseResponse"));
const accountNumber_service_1 = __importDefault(require("../accountNumber/accountNumber.service"));
const billDetail_service_1 = __importDefault(require("../billDetail/billDetail.service"));
const moneyBill_service_1 = __importDefault(require("../moneyBill/moneyBill.service"));
const report_service_1 = __importDefault(require("./report.service"));
const mongoose_1 = __importDefault(require("mongoose"));
const companies_service_1 = __importDefault(require("../companies/companies.service"));
const billTax_service_1 = __importDefault(require("../billTax/billTax.service"));
const forwardTax_service_1 = __importDefault(require("../forwardTax/forwardTax.service"));
const fs_1 = __importDefault(require("fs"));
const xlsx_1 = __importDefault(require("xlsx"));
const errorsCode_1 = require("../../const/errorsCode");
const baseResponse = new baseResponse_1.default("ReportController");
function addAccountNumberMoney(currentNumberMoney, currentNumberData, reportNumberMoney) {
    const returnData = {
        debitReportMoney: reportNumberMoney.debit,
        debitParentReportMoney: 0,
        receiveReportMoney: reportNumberMoney.receive,
        receiveParentReportMoney: 0,
    };
    returnData.debitReportMoney += currentNumberMoney.debit;
    returnData.receiveReportMoney += currentNumberMoney.receive;
    if (currentNumberData.debitLevel !== 1 &&
        currentNumberData.debitParentNumber) {
        returnData.debitParentReportMoney += currentNumberMoney.debit;
    }
    if (currentNumberData.receiveLevel !== 1 &&
        currentNumberData.receiveParentNumber) {
        returnData.receiveParentReportMoney += currentNumberMoney.receive;
    }
    return returnData;
}
function calculateReport(accountNumberId, userId, companyId, fromTime, toTime) {
    return __awaiter(this, void 0, void 0, function* () {
        const accountNumber = yield accountNumber_service_1.default.getById(accountNumberId);
        if (!accountNumber) {
            return false;
        }
        // get money bill in term
        const moneyBill = yield moneyBill_service_1.default.getAllByQuery({
            userCreateId: userId,
            companyId,
            accountNumberId,
            $and: [
                {
                    billCreateAt: {
                        $gte: fromTime,
                    },
                },
                {
                    billCreateAt: {
                        $lte: toTime,
                    },
                },
            ],
        });
        // get bill detail
        const billDetail = yield billDetail_service_1.default.getAllByQuery({
            userCreateId: userId,
            companyId,
            accountNumberId,
            $and: [
                {
                    billCreateAt: {
                        $gte: fromTime,
                    },
                },
                {
                    billCreateAt: {
                        $lte: toTime,
                    },
                },
            ],
        });
        // get billTax
        const billTax = yield billTax_service_1.default.getAllByQuery({
            userCreateId: userId,
            companyId,
            accountNumberId,
            $and: [
                {
                    billCreateAt: {
                        $gte: fromTime,
                    },
                },
                {
                    billCreateAt: {
                        $lte: toTime,
                    },
                },
            ],
        });
        // get forward tax
        const forwardTax = yield forwardTax_service_1.default.getAllByQuery({
            userCreateId: userId,
            accountNumberId,
            companyId,
            $and: [
                {
                    billCreateAt: {
                        $gte: fromTime,
                    },
                },
                {
                    billCreateAt: {
                        $lte: toTime,
                    },
                },
            ],
        });
        const debitReport = {};
        const receiveReport = {};
        for (let i = 0; i < accountNumber.accountNumbers.length; i++) {
            debitReport[`${accountNumber.accountNumbers[i].accountNumber}`] = {
                level: accountNumber.accountNumbers[i].level,
                parentNumber: accountNumber.accountNumbers[i].parentNumber,
                money: 0,
            };
            receiveReport[`${accountNumber.accountNumbers[i].accountNumber}`] = {
                level: accountNumber.accountNumbers[i].level,
                parentNumber: accountNumber.accountNumbers[i].parentNumber,
                money: 0,
            };
        }
        for (let i = 0; i < moneyBill.length; i++) {
            // init account number
            const debitAccountNumber = moneyBill[i].debitAccountNumber;
            const receiveAccountNumber = moneyBill[i].receiveAccountNumber;
            if (!debitReport[debitAccountNumber] ||
                !receiveReport[receiveAccountNumber])
                continue;
            const debitParentNumber = debitReport[debitAccountNumber].parentNumber;
            const receiveParentNumber = receiveReport[receiveAccountNumber].parentNumber;
            const addData = addAccountNumberMoney({
                debit: moneyBill[i].finalMoney,
                receive: moneyBill[i].finalMoney,
            }, {
                debitLevel: debitReport[debitAccountNumber].level,
                debitParentNumber: debitReport[debitAccountNumber].parentNumber,
                receiveLevel: receiveReport[receiveAccountNumber].level,
                receiveParentNumber: receiveReport[receiveAccountNumber].parentNumber,
            }, {
                debit: debitReport[debitAccountNumber].money,
                receive: receiveReport[receiveAccountNumber].money,
            });
            debitReport[debitAccountNumber].money = addData.debitReportMoney;
            receiveReport[receiveAccountNumber].money = addData.receiveReportMoney;
            if (debitParentNumber) {
                debitReport[debitParentNumber].money += addData.debitParentReportMoney;
            }
            if (receiveParentNumber) {
                receiveReport[receiveParentNumber].money +=
                    addData.receiveParentReportMoney;
            }
        }
        for (let i = 0; i < billDetail.length; i++) {
            // init account number
            const debitAccountNumber = billDetail[i].debitAccountNumber;
            const receiveAccountNumber = billDetail[i].receiveAccountNumber;
            if (!debitReport[debitAccountNumber] ||
                !receiveReport[receiveAccountNumber])
                continue;
            const debitParentNumber = debitReport[debitAccountNumber].parentNumber;
            const receiveParentNumber = receiveReport[receiveAccountNumber].parentNumber;
            const addData = addAccountNumberMoney({
                debit: billDetail[i].finalMoney,
                receive: billDetail[i].finalMoney,
            }, {
                debitLevel: debitReport[debitAccountNumber].level,
                debitParentNumber: debitReport[debitAccountNumber].parentNumber,
                receiveLevel: receiveReport[receiveAccountNumber].level,
                receiveParentNumber: receiveReport[receiveAccountNumber].parentNumber,
            }, {
                debit: debitReport[debitAccountNumber].money,
                receive: receiveReport[receiveAccountNumber].money,
            });
            debitReport[debitAccountNumber].money = addData.debitReportMoney;
            receiveReport[receiveAccountNumber].money = addData.receiveReportMoney;
            if (debitParentNumber) {
                debitReport[debitParentNumber].money += addData.debitParentReportMoney;
            }
            if (receiveParentNumber) {
                receiveReport[receiveParentNumber].money +=
                    addData.receiveParentReportMoney;
            }
        }
        for (let i = 0; i < billTax.length; i++) {
            // init account number
            const receiveAccountNumber = billTax[i].accountNumber;
            if (!receiveReport[receiveAccountNumber])
                continue;
            const receiveParentNumber = receiveReport[receiveAccountNumber].parentNumber;
            const addData = addAccountNumberMoney({
                debit: billTax[i].finalMoney,
                receive: billTax[i].finalMoney,
            }, {
                debitLevel: debitReport[receiveAccountNumber].level,
                debitParentNumber: debitReport[receiveAccountNumber].parentNumber,
                receiveLevel: receiveReport[receiveAccountNumber].level,
                receiveParentNumber: receiveReport[receiveAccountNumber].parentNumber,
            }, {
                debit: debitReport[receiveAccountNumber].money,
                receive: receiveReport[receiveAccountNumber].money,
            });
            receiveReport[receiveAccountNumber].money = addData.receiveReportMoney;
            if (receiveParentNumber) {
                receiveReport[receiveParentNumber].money +=
                    addData.receiveParentReportMoney;
            }
        }
        for (let i = 0; i < forwardTax.length; i++) {
            // init account number
            const debitAccountNumber = forwardTax[i].debitAccountNumber;
            const receiveAccountNumber = forwardTax[i].receiveAccountNumber;
            if (!debitReport[debitAccountNumber] ||
                !receiveReport[receiveAccountNumber])
                continue;
            const debitParentNumber = debitReport[debitAccountNumber].parentNumber;
            const receiveParentNumber = receiveReport[receiveAccountNumber].parentNumber;
            const addData = addAccountNumberMoney({
                debit: forwardTax[i].finalMoney,
                receive: forwardTax[i].finalMoney,
            }, {
                debitLevel: debitReport[debitAccountNumber].level,
                debitParentNumber: debitReport[debitAccountNumber].parentNumber,
                receiveLevel: receiveReport[receiveAccountNumber].level,
                receiveParentNumber: receiveReport[receiveAccountNumber].parentNumber,
            }, {
                debit: debitReport[debitAccountNumber].money,
                receive: receiveReport[receiveAccountNumber].money,
            });
            debitReport[debitAccountNumber].money = addData.debitReportMoney;
            receiveReport[receiveAccountNumber].money = addData.receiveReportMoney;
            if (debitParentNumber) {
                debitReport[debitParentNumber].money += addData.debitParentReportMoney;
            }
            if (receiveParentNumber) {
                receiveReport[receiveParentNumber].money +=
                    addData.receiveParentReportMoney;
            }
        }
        return {
            debitReports: debitReport,
            receiveReports: receiveReport,
        };
    });
}
function calculateFinalReport(accountNumberId, inputReport, currentReport) {
    return __awaiter(this, void 0, void 0, function* () {
        const finalReport = {
            debitReports: {},
            receiveReports: {},
        };
        const accountNumber = yield accountNumber_service_1.default.getById(accountNumberId);
        if (!accountNumber) {
            return false;
        }
        for (let i = 0; i < accountNumber.accountNumbers.length; i++) {
            // init variable
            const finalAccountNumber = accountNumber.accountNumbers[i].accountNumber;
            let currentDebit = currentReport.debitReports[`${finalAccountNumber}`];
            let currentReceive = currentReport.receiveReports[`${finalAccountNumber}`];
            let inputDebit = inputReport.debitReports[`${finalAccountNumber}`];
            let inputReceive = inputReport.receiveReports[`${finalAccountNumber}`];
            const tempLevel = accountNumber.accountNumbers[i].level;
            const tempParentNumber = accountNumber.accountNumbers[i].parentNumber;
            if (!currentDebit) {
                currentDebit = {
                    level: tempLevel,
                    parentNumber: tempParentNumber,
                    money: 0
                };
            }
            if (!currentReceive) {
                currentReceive = {
                    level: tempLevel,
                    parentNumber: tempParentNumber,
                    money: 0
                };
            }
            if (!inputDebit) {
                inputDebit = {
                    level: tempLevel,
                    parentNumber: tempParentNumber,
                    money: 0
                };
            }
            if (!inputReceive) {
                inputReceive = {
                    level: tempLevel,
                    parentNumber: tempParentNumber,
                    money: 0
                };
            }
            // init final Report
            finalReport.debitReports[`${finalAccountNumber}`] = {
                accountName: accountNumber.accountNumbers[i].accountName,
                level: accountNumber.accountNumbers[i].level,
                parentNumber: accountNumber.accountNumbers[i].parentNumber,
                money: 0,
            };
            finalReport.receiveReports[`${finalAccountNumber}`] = {
                accountName: accountNumber.accountNumbers[i].accountName,
                level: accountNumber.accountNumbers[i].level,
                parentNumber: accountNumber.accountNumbers[i].parentNumber,
                money: 0,
            };
            // calculate final money
            let finalMoney = 0;
            finalMoney =
                currentDebit.money +
                    inputDebit.money -
                    (currentReceive.money + inputReceive.money);
            if (finalMoney > 0) {
                finalReport.debitReports[`${finalAccountNumber}`].money = finalMoney;
            }
            else if (finalMoney < 0) {
                finalReport.receiveReports[`${finalAccountNumber}`].money = Math.abs(finalMoney);
            }
            else {
                finalReport.debitReports[`${finalAccountNumber}`].money = 0;
                finalReport.receiveReports[`${finalAccountNumber}`].money = 0;
            }
        }
        return finalReport;
    });
}
function changeArrayReportToHashTable(debitReports, receiveReports) {
    const returnData = {
        debitReports: {},
        receiveReports: {},
    };
    for (let i = 0; i < debitReports.length; i++) {
        const debitData = debitReports[i];
        returnData.debitReports[`${debitData.accountNumber}`] = {
            parentNumber: debitData.parentNumber,
            level: debitData.level,
            money: debitData.money,
        };
    }
    for (let i = 0; i < receiveReports.length; i++) {
        const receiveData = receiveReports[i];
        returnData.receiveReports[`${receiveData.accountNumber}`] = {
            parentNumber: receiveData.parentNumber,
            level: receiveData.level,
            money: receiveData.money,
        };
    }
    return returnData;
}
function initEmptyReport(accountNumberId) {
    return __awaiter(this, void 0, void 0, function* () {
        const accountNumber = yield accountNumber_service_1.default.getById(accountNumberId);
        if (!accountNumber) {
            return false;
        }
        const debitReports = {};
        const receiveReports = {};
        for (let i = 0; i < accountNumber.accountNumbers.length; i++) {
            debitReports[`${accountNumber.accountNumbers[i].accountNumber}`] = {
                level: accountNumber.accountNumbers[i].level,
                parentNumber: accountNumber.accountNumbers[i].parentNumber,
                money: 0,
            };
            receiveReports[`${accountNumber.accountNumbers[i].accountNumber}`] = {
                level: accountNumber.accountNumbers[i].level,
                parentNumber: accountNumber.accountNumbers[i].parentNumber,
                money: 0,
            };
        }
        return {
            debitReports,
            receiveReports,
        };
    });
}
function changeInputFileDataToHashTable(data) {
    const returnData = {};
    for (let i = 0; i < data.length; i++) {
        if (data[i]['TÀI KHOẢN'] && data[i]['NỢ'] && data[i]['CÓ']) {
            const debit = parseInt(data[i]['NỢ']);
            const receive = parseInt(data[i]['CÓ']);
            const accountNumber = parseInt(data[i]['TÀI KHOẢN']);
            returnData[accountNumber] = {
                'debit': debit != undefined && debit > 0 ? debit : 0,
                'receive': receive != undefined && receive > 0 ? receive : 0
            };
        }
    }
    return returnData;
}
class ReportController {
    getReport(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { userId } = req.cookies;
                let { companyId, accountNumberId, fromTime, toTime } = req.query;
                // input report
                let inputReport = {};
                console.log("Query first report: ", {
                    userCreateId: userId,
                    companyId,
                    accountNumberId,
                    $and: [
                        {
                            time: {
                                $gte: fromTime,
                            },
                        },
                        {
                            time: {
                                $lte: toTime,
                            },
                        },
                    ],
                });
                // get firstReport - get input report of user
                const firstReport = yield report_service_1.default.populate({
                    query: {
                        userCreateId: userId,
                        companyId,
                        accountNumberId,
                        $and: [
                            {
                                time: {
                                    $gte: fromTime,
                                },
                            },
                            {
                                time: {
                                    $lte: toTime,
                                },
                            },
                        ],
                    },
                    sort: {
                        time: -1,
                    },
                    limit: 1,
                    page: 0,
                });
                // assign query time for current report
                let currentReportTime = {
                    fromTime,
                    toTime,
                };
                if (firstReport.length > 0) {
                    console.log("have first reports");
                    // init data for inputReport
                    inputReport = {
                        debitReports: {},
                        receiveReports: {},
                    };
                    // if user have input in time request - get data for input report
                    inputReport = changeArrayReportToHashTable(firstReport[0].debitReports, firstReport[0].receiveReports);
                    // change time query for current report to first input report
                    currentReportTime.fromTime = firstReport[0].time;
                }
                else {
                    // get lasted input report - if do not have input report from request time
                    const lastedReport = yield report_service_1.default.populate({
                        query: {
                            userCreateId: userId,
                            companyId,
                            accountNumberId,
                            $and: [
                                {
                                    time: {
                                        $lte: fromTime,
                                    },
                                },
                            ],
                        },
                        sort: {
                            time: -1,
                        },
                        limit: 1,
                        page: 0,
                    });
                    if (lastedReport[0]) {
                        console.log("have lasted report");
                        const lastedReportTime = {
                            fromTime: lastedReport[0].time,
                            toTime: fromTime,
                        };
                        console.log("lasted report time: ", lastedReportTime);
                        const tempCurrentReport = yield calculateReport(accountNumberId, userId, companyId, lastedReportTime.fromTime, lastedReportTime.toTime);
                        const tempInputReport = changeArrayReportToHashTable(lastedReport[0] ? lastedReport[0].debitReports : {}, lastedReport[0] ? lastedReport[0].receiveReports : {});
                        // console.log('last_report: ', tempInputReport)
                        console.log('tempCurrentReport: ', tempCurrentReport['debitReports']['131']);
                        console.log('tempInputReport: ', tempInputReport['debitReports']['131']);
                        inputReport = yield calculateFinalReport(accountNumberId, tempInputReport, tempCurrentReport);
                        // console.log('input_report: ', inputReport)
                    }
                    else {
                        console.log("initEmptyReport");
                        inputReport = yield initEmptyReport(accountNumberId);
                    }
                }
                console.log("currentReportTime: ", currentReportTime);
                const currentReport = yield calculateReport(accountNumberId, userId, companyId, currentReportTime.fromTime, currentReportTime.toTime);
                const finalReport = yield calculateFinalReport(accountNumberId, inputReport, currentReport);
                return baseResponse.success({
                    res,
                    data: {
                        inputReport,
                        currentReport,
                        finalReport,
                    },
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    inputReport(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const session = yield mongoose_1.default.startSession();
            session.startTransaction();
            try {
                const options = { session };
                const { userId } = req.cookies;
                const { companyId, debitReports, receiveReports, accountNumberId, time, } = req.body;
                const report = yield report_service_1.default.updateOne({
                    userCreateId: userId,
                    companyId,
                    accountNumberId,
                }, {
                    userCreateId: userId,
                    companyId,
                    accountNumberId,
                    debitReports,
                    receiveReports,
                    time,
                }, Object.assign(Object.assign({}, options), { new: true, upsert: true }));
                yield companies_service_1.default.updateById(companyId, {
                    firstInput: time,
                }, options);
                yield session.commitTransaction();
                return baseResponse.success({
                    res,
                    data: report,
                    message: "input report success",
                });
            }
            catch (error) {
                yield session.abortTransaction();
                return baseResponse.serverError(res, error);
            }
            finally {
                yield session.endSession();
            }
        });
    }
    getFirstInputReport(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { userId } = req.cookies;
                const { companyId, accountNumberId } = req.query;
                const report = yield report_service_1.default.getOne({
                    userCreateId: userId,
                    companyId,
                    accountNumberId,
                });
                return baseResponse.success({
                    res,
                    data: report,
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    getInputFromFileReport(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const root = process.env.FILE_UPLOAD_FOLDER;
                const { fileUrl, accountNumberId } = req.body;
                const excelFile = `${root}/${fileUrl}`;
                if (!fs_1.default.existsSync(excelFile)) {
                    return baseResponse.notAccepted({
                        res,
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.filesNotFound,
                                field: 'fileUrl'
                            }
                        ]
                    });
                }
                const workbook = xlsx_1.default.readFile(excelFile);
                const sheetNameList = workbook.SheetNames;
                const data = xlsx_1.default.utils.sheet_to_json(workbook.Sheets[sheetNameList[0]]);
                if (data.length < 0 || !data[0]['TÀI KHOẢN'] || !data[0]['NỢ'] || !data[0]['CÓ']) {
                    return baseResponse.notAccepted({
                        res,
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.filesWrongFormat,
                                field: 'fileUrl'
                            }
                        ]
                    });
                }
                const inputData = changeInputFileDataToHashTable(data);
                const accountNumbers = yield (yield accountNumber_service_1.default.getById(accountNumberId)).accountNumbers;
                const debitReports = [];
                const receiveReports = [];
                let reportData = {
                    accountNumber: 0,
                    parentNumber: 0,
                    level: 0,
                    money: 0
                };
                console.log(inputData);
                for (let i = 0; i < accountNumbers.length; i++) {
                    reportData.accountNumber = accountNumbers[i].accountNumber;
                    reportData.parentNumber = accountNumbers[i].parentNumber;
                    reportData.level = accountNumbers[i].level;
                    if (inputData[reportData.accountNumber.toString()]) {
                        debitReports.push(Object.assign(Object.assign({}, reportData), { money: inputData[reportData.accountNumber].debit }));
                        receiveReports.push(Object.assign(Object.assign({}, reportData), { money: inputData[reportData.accountNumber].receive }));
                    }
                }
                return baseResponse.success({
                    res,
                    data: {
                        debitReports,
                        receiveReports
                    }
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
}
exports.default = new ReportController();
//# sourceMappingURL=report.controller.js.map