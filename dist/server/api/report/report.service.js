"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ReportService = void 0;
const report_model_1 = require("./report.model");
const baseDb_1 = __importDefault(require("../../services/baseDb"));
class ReportService extends baseDb_1.default {
}
exports.ReportService = ReportService;
exports.default = new ReportService(report_model_1.Report);
//# sourceMappingURL=report.service.js.map