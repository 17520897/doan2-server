"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const const_1 = require("../../const");
const baseResponse_1 = __importDefault(require("../../services/baseResponse"));
const jwt_1 = __importDefault(require("../../services/jwt"));
const responseService_1 = __importDefault(require("../../services/responseService"));
const roles_service_1 = __importDefault(require("../roles/roles.service"));
const admin_service_1 = __importDefault(require("./admin.service"));
const baseResponse = new baseResponse_1.default("AdminController");
class AdminController {
    createAdmin(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { adminId } = req.cookies;
                const admin = yield admin_service_1.default.create(Object.assign(Object.assign({}, req.body), { adminCreateId: adminId })); //hash password from middleware;
                admin.password = undefined;
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    message: "create admin success",
                    data: admin,
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    getAdminInfo(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { adminId } = req.cookies;
                const admin = yield admin_service_1.default.populate({
                    query: {
                        _id: adminId,
                    },
                    populate: [
                        {
                            path: "roleInfo",
                        },
                        {
                            path: "adminCreateInfo",
                            select: "-password",
                        },
                    ],
                });
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    data: admin,
                    message: "get admin info success",
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    loginAdmin(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { adminId, roleId } = req.body; //get from middleware
                const accessToken = yield jwt_1.default.generateAccessToken({
                    adminId,
                    isAdmin: true,
                });
                const refreshToken = yield jwt_1.default.generateRefreshToken({
                    adminId,
                    isAdmin: true,
                });
                const roles = yield roles_service_1.default.getOne({
                    _id: roleId,
                    status: const_1.RoleStatus.enable,
                });
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    data: {
                        accessToken,
                        refreshToken,
                        permissions: roles ? roles.rolePermissions : [],
                    },
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    checkExistedUsernameAdmin(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { username } = req.query;
                const admin = yield admin_service_1.default.getOne({
                    username,
                    status: { $ne: const_1.AdminStatus.deleted },
                });
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    data: { existed: admin ? true : false },
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
}
exports.default = new AdminController();
//# sourceMappingURL=admin.controller.js.map