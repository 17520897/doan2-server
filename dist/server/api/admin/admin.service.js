"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AdminService = void 0;
const admin_model_1 = require("./admin.model");
const baseDb_1 = __importDefault(require("../../services/baseDb"));
class AdminService extends baseDb_1.default {
}
exports.AdminService = AdminService;
exports.default = new AdminService(admin_model_1.Admin);
//# sourceMappingURL=admin.service.js.map