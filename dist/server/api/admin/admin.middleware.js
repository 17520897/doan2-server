"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const const_1 = require("../../const");
const errorsCode_1 = require("../../const/errorsCode");
const baseResponse_1 = __importDefault(require("../../services/baseResponse"));
const responseService_1 = __importDefault(require("../../services/responseService"));
const services_1 = __importDefault(require("../../services/services"));
const admin_service_1 = __importDefault(require("./admin.service"));
const baseResponse = new baseResponse_1.default("AdminMiddleware");
class AdminMiddleware {
    createAdmin(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { password, username } = req.body;
                const checkExistedUsername = yield admin_service_1.default.getOne({
                    username,
                });
                if (checkExistedUsername) {
                    return responseService_1.default.send(res, const_1.HTTPCode.notAccept, {
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.adminUserNameExisted,
                                error: "user name existed",
                                field: "username",
                            },
                        ],
                    });
                }
                req.body.password = yield services_1.default.hashPassword(password, parseInt(process.env.HASH_PASSWORD_SALT));
                next();
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    loginAdmin(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { username, password } = req.body;
                const checkExistedAdmin = yield admin_service_1.default.getOne({
                    username,
                });
                if (!checkExistedAdmin) {
                    return responseService_1.default.send(res, const_1.HTTPCode.notAccept, {
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.adminNotFound,
                                error: "user name not found",
                                field: "username",
                            },
                        ],
                    });
                }
                const comparePassword = yield services_1.default.verifyPassword(password, checkExistedAdmin.password);
                if (!comparePassword) {
                    return responseService_1.default.send(res, const_1.HTTPCode.notAccept, {
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.adminWrongPassword,
                                error: "wrong password",
                                field: "password",
                            },
                        ],
                    });
                }
                req.body.adminId = checkExistedAdmin._id;
                req.body.roleId = checkExistedAdmin.roleId;
                next();
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
}
exports.default = new AdminMiddleware();
//# sourceMappingURL=admin.middleware.js.map