"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Admin = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
const schema = new mongoose_1.default.Schema({
    phone: {
        type: String,
        trim: true,
    },
    username: {
        type: String,
        required: true,
        trim: true,
    },
    password: {
        type: String,
        required: true,
        trim: true,
    },
    name: {
        type: String,
        trim: true,
    },
    roleId: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
    },
    createAt: {
        type: Date,
        default: Date.now,
    },
    status: {
        type: String,
        enum: ["normal", "deleted", "block"],
        default: "normal",
    },
    adminCreateId: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
    },
}, {
    collection: "Admin",
});
schema.virtual("roleInfo", {
    ref: "Roles",
    localField: "roleId",
    foreignField: "_id",
    justOne: true,
});
schema.virtual("adminCreateInfo", {
    ref: "Admin",
    localField: "adminCreateId",
    foreignField: "_id",
    justOne: true,
});
schema.set("toObject", { virtuals: true });
schema.set("toJSON", { virtuals: true });
exports.Admin = mongoose_1.default.model("Admin", schema);
//# sourceMappingURL=admin.model.js.map