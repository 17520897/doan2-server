"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const const_1 = require("../../const");
const permissions_1 = require("../../const/permissions");
const Valid_1 = require("../../middlewares/Valid");
const validate_1 = require("../../middlewares/validate");
const admin_controller_1 = __importDefault(require("./admin.controller"));
const admin_middleware_1 = __importDefault(require("./admin.middleware"));
const checkUsernameAdmin_dto_1 = __importDefault(require("./dto/checkUsernameAdmin.dto"));
const createAdmin_dto_1 = __importDefault(require("./dto/createAdmin.dto"));
const loginAdmin_dto_1 = __importDefault(require("./dto/loginAdmin.dto"));
const adminRouter = express_1.default.Router();
adminRouter.get("/", validate_1.validateRole(null, const_1.AccountType.admin), admin_controller_1.default.getAdminInfo);
adminRouter.get("/checkUsername", Valid_1.Valid(checkUsernameAdmin_dto_1.default, 0 /* query */), validate_1.validateRole(null, const_1.AccountType.admin), admin_controller_1.default.checkExistedUsernameAdmin);
adminRouter.post("/", Valid_1.Valid(createAdmin_dto_1.default, 1 /* body */), validate_1.validateRole(permissions_1.PermissionKeys.adminCreate, const_1.AccountType.admin), admin_middleware_1.default.createAdmin, admin_controller_1.default.createAdmin);
adminRouter.post("/login", Valid_1.Valid(loginAdmin_dto_1.default, 1 /* body */), admin_middleware_1.default.loginAdmin, admin_controller_1.default.loginAdmin);
exports.default = adminRouter;
//# sourceMappingURL=admin.router.js.map