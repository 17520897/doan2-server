"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ServiceBillTaxService = void 0;
const baseDb_1 = __importDefault(require("../../services/baseDb"));
const serviceBillTax_model_1 = require("./serviceBillTax.model");
class ServiceBillTaxService extends baseDb_1.default {
}
exports.ServiceBillTaxService = ServiceBillTaxService;
exports.default = new ServiceBillTaxService(serviceBillTax_model_1.ServiceBillTax);
//# sourceMappingURL=serviceBillTax.service.js.map