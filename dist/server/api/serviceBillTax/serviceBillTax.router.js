"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const const_1 = require("../../const");
const Valid_1 = require("../../middlewares/Valid");
const validate_1 = require("../../middlewares/validate");
const middleware_1 = __importDefault(require("../../middlewares/middleware"));
const createOrUpdateManyServiceBillTax_dto_1 = __importDefault(require("./dto/createOrUpdateManyServiceBillTax.dto"));
const serviceBillTax_controller_1 = __importDefault(require("./serviceBillTax.controller"));
const getServiceBillTax_dto_1 = __importDefault(require("./dto/getServiceBillTax.dto"));
const serviceBillTax_middleware_1 = __importDefault(require("./serviceBillTax.middleware"));
const serviceBillTaxRouter = express_1.default.Router();
serviceBillTaxRouter.post("/", Valid_1.Valid(createOrUpdateManyServiceBillTax_dto_1.default, 1 /* body */), validate_1.validateRole(null, const_1.AccountType.users), middleware_1.default.userHaveCompanies, middleware_1.default.companyHaveAccountNumber, serviceBillTax_middleware_1.default.companyHaveServiceBill, serviceBillTax_controller_1.default.createManyServiceBillTax);
serviceBillTaxRouter.get("/", Valid_1.Valid(getServiceBillTax_dto_1.default, 0 /* query */), validate_1.validateRole(null, const_1.AccountType.users), serviceBillTax_controller_1.default.getServiceBillTax);
exports.default = serviceBillTaxRouter;
//# sourceMappingURL=serviceBillTax.router.js.map