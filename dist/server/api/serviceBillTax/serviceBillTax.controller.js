"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const baseResponse_1 = __importDefault(require("../../services/baseResponse"));
const mongoose_1 = __importDefault(require("mongoose"));
const serviceBill_service_1 = __importDefault(require("../serviceBill/serviceBill.service"));
const serviceBillTax_service_1 = __importDefault(require("./serviceBillTax.service"));
const baseResponse = new baseResponse_1.default("ServiceBillTaxController");
class ServiceBillTaxController {
    createManyServiceBillTax(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const session = yield mongoose_1.default.startSession();
            session.startTransaction();
            try {
                const options = { session };
                const { userId } = req.cookies;
                const { companyId, serviceBillId, serviceBillDetailId, accountNumberId, data, } = req.body;
                let totalTax = 0;
                const createData = [];
                for (let i = 0; i < data.length; i++) {
                    createData.push(Object.assign(Object.assign({}, data[i]), { companyId,
                        serviceBillId,
                        serviceBillDetailId,
                        accountNumberId, userCreateId: userId, finalMoney: (data[i].stockMoney * data[i].percent) / 100 }));
                    totalTax += data[i].stockMoney;
                }
                yield serviceBillTax_service_1.default.deleteMany({
                    serviceBillId,
                }, options);
                yield serviceBillTax_service_1.default.createMany(createData, options);
                yield serviceBill_service_1.default.updateById(serviceBillId, {
                    taxMoney: totalTax,
                }, options);
                yield session.commitTransaction();
                return baseResponse.success({
                    res,
                    message: "create service bill tax success",
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
            finally {
                session.endSession();
            }
        });
    }
    getServiceBillTax(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { userId } = req.cookies;
                const { companyId, serviceBillId } = req.query;
                const serviceBillTax = yield serviceBillTax_service_1.default.populate({
                    query: {
                        userCreateId: userId,
                        companyId,
                        serviceBillId,
                    },
                    sort: {
                        serviceBillDetailId: 1,
                    },
                    populate: [
                        {
                            path: "stockInfo",
                        },
                    ],
                });
                return baseResponse.success({
                    res,
                    data: serviceBillTax,
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
}
exports.default = new ServiceBillTaxController();
//# sourceMappingURL=serviceBillTax.controller.js.map