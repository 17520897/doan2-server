"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MoneyType = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
const schema = new mongoose_1.default.Schema({
    name: {
        type: String,
    },
    defaultExchangeRate: {
        type: Number,
    },
    adminCreateId: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
    },
    status: {
        type: String,
        enum: ["normal", "deleted"],
        default: "normal",
    },
}, {
    collection: "MoneyType",
});
schema.virtual("adminCreateInfo", {
    ref: "Admin",
    localField: "adminCreateId",
    foreignField: "_id",
    justOne: true,
});
schema.set("toObject", { virtuals: true });
schema.set("toJSON", { virtuals: true });
exports.MoneyType = mongoose_1.default.model("MoneyType", schema);
//# sourceMappingURL=moneyType.model.js.map