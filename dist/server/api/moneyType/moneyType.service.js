"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MoneyTypeService = void 0;
const moneyType_model_1 = require("./moneyType.model");
const baseDb_1 = __importDefault(require("../../services/baseDb"));
class MoneyTypeService extends baseDb_1.default {
}
exports.MoneyTypeService = MoneyTypeService;
exports.default = new MoneyTypeService(moneyType_model_1.MoneyType);
//# sourceMappingURL=moneyType.service.js.map