"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const const_1 = require("../../const");
const permissions_1 = require("../../const/permissions");
const validate_1 = require("../../middlewares/validate");
const moneyType_controller_1 = __importDefault(require("./moneyType.controller"));
const moneyType_middleware_1 = __importDefault(require("./moneyType.middleware"));
const moneyTypeRouter = express_1.default.Router();
moneyTypeRouter.get("/", validate_1.validateRole(null, const_1.AccountType.all), moneyType_controller_1.default.getMoneyType);
moneyTypeRouter.post("/", validate_1.validateRole(permissions_1.PermissionKeys.moneyTypeCreate, const_1.AccountType.admin), moneyType_middleware_1.default.createMoneyType, moneyType_controller_1.default.createMoneyType);
moneyTypeRouter.put("/:id", validate_1.validateRole(permissions_1.PermissionKeys.moneyTypeUpdate, const_1.AccountType.admin), moneyType_middleware_1.default.existedMoneyType, moneyType_middleware_1.default.updateMoneyType, moneyType_controller_1.default.updateMoneyType);
moneyTypeRouter.delete("/:id", validate_1.validateRole(permissions_1.PermissionKeys.moneyTypeDelete, const_1.AccountType.admin), moneyType_controller_1.default.deleteMoneyType);
exports.default = moneyTypeRouter;
//# sourceMappingURL=moneyType.router.js.map