"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserUsedHistoryService = void 0;
const userUsedHistory_model_1 = require("./userUsedHistory.model");
const baseDb_1 = __importDefault(require("../../services/baseDb"));
class UserUsedHistoryService extends baseDb_1.default {
}
exports.UserUsedHistoryService = UserUsedHistoryService;
exports.default = new UserUsedHistoryService(userUsedHistory_model_1.UserUsedHistory);
//# sourceMappingURL=userUsedHistory.service.js.map