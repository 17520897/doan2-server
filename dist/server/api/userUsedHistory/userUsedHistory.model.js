"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserUsedHistory = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
const schema = new mongoose_1.default.Schema({
    userId: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
    },
    companies: [],
}, {
    collection: "UserUsedHistory",
});
schema.virtual("userInfo", {
    ref: "Users",
    localField: "userId",
    foreignField: "_id",
    justOne: true,
});
schema.set("toObject", { virtuals: true });
schema.set("toJSON", { virtuals: true });
exports.UserUsedHistory = mongoose_1.default.model("UserUsedHistory", schema);
//# sourceMappingURL=userUsedHistory.model.js.map