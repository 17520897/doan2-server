"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const const_1 = require("../../const");
const validate_1 = require("../../middlewares/validate");
const userUsedHistory_controller_1 = __importDefault(require("./userUsedHistory.controller"));
const userUsedHistoryRouter = express_1.default.Router();
userUsedHistoryRouter.get("/", validate_1.validateRole(null, const_1.AccountType.users), userUsedHistory_controller_1.default.userGetUserUsedHistory);
userUsedHistoryRouter.put("/", validate_1.validateRole(null, const_1.AccountType.users), userUsedHistory_controller_1.default.updateUserUsedHistory);
exports.default = userUsedHistoryRouter;
//# sourceMappingURL=userUsedHistory.router.js.map