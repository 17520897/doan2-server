"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const baseResponse_1 = __importDefault(require("../../services/baseResponse"));
const const_1 = require("../../const");
const responseService_1 = __importDefault(require("../../services/responseService"));
const userUsedHistory_service_1 = __importDefault(require("./userUsedHistory.service"));
const baseResponse = new baseResponse_1.default("UserUsedHistoryController");
class UserUsedHistoryController {
    userGetUserUsedHistory(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { userId } = req.cookies;
                let userUsedHistory = yield userUsedHistory_service_1.default.getOne({
                    userId: userId,
                });
                if (!userUsedHistory) {
                    userUsedHistory = yield userUsedHistory_service_1.default.create({
                        userId: userId,
                    });
                }
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    data: userUsedHistory,
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    updateUserUsedHistory(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { userId } = req.cookies;
                const { companies } = req.body;
                const userUsedHistory = yield userUsedHistory_service_1.default.updateOne({
                    userId: userId,
                }, {
                    companies,
                }, {
                    upsert: true,
                    new: true,
                });
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    data: userUsedHistory,
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
}
exports.default = new UserUsedHistoryController();
//# sourceMappingURL=userUsedHistory.controller.js.map