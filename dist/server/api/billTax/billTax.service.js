"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BillTaxService = void 0;
const baseDb_1 = __importDefault(require("../../services/baseDb"));
const billTax_model_1 = require("./billTax.model");
class BillTaxService extends baseDb_1.default {
}
exports.BillTaxService = BillTaxService;
exports.default = new BillTaxService(billTax_model_1.BillTax);
//# sourceMappingURL=billTax.service.js.map