"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const const_1 = require("../../const");
const Valid_1 = require("../../middlewares/Valid");
const validate_1 = require("../../middlewares/validate");
const middleware_1 = __importDefault(require("../../middlewares/middleware"));
const createOrUpdateManyBillTax_dto_1 = __importDefault(require("./dto/createOrUpdateManyBillTax.dto"));
const billTax_controller_1 = __importDefault(require("./billTax.controller"));
const getBillTax_dto_1 = __importDefault(require("./dto/getBillTax.dto"));
const billTax_middleware_1 = __importDefault(require("./billTax.middleware"));
const billTaxRouter = express_1.default.Router();
billTaxRouter.post("/", Valid_1.Valid(createOrUpdateManyBillTax_dto_1.default, 1 /* body */), validate_1.validateRole(null, const_1.AccountType.users), middleware_1.default.userHaveCompanies, middleware_1.default.companyHaveAccountNumber, billTax_middleware_1.default.companyHaveBill, billTax_controller_1.default.createManyBillTax);
billTaxRouter.get("/", Valid_1.Valid(getBillTax_dto_1.default, 0 /* query */), validate_1.validateRole(null, const_1.AccountType.users), billTax_controller_1.default.getBillTax);
exports.default = billTaxRouter;
//# sourceMappingURL=billTax.router.js.map