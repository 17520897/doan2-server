"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const const_1 = require("../../const");
const settingBank_service_1 = __importDefault(require("../settingBank/settingBank.service"));
const companyBanking_service_1 = __importDefault(require("./companyBanking.service"));
class companyBankingValidate {
    isBankExisted(bankId) {
        return __awaiter(this, void 0, void 0, function* () {
            const bank = yield settingBank_service_1.default.getOne({
                _id: bankId,
                status: const_1.SettingBankStatus.normal,
            });
            return bank ? true : false;
        });
    }
    isBankAccountExisted(userId, bankAccount, checkId = false, id = "") {
        return __awaiter(this, void 0, void 0, function* () {
            const companyBanking = checkId
                ? yield companyBanking_service_1.default.getOne({
                    userCreateId: userId,
                    status: const_1.CompanyBankingStatus.normal,
                    bankAccount,
                    _id: { $ne: id },
                })
                : yield companyBanking_service_1.default.getOne({
                    userCreateId: userId,
                    bankAccount,
                    status: const_1.CompanyBankingStatus.normal,
                });
            return companyBanking ? true : false;
        });
    }
    isCompanyBankingExisted(userId, id) {
        return __awaiter(this, void 0, void 0, function* () {
            const companyBanking = yield companyBanking_service_1.default.getOne({
                _id: id,
                userCreateId: userId,
                status: const_1.CompanyBankingStatus.normal,
            });
            return companyBanking ? true : false;
        });
    }
}
exports.default = new companyBankingValidate();
//# sourceMappingURL=companyBanking.validate.js.map