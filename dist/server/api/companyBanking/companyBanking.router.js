"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const const_1 = require("../../const");
const IdParamsRequest_dto_1 = __importDefault(require("../../const/IdParamsRequest.dto"));
const Valid_1 = require("../../middlewares/Valid");
const validate_1 = require("../../middlewares/validate");
const companyBanking_controller_1 = __importDefault(require("./companyBanking.controller"));
const companyBanking_middleware_1 = __importDefault(require("./companyBanking.middleware"));
const createCompanyBanking_dto_1 = __importDefault(require("./dto/createCompanyBanking.dto"));
const getCompanyBanking_dto_1 = __importDefault(require("./dto/getCompanyBanking.dto"));
const companyBankingRouter = express_1.default.Router();
companyBankingRouter.post("/", Valid_1.Valid(createCompanyBanking_dto_1.default, 1 /* body */), validate_1.validateRole(null, const_1.AccountType.users), companyBanking_middleware_1.default.bankExisted, companyBanking_middleware_1.default.createCompanyBanking, companyBanking_controller_1.default.createCompanyBanking);
companyBankingRouter.put("/:id", Valid_1.Valid(IdParamsRequest_dto_1.default, 2 /* params */), Valid_1.Valid(createCompanyBanking_dto_1.default, 1 /* body */), validate_1.validateRole(null, const_1.AccountType.users), companyBanking_middleware_1.default.companyBankingExisted, companyBanking_middleware_1.default.bankExisted, companyBanking_middleware_1.default.updateCompanyBanking, companyBanking_controller_1.default.updateCompanyBanking);
companyBankingRouter.delete("/:id", Valid_1.Valid(IdParamsRequest_dto_1.default, 2 /* params */), validate_1.validateRole(null, const_1.AccountType.users), companyBanking_middleware_1.default.companyBankingExisted, companyBanking_controller_1.default.deleteCompanyBanking);
companyBankingRouter.get("/", Valid_1.Valid(getCompanyBanking_dto_1.default, 0 /* query */), validate_1.validateRole(null, const_1.AccountType.users), companyBanking_controller_1.default.getAllCompanyBanking);
exports.default = companyBankingRouter;
//# sourceMappingURL=companyBanking.router.js.map