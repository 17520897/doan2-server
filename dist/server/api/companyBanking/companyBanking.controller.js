"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const const_1 = require("../../const");
const baseResponse_1 = __importDefault(require("../../services/baseResponse"));
const responseService_1 = __importDefault(require("../../services/responseService"));
const settingBank_service_1 = __importDefault(require("../settingBank/settingBank.service"));
const companyBanking_service_1 = __importDefault(require("./companyBanking.service"));
const baseResponse = new baseResponse_1.default("CompanyBankingController");
class CompanyBankingController {
    createCompanyBanking(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { userId } = req.cookies;
                const companyBanking = yield companyBanking_service_1.default.create(Object.assign(Object.assign({}, req.body), { userCreateId: userId }));
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    data: companyBanking,
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    updateCompanyBanking(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { id } = req.params;
                const companyBanking = yield companyBanking_service_1.default.updateById(id, Object.assign({}, req.body));
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    data: companyBanking,
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    deleteCompanyBanking(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { id } = req.params;
                yield companyBanking_service_1.default.deleteById(id);
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    message: "delete company banking success",
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    getAllCompanyBanking(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { userId } = req.cookies;
                const { search, page, limit, companyId } = yield req.query;
                let query = {
                    userCreateId: userId,
                    companyId,
                };
                if (search) {
                    const bank = (yield settingBank_service_1.default.getByQuery({
                        query: { name: { $regex: search, $options: "i" } },
                    }));
                    const bankId = [];
                    bank.pop();
                    for (let i = 0; i < bank.length; i++) {
                        bankId.push(bank[i]._id);
                    }
                    query = Object.assign(Object.assign({}, query), { $or: [
                            {
                                bankAccount: { $regex: search, $options: "i" },
                            },
                            {
                                bankAccountName: { $regex: search, $options: "i" },
                            },
                            {
                                bankId: bankId,
                            },
                        ] });
                }
                const companyBanking = yield companyBanking_service_1.default.populate({
                    query,
                    page,
                    limit,
                    sort: {
                        bankAccount: 1,
                        bankAccountName: 1,
                    },
                    populate: [
                        {
                            path: "bankInfo",
                        },
                    ],
                });
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    data: companyBanking,
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
}
exports.default = new CompanyBankingController();
//# sourceMappingURL=companyBanking.controller.js.map