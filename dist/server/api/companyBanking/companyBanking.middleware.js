"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const baseResponse_1 = __importDefault(require("../../services/baseResponse"));
const const_1 = require("../../const");
const errorsCode_1 = require("../../const/errorsCode");
const responseService_1 = __importDefault(require("../../services/responseService"));
const companyBanking_validate_1 = __importDefault(require("./companyBanking.validate"));
const baseResponse = new baseResponse_1.default("CompanyBankingMiddleware");
class CompanyBankingMiddleware {
    createCompanyBanking(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { userId } = req.cookies;
                const { bankAccount } = req.body;
                const isBankAccountExisted = yield companyBanking_validate_1.default.isBankAccountExisted(userId, bankAccount);
                if (isBankAccountExisted) {
                    return responseService_1.default.send(res, const_1.HTTPCode.notAccept, {
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.companyBankingBankAccountExisted,
                                field: "bankAccount",
                            },
                        ],
                    });
                }
                next();
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    companyBankingExisted(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { id } = req.params;
                const { userId } = req.cookies;
                const isCompanyBankingExisted = yield companyBanking_validate_1.default.isCompanyBankingExisted(userId, id);
                if (!isCompanyBankingExisted) {
                    return responseService_1.default.send(res, const_1.HTTPCode.notAccept, {
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.companyBankingNotFound,
                                field: "id",
                            },
                        ],
                    });
                }
                next();
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    bankExisted(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { bankId } = req.body;
                if (bankId) {
                    const isBankExisted = yield companyBanking_validate_1.default.isBankExisted(bankId);
                    if (!isBankExisted) {
                        return responseService_1.default.send(res, const_1.HTTPCode.notAccept, {
                            errors: [
                                {
                                    code: errorsCode_1.ErrorsCode.companyBankingBankNotExisted,
                                    field: "bankId",
                                },
                            ],
                        });
                    }
                }
                next();
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    updateCompanyBanking(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { id } = req.params;
                const { userId } = req.cookies;
                const { bankId, bankAccount } = req.body;
                const isBankAccountExisted = yield companyBanking_validate_1.default.isBankAccountExisted(userId, bankAccount, true, id);
                if (isBankAccountExisted) {
                    return responseService_1.default.send(res, const_1.HTTPCode.notAccept, {
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.companyBankingBankAccountExisted,
                                field: "bankAccount",
                            },
                        ],
                    });
                }
                next();
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
}
exports.default = new CompanyBankingMiddleware();
//# sourceMappingURL=companyBanking.middleware.js.map