"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompanyBankingService = void 0;
const baseDb_1 = __importDefault(require("../../services/baseDb"));
const companyBanking_model_1 = require("./companyBanking.model");
class CompanyBankingService extends baseDb_1.default {
}
exports.CompanyBankingService = CompanyBankingService;
exports.default = new CompanyBankingService(companyBanking_model_1.CompanyBanking);
//# sourceMappingURL=companyBanking.service.js.map