"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompanyBanking = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
const schema = new mongoose_1.default.Schema({
    companyId: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
        required: true,
    },
    userCreateId: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
        required: true,
    },
    bankId: {
        type: String,
    },
    bankAccount: {
        type: String,
    },
    bankAccountName: {
        type: String,
    },
    usedAt: {
        type: Date,
        default: Date.now,
    },
    status: {
        type: String,
        enum: ["normal", "deleted"],
        default: "normal",
    },
}, {
    collection: "CompanyBanking",
});
schema.virtual("userCreateInfo", {
    ref: "User",
    localField: "userCreateId",
    foreignField: "_id",
    justOne: true,
});
schema.virtual("bankInfo", {
    ref: "SettingBank",
    localField: "bankId",
    foreignField: "_id",
    justOne: true,
});
schema.set("toObject", { virtuals: true });
schema.set("toJSON", { virtuals: true });
exports.CompanyBanking = mongoose_1.default.model("CompanyBanking", schema);
//# sourceMappingURL=companyBanking.model.js.map