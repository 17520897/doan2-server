"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const const_1 = require("../../const");
const companies_service_1 = __importDefault(require("../companies/companies.service"));
const person_service_1 = __importDefault(require("./person.service"));
class PersonValidate {
    isCodeExisted(userId, code, companyId, checkId = false, id = "") {
        return __awaiter(this, void 0, void 0, function* () {
            const person = checkId
                ? yield person_service_1.default.getOne({
                    code,
                    userCreateId: userId,
                    companyId,
                    _id: { $ne: id },
                    status: const_1.PersonStatus.normal,
                })
                : yield person_service_1.default.getOne({
                    code,
                    userCreateId: userId,
                    companyId,
                    status: const_1.PersonStatus.normal,
                });
            return person ? true : false;
        });
    }
    isUserHavePerson(userId, id) {
        return __awaiter(this, void 0, void 0, function* () {
            const person = yield person_service_1.default.getOne({
                _id: id,
                userCreateId: userId,
                status: const_1.PersonStatus.normal,
            });
            return person ? true : false;
        });
    }
    isUserHaveCompany(userId, companyId) {
        return __awaiter(this, void 0, void 0, function* () {
            const company = yield companies_service_1.default.getOne({
                _id: companyId,
                userCreateId: userId,
                status: const_1.CompaniesStatus.normal,
            });
            return company ? true : false;
        });
    }
    isPersonExisted(userId, personId) {
        return __awaiter(this, void 0, void 0, function* () {
            const person = yield person_service_1.default.getOne({
                userCreateId: userId,
                _id: personId,
                status: const_1.PersonStatus.normal,
            });
            return person ? true : false;
        });
    }
}
exports.default = new PersonValidate();
//# sourceMappingURL=person.validate.js.map