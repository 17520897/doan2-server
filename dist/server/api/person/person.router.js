"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const const_1 = require("../../const");
const Valid_1 = require("../../middlewares/Valid");
const validate_1 = require("../../middlewares/validate");
const createPerson_dto_1 = __importDefault(require("./dto/createPerson.dto"));
const updatePerson_dto_1 = __importDefault(require("./dto/updatePerson.dto"));
const person_controller_1 = __importDefault(require("./person.controller"));
const person_middleware_1 = __importDefault(require("./person.middleware"));
const getPerson_dto_1 = __importDefault(require("./dto/getPerson.dto"));
const countPerson_dto_1 = __importDefault(require("./dto/countPerson.dto"));
const IdParamsRequest_dto_1 = __importDefault(require("../../const/IdParamsRequest.dto"));
const checkCodeExisted_dto_1 = __importDefault(require("./dto/checkCodeExisted.dto"));
const middleware_1 = __importDefault(require("../../middlewares/middleware"));
const personRouter = express_1.default.Router();
personRouter.post("/", Valid_1.Valid(createPerson_dto_1.default, 1 /* body */), validate_1.validateRole(null, const_1.AccountType.users), middleware_1.default.userHaveCompanies, person_middleware_1.default.createPerson, person_controller_1.default.createPerson);
personRouter.put("/:id", Valid_1.Valid(IdParamsRequest_dto_1.default, 2 /* params */), Valid_1.Valid(updatePerson_dto_1.default, 1 /* body */), validate_1.validateRole(null, const_1.AccountType.users), middleware_1.default.userHaveCompanies, person_middleware_1.default.userHavePerson, person_middleware_1.default.updatePerson, person_controller_1.default.updatePerson);
personRouter.delete("/:id", Valid_1.Valid(IdParamsRequest_dto_1.default, 2 /* params */), validate_1.validateRole(null, const_1.AccountType.users), person_middleware_1.default.userHavePerson, person_controller_1.default.deletePerson);
personRouter.get("/", validate_1.validateRole(null, const_1.AccountType.users), Valid_1.Valid(getPerson_dto_1.default, 0 /* query */), person_controller_1.default.searchPerson);
personRouter.put("/used/:id", validate_1.validateRole(null, const_1.AccountType.users), person_controller_1.default.usedPerson);
personRouter.get("/count", Valid_1.Valid(countPerson_dto_1.default, 0 /* query */), validate_1.validateRole(null, const_1.AccountType.users), person_controller_1.default.countPerson);
personRouter.get("/checkCodeExisted", Valid_1.Valid(checkCodeExisted_dto_1.default, 0 /* query */), validate_1.validateRole(null, const_1.AccountType.users), person_controller_1.default.checkCodeExisted);
exports.default = personRouter;
//# sourceMappingURL=person.router.js.map