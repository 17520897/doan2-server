"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Person = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
const schema = new mongoose_1.default.Schema({
    companyId: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
        required: true,
    },
    userCreateId: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
        required: true,
    },
    code: {
        type: String,
        index: "text",
    },
    name: {
        type: String,
        index: "text",
    },
    shortName: {
        type: String,
        index: "text",
    },
    slugName: {
        type: String,
        index: "text",
    },
    taxNumber: {
        type: String,
        index: "text",
    },
    phone: {
        type: String,
        index: "text",
    },
    address: {
        type: String,
        index: "text",
    },
    usedAt: {
        type: Date,
        default: Date.now(),
    },
    status: {
        enum: ["normal", "deleted"],
        type: String,
        default: "normal",
    },
    personType: {
        enum: ["people", "company", "employee"],
        type: String,
        default: "people",
        index: "text",
    },
    note: {
        type: String,
    },
}, {
    collection: "Person",
});
schema.virtual("userCreateInfo", {
    ref: "User",
    localField: "userCreateId",
    foreignField: "_id",
    justOne: true,
});
schema.set("toObject", { virtuals: true });
schema.set("toJSON", { virtuals: true });
exports.Person = mongoose_1.default.model("Person", schema);
//# sourceMappingURL=person.model.js.map