"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const baseResponse_1 = __importDefault(require("../../services/baseResponse"));
const logger_1 = __importDefault(require("../../common/logger"));
const const_1 = require("../../const");
const responseService_1 = __importDefault(require("../../services/responseService"));
const person_service_1 = __importDefault(require("./person.service"));
const person_validate_1 = __importDefault(require("./person.validate"));
const slugify_1 = __importDefault(require("slugify"));
const baseResponse = new baseResponse_1.default("PersonController");
class PersonController {
    createPerson(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { userId } = req.cookies;
                console.log(userId);
                const person = yield person_service_1.default.create(Object.assign(Object.assign({}, req.body), { userCreateId: userId, slugName: slugify_1.default(req.body.name, {
                        lower: true,
                        replacement: '_',
                        trim: true,
                        locale: 'vi'
                    }) }));
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    data: person,
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    updatePerson(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { id } = req.params;
                const person = yield person_service_1.default.updateById(id, Object.assign(Object.assign({}, req.body), { usedAt: Date.now(), slugName: req.body.name ? slugify_1.default(req.body.name, {
                        lower: true,
                        replacement: '_',
                        trim: true,
                        locale: 'vi'
                    }) : undefined }));
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    data: person,
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    deletePerson(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { id } = req.params;
                yield person_service_1.default.deleteById(id);
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    message: "delete success",
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    searchPerson(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { userId } = req.cookies;
                const { search, page, limit, type, companyId } = req.query;
                let query = {
                    userCreateId: userId,
                    status: const_1.PersonStatus.normal,
                    companyId,
                };
                if (search) {
                    query = Object.assign(Object.assign({}, query), { $or: [
                            {
                                name: { $regex: search, $options: "i" },
                            },
                            {
                                shortName: { $regex: search, $options: "i" },
                            },
                            {
                                code: { $regex: search, $options: "i" },
                            },
                            {
                                address: { $regex: search, $options: "i" },
                            },
                            {
                                taxNumber: { $regex: search, $options: "i" },
                            },
                        ] });
                }
                if (type) {
                    query = Object.assign(Object.assign({}, query), { personType: type });
                }
                const person = yield person_service_1.default.getByQuery({
                    query,
                    page,
                    limit,
                    sort: {
                        name: -1,
                    },
                });
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    data: person,
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    usedPerson(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { id } = req.params;
                yield person_service_1.default.updateById(id, {
                    usedAt: Date.now(),
                });
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    message: "used success",
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    countPerson(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { userId } = req.cookies;
                const { companyId } = req.query;
                const personCount = yield person_service_1.default.count({
                    userCreateId: userId,
                    companyId,
                });
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    data: {
                        count: personCount,
                    },
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    checkCodeExisted(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { code, companyId } = req.query;
                const { userId } = req.cookies;
                const isCodeExisted = yield person_validate_1.default.isCodeExisted(userId, code, companyId);
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    data: {
                        isExisted: isCodeExisted ? true : false,
                    },
                });
            }
            catch (error) {
                logger_1.default.error("checkCodeExisted controller: ", error);
                return baseResponse.serverError(res, error);
            }
        });
    }
}
exports.default = new PersonController();
//# sourceMappingURL=person.controller.js.map