"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PersonService = void 0;
const person_model_1 = require("./person.model");
const baseDb_1 = __importDefault(require("../../services/baseDb"));
class PersonService extends baseDb_1.default {
}
exports.PersonService = PersonService;
exports.default = new PersonService(person_model_1.Person);
//# sourceMappingURL=person.service.js.map