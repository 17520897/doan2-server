"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ServiceBillService = void 0;
const baseDb_1 = __importDefault(require("../../services/baseDb"));
const serviceBillDetail_model_1 = require("./serviceBillDetail.model");
class ServiceBillService extends baseDb_1.default {
}
exports.ServiceBillService = ServiceBillService;
exports.default = new ServiceBillService(serviceBillDetail_model_1.ServiceBillDetail);
//# sourceMappingURL=serviceBillDetail.service.js.map