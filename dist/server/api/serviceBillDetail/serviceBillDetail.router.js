"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const const_1 = require("../../const");
const Valid_1 = require("../../middlewares/Valid");
const validate_1 = require("../../middlewares/validate");
const middleware_1 = __importDefault(require("../../middlewares/middleware"));
const serviceBillDetail_controller_1 = __importDefault(require("./serviceBillDetail.controller"));
const serviceBillDetail_middleware_1 = __importDefault(require("./serviceBillDetail.middleware"));
const createOrUpdateManyServiceBillDetail_dto_1 = __importDefault(require("./dto/createOrUpdateManyServiceBillDetail.dto"));
const getServiceBillDetail_dto_1 = __importDefault(require("./dto/getServiceBillDetail.dto"));
const serviceBillDetailRouter = express_1.default.Router();
serviceBillDetailRouter.post("/", Valid_1.Valid(createOrUpdateManyServiceBillDetail_dto_1.default, 1 /* body */), validate_1.validateRole(null, const_1.AccountType.users), middleware_1.default.manyAccountNumberIsSame, middleware_1.default.userHaveCompanies, middleware_1.default.companyHaveAccountNumber, serviceBillDetail_middleware_1.default.companyHaveServiceBill, serviceBillDetail_controller_1.default.createManyServiceBillDetail);
serviceBillDetailRouter.get("/", Valid_1.Valid(getServiceBillDetail_dto_1.default, 0 /* query */), validate_1.validateRole(null, const_1.AccountType.users), serviceBillDetail_controller_1.default.getServiceBillDetail);
exports.default = serviceBillDetailRouter;
//# sourceMappingURL=serviceBillDetail.router.js.map