"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const errorsCode_1 = require("../../const/errorsCode");
const baseResponse_1 = __importDefault(require("../../services/baseResponse"));
const serviceBill_validate_1 = __importDefault(require("../serviceBill/serviceBill.validate"));
const baseResponse = new baseResponse_1.default("ServiceBillDetailMiddleware");
class ServiceBillDetailMiddleware {
    companyHaveServiceBill(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { userId } = req.cookies;
                const { serviceBillId, companyId } = req.body;
                const isCompanyHaveServiceBill = yield serviceBill_validate_1.default.isCompaniesHaveServiceBill(userId, companyId, serviceBillId);
                if (!isCompanyHaveServiceBill) {
                    return baseResponse.notAccepted({
                        res,
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.serviceBillCompaniesNotHaveServiceBill,
                                field: "serviceBillId",
                            },
                        ],
                    });
                }
                next();
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
}
exports.default = new ServiceBillDetailMiddleware();
//# sourceMappingURL=serviceBillDetail.middleware.js.map