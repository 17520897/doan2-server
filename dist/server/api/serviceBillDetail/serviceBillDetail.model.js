"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ServiceBillDetail = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
const schema = new mongoose_1.default.Schema({
    companyId: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
        required: true,
        index: true,
    },
    userCreateId: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
        required: true,
        index: true,
    },
    serviceBillId: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
        required: true,
        index: true,
    },
    accountNumberId: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
        required: true,
        index: true,
    },
    stockId: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
        required: true,
        index: true,
    },
    receiveAccountNumber: {
        type: Number,
        index: true,
    },
    debitAccountNumber: {
        type: Number,
        index: true,
    },
    money: {
        type: Number,
    },
    moneyType: {
        type: String,
        default: "VND",
    },
    exchangeRate: {
        type: Number,
        default: 1,
    },
    finalMoney: {
        type: Number,
        required: true,
    },
    userCreateAt: {
        type: Date,
        default: Date.now,
    },
}, {
    collection: "ServiceBillDetail",
});
schema.virtual("userCreateInfo", {
    ref: "User",
    localField: "userCreateId",
    foreignField: "_id",
    justOne: true,
});
schema.virtual("companyInfo", {
    ref: "Companies",
    localField: "companyId",
    foreignField: "_id",
    justOne: true,
});
schema.virtual("stockInfo", {
    ref: "Stock",
    localField: "stockId",
    foreignField: "_id",
    justOne: true,
});
schema.set("toObject", { virtuals: true });
schema.set("toJSON", { virtuals: true });
exports.ServiceBillDetail = mongoose_1.default.model("ServiceBillDetail", schema);
//# sourceMappingURL=serviceBillDetail.model.js.map