"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const baseResponse_1 = __importDefault(require("../../services/baseResponse"));
const const_1 = require("../../const");
const errorsCode_1 = require("../../const/errorsCode");
const responseService_1 = __importDefault(require("../../services/responseService"));
const services_1 = __importDefault(require("../../services/services"));
const moneyBill_service_1 = __importDefault(require("./moneyBill.service"));
const node_xlsx_1 = __importDefault(require("node-xlsx"));
const moneyBill_validate_1 = __importDefault(require("./moneyBill.validate"));
const baseResponse = new baseResponse_1.default("MoneyBillMiddleware");
class MoneyBillMiddleware {
    userHaveBill(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { id } = req.params;
                const { userId } = req.cookies;
                const moneyBill = yield moneyBill_service_1.default.getOne({
                    _id: id,
                    userCreateId: userId,
                });
                if (!moneyBill) {
                    return responseService_1.default.send(res, const_1.HTTPCode.notAccept, {
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.moneyBillNotFound,
                                field: "id",
                            },
                        ],
                    });
                }
                next();
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    userGetDataFromFileMoneyBill(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { userId } = req.cookies;
                const { accountNumberId, companyId, billType } = req.body;
                const { moneyType, exchangeRate } = { moneyType: "VND", exchangeRate: 1 };
                const fileUrl = services_1.default.fileUploadDir(req);
                if (fileUrl === "")
                    return responseService_1.default.send(res, const_1.HTTPCode.notAccept, {
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.filesOnlyAcceptXlsxFiles,
                                error: "not accept this file - only support: xlsx",
                                field: "file",
                            },
                        ],
                    });
                const data = node_xlsx_1.default.parse(`${fileUrl}`);
                if (data[0].data.length <= 1) {
                    return responseService_1.default.send(res, const_1.HTTPCode.notAccept, {
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.moneyBillFromFileNotHaveData,
                                field: "file",
                                error: `Không có dữ liệu từ file`,
                            },
                        ],
                    });
                }
                const moneyData = [];
                const xlsxData = data[0].data;
                for (let i = 1; i < xlsxData.length; i++) {
                    const createName = xlsxData[i][0];
                    const receiveName = xlsxData[i][1];
                    const billCreateAt = moneyBill_validate_1.default.isCanConvertDateFromFile(xlsxData[i][2]);
                    const receiveAccountNumber = xlsxData[i][3];
                    const debitAccountNumber = xlsxData[i][4];
                    const money = xlsxData[i][5];
                    const address = xlsxData[i][6];
                    const purport = xlsxData[i][7];
                    if (!billCreateAt.success) {
                        return responseService_1.default.send(res, const_1.HTTPCode.notAccept, {
                            errors: [
                                {
                                    code: errorsCode_1.ErrorsCode.moneyBillFromFileWrongDateFormat,
                                    field: "file",
                                    error: `Dòng ${i + 1} - Sai dữ liệu ngày`,
                                },
                            ],
                        });
                    }
                    if (moneyBill_validate_1.default.isAccountNumberIsSame(receiveAccountNumber, debitAccountNumber)) {
                        return responseService_1.default.send(res, const_1.HTTPCode.notAccept, {
                            errors: [
                                {
                                    code: errorsCode_1.ErrorsCode.moneyBillFromFileNotAccountNumberIsSame,
                                    field: "file",
                                    error: `Dòng ${i + 1} - Tài khoản nợ và có không được trùng`,
                                },
                            ],
                        });
                    }
                    if (!moneyBill_validate_1.default.isExistedAccountNumber(accountNumberId, receiveAccountNumber)) {
                        return responseService_1.default.send(res, const_1.HTTPCode.notAccept, {
                            errors: [
                                {
                                    code: errorsCode_1.ErrorsCode.moneyBillFromFileNotExistedAccountNumber,
                                    field: "file",
                                    error: `Dòng ${i + 1} - Tài khoản có không tồn tại`,
                                },
                            ],
                        });
                    }
                    if (!moneyBill_validate_1.default.isExistedAccountNumber(accountNumberId, debitAccountNumber)) {
                        return responseService_1.default.send(res, const_1.HTTPCode.notAccept, {
                            errors: [
                                {
                                    code: errorsCode_1.ErrorsCode.moneyBillFromFileNotExistedAccountNumber,
                                    field: "file",
                                    error: `Dòng ${i + 1} - Tài khoản nợ không tồn tại`,
                                },
                            ],
                        });
                    }
                    if (!moneyBill_validate_1.default.isMoneyGreaterThenZero(money)) {
                        return responseService_1.default.send(res, const_1.HTTPCode.notAccept, {
                            errors: [
                                {
                                    code: errorsCode_1.ErrorsCode.moneyBillFromFileMoneyMustGreaterThanZero,
                                    field: "file",
                                    error: `Dòng ${i + 1} - Số tiền phải lớn hơn 0`,
                                },
                            ],
                        });
                    }
                    moneyData.push({
                        userCreateId: userId,
                        companyId,
                        billType,
                        accountNumberId,
                        receiveAccountNumber: parseInt(receiveAccountNumber),
                        debitAccountNumber: parseInt(debitAccountNumber),
                        createName,
                        receiveName,
                        address,
                        purport,
                        money: parseInt(money),
                        moneyType,
                        exchangeRate,
                        finalMoney: money,
                        billCreateAt: billCreateAt.date,
                    });
                }
                req.body.moneyData = moneyData;
                next();
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
}
exports.default = new MoneyBillMiddleware();
//# sourceMappingURL=moneyBill.middleware.js.map