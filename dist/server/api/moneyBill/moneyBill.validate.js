"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const const_1 = require("../../const");
const accountNumber_service_1 = __importDefault(require("../accountNumber/accountNumber.service"));
const person_service_1 = __importDefault(require("../person/person.service"));
class MoneyBillValidate {
    isAccountNumberIsSame(receiveAccountNumber, debitAccountNumber) {
        return parseInt(receiveAccountNumber) === parseInt(debitAccountNumber)
            ? true
            : false;
    }
    isPersonExisted(userId, personId) {
        return __awaiter(this, void 0, void 0, function* () {
            const person = yield person_service_1.default.getOne({
                userCreateId: userId,
                _id: personId,
                status: const_1.PersonStatus.normal,
            });
            return person ? true : false;
        });
    }
    isExistedAccountNumber(accountNumberId, accountNumber) {
        return __awaiter(this, void 0, void 0, function* () {
            const checkAccountNumber = yield accountNumber_service_1.default.getOne({
                _id: accountNumberId,
                "accountNumbers.accountNumber": parseInt(accountNumber),
            });
            if (!checkAccountNumber) {
                return false;
            }
            return true;
        });
    }
    isMoneyGreaterThenZero(money) {
        return parseInt(money) > 0 ? true : false;
    }
    isCanConvertDateFromFile(date) {
        const splitDate = date.split("/");
        if (splitDate.length !== 3)
            return { success: false };
        const newDate = new Date(`${splitDate[2]}-${splitDate[1]}-${splitDate[0]}`);
        return isNaN(newDate.getDay()) === false
            ? { success: true, date: newDate }
            : { success: false };
    }
}
exports.default = new MoneyBillValidate();
//# sourceMappingURL=moneyBill.validate.js.map