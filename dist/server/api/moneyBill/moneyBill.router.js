"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const validate_1 = require("../../middlewares/validate");
const const_1 = require("../../const");
const moneyBill_middleware_1 = __importDefault(require("./moneyBill.middleware"));
const moneyBill_controller_1 = __importDefault(require("./moneyBill.controller"));
const Valid_1 = require("../../middlewares/Valid");
const userCreateOrUpdateMoneyBill_dto_1 = __importDefault(require("./dto/userCreateOrUpdateMoneyBill.dto"));
const getMoneyBill_dto_1 = __importDefault(require("./dto/getMoneyBill.dto"));
const IdParamsRequest_dto_1 = __importDefault(require("../../const/IdParamsRequest.dto"));
const middleware_1 = __importDefault(require("../../middlewares/middleware"));
const moneyBillRouter = express_1.default.Router();
moneyBillRouter.post("/", Valid_1.Valid(userCreateOrUpdateMoneyBill_dto_1.default, 1 /* body */), validate_1.validateRole(null, const_1.AccountType.users), middleware_1.default.personIsSame, middleware_1.default.accountNumberIsSame, middleware_1.default.userHaveCompanies, middleware_1.default.existedAccountNumber, middleware_1.default.personCreateOrReceiveExisted, moneyBill_controller_1.default.userCreateMoneyBill);
moneyBillRouter.get("/", Valid_1.Valid(getMoneyBill_dto_1.default, 0 /* query */), validate_1.validateRole(null, const_1.AccountType.users), moneyBill_controller_1.default.userSearchMoneyBill);
moneyBillRouter.put("/:id", Valid_1.Valid(IdParamsRequest_dto_1.default, 2 /* params */), Valid_1.Valid(userCreateOrUpdateMoneyBill_dto_1.default, 1 /* body */), validate_1.validateRole(null, const_1.AccountType.users), moneyBill_middleware_1.default.userHaveBill, middleware_1.default.personIsSame, middleware_1.default.accountNumberIsSame, middleware_1.default.userHaveCompanies, middleware_1.default.existedAccountNumber, middleware_1.default.personCreateOrReceiveExisted, moneyBill_controller_1.default.userUpdateMoneyBill);
moneyBillRouter.delete("/:id", Valid_1.Valid(IdParamsRequest_dto_1.default, 2 /* params */), validate_1.validateRole(null, const_1.AccountType.users), moneyBill_middleware_1.default.userHaveBill, moneyBill_controller_1.default.userDeleteMoneyBill);
// moneyBillRouter.post(
//   "/info/file",
//   validateRole(null, AccountType.users),
//   uploadFile.uploadFile(["xlsx"], false, "HDTM").single("file"),
//   MoneyBillMiddleware.userGetDataFromFileMoneyBill,
//   moneyBillController.userGetDataFromFileMoneyBill
// );
// moneyBillRouter.post(
//   "/file",
//   validateRole(null, AccountType.users),
//   uploadFile.uploadFile(["xlsx"], false, "HDTM").single("file"),
//   MoneyBillMiddleware.userGetDataFromFileMoneyBill,
//   moneyBillController.userCreateFromFileMoneyBill
// );
exports.default = moneyBillRouter;
//# sourceMappingURL=moneyBill.router.js.map