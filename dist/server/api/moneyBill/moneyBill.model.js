"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MoneyBill = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
const schema = new mongoose_1.default.Schema({
    companyId: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
        required: true,
    },
    userCreateId: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
        required: true,
    },
    billType: {
        type: String,
        enum: ["receipt", "expenses"],
        required: true,
    },
    billNumber: {
        type: String,
    },
    accountNumberId: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
    },
    receiveAccountNumber: {
        type: Number,
        index: true,
    },
    debitAccountNumber: {
        type: Number,
        index: true,
    },
    personCreateId: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
    },
    personReceiveId: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
    },
    address: {
        type: String,
    },
    purport: {
        type: String,
        index: "text",
    },
    billCreateAt: {
        type: Date,
    },
    money: {
        type: Number,
    },
    moneyType: {
        type: String,
        default: "VND",
    },
    exchangeRate: {
        type: Number,
        default: 1,
    },
    finalMoney: {
        type: Number,
        required: true,
    },
    billCreatAt: {
        type: Date,
    },
    userCreateAt: {
        type: Date,
        default: Date.now,
    },
}, {
    collection: "MoneyBill",
});
schema.virtual("userInfo", {
    ref: "Users",
    localField: "userCreateId",
    foreignField: "_id",
    justOne: true,
});
schema.virtual("companyInfo", {
    ref: "Companies",
    localField: "companyId",
    foreignField: "_id",
    justOne: true,
});
schema.virtual("personCreateInfo", {
    ref: "Person",
    localField: "personCreateId",
    foreignField: "_id",
    justOne: true,
});
schema.virtual("personReceiveInfo", {
    ref: "Person",
    localField: "personReceiveId",
    foreignField: "_id",
    justOne: true,
});
schema.set("toObject", { virtuals: true });
schema.set("toJSON", { virtuals: true });
exports.MoneyBill = mongoose_1.default.model("MoneyBill", schema);
//# sourceMappingURL=moneyBill.model.js.map