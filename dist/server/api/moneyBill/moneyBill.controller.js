"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const baseResponse_1 = __importDefault(require("../../services/baseResponse"));
const const_1 = require("../../const");
const responseService_1 = __importDefault(require("../../services/responseService"));
const moneyBill_service_1 = __importDefault(require("./moneyBill.service"));
const baseResponse = new baseResponse_1.default("MoneyBillController");
class MoneyBillController {
    userCreateMoneyBill(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { userId } = req.cookies;
                const moneyBill = yield moneyBill_service_1.default.create(Object.assign(Object.assign({}, req.body), { userCreateId: userId }));
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    data: moneyBill,
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    userUpdateMoneyBill(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { id } = req.params;
                const { userId } = req.cookies;
                const moneyBill = yield moneyBill_service_1.default.updateById(id, Object.assign(Object.assign({}, req.body), { userCreateId: userId, userCreateAt: Date.now() }));
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    message: "update money bill success",
                    data: moneyBill,
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    userSearchMoneyBill(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { userId } = req.cookies;
                const { page, limit, receiveAccountNumber, debitAccountNumber, beginBillCreateAt, endBillCreateAt, personCreateId, personReceiveId, minMoney, maxMoney, sortType = -1, sortProperties, companyId, billType, } = req.query;
                let query = {
                    userCreateId: userId,
                    companyId,
                };
                let sort = {};
                if (sortProperties) {
                    sort[sortProperties] = parseInt(sortType);
                }
                console.log("sort: ", sort);
                if (receiveAccountNumber)
                    query["receiveAccountNumber"] = parseInt(receiveAccountNumber);
                if (debitAccountNumber)
                    query["debitAccountNumber"] = parseInt(debitAccountNumber);
                if (beginBillCreateAt && endBillCreateAt)
                    query = Object.assign(Object.assign({}, query), { $and: [
                            {
                                billCreateAt: { $gte: beginBillCreateAt },
                            },
                            {
                                billCreateAt: { $lte: endBillCreateAt },
                            },
                        ] });
                if (personCreateId) {
                    query["personCreateId"] = personCreateId;
                }
                if (personReceiveId) {
                    query["personReceiveId"] = personReceiveId;
                }
                if (minMoney || maxMoney) {
                    query = Object.assign(Object.assign({}, query), { $or: [
                            {
                                $and: [
                                    {
                                        money: { $gte: minMoney ? minMoney : 0 },
                                    },
                                    {
                                        money: { $lte: maxMoney ? maxMoney : Infinity },
                                    },
                                ],
                            },
                            {
                                $and: [
                                    {
                                        finalMoney: { $gte: minMoney ? minMoney : 0 },
                                    },
                                    {
                                        finalMoney: { $lte: maxMoney ? maxMoney : Infinity },
                                    },
                                ],
                            },
                        ] });
                }
                if (billType) {
                    query["billType"] = billType;
                }
                const moneyBill = yield moneyBill_service_1.default.populate({
                    query,
                    page,
                    limit,
                    sort: Object.assign({}, sort),
                    populate: [
                        {
                            path: "companyInfo",
                        },
                        {
                            path: "personCreateInfo",
                        },
                        {
                            path: "personReceiveInfo",
                        },
                    ],
                });
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    data: moneyBill,
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    userGetDataFromFileMoneyBill(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { moneyData } = req.body;
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    data: moneyData,
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    userCreateFromFileMoneyBill(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { moneyData } = req.body;
                yield moneyBill_service_1.default.createMany(moneyData);
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    data: {
                        count: moneyData.length,
                    },
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    userDeleteMoneyBill(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { id } = req.params;
                yield moneyBill_service_1.default.deleteById(id);
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    message: "delete money bill success",
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
}
exports.default = new MoneyBillController();
//# sourceMappingURL=moneyBill.controller.js.map