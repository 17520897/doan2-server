"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.MoneyBillService = void 0;
const moneyBill_model_1 = require("./moneyBill.model");
const baseDb_1 = __importDefault(require("../../services/baseDb"));
class MoneyBillService extends baseDb_1.default {
}
exports.MoneyBillService = MoneyBillService;
exports.default = new MoneyBillService(moneyBill_model_1.MoneyBill);
//# sourceMappingURL=moneyBill.service.js.map