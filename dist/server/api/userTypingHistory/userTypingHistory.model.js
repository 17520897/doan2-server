"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserTypingHistory = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
const schema = new mongoose_1.default.Schema({
    userCreateId: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
        required: true,
    },
    address: [],
    money: [],
}, {
    collection: "UserTypingHistory",
});
schema.virtual("userCreateInfo", {
    ref: "User",
    localField: "userCreateId",
    foreignField: "_id",
    justOne: true,
});
schema.set("toObject", { virtuals: true });
schema.set("toJSON", { virtuals: true });
exports.UserTypingHistory = mongoose_1.default.model("UserTypingHistory", schema);
//# sourceMappingURL=userTypingHistory.model.js.map