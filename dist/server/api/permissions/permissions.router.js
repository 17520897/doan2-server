"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const permissions_controller_1 = __importDefault(require("./permissions.controller"));
const permissionRouter = express_1.default.Router();
permissionRouter.get("/", permissions_controller_1.default.getListPermissions);
exports.default = permissionRouter;
//# sourceMappingURL=permissions.router.js.map