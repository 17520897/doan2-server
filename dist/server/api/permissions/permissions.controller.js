"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PermissionController = void 0;
const responseService_1 = __importDefault(require("../../services/responseService"));
const permissions_service_1 = __importDefault(require("./permissions.service"));
const baseResponse_1 = __importDefault(require("../../services/baseResponse"));
const const_1 = require("../../const");
const baseResponse = new baseResponse_1.default("PermissionController");
class PermissionController {
    getListPermissions(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { page, limit } = req.query;
                const permissions = yield permissions_service_1.default.getByQuery({
                    page,
                    limit,
                    sort: {
                        groupName: 1,
                    },
                });
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    message: "get all permissions success",
                    data: permissions,
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
}
exports.PermissionController = PermissionController;
exports.default = new PermissionController();
//# sourceMappingURL=permissions.controller.js.map