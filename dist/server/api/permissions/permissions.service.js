"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PermissionsService = void 0;
const baseDb_1 = __importDefault(require("../../services/baseDb"));
const permissions_model_1 = require("./permissions.model");
class PermissionsService extends baseDb_1.default {
}
exports.PermissionsService = PermissionsService;
exports.default = new PermissionsService(permissions_model_1.Permissions);
//# sourceMappingURL=permissions.service.js.map