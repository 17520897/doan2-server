"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SettingTaxService = void 0;
const settingTax_model_1 = require("./settingTax.model");
const baseDb_1 = __importDefault(require("../../services/baseDb"));
class SettingTaxService extends baseDb_1.default {
}
exports.SettingTaxService = SettingTaxService;
exports.default = new SettingTaxService(settingTax_model_1.SettingTax);
//# sourceMappingURL=settingTax.service.js.map