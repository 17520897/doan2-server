"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const baseResponse_1 = __importDefault(require("../../services/baseResponse"));
const const_1 = require("../../const");
const responseService_1 = __importDefault(require("../../services/responseService"));
const settingTax_service_1 = __importDefault(require("./settingTax.service"));
const mongoose_1 = __importDefault(require("mongoose"));
const baseResponse = new baseResponse_1.default("SettingTaxController");
class SettingTaxController {
    createSettingTax(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { adminId } = req.cookies;
                const settingTax = yield settingTax_service_1.default.create(Object.assign(Object.assign({}, req.body), { adminCreateId: adminId }));
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    data: settingTax,
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    updateSettingTax(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { adminId } = req.cookies;
                const { id } = req.params;
                const settingTax = yield settingTax_service_1.default.updateById(id, Object.assign(Object.assign({}, req.body), { adminCreateId: adminId }));
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    data: settingTax,
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    getAllSettingTax(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { search } = req.query;
                const query = {};
                if (search) {
                    query["name"] = { $regex: search, $options: "i" };
                }
                const settingTax = yield settingTax_service_1.default.getByQuery({
                    query,
                    sort: {
                        accountNumber: 1,
                        percent: 1,
                        name: 1,
                    },
                });
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    data: settingTax,
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    deleteSettingTax(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const session = yield mongoose_1.default.startSession();
            session.startTransaction();
            try {
                const options = { session };
                const { id } = req.params;
                yield settingTax_service_1.default.deleteById(id, options);
                yield session.commitTransaction();
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    message: "delete setting tax success",
                });
            }
            catch (error) {
                yield session.abortTransaction();
                return baseResponse.serverError(res, error);
            }
            finally {
                yield session.endSession();
            }
        });
    }
}
exports.default = new SettingTaxController();
//# sourceMappingURL=settingTax.controller.js.map