"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const const_1 = require("../../const");
const IdParamsRequest_dto_1 = __importDefault(require("../../const/IdParamsRequest.dto"));
const Valid_1 = require("../../middlewares/Valid");
const validate_1 = require("../../middlewares/validate");
const createSettingTax_dto_1 = __importDefault(require("./dto/createSettingTax.dto"));
const getSettingTax_dto_1 = __importDefault(require("./dto/getSettingTax.dto"));
const settingTax_controller_1 = __importDefault(require("./settingTax.controller"));
const settingTax_middleware_1 = __importDefault(require("./settingTax.middleware"));
const settingTaxRouter = express_1.default.Router();
settingTaxRouter.post("/", Valid_1.Valid(createSettingTax_dto_1.default, 1 /* body */), validate_1.validateRole(null, const_1.AccountType.admin), settingTax_middleware_1.default.accountNumberExisted, settingTax_middleware_1.default.createSettingTax, settingTax_controller_1.default.createSettingTax);
settingTaxRouter.put("/:id", Valid_1.Valid(IdParamsRequest_dto_1.default, 2 /* params */), Valid_1.Valid(createSettingTax_dto_1.default, 1 /* body */), validate_1.validateRole(null, const_1.AccountType.admin), settingTax_middleware_1.default.settingTaxExisted, settingTax_middleware_1.default.accountNumberExisted, settingTax_middleware_1.default.updateSettingTax, settingTax_controller_1.default.updateSettingTax);
settingTaxRouter.delete("/:id", Valid_1.Valid(IdParamsRequest_dto_1.default, 2 /* params */), validate_1.validateRole(null, const_1.AccountType.admin), settingTax_middleware_1.default.settingTaxExisted, settingTax_controller_1.default.deleteSettingTax);
settingTaxRouter.get("/", Valid_1.Valid(getSettingTax_dto_1.default, 0 /* query */), validate_1.validateRole(null, const_1.AccountType.all), settingTax_controller_1.default.getAllSettingTax);
exports.default = settingTaxRouter;
//# sourceMappingURL=settingTax.router.js.map