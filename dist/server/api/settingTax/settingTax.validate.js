"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const accountNumber_service_1 = __importDefault(require("../accountNumber/accountNumber.service"));
const settingTax_service_1 = __importDefault(require("./settingTax.service"));
class SettingTaxValidate {
    isNameExisted(name, checkId = false, id = "") {
        return __awaiter(this, void 0, void 0, function* () {
            const settingTax = checkId
                ? yield settingTax_service_1.default.getOne({
                    name,
                    _id: { $ne: id },
                })
                : yield settingTax_service_1.default.getOne({ name });
            return settingTax ? true : false;
        });
    }
    isSettingTaxExisted(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const settingTax = yield settingTax_service_1.default.getById(id);
            return settingTax ? true : false;
        });
    }
    isAccountNumberExisted(accountNumber) {
        return __awaiter(this, void 0, void 0, function* () {
            const isExistedAccountNumber = yield accountNumber_service_1.default.getOne({
                "accountNumbers.accountNumber": accountNumber,
            });
            return isExistedAccountNumber ? true : false;
        });
    }
}
exports.default = new SettingTaxValidate();
//# sourceMappingURL=settingTax.validate.js.map