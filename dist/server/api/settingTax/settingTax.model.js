"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SettingTax = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
const schema = new mongoose_1.default.Schema({
    adminCreateId: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
        required: true,
    },
    name: {
        type: String,
    },
    percent: {
        type: Number,
    },
    accountNumber: {
        type: Number,
    },
}, {
    collection: "SettingTax",
});
schema.virtual("adminCreateInfo", {
    ref: "Admin",
    localField: "adminCreateId",
    foreignField: "_id",
    justOne: true,
});
schema.set("toObject", { virtuals: true });
schema.set("toJSON", { virtuals: true });
exports.SettingTax = mongoose_1.default.model("SettingTax", schema);
//# sourceMappingURL=settingTax.model.js.map