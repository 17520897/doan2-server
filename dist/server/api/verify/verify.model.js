"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Verify = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
const schema = new mongoose_1.default.Schema({
    code: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
    },
    phone: {
        type: String,
    },
    name: {
        type: String,
    },
    createAt: {
        type: Date,
        default: Date.now,
        index: {
            expires: "5m",
        },
    },
    accountType: {
        type: String,
        enum: ["user", "shop", "inspector", "shopEmployee"],
    },
    type: {
        type: String,
        enum: ["register", "forgotPassword"],
    },
}, {
    collection: "Verify",
});
exports.Verify = mongoose_1.default.model("Verify", schema);
//# sourceMappingURL=verify.model.js.map