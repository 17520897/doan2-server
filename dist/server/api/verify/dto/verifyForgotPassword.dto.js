"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const class_validator_1 = require("class-validator");
class VerifyForgotPasswordDto {
}
__decorate([
    class_validator_1.IsOptional(),
    class_validator_1.IsMongoId(),
    __metadata("design:type", String)
], VerifyForgotPasswordDto.prototype, "userId", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.Length(6),
    __metadata("design:type", String)
], VerifyForgotPasswordDto.prototype, "code", void 0);
__decorate([
    class_validator_1.IsEmail(),
    __metadata("design:type", String)
], VerifyForgotPasswordDto.prototype, "email", void 0);
__decorate([
    class_validator_1.IsString(),
    class_validator_1.MinLength(8),
    __metadata("design:type", String)
], VerifyForgotPasswordDto.prototype, "password", void 0);
exports.default = VerifyForgotPasswordDto;
//# sourceMappingURL=verifyForgotPassword.dto.js.map