"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.VerifyController = void 0;
const responseService_1 = __importDefault(require("../../services/responseService"));
const index_1 = require("../../const/index");
const users_service_1 = __importDefault(require("../users/users.service"));
const verify_service_1 = __importDefault(require("./verify.service"));
const randomstring_1 = __importDefault(require("randomstring"));
const bcrypt_1 = __importDefault(require("bcrypt"));
const sendSms_1 = __importDefault(require("../../services/sendSms"));
const errorsCode_1 = require("../../const/errorsCode");
const baseResponse_1 = __importDefault(require("../../services/baseResponse"));
const mongoose_1 = __importDefault(require("mongoose"));
const services_1 = __importDefault(require("../../services/services"));
const baseResponse = new baseResponse_1.default("VerifyController");
class VerifyController {
    verifyRegister(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const session = yield mongoose_1.default.startSession();
            session.startTransaction();
            try {
                const options = { session };
                const { email, password, name } = req.body;
                const hash = yield services_1.default.hashPassword(password, parseInt(process.env.HASH_PASSWORD_SALT));
                const user = yield users_service_1.default.create({
                    email,
                    password: hash,
                    name,
                }, options);
                yield verify_service_1.default.deleteMany({
                    email,
                    type: index_1.VerifyType.register,
                    accountType: index_1.VerifyAccountType.user,
                }, options);
                // await userUsedHistoryService.create(
                //   {
                //     userId: user._id,
                //   },
                //   options
                // );
                user.password = undefined;
                yield session.commitTransaction();
                session.endSession();
                return responseService_1.default.send(res, index_1.HTTPCode.success, {
                    message: "verify new user success",
                    data: user,
                });
            }
            catch (error) {
                session.abortTransaction();
                return baseResponse.serverError(res, error);
            }
            finally {
                session.endSession();
            }
        });
    }
    getRegisterVerifyCode(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { phone } = req.body;
                const code = randomstring_1.default.generate({
                    length: 6,
                    charset: "numeric",
                });
                if (process.env.NODE_ENV !== "dev") {
                    const sms = yield sendSms_1.default.send(phone, code, index_1.SMSType.register);
                    if (sms.errors) {
                        return responseService_1.default.send(res, index_1.HTTPCode.serverError, {
                            errors: [
                                ...sms.errors,
                                {
                                    code: errorsCode_1.ErrorsCode.smsCanNotSend,
                                    error: "can not send sms",
                                },
                            ],
                        });
                    }
                }
                yield verify_service_1.default.create({
                    code,
                    phone: phone,
                    type: index_1.VerifyType.register,
                    accountType: index_1.VerifyAccountType.user,
                });
                return responseService_1.default.send(res, index_1.HTTPCode.success, {
                    message: "get new verify code success",
                    data: [
                        {
                            phone: req.query.phone,
                        },
                    ],
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    getForgotPasswordVerifyCode(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { userId, phone } = req.body;
                const code = randomstring_1.default.generate({ length: 6, charset: "numeric" });
                const sms = yield sendSms_1.default.send(phone, code, index_1.SMSType.forgotPassword);
                if (sms.errors) {
                    return responseService_1.default.send(res, index_1.HTTPCode.serverError, {
                        errors: [
                            ...sms.errors,
                            {
                                code: errorsCode_1.ErrorsCode.smsCanNotSend,
                                error: "can not send sms",
                            },
                        ],
                    });
                }
                yield verify_service_1.default.create({
                    userId,
                    code,
                    phone,
                    type: index_1.VerifyType.forgotPassword,
                    accountType: index_1.VerifyAccountType.user,
                });
                return responseService_1.default.send(res, index_1.HTTPCode.success, {
                    message: "get forgot password verify code success",
                    data: [
                        {
                            phone,
                        },
                    ],
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    resetPassword(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const session = yield mongoose_1.default.startSession();
            session.startTransaction();
            try {
                const options = { session };
                const { password, phone } = req.body;
                const salt = yield bcrypt_1.default.genSalt(10);
                const hash = yield bcrypt_1.default.hash(password, salt);
                const user = yield users_service_1.default.updateOne({ phone }, { password: hash }, options);
                yield verify_service_1.default.deleteMany({
                    phone,
                    accountType: index_1.VerifyAccountType.user,
                }, options);
                yield session.commitTransaction();
                session.endSession();
                return responseService_1.default.send(res, index_1.HTTPCode.success, {
                    message: "reset password success",
                    data: {
                        userId: user._id,
                        phone: user.phone,
                    },
                });
            }
            catch (error) {
                session.abortTransaction();
                return baseResponse.serverError(res, error);
            }
            finally {
                session.endSession();
            }
        });
    }
}
exports.VerifyController = VerifyController;
exports.default = new VerifyController();
//# sourceMappingURL=verify.controller.js.map