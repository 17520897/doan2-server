"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.VerifyService = void 0;
const verify_model_1 = require("./verify.model");
const baseDb_1 = __importDefault(require("../../services/baseDb"));
class VerifyService extends baseDb_1.default {
}
exports.VerifyService = VerifyService;
exports.default = new VerifyService(verify_model_1.Verify);
//# sourceMappingURL=verify.service.js.map