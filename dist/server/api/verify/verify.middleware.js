"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const responseService_1 = __importDefault(require("../../services/responseService"));
const index_1 = require("../../const/index");
const users_service_1 = __importDefault(require("../users/users.service"));
const verify_service_1 = __importDefault(require("./verify.service"));
const logger_1 = __importDefault(require("../../common/logger"));
const errorsCode_1 = require("../../const/errorsCode");
const baseResponse_1 = __importDefault(require("../../services/baseResponse"));
function allowSendCode(createAt) {
    const now = Date.now();
    const createTime = createAt.getTime();
    return now - createTime > parseInt(process.env.TIME_WAIT_SEND_VERIFY_CODE)
        ? true
        : false;
}
const baseResponse = new baseResponse_1.default("VerifyMiddleware");
class VerifyMiddleware {
    verifyRegister(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { email, code } = req.body;
                const verify = yield verify_service_1.default.getOne({
                    email,
                    code,
                    type: index_1.VerifyType.register,
                    accountType: index_1.VerifyAccountType.user,
                });
                if (!verify)
                    return responseService_1.default.send(res, index_1.HTTPCode.notAccept, {
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.verifyCodeNotFound,
                                field: "code",
                                error: "verify not found",
                            },
                        ],
                    });
                const users = yield users_service_1.default.getOne({ email: req.body.email });
                if (users)
                    return responseService_1.default.send(res, index_1.HTTPCode.notAccept, {
                        message: "user has been registered",
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.usersEmailRegistered,
                                field: "email",
                                error: "email number has been registered",
                            },
                        ],
                    });
                req.body.verifyId = verify._id;
                next();
            }
            catch (error) {
                return responseService_1.default.send(res, index_1.HTTPCode.serverError, {
                    errors: [
                        {
                            code: errorsCode_1.ErrorsCode.serverError,
                            error: error.message,
                        },
                    ],
                });
            }
        });
    }
    getRegisterVerifyCode(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { email } = req.query;
                const user = yield users_service_1.default.getOne({ email });
                if (user)
                    return responseService_1.default.send(res, index_1.HTTPCode.notAccept, {
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.verifyEmailHasBeenRegistered,
                                field: "phone",
                            },
                        ],
                    });
                const verify = yield verify_service_1.default.getSort({ email, type: index_1.VerifyType.register }, { createAt: -1 });
                if (verify.length > 0 && !allowSendCode(verify[0].createAt))
                    return responseService_1.default.send(res, index_1.HTTPCode.notAccept, {
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.verifyCodeHasSent,
                                error: "code has sent, can get code after 1 min",
                                field: "createAt",
                            },
                        ],
                    });
                req.body.email = email;
                next();
            }
            catch (error) {
                return responseService_1.default.send(res, index_1.HTTPCode.serverError, {
                    errors: [{ code: errorsCode_1.ErrorsCode.serverError, error: error.message }],
                });
            }
        });
    }
    getForgotPasswordCode(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { email } = req.query;
                const user = yield users_service_1.default.getOne({ email });
                if (!user)
                    return responseService_1.default.send(res, index_1.HTTPCode.notAccept, {
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.verifyCantForgotPassword,
                                error: "user not found",
                                field: "phone",
                            },
                        ],
                    });
                const verify = yield verify_service_1.default.getSort({
                    email,
                    type: index_1.VerifyType.forgotPassword,
                    accountType: index_1.VerifyAccountType.user,
                }, { createAt: -1 });
                if (verify.length > 0 && !allowSendCode(verify[0].createAt))
                    return responseService_1.default.send(res, index_1.HTTPCode.notAccept, {
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.verifyCodeHasSent,
                                error: "code has sent, can get code after 1 min",
                                field: "createAt",
                            },
                        ],
                    });
                req.body.userId = user._id;
                req.body.email = user.email;
                next();
            }
            catch (error) {
                return responseService_1.default.send(res, index_1.HTTPCode.serverError, {
                    errors: [
                        {
                            code: errorsCode_1.ErrorsCode.serverError,
                            error: error.message,
                        },
                    ],
                });
            }
        });
    }
    resetPassword(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { email, code } = req.body;
                const verify = yield verify_service_1.default.getOne({
                    email,
                    code,
                    type: index_1.VerifyType.forgotPassword,
                    accountType: index_1.VerifyAccountType.user,
                });
                if (!verify)
                    return responseService_1.default.send(res, index_1.HTTPCode.notAccept, {
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.verifyCodeNotFound,
                                error: "verify code not found",
                                field: "code",
                            },
                        ],
                    });
                next();
            }
            catch (error) {
                logger_1.default.info(`middleware reset password: ${error.message}`);
                return responseService_1.default.send(res, index_1.HTTPCode.serverError, {
                    errors: [
                        {
                            code: errorsCode_1.ErrorsCode.serverError,
                            error: error.message,
                        },
                    ],
                });
            }
        });
    }
}
exports.default = new VerifyMiddleware();
//# sourceMappingURL=verify.middleware.js.map