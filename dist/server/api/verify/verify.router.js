"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const Valid_1 = require("../../middlewares/Valid");
const verifyRegister_dto_1 = __importDefault(require("./dto/verifyRegister.dto"));
const verifyForgotPassword_dto_1 = __importDefault(require("./dto/verifyForgotPassword.dto"));
const verifyGetRegister_dto_1 = __importDefault(require("./dto/verifyGetRegister.dto"));
const verify_controller_1 = __importDefault(require("./verify.controller"));
const verify_middleware_1 = __importDefault(require("./verify.middleware"));
const verifyRouter = express_1.default.Router();
verifyRouter.post("/register", Valid_1.Valid(verifyRegister_dto_1.default, 1 /* body */), verify_middleware_1.default.verifyRegister, verify_controller_1.default.verifyRegister);
verifyRouter.get("/register", Valid_1.Valid(verifyGetRegister_dto_1.default, 0 /* query */), verify_middleware_1.default.getRegisterVerifyCode, verify_controller_1.default.getRegisterVerifyCode);
verifyRouter.get("/forgotPassword", verify_middleware_1.default.getForgotPasswordCode, verify_controller_1.default.getForgotPasswordVerifyCode);
verifyRouter.post("/forgotPassword", Valid_1.Valid(verifyForgotPassword_dto_1.default, 1 /* body */), verify_middleware_1.default.resetPassword, verify_controller_1.default.resetPassword);
exports.default = verifyRouter;
//# sourceMappingURL=verify.router.js.map