"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Products = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
const schema = new mongoose_1.default.Schema({
    userCreateId: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
        required: true,
    },
    companyId: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
        required: true,
    },
    code: {
        type: String,
        index: "text",
    },
    name: {
        type: String,
        index: "text",
    },
    taxConfig: [
        {
            name: {
                type: String,
            },
            percent: {
                type: Number,
            },
            accountNumber: {
                type: Number,
            },
        },
    ],
    status: {
        type: String,
        default: "normal",
        enum: ["normal", "deleted"],
    },
}, {
    collection: "Products",
});
schema.virtual("adminCreateInfo", {
    ref: "Admin",
    localField: "adminCreateId",
    foreignField: "_id",
    justOne: true,
});
schema.set("toObject", { virtuals: true });
schema.set("toJSON", { virtuals: true });
exports.Products = mongoose_1.default.model("Products", schema);
//# sourceMappingURL=products.model.js.map