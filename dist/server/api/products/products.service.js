"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const baseDb_1 = __importDefault(require("../../services/baseDb"));
const products_model_1 = require("./products.model");
class ProductsService extends baseDb_1.default {
}
exports.default = new ProductsService(products_model_1.Products);
//# sourceMappingURL=products.service.js.map