"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const const_1 = require("../../const");
const baseResponse_1 = __importDefault(require("../../services/baseResponse"));
const products_service_1 = __importDefault(require("./products.service"));
const baseResponse = new baseResponse_1.default("ProductsController");
class ProductsController {
    createProducts(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { userId } = req.cookies;
                const products = yield products_service_1.default.create(Object.assign(Object.assign({}, req.body), { userCreateId: userId }));
                return baseResponse.success({
                    res,
                    data: products,
                    message: "create products success",
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    updateProducts(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { id } = req.params;
                const products = yield products_service_1.default.updateById(id, req.body);
                return baseResponse.success({
                    res,
                    data: products,
                    message: "update products success",
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    deleteProducts(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { id } = req.params;
                yield products_service_1.default.updateById(id, {
                    status: const_1.ProductsStatus.deleted,
                });
                return baseResponse.success({
                    res,
                    message: "delete products success",
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    getProducts(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { page, limit, search, companyId } = req.query;
                const { userId } = req.cookies;
                let query = {
                    userCreateId: userId,
                    companyId,
                };
                if (search) {
                    query = Object.assign(Object.assign({}, query), { $or: [
                            {
                                code: { $regex: search, $options: "i" },
                            },
                            {
                                name: { $regex: search, $options: "i" },
                            },
                        ] });
                }
                const products = yield products_service_1.default.getByQuery({
                    query,
                    page,
                    limit,
                    sort: {
                        code: 1,
                        name: 1,
                    },
                });
                return baseResponse.success({
                    res,
                    data: products,
                    message: "get products success",
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
}
exports.default = new ProductsController();
//# sourceMappingURL=products.controller.js.map