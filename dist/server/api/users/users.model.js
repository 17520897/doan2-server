"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Users = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
const schema = new mongoose_1.default.Schema({
    email: {
        type: String,
        required: true,
    },
    password: {
        type: String,
        required: true,
    },
    name: {
        type: String,
        index: "text",
    },
    phone: {
        type: String,
        index: "text",
    },
    identityNumber: {
        type: String,
        index: "text",
    },
    temporaryAddress: {
        home: {
            type: String,
        },
        ward: {
            type: Number,
        },
        district: {
            type: Number,
        },
        city: {
            type: Number,
        },
        wardText: {
            type: String,
        },
        districtText: {
            type: String,
        },
        cityText: {
            type: String,
        },
    },
    permanentAddress: {
        home: {
            type: String,
        },
        ward: {
            type: Number,
        },
        district: {
            type: Number,
        },
        city: {
            type: Number,
        },
        wardText: {
            type: Number,
        },
        districtText: {
            type: Number,
        },
        cityText: {
            type: Number,
        },
    },
    gender: {
        type: String,
        enum: ["male", "female", "other"],
    },
    dayOfBirth: {
        type: Date,
    },
    dateOfIssue: {
        type: Date,
    },
    placeOfIssue: {
        type: Number,
    },
    createAt: {
        type: Date,
        default: Date.now,
    },
    status: {
        type: String,
        enum: ["normal", "block"],
        default: "normal",
    },
    roleId: {
        type: mongoose_1.default.Types.ObjectId,
    },
}, {
    collection: "Users",
});
schema.virtual("roleInfo", {
    ref: "Roles",
    localField: "roleId",
    foreignField: "_id",
    justOne: true,
});
schema.set("toObject", { virtuals: true });
schema.set("toJSON", { virtuals: true });
exports.Users = mongoose_1.default.model("Users", schema);
//# sourceMappingURL=users.model.js.map