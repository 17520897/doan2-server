"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UsersController = void 0;
const verify_service_1 = __importDefault(require("../verify/verify.service"));
const jwt_1 = __importDefault(require("../../services/jwt"));
const responseService_1 = __importDefault(require("../../services/responseService"));
const randomstring_1 = __importDefault(require("randomstring"));
const index_1 = require("../../const/index");
const users_service_1 = __importDefault(require("./users.service"));
const errorsCode_1 = require("../../const/errorsCode");
const responseService_2 = __importDefault(require("../../services/responseService"));
const baseResponse_1 = __importDefault(require("../../services/baseResponse"));
const mongoose_1 = __importDefault(require("mongoose"));
const userRoles_service_1 = __importDefault(require("../userRoles/userRoles.service"));
const systemConst_service_1 = __importDefault(require("../systemConst/systemConst.service"));
const mailService_1 = __importDefault(require("../../services/mailService"));
const baseResponse = new baseResponse_1.default("UsersController");
class UsersController {
    register(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const code = randomstring_1.default.generate({
                    length: 6,
                    charset: "numeric",
                });
                const { phone, email } = req.body;
                // if (process.env.NODE_ENV !== "dev") {
                //   const sms = await sendSMS.send(phone, code, SMSType.register);
                //   if (sms.errors) {
                //     return ResponseService.send(res, HTTPCode.serverError, {
                //       errors: [
                //         {
                //           code: ErrorsCode.smsCanNotSend,
                //           error: "can not send sms",
                //         },
                //       ],
                //     });
                //   }
                // }
                const mail = yield mailService_1.default.send({
                    to: email,
                    subject: "[PD Accounting] Register Code",
                    html: `
          <h1>PD Accounting Register Successful</h1>
          <br>Your verify code is: <b>${code}</b>
          <br>Your verify code will be valid for 5 minute
          <br><b>Thank you for using our services!</b>
        `,
                });
                if (mail.errors) {
                    return responseService_2.default.send(res, index_1.HTTPCode.serverError, {
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.emailCanNotSend,
                                error: "email can not send",
                            },
                        ],
                    });
                }
                //make verify code
                yield verify_service_1.default.create({
                    email: email,
                    code,
                    type: index_1.VerifyType.register,
                    accountType: index_1.VerifyAccountType.user,
                });
                return responseService_1.default.send(res, index_1.HTTPCode.success, {
                    message: "register success",
                    data: null,
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    login(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                //Get from middleware
                const { userId, roleId } = req.body;
                const [userRoles, systemConst] = yield Promise.all([
                    userRoles_service_1.default.getOne({
                        _id: roleId,
                        status: index_1.UserRoleStatus.enable,
                    }),
                    systemConst_service_1.default.getOne({}),
                ]);
                let permissions;
                if (!userRoles) {
                    const userFreeRoles = yield userRoles_service_1.default.getById(systemConst.userRoleFreeId);
                    permissions = userFreeRoles.rolePermissions;
                }
                else {
                    permissions = userRoles.rolePermissions;
                }
                return responseService_1.default.send(res, index_1.HTTPCode.success, {
                    message: "login success",
                    data: {
                        accessToken: jwt_1.default.generateAccessToken({
                            userId,
                            isUser: true,
                        }),
                        refreshToken: jwt_1.default.generateRefreshToken({
                            userId,
                            isUser: true,
                        }),
                        permissions,
                    },
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    getUsersInfo(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { userId } = req.cookies;
                const user = yield users_service_1.default.populate({
                    query: {
                        _id: userId,
                        status: index_1.UsersStatus.normal,
                    },
                    populate: [
                        {
                            path: "roleInfo",
                        },
                    ],
                    select: "-password",
                });
                return responseService_1.default.send(res, index_1.HTTPCode.success, {
                    message: "Get user info success",
                    data: user,
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    updateUserInfo(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const session = yield mongoose_1.default.startSession();
            session.startTransaction();
            try {
                const options = { session };
                const { userId } = req.cookies;
                const user = yield users_service_1.default.updateById(userId, req.body, options);
                user.password = undefined;
                yield session.commitTransaction();
                session.endSession();
                return responseService_1.default.send(res, index_1.HTTPCode.success, {
                    message: "update user success",
                    data: user,
                });
            }
            catch (error) {
                yield session.abortTransaction();
                return baseResponse.serverError(res, error);
            }
            finally {
                session.endSession();
            }
        });
    }
}
exports.UsersController = UsersController;
exports.default = new UsersController();
//# sourceMappingURL=users.controller.js.map