"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BorrowerUserService = void 0;
const users_model_1 = require("./users.model");
const baseDb_1 = __importDefault(require("../../services/baseDb"));
class BorrowerUserService extends baseDb_1.default {
}
exports.BorrowerUserService = BorrowerUserService;
exports.default = new BorrowerUserService(users_model_1.Users);
//# sourceMappingURL=users.service.js.map