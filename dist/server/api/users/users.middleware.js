"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const responseService_1 = __importDefault(require("../../services/responseService"));
const bcrypt_1 = __importDefault(require("bcrypt"));
const errorsCode_1 = require("../../const/errorsCode");
const users_service_1 = __importDefault(require("./users.service"));
const const_1 = require("../../const");
const responseService_2 = __importDefault(require("../../services/responseService"));
const verify_service_1 = __importDefault(require("../verify/verify.service"));
const baseResponse_1 = __importDefault(require("../../services/baseResponse"));
const location_service_1 = __importDefault(require("../location/location.service"));
const baseResponse = new baseResponse_1.default("UsersMiddleware");
class UsersMiddleware {
    register(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { email } = req.body;
                const verify = yield verify_service_1.default.getByQuery({
                    query: {
                        email,
                        createAt: {
                            $gt: Date.now() - parseInt(process.env.TIME_WAIT_SEND_VERIFY_CODE),
                        },
                        accountType: const_1.VerifyAccountType.user,
                    },
                });
                if (verify.length > 0) {
                    return responseService_1.default.send(res, const_1.HTTPCode.notAccept, {
                        message: "verify code has been sent",
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.verifyCodeHasSent,
                                field: "createAt",
                                error: "only 1 code in 1 minute",
                            },
                        ],
                    });
                }
                const users = yield users_service_1.default.getOne({ email: req.body.email });
                if (users)
                    return responseService_1.default.send(res, const_1.HTTPCode.notAccept, {
                        message: "user has been registered",
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.usersEmailRegistered,
                                field: "email",
                                error: "email has been registered",
                            },
                        ],
                    });
                next();
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    login(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const body = req.body;
                const { email } = req.body;
                const [checkExistedUsers, checkBlockedUsers] = yield Promise.all([
                    users_service_1.default.getOne({ email }),
                    users_service_1.default.getOne({ email, status: const_1.UsersStatus.normal }),
                ]);
                if (!checkExistedUsers)
                    return responseService_1.default.send(res, const_1.HTTPCode.notAccept, {
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.usersNotFound,
                                field: "phone",
                                error: "user not found",
                            },
                        ],
                    });
                if (!checkBlockedUsers) {
                    return responseService_1.default.send(res, const_1.HTTPCode.notAccept, {
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.usersIsBlocked,
                                field: "status",
                                error: "user is blocked",
                            },
                        ],
                    });
                }
                const match = yield bcrypt_1.default.compare(body.password, checkExistedUsers.password);
                if (!match)
                    return responseService_1.default.send(res, const_1.HTTPCode.notAccept, {
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.usersWrongPassword,
                                field: "password",
                                error: "wrong password",
                            },
                        ],
                        message: "wrong password",
                    });
                req.body.userId = checkExistedUsers._id;
                req.body.roleId = checkExistedUsers.roleId;
                next();
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    updateUsersInfo(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { password, temporaryAddress, permanentAddress, } = req.body;
                const { oldPassword } = req.body;
                if (temporaryAddress) {
                    const checkTemporaryAddress = yield location_service_1.default.getByAddress({
                        ward: temporaryAddress.ward,
                        district: temporaryAddress.district,
                        city: temporaryAddress.city,
                    });
                    if (!checkTemporaryAddress.city ||
                        checkTemporaryAddress.district ||
                        checkTemporaryAddress.ward) {
                        return responseService_2.default.send(res, const_1.HTTPCode.notAccept, {
                            errors: [
                                {
                                    code: errorsCode_1.ErrorsCode.locationNotFound,
                                    field: "temporaryAddress",
                                },
                            ],
                        });
                    }
                }
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
}
exports.default = new UsersMiddleware();
//# sourceMappingURL=users.middleware.js.map