"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const const_1 = require("../../const");
const Valid_1 = require("../../middlewares/Valid");
const validate_1 = require("../../middlewares/validate");
const usesRegister_dto_1 = __importDefault(require("./dto/usesRegister.dto"));
const users_controller_1 = __importDefault(require("./users.controller"));
const users_middleware_1 = __importDefault(require("./users.middleware"));
const usersRouter = express_1.default.Router();
usersRouter.post("/register", Valid_1.Valid(usesRegister_dto_1.default, 1 /* body */), users_middleware_1.default.register, users_controller_1.default.register);
usersRouter.post("/login", Valid_1.Valid(usesRegister_dto_1.default, 1 /* body */), users_middleware_1.default.login, users_controller_1.default.login);
usersRouter.put("/", validate_1.validateRole(null, const_1.AccountType.users), users_middleware_1.default.updateUsersInfo, users_controller_1.default.updateUserInfo);
usersRouter.get("/", validate_1.validateRole(null, const_1.AccountType.users), users_controller_1.default.getUsersInfo);
exports.default = usersRouter;
//# sourceMappingURL=users.router.js.map