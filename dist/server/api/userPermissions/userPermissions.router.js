"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const userPermissions_controller_1 = __importDefault(require("./userPermissions.controller"));
const userPermissionsRouter = express_1.default.Router();
userPermissionsRouter.get("/", userPermissions_controller_1.default.getListUserPermissions);
exports.default = userPermissionsRouter;
//# sourceMappingURL=userPermissions.router.js.map