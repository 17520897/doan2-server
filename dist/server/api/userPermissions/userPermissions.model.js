"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserPermissions = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
const schema = new mongoose_1.default.Schema({
    groupName: {
        type: String,
        required: true,
    },
    groupKey: {
        type: String,
        required: true,
        unique: true,
    },
    permissions: [
        {
            name: {
                type: String,
                required: true,
            },
            key: {
                type: String,
                required: true,
                unique: true,
            },
            dependOn: [],
        },
    ],
}, {
    collection: "UserPermissions",
});
exports.UserPermissions = mongoose_1.default.model("UserPermissions", schema);
//# sourceMappingURL=userPermissions.model.js.map