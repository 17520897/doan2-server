"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserPermissionsService = void 0;
const baseDb_1 = __importDefault(require("../../services/baseDb"));
const userPermissions_model_1 = require("./userPermissions.model");
class UserPermissionsService extends baseDb_1.default {
}
exports.UserPermissionsService = UserPermissionsService;
exports.default = new UserPermissionsService(userPermissions_model_1.UserPermissions);
//# sourceMappingURL=userPermissions.service.js.map