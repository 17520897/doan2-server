"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const const_1 = require("../../const");
const Valid_1 = require("../../middlewares/Valid");
const validate_1 = require("../../middlewares/validate");
const IdParamsRequest_dto_1 = __importDefault(require("../../const/IdParamsRequest.dto"));
const serviceBill_controller_1 = __importDefault(require("./serviceBill.controller"));
const createOrUpdateServiceBill_1 = __importDefault(require("./dto/createOrUpdateServiceBill"));
const getServiceBill_dto_1 = __importDefault(require("./dto/getServiceBill.dto"));
const middleware_1 = __importDefault(require("../../middlewares/middleware"));
const serviceBillRouter = express_1.default.Router();
serviceBillRouter.post("/", Valid_1.Valid(createOrUpdateServiceBill_1.default, 1 /* body */), validate_1.validateRole(null, const_1.AccountType.users), middleware_1.default.personIsSame, middleware_1.default.userHaveCompanies, middleware_1.default.personCreateOrReceiveExisted, middleware_1.default.userHaveCompaniesBank, middleware_1.default.companyHaveAccountNumber, middleware_1.default.existedAccountNumber, serviceBill_controller_1.default.createServiceBill);
serviceBillRouter.put("/:id", Valid_1.Valid(IdParamsRequest_dto_1.default, 2 /* params */), Valid_1.Valid(createOrUpdateServiceBill_1.default, 1 /* body */), validate_1.validateRole(null, const_1.AccountType.users), middleware_1.default.personIsSame, middleware_1.default.userHaveCompanies, middleware_1.default.personCreateOrReceiveExisted, middleware_1.default.userHaveCompaniesBank, middleware_1.default.companyHaveAccountNumber, middleware_1.default.existedAccountNumber, serviceBill_controller_1.default.updateServiceBill);
serviceBillRouter.get("/", Valid_1.Valid(getServiceBill_dto_1.default, 0 /* query */), validate_1.validateRole(null, const_1.AccountType.users), serviceBill_controller_1.default.searchServiceBill);
exports.default = serviceBillRouter;
//# sourceMappingURL=serviceBill.router.js.map