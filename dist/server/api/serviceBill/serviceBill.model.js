"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ServiceBill = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
const schema = new mongoose_1.default.Schema({
    companyId: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
        required: true,
    },
    userCreateId: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
        required: true,
    },
    billType: {
        type: String,
        enum: ["receipt", "expenses"],
        required: true,
    },
    paidType: {
        type: String,
        enum: ["bank", "cash"],
        default: "cash",
    },
    billNumber: {
        type: String,
    },
    companyBankId: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
    },
    // customerBankId: {
    //   type: mongoose.SchemaTypes.ObjectId,
    // },
    accountNumberId: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
    },
    personCreateId: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
    },
    personReceiveId: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
    },
    address: {
        type: String,
    },
    taxMoney: {
        type: Number,
        default: 0,
        index: true,
    },
    money: {
        type: Number,
        index: true,
        default: 0,
    },
    billCreateAt: {
        type: Date,
    },
    userCreateAt: {
        type: Date,
        default: Date.now,
    },
}, {
    collection: "ServiceBill",
});
schema.virtual("userCreateInfo", {
    ref: "User",
    localField: "userCreateId",
    foreignField: "_id",
    justOne: true,
});
schema.virtual("companyInfo", {
    ref: "Companies",
    localField: "companyId",
    foreignField: "_id",
    justOne: true,
});
schema.virtual("personCreateInfo", {
    ref: "Person",
    localField: "personCreateId",
    foreignField: "_id",
    justOne: true,
});
schema.virtual("personReceiveInfo", {
    ref: "Person",
    localField: "personReceiveId",
    foreignField: "_id",
    justOne: true,
});
schema.set("toObject", { virtuals: true });
schema.set("toJSON", { virtuals: true });
exports.ServiceBill = mongoose_1.default.model("ServiceBill", schema);
//# sourceMappingURL=serviceBill.model.js.map