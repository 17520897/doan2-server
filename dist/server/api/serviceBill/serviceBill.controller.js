"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const baseResponse_1 = __importDefault(require("../../services/baseResponse"));
const const_1 = require("../../const");
const responseService_1 = __importDefault(require("../../services/responseService"));
const serviceBill_service_1 = __importDefault(require("./serviceBill.service"));
const baseResponse = new baseResponse_1.default("ServiceBillController");
class ServiceBillController {
    createServiceBill(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { userId } = req.cookies;
                const moneyType = yield serviceBill_service_1.default.create(Object.assign(Object.assign({}, req.body), { userCreateId: userId }));
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    data: moneyType,
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    searchServiceBill(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { userId } = req.cookies;
                const { page, limit, companyId, beginBillCreateAt, endBillCreateAt, personCreateId, personReceiveId, sortType = -1, sortProperties, } = req.query;
                let query = {
                    userCreateId: userId,
                    companyId,
                };
                let sort = {};
                if (sortProperties) {
                    sort[sortProperties] = parseInt(sortType);
                }
                if (beginBillCreateAt && endBillCreateAt)
                    query = Object.assign(Object.assign({}, query), { $and: [
                            {
                                billCreateAt: { $gte: beginBillCreateAt },
                            },
                            {
                                billCreateAt: { $lte: endBillCreateAt },
                            },
                        ] });
                if (personCreateId) {
                    query["personCreateId"] = personCreateId;
                }
                if (personReceiveId) {
                    query["personReceiveId"] = personReceiveId;
                }
                const moneyBill = yield serviceBill_service_1.default.populate({
                    query,
                    page,
                    limit,
                    sort: Object.assign({}, sort),
                    populate: [
                        {
                            path: "personCreateInfo",
                        },
                        {
                            path: "personReceiveInfo",
                        },
                    ],
                });
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    data: moneyBill,
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    updateServiceBill(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { id } = req.params;
                const { userId } = req.cookies;
                const serviceBill = yield serviceBill_service_1.default.updateById(id, Object.assign(Object.assign({}, req.body), { userCreateId: userId, userCreateAt: Date.now() }));
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    message: "update service bill success",
                    data: serviceBill,
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
}
exports.default = new ServiceBillController();
//# sourceMappingURL=serviceBill.controller.js.map