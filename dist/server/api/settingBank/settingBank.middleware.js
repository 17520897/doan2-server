"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const baseResponse_1 = __importDefault(require("../../services/baseResponse"));
const const_1 = require("../../const");
const errorsCode_1 = require("../../const/errorsCode");
const responseService_1 = __importDefault(require("../../services/responseService"));
const settingBank_validate_1 = __importDefault(require("./settingBank.validate"));
const baseResponse = new baseResponse_1.default("SettingBankMiddleware");
class SettingBankMiddleware {
    createSettingBank(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { name } = req.body;
                const isNameExisted = yield settingBank_validate_1.default.isNameExisted(name);
                if (isNameExisted) {
                    return responseService_1.default.send(res, const_1.HTTPCode.notAccept, {
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.settingBankNameExisted,
                                field: "name",
                            },
                        ],
                    });
                }
                next();
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    updateSettingBank(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { id } = req.params;
                const { name } = req.body;
                const isNameExisted = yield settingBank_validate_1.default.isNameExisted(name, true, id);
                if (isNameExisted) {
                    return responseService_1.default.send(res, const_1.HTTPCode.notAccept, {
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.settingBankNameExisted,
                                field: "name",
                            },
                        ],
                    });
                }
                next();
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    settingBankExisted(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { id } = req.params;
                const isSettingBankExisted = yield settingBank_validate_1.default.isSettingBankExisted(id);
                if (!isSettingBankExisted) {
                    return responseService_1.default.send(res, const_1.HTTPCode.notAccept, {
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.settingBankNotFound,
                                field: "id",
                            },
                        ],
                    });
                }
                next();
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
}
exports.default = new SettingBankMiddleware();
//# sourceMappingURL=settingBank.middleware.js.map