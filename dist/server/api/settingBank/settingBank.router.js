"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const const_1 = require("../../const");
const validate_1 = require("../../middlewares/validate");
const settingBank_controller_1 = __importDefault(require("./settingBank.controller"));
const settingBank_middleware_1 = __importDefault(require("./settingBank.middleware"));
const settingBankRouter = express_1.default.Router();
settingBankRouter.post("/", validate_1.validateRole(null, const_1.AccountType.admin), settingBank_middleware_1.default.createSettingBank, settingBank_controller_1.default.createSettingBank);
settingBankRouter.put("/:id", validate_1.validateRole(null, const_1.AccountType.admin), settingBank_middleware_1.default.settingBankExisted, settingBank_middleware_1.default.updateSettingBank, settingBank_controller_1.default.updateSettingBank);
settingBankRouter.delete("/:id", validate_1.validateRole(null, const_1.AccountType.admin), settingBank_middleware_1.default.settingBankExisted, settingBank_controller_1.default.deleteSettingBank);
settingBankRouter.get("/", validate_1.validateRole(null, const_1.AccountType.all), settingBank_controller_1.default.getSettingBank);
exports.default = settingBankRouter;
//# sourceMappingURL=settingBank.router.js.map