"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SettingBankService = void 0;
const settingBank_model_1 = require("./settingBank.model");
const baseDb_1 = __importDefault(require("../../services/baseDb"));
class SettingBankService extends baseDb_1.default {
}
exports.SettingBankService = SettingBankService;
exports.default = new SettingBankService(settingBank_model_1.SettingBank);
//# sourceMappingURL=settingBank.service.js.map