"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SettingBank = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
const schema = new mongoose_1.default.Schema({
    logo: {
        type: String,
    },
    name: {
        type: String,
    },
    adminCreateId: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
    },
    status: {
        type: String,
        enum: ["normal", "deleted"],
        default: "normal",
    },
}, {
    collection: "SettingBank",
});
schema.virtual("adminCreateInfo", {
    ref: "Admin",
    localField: "adminCreateId",
    foreignField: "_id",
    justOne: true,
});
schema.set("toObject", { virtuals: true });
schema.set("toJSON", { virtuals: true });
exports.SettingBank = mongoose_1.default.model("SettingBank", schema);
//# sourceMappingURL=settingBank.model.js.map