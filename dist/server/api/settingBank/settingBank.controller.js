"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const baseResponse_1 = __importDefault(require("../../services/baseResponse"));
const const_1 = require("../../const");
const responseService_1 = __importDefault(require("../../services/responseService"));
const settingBank_service_1 = __importDefault(require("./settingBank.service"));
const baseResponse = new baseResponse_1.default("SettingBankController");
class SettingBankController {
    createSettingBank(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { adminId } = req.cookies;
                const settingBank = yield settingBank_service_1.default.create(Object.assign(Object.assign({}, req.body), { adminCreateId: adminId }));
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    data: settingBank,
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    updateSettingBank(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { id } = req.params;
                const { adminId } = req.cookies;
                const settingBank = yield settingBank_service_1.default.updateById(id, Object.assign(Object.assign({}, req.body), { adminCreateId: adminId }));
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    data: settingBank,
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    deleteSettingBank(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { id } = req.params;
                yield settingBank_service_1.default.updateById(id, {
                    status: const_1.SettingBankStatus.deleted,
                });
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    message: "delete setting bank success",
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    getSettingBank(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { page, limit, name } = req.query;
                const query = { status: const_1.SettingBankStatus.normal };
                if (name)
                    query["name"] = { $regex: name, $options: "i" };
                const settingBank = yield settingBank_service_1.default.getByQuery({
                    query,
                    page,
                    limit,
                    sort: {
                        name: 1,
                    },
                });
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    data: settingBank,
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
}
exports.default = new SettingBankController();
//# sourceMappingURL=settingBank.controller.js.map