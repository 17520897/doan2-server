"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.RefreshTokenMiddleware = void 0;
const baseResponse_1 = __importDefault(require("../../services/baseResponse"));
const responseService_1 = __importDefault(require("../../services/responseService"));
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
const const_1 = require("../../const");
const baseResponse = new baseResponse_1.default("RefreshTokenMiddleware");
class RefreshTokenMiddleware {
    getNewAccessToken(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                if (req.headers["authorization"]) {
                    const headersToken = req.headers["authorization"].split(" ");
                    if (headersToken[0] !== "Bearer")
                        return responseService_1.default.send(res, const_1.HTTPCode.unauthorize, {
                            message: "you are not authorize",
                        });
                    if (!headersToken[1]) {
                        return responseService_1.default.send(res, const_1.HTTPCode.unauthorize, {
                            message: "you are not authorize",
                        });
                    }
                    const token = jsonwebtoken_1.default.verify(headersToken[1], process.env.JWT_SECRET);
                    if (!token || !token.isRefresh)
                        return responseService_1.default.send(res, const_1.HTTPCode.unauthorize, {
                            message: "you are not authorize",
                        });
                    delete token.iat;
                    delete token.exp;
                    req.body.token = token;
                    next();
                }
                else {
                    return responseService_1.default.send(res, const_1.HTTPCode.unauthorize, {
                        message: "you are not authorize",
                    });
                }
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
}
exports.RefreshTokenMiddleware = RefreshTokenMiddleware;
exports.default = new RefreshTokenMiddleware();
//# sourceMappingURL=refreshToken.middleware.js.map