"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const refreshToken_middleware_1 = __importDefault(require("./refreshToken.middleware"));
const refreshToken_controller_1 = __importDefault(require("./refreshToken.controller"));
const router = express_1.default.Router();
router.get("/", refreshToken_middleware_1.default.getNewAccessToken, refreshToken_controller_1.default.getNewAccessToken);
exports.default = router;
//# sourceMappingURL=refreshToken.router.js.map