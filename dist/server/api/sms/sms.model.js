"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.SMS = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
const schema = new mongoose_1.default.Schema({
    phone: {
        type: String,
        required: true
    },
    code: {
        type: String
    },
    type: {
        type: String,
        enum: ["register", "forgotPassword"]
    },
    createAt: {
        type: Date,
        default: Date.now
    }
}, {
    collection: "SMS"
});
exports.SMS = mongoose_1.default.model("SMS", schema);
//# sourceMappingURL=sms.model.js.map