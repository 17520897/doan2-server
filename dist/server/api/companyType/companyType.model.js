"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompanyType = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
const schema = new mongoose_1.default.Schema({
    name: {
        type: String,
        trim: true,
    },
    numberOfCompany: {
        type: Number,
        default: 0,
    },
    status: {
        type: String,
        default: "normal",
        enum: ["normal", "deleted"],
    },
    createAt: {
        type: Date,
        default: Date.now,
    },
    adminCreateId: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
    },
}, {
    collection: "CompanyType",
});
schema.virtual("adminCreateInfo", {
    ref: "Admin",
    localField: "adminCreateId",
    foreignField: "_id",
    justOne: true,
});
schema.set("toObject", { virtuals: true });
schema.set("toJSON", { virtuals: true });
exports.CompanyType = mongoose_1.default.model("CompanyType", schema);
//# sourceMappingURL=companyType.model.js.map