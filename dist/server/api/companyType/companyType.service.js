"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompanyTypeService = void 0;
const companyType_model_1 = require("./companyType.model");
const baseDb_1 = __importDefault(require("../../services/baseDb"));
class CompanyTypeService extends baseDb_1.default {
}
exports.CompanyTypeService = CompanyTypeService;
exports.default = new CompanyTypeService(companyType_model_1.CompanyType);
//# sourceMappingURL=companyType.service.js.map