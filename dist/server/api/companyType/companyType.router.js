"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const const_1 = require("../../const");
const IdParamsRequest_dto_1 = __importDefault(require("../../const/IdParamsRequest.dto"));
const permissions_1 = require("../../const/permissions");
const Valid_1 = require("../../middlewares/Valid");
const validate_1 = require("../../middlewares/validate");
const companyType_controller_1 = __importDefault(require("./companyType.controller"));
const companyType_middleware_1 = __importDefault(require("./companyType.middleware"));
const createCompanyType_dto_1 = __importDefault(require("./dto/createCompanyType.dto"));
const getListCompanyType_dto_1 = __importDefault(require("./dto/getListCompanyType.dto"));
const updateCompanyType_dto_1 = __importDefault(require("./dto/updateCompanyType.dto"));
const companyTypeRouter = express_1.default.Router();
companyTypeRouter.get("/", Valid_1.Valid(getListCompanyType_dto_1.default, 0 /* query */), validate_1.validateRole(null, const_1.AccountType.all), companyType_controller_1.default.getListCompanyType);
companyTypeRouter.post("/", validate_1.validateRole(permissions_1.PermissionKeys.companyTypeCreate, const_1.AccountType.admin), Valid_1.Valid(createCompanyType_dto_1.default, 1 /* body */), companyType_middleware_1.default.createCompanyType, companyType_controller_1.default.createCompanyType);
companyTypeRouter.put("/:id", Valid_1.Valid(IdParamsRequest_dto_1.default, 2 /* params */), Valid_1.Valid(updateCompanyType_dto_1.default, 2 /* params */), validate_1.validateRole(permissions_1.PermissionKeys.companyTypeUpdate, const_1.AccountType.admin), companyType_middleware_1.default.updateCompanyType, companyType_controller_1.default.updateCompanyType);
companyTypeRouter.delete("/:id", validate_1.validateRole(permissions_1.PermissionKeys.companyTypeDelete, const_1.AccountType.admin), companyType_controller_1.default.deleteCompanyType);
exports.default = companyTypeRouter;
//# sourceMappingURL=companyType.router.js.map