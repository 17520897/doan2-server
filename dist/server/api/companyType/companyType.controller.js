"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const baseResponse_1 = __importDefault(require("../../services/baseResponse"));
const const_1 = require("../../const");
const responseService_1 = __importDefault(require("../../services/responseService"));
const companyType_service_1 = __importDefault(require("./companyType.service"));
const baseResponse = new baseResponse_1.default("CompanyTypeController");
class CompanyTypeController {
    createCompanyType(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { adminId } = req.cookies;
                const companyType = yield companyType_service_1.default.create(Object.assign(Object.assign({}, req.body), { adminCreateId: adminId }));
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    data: companyType,
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    getListCompanyType(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { page, limit } = req.cookies;
                const listCompanyType = yield companyType_service_1.default.populate({
                    query: {
                        status: const_1.CompanyTypeStatus.normal,
                    },
                    populate: [
                        {
                            path: "adminCreateInfo",
                            select: "-password",
                        },
                    ],
                    sort: {
                        numberOfCompany: -1,
                        createAt: -1,
                    },
                    page,
                    limit,
                });
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    data: listCompanyType,
                    message: "get list company type success",
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    updateCompanyType(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { adminId } = req.cookies;
                const { id } = req.params;
                const companyType = yield companyType_service_1.default.updateById(id, Object.assign(Object.assign({}, req.body), { adminCreateId: adminId }));
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    data: companyType,
                    message: "update company type success",
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    deleteCompanyType(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { id } = req.params;
                yield companyType_service_1.default.updateById(id, {
                    status: const_1.CompanyTypeStatus.deleted,
                });
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    message: "delete company type success",
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
}
exports.default = new CompanyTypeController();
//# sourceMappingURL=companyType.controller.js.map