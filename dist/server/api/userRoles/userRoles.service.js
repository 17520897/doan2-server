"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.UserRolesService = void 0;
const userRoles_model_1 = require("./userRoles.model");
const baseDb_1 = __importDefault(require("../../services/baseDb"));
class UserRolesService extends baseDb_1.default {
}
exports.UserRolesService = UserRolesService;
exports.default = new UserRolesService(userRoles_model_1.UserRoles);
//# sourceMappingURL=userRoles.service.js.map