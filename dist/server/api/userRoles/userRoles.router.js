"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const userRoles_middleware_1 = __importDefault(require("./userRoles.middleware"));
const userRoles_controller_1 = __importDefault(require("./userRoles.controller"));
const Valid_1 = require("../../middlewares/Valid");
const validate_1 = require("../../middlewares/validate");
const permissions_1 = require("../../const/permissions");
const createUserRoles_dto_1 = __importDefault(require("./dto/createUserRoles.dto"));
const getUserRoles_dto_1 = __importDefault(require("./dto/getUserRoles.dto"));
const paramsUserRoles_dto_1 = __importDefault(require("./dto/paramsUserRoles.dto"));
const updateUserRoles_dto_1 = __importDefault(require("./dto/updateUserRoles.dto"));
const const_1 = require("../../const");
const userRolesRouter = express_1.default.Router();
userRolesRouter.post("/", validate_1.validateRole(permissions_1.PermissionKeys.userRoleCreate, const_1.AccountType.admin), Valid_1.Valid(createUserRoles_dto_1.default, 1 /* body */), userRoles_middleware_1.default.createRoles, userRoles_controller_1.default.createRoles);
userRolesRouter.get("/", Valid_1.Valid(getUserRoles_dto_1.default, 0 /* query */), validate_1.validateRole(permissions_1.PermissionKeys.userRoleGetList, const_1.AccountType.admin), userRoles_controller_1.default.getRoles);
userRolesRouter.put("/disable/:id", validate_1.validateRole(permissions_1.PermissionKeys.userRoleDisableEnable, const_1.AccountType.admin), Valid_1.Valid(paramsUserRoles_dto_1.default, 2 /* params */), userRoles_controller_1.default.disableRoles);
userRolesRouter.put("/enable/:id", validate_1.validateRole(permissions_1.PermissionKeys.userRoleDisableEnable, const_1.AccountType.admin), Valid_1.Valid(paramsUserRoles_dto_1.default, 2 /* params */), userRoles_controller_1.default.enableRoles);
userRolesRouter.put("/:id", validate_1.validateRole(permissions_1.PermissionKeys.userRoleDelete, const_1.AccountType.admin), Valid_1.Valid(paramsUserRoles_dto_1.default, 2 /* params */), Valid_1.Valid(updateUserRoles_dto_1.default, 1 /* body */), userRoles_controller_1.default.updateRoles);
userRolesRouter.delete("/:id", validate_1.validateRole(permissions_1.PermissionKeys.roleDelete, const_1.AccountType.admin), Valid_1.Valid(paramsUserRoles_dto_1.default, 2 /* params */), userRoles_controller_1.default.deleteRoles);
exports.default = userRolesRouter;
//# sourceMappingURL=userRoles.router.js.map