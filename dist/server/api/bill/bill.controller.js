"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const baseResponse_1 = __importDefault(require("../../services/baseResponse"));
const const_1 = require("../../const");
const responseService_1 = __importDefault(require("../../services/responseService"));
const bill_service_1 = __importDefault(require("./bill.service"));
const mongoose_1 = __importDefault(require("mongoose"));
const billDetail_service_1 = __importDefault(require("../billDetail/billDetail.service"));
const billTax_service_1 = __importDefault(require("../billTax/billTax.service"));
const xlsx_1 = __importDefault(require("xlsx"));
const person_service_1 = __importDefault(require("../person/person.service"));
const slugify_1 = __importDefault(require("slugify"));
const stock_service_1 = __importDefault(require("../stock/stock.service"));
const errorsCode_1 = require("../../const/errorsCode");
const fs_1 = __importDefault(require("fs"));
const baseResponse = new baseResponse_1.default("ServiceBillController");
function slugName(name) {
    return slugify_1.default(name, {
        lower: true,
        replacement: "_",
        trim: true,
        locale: "vi",
    });
}
function formatDate(data) {
    try {
        const dateSplit = data.split("/");
        const date = new Date(`${dateSplit[2]}-${dateSplit[1]}-${dateSplit[0]}`);
        return date.toISOString();
    }
    catch (error) {
        return new Date().toISOString();
    }
}
function getBillData(sheetData, companyId = "", userId = "") {
    return __awaiter(this, void 0, void 0, function* () {
        const billNumber = "SỐ HÓA ĐƠN";
        const billPersonCreate = "NGƯỜI TẠO";
        const billPersonReceive = "NGƯỜI NHẬN";
        const billCreateAt = "NGÀY TẠO";
        const billAddress = "ĐỊA CHỈ";
        const returnData = [];
        const newPersonData = {};
        for (let i = 0; i < sheetData.length; i++) {
            //init return object
            const returnObject = {
                billNumber: sheetData[i][billNumber],
                billCreateAt: formatDate(sheetData[i][billCreateAt]),
                address: sheetData[i][billAddress],
            };
            //check person create existed
            const personCreate = yield person_service_1.default.getOne({
                userCreateId: userId,
                companyId,
                status: const_1.PersonStatus.normal,
                $or: [
                    {
                        name: sheetData[i][billPersonCreate],
                    },
                    {
                        slugName: slugName(sheetData[i][billPersonCreate]),
                    },
                ],
            });
            //check person receive existed
            const personReceive = yield person_service_1.default.getOne({
                userCreateId: userId,
                companyId,
                status: const_1.PersonStatus.normal,
                $or: [
                    {
                        name: sheetData[i][billPersonReceive],
                    },
                    {
                        slugName: slugName(sheetData[i][billPersonReceive]),
                    },
                ],
            });
            if (personCreate) {
                returnObject["personCreateId"] = personCreate._id;
                returnObject["personCreateInfo"] = Object.assign({}, personCreate);
            }
            else {
                if (newPersonData[sheetData[i][billPersonCreate]]) {
                    newPersonData[sheetData[i][billPersonCreate]].push(i + 2);
                }
                else {
                    newPersonData[sheetData[i][billPersonCreate]] = [i + 2];
                }
            }
            if (personReceive) {
                returnObject["personReceiveId"] = personReceive._id;
                returnObject["personReceiveInfo"] = Object.assign({}, personReceive);
            }
            else {
                if (newPersonData[sheetData[i][billPersonReceive]]) {
                    newPersonData[sheetData[i][billPersonReceive]].push(i + 2);
                }
                else {
                    newPersonData[sheetData[i][billPersonReceive]] = [i + 2];
                }
            }
            returnData.push(returnObject);
        }
        return {
            billData: returnData,
            newPersonData,
        };
    });
}
function getBillDetailData(sheetData, companyId = "", userId = "", stockType = "services") {
    return __awaiter(this, void 0, void 0, function* () {
        const billNumber = "SỐ HÓA ĐƠN";
        const stock = "HÀNG HÓA";
        const stockPrice = "ĐƠN GIÁ";
        const stockQuantity = "SỐ LƯỢNG";
        const receiveNumber = "TK CÓ";
        const debitNumber = "TK NỢ";
        const returnData = [];
        const newStockData = {};
        for (let i = 0; i < sheetData.length; i++) {
            const returnObject = {
                billNumber: sheetData[i][billNumber],
                receiveAccountNumber: sheetData[i][receiveNumber],
                debitAccountNumber: sheetData[i][debitNumber],
                money: sheetData[i][stockPrice],
                moneyType: "VND",
                exchangeRate: 1,
                quantity: sheetData[i][stockQuantity],
                finalMoney: sheetData[i][stockPrice] * sheetData[i][stockQuantity],
            };
            const stockName = sheetData[i][stock];
            const checkStock = yield stock_service_1.default.getOne({
                type: stockType,
                userCreateId: userId,
                companyId,
                $or: [
                    {
                        name: stockName,
                    },
                    {
                        slugName: slugName(stockName),
                    },
                ],
            });
            if (checkStock) {
                returnObject["stockId"] = checkStock._id;
                returnObject["stockInfo"] = Object.assign({}, checkStock);
                returnData.push(returnObject);
            }
            else {
                if (newStockData[stockName]) {
                    newStockData[stockName].push(i + 2);
                }
                else {
                    newStockData[stockName] = [i + 2];
                }
            }
        }
        return {
            newStockData,
            billDetailData: returnData,
        };
    });
}
function getBillTaxData(sheetData, companyId = "", userId = "", stockType = "service") {
    return __awaiter(this, void 0, void 0, function* () {
        const billNumber = "SỐ HÓA ĐƠN";
        const stock = "HÀNG HÓA";
        const stockMoney = "SỐ TIỀN";
        const taxPercent = "PHẦN TRĂM";
        const receiveNumber = "TK CÓ";
        const returnData = [];
        for (let i = 0; i < sheetData.length; i++) {
            const returnObject = {
                billNumber: sheetData[i][billNumber],
                accountNumber: sheetData[i][receiveNumber],
                stockMoney: sheetData[i][stockMoney],
                percent: sheetData[i][taxPercent],
                finalMoney: parseFloat(sheetData[i][stockMoney]) *
                    parseFloat(sheetData[i][taxPercent]) / 100,
            };
            const stockName = sheetData[i][stock];
            const checkStock = yield stock_service_1.default.getOne({
                type: stockType,
                userCreateId: userId,
                companyId,
                $or: [
                    {
                        name: stockName,
                    },
                    {
                        slugName: slugName(stockName),
                    },
                ],
            });
            if (checkStock) {
                returnObject["stockId"] = checkStock._id;
                returnObject["stockInfo"] = Object.assign({}, checkStock);
                returnData.push(returnObject);
            }
        }
        return returnData;
    });
}
function makeBillArrayForMany(billData, userId, companyId, accountNumberId, billType, billFunction, billDetailData = [], billTaxData = []) {
    const moneyData = {};
    const taxMoneyData = {};
    for (let i = 0; i < billDetailData.length; i++) {
        if (moneyData.hasOwnProperty(billDetailData[i]['billNumber'])) {
            moneyData[billDetailData[i]['billNumber']] += billDetailData[i]['finalMoney'];
        }
        else {
            moneyData[billDetailData[i]['billNumber']] = billDetailData[i]['finalMoney'];
        }
    }
    for (let i = 0; i < billTaxData.length; i++) {
        if (taxMoneyData.hasOwnProperty(billTaxData[i]['billNumber'])) {
            taxMoneyData[billTaxData[i]['billNumber']] += billTaxData[i]['finalMoney'];
        }
        else {
            taxMoneyData[billTaxData[i]['billNumber']] = billTaxData[i]['finalMoney'];
        }
    }
    const returnData = [];
    console.log(moneyData, taxMoneyData);
    for (let i = 0; i < billData.length; i++) {
        returnData.push({
            billNumber: billData[i].billNumber,
            billCreateAt: billData[i].billCreateAt,
            address: billData[i].address,
            personCreateId: billData[i].personCreateId,
            personReceiveId: billData[i].personReceiveId,
            companyId,
            accountNumberId,
            billType,
            billFunction,
            userCreateId: userId,
            paidType: "cash",
            money: moneyData.hasOwnProperty(billData[i]['billNumber']) ? moneyData[billData[i]['billNumber']] : 0,
            taxMoney: taxMoneyData.hasOwnProperty(billData[i]['billNumber']) ? taxMoneyData[billData[i]['billNumber']] : 0,
        });
    }
    return returnData;
}
function createManyBill(billArray, options = {}) {
    return __awaiter(this, void 0, void 0, function* () {
        const returnData = {};
        for (let i = 0; i < billArray.length; i++) {
            const bill = yield bill_service_1.default.create(billArray[i], options);
            returnData[billArray[i]["billNumber"]] = {
                _id: bill._id,
                billCreateAt: billArray[i]["billCreateAt"],
            };
        }
        return returnData;
    });
}
function createManyBillDetail(billDetailData, billCreateArray, companyId, accountNumberId, userId, options = {}) {
    return __awaiter(this, void 0, void 0, function* () {
        const createData = [];
        for (let i = 0; i < billDetailData.length; i++) {
            const billId = billCreateArray[billDetailData[i]["billNumber"]]._id
                ? billCreateArray[billDetailData[i]["billNumber"]]
                : undefined;
            if (billId) {
                createData.push({
                    companyId,
                    userCreateId: userId,
                    billId,
                    accountNumberId,
                    stockId: billDetailData[i]['stockId'],
                    receiveAccountNumber: billDetailData[i]['receiveAccountNumber'],
                    debitAccountNumber: billDetailData[i]['debitAccountNumber'],
                    money: billDetailData[i]['money'],
                    moneyType: billDetailData[i]['moneyType'],
                    quantity: billDetailData[i]['quantity'],
                    exchangeRate: billDetailData[i]['exchangeRate'],
                    finalMoney: billDetailData[i]['finalMoney'],
                    billCreateAt: billCreateArray[billDetailData[i]['billNumber']]["billCreateAt"],
                });
            }
        }
        yield billDetail_service_1.default.createMany(createData, options);
        return;
    });
}
function createManyBillTax(billTaxData, billCreateArray, companyId, accountNumberId, userId, options = {}) {
    return __awaiter(this, void 0, void 0, function* () {
        const createData = [];
        for (let i = 0; i < billTaxData.length; i++) {
            const billId = billCreateArray[billTaxData[i]["billNumber"]]._id
                ? billCreateArray[billTaxData[i]["billNumber"]]
                : undefined;
            if (billId) {
                createData.push({
                    companyId,
                    userCreateId: userId,
                    billId,
                    accountNumberId,
                    stockId: billTaxData[i]['stockId'],
                    accountNumber: billTaxData[i]['accountNumber'],
                    stockMoney: billTaxData[i]['stockMoney'],
                    percent: billTaxData[i]['percent'],
                    finalMoney: billTaxData[i]['finalMoney'],
                    billCreateAt: billCreateArray[billTaxData[i]['billNumber']]["billCreateAt"],
                });
            }
        }
        yield billTax_service_1.default.createMany(createData, options);
        return;
    });
}
class ServiceBillController {
    createServiceBill(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { userId } = req.cookies;
                const moneyType = yield bill_service_1.default.create(Object.assign(Object.assign({}, req.body), { userCreateId: userId }));
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    data: moneyType,
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    searchServiceBill(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { userId } = req.cookies;
                const { page, limit, companyId, beginBillCreateAt, endBillCreateAt, personCreateId, personReceiveId, sortType = -1, sortProperties, billFunction, receiveAccountNumber, debitAccountNumber, billType, } = req.query;
                let query = {
                    userCreateId: userId,
                    billFunction,
                    companyId,
                };
                let sort = {};
                if (sortProperties) {
                    sort[sortProperties] = parseInt(sortType);
                }
                if (beginBillCreateAt && endBillCreateAt)
                    query = Object.assign(Object.assign({}, query), { $and: [
                            {
                                billCreateAt: { $gte: beginBillCreateAt },
                            },
                            {
                                billCreateAt: { $lte: endBillCreateAt },
                            },
                        ] });
                if (receiveAccountNumber) {
                    const billArrayIds = yield billDetail_service_1.default.distinct("billId", {
                        receiveAccountNumber: parseInt(receiveAccountNumber),
                    });
                    query = Object.assign(Object.assign({}, query), { _id: { $in: billArrayIds } });
                }
                if (debitAccountNumber) {
                    const billArrayIds = yield billDetail_service_1.default.distinct("billId", {
                        debitAccountNumber: parseInt(debitAccountNumber),
                    });
                    query = Object.assign(Object.assign({}, query), { _id: { $in: billArrayIds } });
                }
                if (personCreateId) {
                    query["personCreateId"] = personCreateId;
                }
                if (personReceiveId) {
                    query["personReceiveId"] = personReceiveId;
                }
                if (billType) {
                    query["billType"] = billType;
                }
                const bill = yield bill_service_1.default.populate({
                    query,
                    page,
                    limit,
                    sort: Object.assign({}, sort),
                    populate: [
                        {
                            path: "personCreateInfo",
                        },
                        {
                            path: "personReceiveInfo",
                        },
                    ],
                });
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    data: bill,
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    updateServiceBill(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { id } = req.params;
                const { userId } = req.cookies;
                const serviceBill = yield bill_service_1.default.updateById(id, Object.assign(Object.assign({}, req.body), { userCreateId: userId, userCreateAt: Date.now() }));
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    message: "update bill success",
                    data: serviceBill,
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    deleteBill(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const session = yield mongoose_1.default.startSession();
            session.startTransaction();
            try {
                const options = { session };
                const { id } = req.params;
                const idObject = new mongoose_1.default.Types.ObjectId(id);
                yield billDetail_service_1.default.deleteMany({
                    billId: idObject,
                }, options);
                yield billTax_service_1.default.deleteMany({
                    billId: idObject,
                }, options);
                yield bill_service_1.default.deleteById(id, options);
                yield session.commitTransaction();
                return baseResponse.success({
                    res,
                    message: "delete bill success",
                });
            }
            catch (error) {
                yield session.abortTransaction;
            }
            finally {
                yield session.endSession();
            }
        });
    }
    getInputFromFileBill(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const root = process.env.FILE_UPLOAD_FOLDER;
                const { userId } = req.cookies;
                const { fileUrl, companyId, stockType } = req.body;
                const excelFile = `${root}/${fileUrl}`;
                const billNumber = "SỐ HÓA ĐƠN";
                const billPersonCreate = "NGƯỜI TẠO";
                const billPersonReceive = "NGƯỜI NHẬN";
                const billCreateAt = "NGÀY TẠO";
                const billAddress = "ĐỊA CHỈ";
                const stockMoney = "SỐ TIỀN";
                const stockPrice = "ĐƠN GIÁ";
                const taxPercent = "PHẦN TRĂM";
                const receiveNumber = "TK CÓ";
                const debitNumber = "TK NỢ";
                const stock = "HÀNG HÓA";
                const stockQuantity = "SỐ LƯỢNG";
                if (!fs_1.default.existsSync(excelFile)) {
                    return baseResponse.notAccepted({
                        res,
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.filesNotFound,
                                field: "fileUrl",
                            },
                        ],
                    });
                }
                //read data
                const workbook = xlsx_1.default.readFile(excelFile);
                const sheetNameList = workbook.SheetNames;
                //bill data
                const billData = xlsx_1.default.utils.sheet_to_json(workbook.Sheets[sheetNameList[0]]);
                //bill detail data
                const billDetailData = xlsx_1.default.utils.sheet_to_json(workbook.Sheets[sheetNameList[1]]);
                //bill tax data
                const billTaxData = xlsx_1.default.utils.sheet_to_json(workbook.Sheets[sheetNameList[2]]);
                // validate data
                if (billData.length <= 0 ||
                    billDetailData.length <= 0 ||
                    billTaxData.length <= 0) {
                    return baseResponse.notAccepted({
                        res,
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.filesNotHaveData,
                            },
                        ],
                    });
                }
                if (!billData[0].hasOwnProperty(billNumber) ||
                    !billData[0].hasOwnProperty(billPersonCreate) ||
                    !billData[0].hasOwnProperty(billPersonReceive) ||
                    !billData[0].hasOwnProperty(billCreateAt) ||
                    !billData[0].hasOwnProperty(billAddress)) {
                    console.log("billData");
                    return baseResponse.notAccepted({
                        res,
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.filesWrongFormat,
                            },
                        ],
                    });
                }
                if (!billTaxData[0].hasOwnProperty(stock) ||
                    !billTaxData[0].hasOwnProperty(billNumber) ||
                    !billTaxData[0].hasOwnProperty(stockMoney) ||
                    !billTaxData[0].hasOwnProperty(taxPercent) ||
                    !billTaxData[0].hasOwnProperty(receiveNumber)) {
                    console.log("billTaxData");
                    return baseResponse.notAccepted({
                        res,
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.filesWrongFormat,
                            },
                        ],
                    });
                }
                if (!billDetailData[0].hasOwnProperty(billNumber) ||
                    !billDetailData[0].hasOwnProperty(stock) ||
                    !billDetailData[0].hasOwnProperty(stockPrice) ||
                    !billDetailData[0].hasOwnProperty(stockQuantity) ||
                    !billDetailData[0].hasOwnProperty(receiveNumber) ||
                    !billDetailData[0].hasOwnProperty(debitNumber)) {
                    console.log("billDetailData", billDetailData[0]);
                    return baseResponse.notAccepted({
                        res,
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.filesWrongFormat,
                            },
                        ],
                    });
                }
                return baseResponse.success({
                    res,
                    data: {
                        billData: yield getBillData(billData, companyId, userId),
                        billDetailData: yield getBillDetailData(billDetailData, companyId, userId, stockType),
                        billTaxData: yield getBillTaxData(billTaxData, companyId, userId, stockType),
                    },
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    createManyBill(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const session = yield mongoose_1.default.startSession();
            session.startTransaction();
            try {
                const options = { session };
                const { userId } = req.cookies;
                const { companyId, accountNumberId, billType, billFunction, billData, billDetailData, billTaxData, } = req.body;
                // bill array for create bill info
                const billArray = makeBillArrayForMany(billData, userId, companyId, accountNumberId, billType, billFunction, billDetailData, billTaxData);
                const billCreateData = yield createManyBill(billArray, options);
                yield createManyBillDetail(billDetailData, billCreateData, companyId, accountNumberId, userId, options);
                yield createManyBillTax(billTaxData, billCreateData, companyId, accountNumberId, userId, options);
                yield session.commitTransaction();
                return baseResponse.success({
                    res,
                    message: "create data success"
                });
            }
            catch (error) {
                yield session.abortTransaction();
                return baseResponse.serverError(res, error);
            }
            finally {
                session.endSession();
            }
        });
    }
}
exports.default = new ServiceBillController();
//# sourceMappingURL=bill.controller.js.map