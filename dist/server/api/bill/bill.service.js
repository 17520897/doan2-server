"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BillService = void 0;
const baseDb_1 = __importDefault(require("../../services/baseDb"));
const bill_model_1 = require("./bill.model");
class BillService extends baseDb_1.default {
}
exports.BillService = BillService;
exports.default = new BillService(bill_model_1.Bill);
//# sourceMappingURL=bill.service.js.map