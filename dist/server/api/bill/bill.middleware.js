"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const baseResponse_1 = __importDefault(require("../../services/baseResponse"));
const bill_service_1 = __importDefault(require("./bill.service"));
const errorsCode_1 = require("../../const/errorsCode");
const baseResponse = new baseResponse_1.default("ServiceBillMiddleware");
class ServiceBillMiddleware {
    createManyBill(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { userId } = req.cookies;
                const { billData, companyId, billFunction } = req.body;
                const returnData = [];
                for (let i = 0; i < billData.length; i++) {
                    const bill = yield bill_service_1.default.getOne({
                        companyId,
                        userCreateId: userId,
                        billNumber: billData[i]['billNumber'],
                        billFunction
                    });
                    if (bill) {
                        returnData.push(bill['billNumber']);
                    }
                }
                if (returnData.length > 0) {
                    return baseResponse.notAccepted({
                        res,
                        message: `Số hóa đơn đã có trong hệ thống ${returnData.join(',')}`,
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.billExisted,
                                field: 'billNumber'
                            }
                        ]
                    });
                }
                next();
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
}
exports.default = new ServiceBillMiddleware();
//# sourceMappingURL=bill.middleware.js.map