"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const const_1 = require("../../const");
const Valid_1 = require("../../middlewares/Valid");
const validate_1 = require("../../middlewares/validate");
const IdParamsRequest_dto_1 = __importDefault(require("../../const/IdParamsRequest.dto"));
const bill_controller_1 = __importDefault(require("./bill.controller"));
const createOrUpdateBill_1 = __importDefault(require("./dto/createOrUpdateBill"));
const getBill_dto_1 = __importDefault(require("./dto/getBill.dto"));
const middleware_1 = __importDefault(require("../../middlewares/middleware"));
const bill_middleware_1 = __importDefault(require("./bill.middleware"));
const createManyBill_1 = __importDefault(require("./dto/createManyBill"));
const getFromFile_1 = __importDefault(require("./dto/getFromFile"));
const billRouter = express_1.default.Router();
billRouter.post("/", Valid_1.Valid(createOrUpdateBill_1.default, 1 /* body */), validate_1.validateRole(null, const_1.AccountType.users), middleware_1.default.personIsSame, middleware_1.default.userHaveCompanies, middleware_1.default.personCreateOrReceiveExisted, middleware_1.default.userHaveCompaniesBank, middleware_1.default.companyHaveAccountNumber, middleware_1.default.existedAccountNumber, bill_controller_1.default.createServiceBill);
billRouter.post("/file", Valid_1.Valid(getFromFile_1.default, 1 /* body */), validate_1.validateRole(null, const_1.AccountType.users), middleware_1.default.userHaveCompanies, bill_controller_1.default.getInputFromFileBill);
billRouter.post("/many", Valid_1.Valid(createManyBill_1.default, 1 /* body */), validate_1.validateRole(null, const_1.AccountType.users), middleware_1.default.userHaveCompanies, middleware_1.default.companyHaveAccountNumber, bill_middleware_1.default.createManyBill, bill_controller_1.default.createManyBill);
billRouter.put("/:id", Valid_1.Valid(IdParamsRequest_dto_1.default, 2 /* params */), Valid_1.Valid(createOrUpdateBill_1.default, 1 /* body */), validate_1.validateRole(null, const_1.AccountType.users), middleware_1.default.personIsSame, middleware_1.default.userHaveCompanies, middleware_1.default.personCreateOrReceiveExisted, middleware_1.default.userHaveCompaniesBank, middleware_1.default.companyHaveAccountNumber, middleware_1.default.existedAccountNumber, bill_controller_1.default.updateServiceBill);
billRouter.get("/", Valid_1.Valid(getBill_dto_1.default, 0 /* query */), validate_1.validateRole(null, const_1.AccountType.users), bill_controller_1.default.searchServiceBill);
billRouter.delete("/:id", Valid_1.Valid(IdParamsRequest_dto_1.default, 2 /* params */), validate_1.validateRole(null, const_1.AccountType.users), bill_controller_1.default.deleteBill);
exports.default = billRouter;
//# sourceMappingURL=bill.router.js.map