"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const bill_service_1 = __importDefault(require("./bill.service"));
class BillValidate {
    isCompaniesHaveBill(userId, companyId, billId) {
        return __awaiter(this, void 0, void 0, function* () {
            const serviceBill = yield bill_service_1.default.getOne({
                userCreateId: userId,
                _id: billId,
                companyId,
            });
            return serviceBill ? true : false;
        });
    }
}
exports.default = new BillValidate();
//# sourceMappingURL=bill.validate.js.map