"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const baseResponse_1 = __importDefault(require("../../services/baseResponse"));
const mongoose_1 = __importDefault(require("mongoose"));
const billDetail_service_1 = __importDefault(require("./billDetail.service"));
const bill_service_1 = __importDefault(require("../bill/bill.service"));
const baseResponse = new baseResponse_1.default("billDetailController");
class billDetailController {
    createManyBillDetail(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const session = yield mongoose_1.default.startSession();
            session.startTransaction();
            try {
                const options = { session };
                const { userId } = req.cookies;
                const { companyId, billId, accountNumberId, data, billCreateAt, } = req.body;
                let totalMoney = 0;
                const createData = [];
                for (let i = 0; i < data.length; i++) {
                    createData.push(Object.assign(Object.assign({}, data[i]), { companyId,
                        billId,
                        accountNumberId,
                        billCreateAt, userCreateId: userId }));
                    totalMoney += data[i].finalMoney;
                }
                yield billDetail_service_1.default.deleteMany({
                    billId,
                }, options);
                yield bill_service_1.default.updateById(billId, {
                    money: totalMoney,
                }, options);
                yield billDetail_service_1.default.createMany(createData, options);
                yield session.commitTransaction();
                return baseResponse.success({
                    res,
                    message: "create bill detail success",
                });
            }
            catch (error) {
                yield session.abortTransaction();
                return baseResponse.serverError(res, error);
            }
            finally {
                session.endSession();
            }
        });
    }
    getBillDetail(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { userId } = req.cookies;
                const { companyId, billId } = req.query;
                const billDetail = yield billDetail_service_1.default.populate({
                    query: {
                        userCreateId: userId,
                        companyId,
                        billId,
                    },
                    sort: {
                        stockId: 1,
                    },
                    populate: [
                        {
                            path: "stockInfo",
                        },
                    ],
                });
                return baseResponse.success({
                    res,
                    data: billDetail,
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
}
exports.default = new billDetailController();
//# sourceMappingURL=billDetail.controller.js.map