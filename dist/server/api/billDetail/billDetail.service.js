"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.BillDetailService = void 0;
const baseDb_1 = __importDefault(require("../../services/baseDb"));
const billDetail_model_1 = require("./billDetail.model");
class BillDetailService extends baseDb_1.default {
}
exports.BillDetailService = BillDetailService;
exports.default = new BillDetailService(billDetail_model_1.BillDetail);
//# sourceMappingURL=billDetail.service.js.map