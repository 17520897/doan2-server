"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const const_1 = require("../../const");
const Valid_1 = require("../../middlewares/Valid");
const validate_1 = require("../../middlewares/validate");
const middleware_1 = __importDefault(require("../../middlewares/middleware"));
const billDetail_controller_1 = __importDefault(require("./billDetail.controller"));
const billDetail_middleware_1 = __importDefault(require("./billDetail.middleware"));
const createOrUpdateManyBillDetail_dto_1 = __importDefault(require("./dto/createOrUpdateManyBillDetail.dto"));
const getBillDetail_dto_1 = __importDefault(require("./dto/getBillDetail.dto"));
const billDetailRouter = express_1.default.Router();
billDetailRouter.post("/", Valid_1.Valid(createOrUpdateManyBillDetail_dto_1.default, 1 /* body */), validate_1.validateRole(null, const_1.AccountType.users), middleware_1.default.manyAccountNumberIsSame, middleware_1.default.userHaveCompanies, middleware_1.default.companyHaveAccountNumber, billDetail_middleware_1.default.companyHaveBill, billDetail_controller_1.default.createManyBillDetail);
billDetailRouter.get("/", Valid_1.Valid(getBillDetail_dto_1.default, 0 /* query */), validate_1.validateRole(null, const_1.AccountType.users), billDetail_controller_1.default.getBillDetail);
exports.default = billDetailRouter;
//# sourceMappingURL=billDetail.router.js.map