"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const baseResponse_1 = __importDefault(require("../../services/baseResponse"));
const logger_1 = __importDefault(require("../../common/logger"));
const const_1 = require("../../const");
const responseService_1 = __importDefault(require("../../services/responseService"));
const stock_service_1 = __importDefault(require("./stock.service"));
const slugify_1 = __importDefault(require("slugify"));
const baseResponse = new baseResponse_1.default("StockController");
class StockController {
    createStock(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { userId } = req.cookies;
                const stock = yield stock_service_1.default.create(Object.assign(Object.assign({}, req.body), { userCreateId: userId, slugName: slugify_1.default(req.body.name, {
                        lower: true,
                        replacement: '_',
                        trim: true,
                        locale: 'vi'
                    }) }));
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    data: stock,
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    updateStock(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { id } = req.params;
                const stock = yield stock_service_1.default.updateById(id, Object.assign(Object.assign({}, req.body), { createAt: Date.now(), slugName: req.body.name ? slugify_1.default(req.body.name, {
                        lower: true,
                        replacement: '_',
                        trim: true,
                        locale: 'vi'
                    }) : undefined }));
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    data: stock,
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    deleteStock(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { id } = req.params;
                yield stock_service_1.default.deleteById(id);
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    message: "delete success",
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    getStock(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { userId } = req.cookies;
                const { search, page, limit, companyId, type } = req.query;
                let query = {
                    userCreateId: userId,
                    companyId,
                };
                if (search) {
                    query = Object.assign(Object.assign({}, query), { $or: [
                            {
                                name: { $regex: search, $options: "i" },
                            },
                            {
                                shortName: { $regex: search, $options: "i" },
                            },
                            {
                                code: { $regex: search, $options: "i" },
                            },
                        ] });
                }
                if (type) {
                    query = Object.assign(Object.assign({}, query), { type });
                }
                const stock = yield stock_service_1.default.getByQuery({
                    query,
                    page,
                    limit,
                    sort: {
                        name: 1,
                    },
                });
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    data: stock,
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    checkCodeExisted(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { code, companyId } = req.query;
                const { userId } = req.cookies;
                const isCodeExisted = yield stock_service_1.default.getOne({
                    code,
                    companyId,
                    userCreateId: userId,
                });
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    data: {
                        isExisted: isCodeExisted ? true : false,
                    },
                });
            }
            catch (error) {
                logger_1.default.error("checkCodeExisted controller: ", error);
                return baseResponse.serverError(res, error);
            }
        });
    }
}
exports.default = new StockController();
//# sourceMappingURL=stock.controller.js.map