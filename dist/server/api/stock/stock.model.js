"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Stock = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
const schema = new mongoose_1.default.Schema({
    companyId: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
        required: true,
    },
    userCreateId: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
        required: true,
    },
    code: {
        type: String,
        index: "text",
    },
    name: {
        type: String,
        index: "text",
    },
    slugName: {
        type: String,
        index: "text",
    },
    shortName: {
        type: String,
        index: "text",
    },
    tax: [
        {
            accountNumber: {
                type: Number,
            },
            percent: {
                type: Number,
            },
        },
    ],
    unit: {
        type: String,
        trim: true,
    },
    type: {
        type: String,
        enum: ["services", "goods", "fixedAssets", "material"],
        default: "goods",
        required: true,
    },
    note: {
        type: String,
    },
    createAt: {
        type: Date,
        default: Date.now,
    },
}, {
    collection: "Stock",
});
exports.Stock = mongoose_1.default.model("Stock", schema);
//# sourceMappingURL=stock.model.js.map