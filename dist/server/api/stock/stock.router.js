"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const const_1 = require("../../const");
const Valid_1 = require("../../middlewares/Valid");
const validate_1 = require("../../middlewares/validate");
const IdParamsRequest_dto_1 = __importDefault(require("../../const/IdParamsRequest.dto"));
const stock_middleware_1 = __importDefault(require("./stock.middleware"));
const stock_controller_1 = __importDefault(require("./stock.controller"));
const checkCodeExisted_dto_1 = __importDefault(require("./dto/checkCodeExisted.dto"));
const createStock_dto_1 = __importDefault(require("./dto/createStock.dto"));
const getStock_dto_1 = __importDefault(require("./dto/getStock.dto"));
const stockRouter = express_1.default.Router();
stockRouter.get("/", Valid_1.Valid(getStock_dto_1.default, 0 /* query */), validate_1.validateRole(null, const_1.AccountType.users), stock_controller_1.default.getStock);
stockRouter.get("/checkCodeExisted", validate_1.validateRole(null, const_1.AccountType.users), Valid_1.Valid(checkCodeExisted_dto_1.default, 0 /* query */), stock_controller_1.default.checkCodeExisted);
stockRouter.post("/", validate_1.validateRole(null, const_1.AccountType.users), Valid_1.Valid(createStock_dto_1.default, 1 /* body */), stock_middleware_1.default.userHaveCompanies, stock_middleware_1.default.createStock, stock_controller_1.default.createStock);
stockRouter.put("/:id", validate_1.validateRole(null, const_1.AccountType.users), Valid_1.Valid(IdParamsRequest_dto_1.default, 2 /* params */), Valid_1.Valid(createStock_dto_1.default, 1 /* body */), stock_middleware_1.default.userHaveCompanies, stock_middleware_1.default.updateStock, stock_controller_1.default.updateStock);
stockRouter.delete("/:id", validate_1.validateRole(null, const_1.AccountType.users), Valid_1.Valid(IdParamsRequest_dto_1.default, 2 /* params */), stock_controller_1.default.deleteStock);
exports.default = stockRouter;
//# sourceMappingURL=stock.router.js.map