"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.StockService = void 0;
const stock_model_1 = require("./stock.model");
const baseDb_1 = __importDefault(require("../../services/baseDb"));
class StockService extends baseDb_1.default {
}
exports.StockService = StockService;
exports.default = new StockService(stock_model_1.Stock);
//# sourceMappingURL=stock.service.js.map