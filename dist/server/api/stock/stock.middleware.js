"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const baseResponse_1 = __importDefault(require("../../services/baseResponse"));
const const_1 = require("../../const");
const errorsCode_1 = require("../../const/errorsCode");
const responseService_1 = __importDefault(require("../../services/responseService"));
const stock_validate_1 = __importDefault(require("./stock.validate"));
const companies_validate_1 = __importDefault(require("../companies/companies.validate"));
const baseResponse = new baseResponse_1.default("StockMiddleware");
class StockMiddleware {
    userHaveCompanies(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { companyId } = req.body;
                const { userId } = req.cookies;
                if (companyId) {
                    const checkUserCompanies = yield companies_validate_1.default.isUserHaveCompanies(userId, companyId);
                    if (!checkUserCompanies) {
                        baseResponse.notAccepted({
                            res,
                            errors: [
                                {
                                    code: errorsCode_1.ErrorsCode.companiesNotFound,
                                    field: "companyId",
                                    error: "user did not have companies",
                                },
                            ],
                        });
                    }
                }
                next();
            }
            catch (error) {
                baseResponse.serverError(res, error);
            }
        });
    }
    createStock(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { userId } = req.cookies;
                const { code, companyId } = req.body;
                const isCodeExisted = yield stock_validate_1.default.isCodeExisted(userId, companyId, code);
                if (isCodeExisted) {
                    return responseService_1.default.send(res, const_1.HTTPCode.notAccept, {
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.stockCodeExisted,
                                field: "code",
                            },
                        ],
                    });
                }
                next();
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    updateStock(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { id } = req.params;
                const { userId } = req.cookies;
                const { code, companyId } = req.body;
                const isCodeExisted = yield stock_validate_1.default.isCodeExisted(userId, companyId, code, true, id);
                if (isCodeExisted) {
                    return responseService_1.default.send(res, const_1.HTTPCode.notAccept, {
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.stockCodeExisted,
                                field: "code",
                            },
                        ],
                    });
                }
                next();
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
}
exports.default = new StockMiddleware();
//# sourceMappingURL=stock.middleware.js.map