"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const stock_service_1 = __importDefault(require("./stock.service"));
class stockValidate {
    isCodeExisted(userId, companyId, code, checkId = false, id = "") {
        return __awaiter(this, void 0, void 0, function* () {
            const stock = checkId
                ? yield stock_service_1.default.getOne({
                    code,
                    companyId,
                    userCreateId: userId,
                    _id: { $ne: id },
                })
                : yield stock_service_1.default.getOne({ code, companyId, userCreateId: userId });
            return stock ? true : false;
        });
    }
    isCompanyHaveStock(userId, companyId, stockId) {
        return __awaiter(this, void 0, void 0, function* () {
            const stock = yield stock_service_1.default.getOne({
                userCreateId: userId,
                companyId,
                _id: stockId,
            });
            return stock ? true : false;
        });
    }
}
exports.default = new stockValidate();
//# sourceMappingURL=stock.validate.js.map