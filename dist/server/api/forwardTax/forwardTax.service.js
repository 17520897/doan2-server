"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ForwardTaxService = void 0;
const forwardTax_model_1 = require("./forwardTax.model");
const baseDb_1 = __importDefault(require("../../services/baseDb"));
class ForwardTaxService extends baseDb_1.default {
}
exports.ForwardTaxService = ForwardTaxService;
exports.default = new ForwardTaxService(forwardTax_model_1.ForwardTax);
//# sourceMappingURL=forwardTax.service.js.map