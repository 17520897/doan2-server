"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const baseResponse_1 = __importDefault(require("../../services/baseResponse"));
const forwardTax_service_1 = __importDefault(require("./forwardTax.service"));
const baseResponse = new baseResponse_1.default("ForwardTaxController");
class ForwardTaxController {
    createForwardTax(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { userId } = req.cookies;
                const forwardTax = yield forwardTax_service_1.default.create(Object.assign(Object.assign({}, req.body), { userCreateId: userId }));
                return baseResponse.success({
                    res,
                    data: forwardTax,
                    message: "create forward tax success",
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    updateForwardTax(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { id } = req.params;
                const forwardTax = yield forwardTax_service_1.default.updateById(id, Object.assign(Object.assign({}, req.body), { userCreateAt: Date.now() }));
                return baseResponse.success({
                    res,
                    data: forwardTax,
                    message: "update forward tax success",
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    deleteForwardTax(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { id } = req.params;
                yield forwardTax_service_1.default.deleteById(id);
                return baseResponse.success({
                    res,
                    message: "delete forward tax success",
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    searchForwardTaxBill(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { userId } = req.cookies;
                const { page, limit, companyId, beginBillCreateAt, endBillCreateAt, sortType = -1, sortProperties, receiveAccountNumber, debitAccountNumber, } = req.query;
                let query = {
                    userCreateId: userId,
                    companyId,
                };
                let sort = {};
                if (sortProperties) {
                    sort[sortProperties] = parseInt(sortType);
                }
                if (receiveAccountNumber)
                    query["receiveAccountNumber"] = parseInt(receiveAccountNumber);
                if (debitAccountNumber)
                    query["debitAccountNumber"] = parseInt(debitAccountNumber);
                if (beginBillCreateAt && endBillCreateAt)
                    query = Object.assign(Object.assign({}, query), { $and: [
                            {
                                billCreateAt: { $gte: beginBillCreateAt },
                            },
                            {
                                billCreateAt: { $lte: endBillCreateAt },
                            },
                        ] });
                const forwardTax = yield forwardTax_service_1.default.populate({
                    query,
                    page,
                    limit,
                    sort: Object.assign({}, sort),
                    populate: [
                        {
                            path: "personCreateInfo",
                        },
                        {
                            path: "personReceiveInfo",
                        },
                    ],
                });
                return baseResponse.success({
                    res,
                    data: forwardTax,
                    message: "get forward tax success",
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
}
exports.default = new ForwardTaxController();
//# sourceMappingURL=forwardTax.controller.js.map