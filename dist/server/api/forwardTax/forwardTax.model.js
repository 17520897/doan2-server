"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ForwardTax = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
const schema = new mongoose_1.default.Schema({
    companyId: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
        required: true,
        index: true,
    },
    userCreateId: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
        required: true,
        index: true,
    },
    accountNumberId: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
        index: true,
    },
    receiveAccountNumber: {
        type: Number,
        index: true,
    },
    debitAccountNumber: {
        type: Number,
        index: true,
    },
    note: {
        type: String,
    },
    finalMoney: {
        type: Number,
        required: true,
    },
    billCreateAt: {
        type: Date,
        required: true,
    },
    userCreateAt: {
        type: Date,
        default: Date.now,
    },
}, {
    collection: "ForwardTax",
});
schema.virtual("userCreateInfo", {
    ref: "User",
    localField: "userCreateId",
    foreignField: "_id",
    justOne: true,
});
schema.virtual("companyInfo", {
    ref: "Companies",
    localField: "companyId",
    foreignField: "_id",
    justOne: true,
});
schema.virtual("stockInfo", {
    ref: "Stock",
    localField: "stockId",
    foreignField: "_id",
    justOne: true,
});
schema.set("toObject", { virtuals: true });
schema.set("toJSON", { virtuals: true });
exports.ForwardTax = mongoose_1.default.model("ForwardTax", schema);
//# sourceMappingURL=forwardTax.model.js.map