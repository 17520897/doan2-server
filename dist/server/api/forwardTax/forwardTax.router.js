"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const const_1 = require("../../const");
const Valid_1 = require("../../middlewares/Valid");
const validate_1 = require("../../middlewares/validate");
const IdParamsRequest_dto_1 = __importDefault(require("../../const/IdParamsRequest.dto"));
const createOrUpdateForwardTax_dto_1 = __importDefault(require("./dto/createOrUpdateForwardTax.dto"));
const forwardTax_middleware_1 = __importDefault(require("./forwardTax.middleware"));
const middleware_1 = __importDefault(require("../../middlewares/middleware"));
const forwardTax_controller_1 = __importDefault(require("./forwardTax.controller"));
const getForwardTax_dto_1 = __importDefault(require("./dto/getForwardTax.dto"));
const forwardTaxRouter = express_1.default.Router();
forwardTaxRouter.post("/", Valid_1.Valid(createOrUpdateForwardTax_dto_1.default, 1 /* body */), validate_1.validateRole(null, const_1.AccountType.users), middleware_1.default.accountNumberIsSame, middleware_1.default.companyHaveAccountNumber, middleware_1.default.existedAccountNumber, forwardTax_controller_1.default.createForwardTax);
forwardTaxRouter.put("/:id", Valid_1.Valid(IdParamsRequest_dto_1.default, 2 /* params */), Valid_1.Valid(createOrUpdateForwardTax_dto_1.default, 1 /* body */), validate_1.validateRole(null, const_1.AccountType.users), middleware_1.default.accountNumberIsSame, middleware_1.default.companyHaveAccountNumber, middleware_1.default.existedAccountNumber, forwardTax_middleware_1.default.userHaveForwardTax, forwardTax_controller_1.default.updateForwardTax);
forwardTaxRouter.delete("/:id", Valid_1.Valid(IdParamsRequest_dto_1.default, 2 /* params */), validate_1.validateRole(null, const_1.AccountType.users), forwardTax_middleware_1.default.userHaveForwardTax, forwardTax_controller_1.default.deleteForwardTax);
forwardTaxRouter.get("/", Valid_1.Valid(getForwardTax_dto_1.default, 0 /* query */), validate_1.validateRole(null, const_1.AccountType.users), forwardTax_controller_1.default.searchForwardTaxBill);
exports.default = forwardTaxRouter;
//# sourceMappingURL=forwardTax.router.js.map