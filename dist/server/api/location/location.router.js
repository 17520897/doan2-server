"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const location_controller_1 = __importDefault(require("./location.controller"));
const getLocaion_dto_1 = __importDefault(require("./dto/getLocaion.dto"));
const Valid_1 = require("../../middlewares/Valid");
const getByAddressLocation_dto_1 = __importDefault(require("./dto/getByAddressLocation.dto"));
const location_middleware_1 = __importDefault(require("./location.middleware"));
const getAddressByCode_dto_1 = __importDefault(require("./dto/getAddressByCode.dto"));
const locationRouter = express_1.default.Router();
locationRouter.get("/", Valid_1.Valid(getLocaion_dto_1.default, 0 /* query */), location_controller_1.default.getLocation);
locationRouter.get("/address", Valid_1.Valid(getByAddressLocation_dto_1.default, 0 /* query */), location_middleware_1.default.getByAddressLocation, location_controller_1.default.getByAddressLocation);
locationRouter.get("/code", Valid_1.Valid(getAddressByCode_dto_1.default, 0 /* query */), location_controller_1.default.getAddressByCode);
locationRouter.get("/child/code", location_controller_1.default.getAllChildByCodeArray);
exports.default = locationRouter;
//# sourceMappingURL=location.router.js.map