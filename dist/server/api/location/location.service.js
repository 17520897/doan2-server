"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocationService = void 0;
const city_json_1 = __importDefault(require("../../../config/city.json"));
const districts_json_1 = __importDefault(require("../../../config/districts.json"));
const wards_json_1 = __importDefault(require("../../../config/wards.json"));
class LocationService {
    //lấy toàn bộ dữ liệu từ level và parent code của dữ liệu
    getByLevel({ level, parentCode }) {
        level = parseInt(level);
        parentCode = parseInt(parentCode);
        if (level === 2) {
            return districts_json_1.default.filter((item) => {
                return item.parentCode === parentCode;
            });
        }
        else if (level === 3) {
            return wards_json_1.default.filter((item) => {
                return item.parentCode === parentCode;
            });
        }
        else {
            return city_json_1.default;
        }
    }
    getByAddress({ city, district, ward }) {
        city = parseInt(city);
        district = parseInt(district);
        ward = parseInt(ward);
        let checkDistrict = districts_json_1.default.filter((item) => {
            return item.code === district;
        });
        let checkWard = wards_json_1.default.filter((item) => {
            return item.code === ward;
        });
        if (checkDistrict.length !== 1 || checkDistrict[0].parentCode !== city)
            return null;
        else if (checkWard.length !== 1 || checkWard[0].parentCode !== district)
            return null;
        else
            return {
                city: city_json_1.default[city],
                district: checkDistrict[0],
                ward: checkWard[0],
            };
    }
    //Lấy record từ level và mã code của record
    getByCode({ level, code }) {
        level = parseInt(level);
        code = parseInt(code);
        if (level === 2) {
            const district = districts_json_1.default.filter((item) => {
                return item.code === code;
            });
            return district.length === 1 ? district[0] : null;
        }
        else if (level === 3) {
            const ward = wards_json_1.default.filter((item) => {
                return item.code === code;
            });
            return ward.length === 1 ? ward[0] : null;
        }
        else {
            return city_json_1.default[code];
        }
    }
    getAddressByCode(code) {
        code = parseInt(code);
        let ward = wards_json_1.default.filter((item) => {
            return item.code === code;
        });
        if (ward.length === 1) {
            let district = districts_json_1.default.filter((item) => {
                return item.code === ward[0].parentCode;
            });
            let city = city_json_1.default[district[0].parentCode];
            return {
                city,
                district: district[0],
                ward: ward[0],
            };
        }
        else {
            let district = districts_json_1.default.filter((item) => {
                return item.code === code;
            });
            if (district.length === 1) {
                let city = city_json_1.default[district[0].parentCode];
                return {
                    city,
                    district: district[0],
                };
            }
            else {
                let city = city_json_1.default[code];
                if (city)
                    return {
                        city,
                    };
                else
                    return null;
            }
        }
    }
    getAllChildByCode(code) {
        code = parseInt(code);
        //Tìm tất cả quận của một thành phố nếu code đó là thành phố
        let districtOfCode = districts_json_1.default.filter((item) => {
            return item.parentCode === code;
        });
        if (districtOfCode.length <= 0) {
            //Tìm tất cả phường của một quận nếu code là quận
            let wardOfCode = wards_json_1.default.filter((item) => {
                return item.parentCode === code;
            });
            return wardOfCode;
        }
        else
            return districtOfCode;
    }
}
exports.LocationService = LocationService;
exports.default = new LocationService();
//# sourceMappingURL=location.service.js.map