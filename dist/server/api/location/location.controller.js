"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocationController = void 0;
const baseResponse_1 = __importDefault(require("../../services/baseResponse"));
const responseService_1 = __importDefault(require("../../services/responseService"));
const location_service_1 = __importDefault(require("./location.service"));
const const_1 = require("../../const");
const baseResponse = new baseResponse_1.default("LocationController");
class LocationController {
    getLocation(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let { level, parentCode } = req.query;
                let data = location_service_1.default.getByLevel({ level, parentCode });
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    message: "get location by level success",
                    data,
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    getByAddressLocation(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let { city, district, ward } = req.query;
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    message: "get address success",
                    data: location_service_1.default.getByAddress({ city, district, ward }),
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    getAddressByCode(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let { code } = req.query;
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    message: "get address by code success",
                    data: location_service_1.default.getAddressByCode(code),
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    getAllChildByCodeArray(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                let { code } = req.query;
                code = code.toString().split(",");
                const locations = {};
                const codeLength = code.length;
                for (let i = 0; i < code.length; i++) {
                    const location = location_service_1.default.getAllChildByCode(code[i]);
                    locations[`${code[i]}`] = location;
                }
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    data: locations,
                    message: "get child by code success ",
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
}
exports.LocationController = LocationController;
exports.default = new LocationController();
//# sourceMappingURL=location.controller.js.map