"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.LocationMiddleware = void 0;
const baseResponse_1 = __importDefault(require("../../services/baseResponse"));
const responseService_1 = __importDefault(require("../../services/responseService"));
const errorsCode_1 = require("../../const/errorsCode");
const location_service_1 = __importDefault(require("./location.service"));
const const_1 = require("../../const");
const baseResponse = new baseResponse_1.default("LocationMiddleware");
class LocationMiddleware {
    //   async getLocation(req: Request, res: Response, next: NextFunction) {
    //     try {
    //       let { parentCode, level } = req.query as any;
    //       parentCode = parseInt(parentCode);
    //       level = parseInt(level);
    //       if (level > 1 && parentCode < 0) {
    //         return responseService.send(res, HTTPCode.success, {
    //           errors: [
    //             {
    //               code: ErrorsCode.locationParentCodeMustBeGreaterThanZero,
    //               error: "parent code must be greater than zero",
    //             },
    //           ],
    //         });
    //       }
    //       next();
    //     } catch (error) {
    //       ;
    //
    //       return responseService.send(res, HTTPCode.serverError, {
    //         errors: [
    //           {
    //             code: ErrorsCode.serverError,
    //             error: error.message,
    //           },
    //         ],
    //       });
    //     }
    //   }
    getByAddressLocation(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { district, ward, city } = req.query;
                const checkDistrict = location_service_1.default.getByCode({
                    level: 2,
                    code: district,
                });
                if (!checkDistrict) {
                    return responseService_1.default.send(res, const_1.HTTPCode.notAccept, {
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.locationCodeNotFound,
                                error: "district code not found",
                                field: "district",
                            },
                        ],
                    });
                }
                if (checkDistrict.parentCode !== parseInt(city)) {
                    return responseService_1.default.send(res, const_1.HTTPCode.notAccept, {
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.locationDistrictNotInThisCity,
                                error: "this district not in this city",
                                field: "ward",
                            },
                        ],
                    });
                }
                const checkWard = location_service_1.default.getByCode({
                    level: 3,
                    code: ward,
                });
                if (!checkWard) {
                    return responseService_1.default.send(res, const_1.HTTPCode.notAccept, {
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.locationCodeNotFound,
                                error: "district code not found",
                                field: "ward",
                            },
                        ],
                    });
                }
                if (checkWard.parentCode !== parseInt(district)) {
                    return responseService_1.default.send(res, const_1.HTTPCode.notAccept, {
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.locationWardNotInThisDistrict,
                                error: "this ward not in this district",
                            },
                        ],
                    });
                }
                next();
            }
            catch (error) {
                return responseService_1.default.send(res, const_1.HTTPCode.serverError, {
                    errors: [
                        {
                            code: errorsCode_1.ErrorsCode.serverError,
                            error: error.message,
                        },
                    ],
                });
            }
        });
    }
}
exports.LocationMiddleware = LocationMiddleware;
exports.default = new LocationMiddleware();
//# sourceMappingURL=location.middleware.js.map