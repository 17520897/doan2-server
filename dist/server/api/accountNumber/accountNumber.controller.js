"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const const_1 = require("../../const");
const baseResponse_1 = __importDefault(require("../../services/baseResponse"));
const responseService_1 = __importDefault(require("../../services/responseService"));
const accountNumber_service_1 = __importDefault(require("./accountNumber.service"));
const baseResponse = new baseResponse_1.default("AccountNumberController");
class AccountNumberController {
    createAccountNumber(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { adminId } = req.cookies;
                const accountNumber = yield accountNumber_service_1.default.create(Object.assign(Object.assign({}, req.body), { adminCreateId: adminId }));
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    data: accountNumber,
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    updateAccountNumber(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { id } = req.params;
                const { adminId } = req.cookies;
                const accountNumber = yield accountNumber_service_1.default.updateById(id, Object.assign(Object.assign({}, req.body), { adminCreateId: adminId }));
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    data: accountNumber,
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    getAccountNumber(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const accountNumber = yield accountNumber_service_1.default.getByQuery({});
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    data: accountNumber,
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    getByIdAccountNumber(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { id } = req.params;
                console.log(id);
                const accountNumber = yield accountNumber_service_1.default.getById(id);
                return baseResponse.success({
                    res,
                    data: accountNumber,
                    message: "get by id account number",
                    success: true,
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    deleteAccountNumber(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { id } = req.params;
                yield accountNumber_service_1.default.deleteById(id);
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    message: "delete account number success",
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
}
exports.default = new AccountNumberController();
//# sourceMappingURL=accountNumber.controller.js.map