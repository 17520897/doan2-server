"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const const_1 = require("../../const");
const IdParamsRequest_dto_1 = __importDefault(require("../../const/IdParamsRequest.dto"));
const permissions_1 = require("../../const/permissions");
const Valid_1 = require("../../middlewares/Valid");
const validate_1 = require("../../middlewares/validate");
const accountNumber_controller_1 = __importDefault(require("./accountNumber.controller"));
const accountNumber_middleware_1 = __importDefault(require("./accountNumber.middleware"));
const createAccountNumber_dto_1 = __importDefault(require("./dto/createAccountNumber.dto"));
const updateAccountNumber_dto_1 = __importDefault(require("./dto/updateAccountNumber.dto"));
const accountNumberRouter = express_1.default.Router();
accountNumberRouter.post("/", Valid_1.Valid(createAccountNumber_dto_1.default, 1 /* body */), validate_1.validateRole(permissions_1.PermissionKeys.accountNumberCreate, const_1.AccountType.admin), accountNumber_middleware_1.default.checkFormatAccountNumber, accountNumber_middleware_1.default.createAccountNumber, accountNumber_controller_1.default.createAccountNumber);
accountNumberRouter.put("/:id", Valid_1.Valid(IdParamsRequest_dto_1.default, 2 /* params */), Valid_1.Valid(updateAccountNumber_dto_1.default, 1 /* body */), validate_1.validateRole(permissions_1.PermissionKeys.accountNumberUpdate, const_1.AccountType.admin), accountNumber_middleware_1.default.checkFormatAccountNumber, accountNumber_middleware_1.default.updateAccountNumber, accountNumber_controller_1.default.updateAccountNumber);
accountNumberRouter.delete("/:id", Valid_1.Valid(IdParamsRequest_dto_1.default, 2 /* params */), validate_1.validateRole(permissions_1.PermissionKeys.accountNumberDelete, const_1.AccountType.admin), accountNumber_controller_1.default.deleteAccountNumber);
accountNumberRouter.get("/", validate_1.validateRole(null, const_1.AccountType.all), accountNumber_controller_1.default.getAccountNumber);
accountNumberRouter.get("/:id", Valid_1.Valid(IdParamsRequest_dto_1.default, 2 /* params */), validate_1.validateRole(null, const_1.AccountType.all), accountNumber_controller_1.default.getByIdAccountNumber);
exports.default = accountNumberRouter;
//# sourceMappingURL=accountNumber.router.js.map