"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccountNumber = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
const schema = new mongoose_1.default.Schema({
    circulars: {
        type: String,
    },
    accountNumbers: [
        {
            accountName: {
                type: String,
            },
            accountNumber: {
                type: Number,
            },
            parentNumber: {
                type: Number,
            },
            level: {
                type: Number,
                default: 1,
            },
        },
    ],
    adminCreateId: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
    },
}, {
    collection: "AccountNumber",
});
schema.virtual("adminCreateInfo", {
    ref: "Admin",
    localField: "adminCreateId",
    foreignField: "_id",
    justOne: true,
});
schema.set("toObject", { virtuals: true });
schema.set("toJSON", { virtuals: true });
exports.AccountNumber = mongoose_1.default.model("AccountNumber", schema);
//# sourceMappingURL=accountNumber.model.js.map