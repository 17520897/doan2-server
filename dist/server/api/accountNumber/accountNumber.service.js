"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AccountNumberService = void 0;
const accountNumber_model_1 = require("./accountNumber.model");
const baseDb_1 = __importDefault(require("../../services/baseDb"));
class AccountNumberService extends baseDb_1.default {
}
exports.AccountNumberService = AccountNumberService;
exports.default = new AccountNumberService(accountNumber_model_1.AccountNumber);
//# sourceMappingURL=accountNumber.service.js.map