"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const baseResponse_1 = __importDefault(require("../../services/baseResponse"));
const const_1 = require("../../const");
const errorsCode_1 = require("../../const/errorsCode");
const responseService_1 = __importDefault(require("../../services/responseService"));
const accountNumber_service_1 = __importDefault(require("./accountNumber.service"));
const baseResponse = new baseResponse_1.default("AccountNumberMiddleware");
class AccountNumberMiddleware {
    checkFormatAccountNumber(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { accountNumbers } = req.body;
                if (accountNumbers) {
                    const accountNumberLength = accountNumbers.length;
                    for (let i = 0; i < accountNumberLength; i++) {
                        for (let j = 0; j < accountNumberLength; j++) {
                            if ((accountNumbers[j].accountNumber ===
                                accountNumbers[i].accountNumber &&
                                j !== i) ||
                                !accountNumbers[i].level ||
                                !accountNumbers[j].level) {
                                return responseService_1.default.send(res, const_1.HTTPCode.notAccept, {
                                    errors: [
                                        {
                                            code: errorsCode_1.ErrorsCode.accountNumberArrayAccountNumbersWrongFormat,
                                            field: "accountNumbers",
                                        },
                                    ],
                                });
                            }
                        }
                    }
                }
                next();
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    createAccountNumber(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { circulars } = req.body;
                const accountNumber = yield accountNumber_service_1.default.getOne({ circulars });
                if (accountNumber) {
                    return responseService_1.default.send(res, const_1.HTTPCode.notAccept, {
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.accountNumberCircularsExisted,
                                field: "circulars",
                            },
                        ],
                    });
                }
                next();
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    updateAccountNumber(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { id } = req.params;
                const { circulars } = req.body;
                const accountNumber = yield accountNumber_service_1.default.getOne({
                    circulars,
                    _id: { $ne: id },
                });
                if (accountNumber) {
                    return responseService_1.default.send(res, const_1.HTTPCode.notAccept, {
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.accountNumberCircularsExisted,
                                field: "circulars",
                            },
                        ],
                    });
                }
                next();
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
}
exports.default = new AccountNumberMiddleware();
//# sourceMappingURL=accountNumber.middleware.js.map