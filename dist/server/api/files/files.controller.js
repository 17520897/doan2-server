"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Controller = void 0;
const const_1 = require("../../const");
const responseService_1 = __importDefault(require("../../services/responseService"));
class Controller {
    uploadImages(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { url, originalName } = req.body;
            return responseService_1.default.send(res, const_1.HTTPCode.success, {
                data: { url, originalName },
                message: "upload file success",
            });
        });
    }
}
exports.Controller = Controller;
exports.default = new Controller();
//# sourceMappingURL=files.controller.js.map