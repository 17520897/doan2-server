"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const files_controller_1 = __importDefault(require("./files.controller"));
const uploadFile_1 = __importDefault(require("../../services/uploadFile"));
const files_middleware_1 = __importDefault(require("./files.middleware"));
const fileRouter = express_1.default.Router();
fileRouter.post("/images", uploadFile_1.default.uploadFile(["jpg", "jpeg", "png"]).single("file"), files_middleware_1.default.uploadImages, files_controller_1.default.uploadImages);
fileRouter.post("/xlsx", uploadFile_1.default.uploadFile(["xls", "xlsx"]).single("file"), files_middleware_1.default.uploadXlsx, files_controller_1.default.uploadImages);
exports.default = fileRouter;
//# sourceMappingURL=files.router.js.map