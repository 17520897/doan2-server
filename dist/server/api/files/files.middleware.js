"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Middleware = void 0;
const responseService_1 = __importDefault(require("../../services/responseService"));
const errorsCode_1 = require("../../const/errorsCode");
const baseResponse_1 = __importDefault(require("../../services/baseResponse"));
const const_1 = require("../../const");
const baseResponse = new baseResponse_1.default(`FilesMiddleware`);
class Middleware {
    uploadImages(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { filename, originalname } = req.file;
                if (filename === "null") {
                    return responseService_1.default.send(res, const_1.HTTPCode.notAccept, {
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.filesOnlyAcceptImageFiles,
                                error: "not accept this file - only support: jpg, jpeg, png",
                                field: "file",
                            },
                        ],
                    });
                }
                req.body.url = `${process.env.UPLOAD_FOLDER}/${filename}`;
                req.body.originalName = req.file.originalname;
                next();
            }
            catch (error) {
                return responseService_1.default.send(res, const_1.HTTPCode.serverError, {
                    errors: [
                        {
                            code: errorsCode_1.ErrorsCode.serverError,
                            error: error.message,
                        },
                    ],
                });
            }
        });
    }
    uploadXlsx(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { filename, originalname } = req.file;
                if (filename === "null") {
                    return responseService_1.default.send(res, const_1.HTTPCode.notAccept, {
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.filesOnlyAcceptImageFiles,
                                error: "not accept this file - only support: xls, xlsx",
                                field: "file",
                            },
                        ],
                    });
                }
                req.body.url = `${process.env.UPLOAD_FOLDER}/${filename}`;
                req.body.originalName = req.file.originalname;
                next();
            }
            catch (error) {
                return responseService_1.default.send(res, const_1.HTTPCode.serverError, {
                    errors: [
                        {
                            code: errorsCode_1.ErrorsCode.serverError,
                            error: error.message,
                        },
                    ],
                });
            }
        });
    }
}
exports.Middleware = Middleware;
exports.default = new Middleware();
//# sourceMappingURL=files.middleware.js.map