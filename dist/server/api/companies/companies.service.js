"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.CompaniesService = void 0;
const companies_model_1 = require("./companies.model");
const baseDb_1 = __importDefault(require("../../services/baseDb"));
class CompaniesService extends baseDb_1.default {
}
exports.CompaniesService = CompaniesService;
exports.default = new CompaniesService(companies_model_1.Companies);
//# sourceMappingURL=companies.service.js.map