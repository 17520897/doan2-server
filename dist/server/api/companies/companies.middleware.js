"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const const_1 = require("../../const");
const errorsCode_1 = require("../../const/errorsCode");
const baseResponse_1 = __importDefault(require("../../services/baseResponse"));
const responseService_1 = __importDefault(require("../../services/responseService"));
const services_1 = __importDefault(require("../../services/services"));
const accountNumber_service_1 = __importDefault(require("../accountNumber/accountNumber.service"));
const systemConst_service_1 = __importDefault(require("../systemConst/systemConst.service"));
const companies_service_1 = __importDefault(require("./companies.service"));
const baseResponse = new baseResponse_1.default("CompaniesMiddleware");
class CompaniesMiddleware {
    checkRightAddressCompanies(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { address } = req.body;
                const isRightAddress = services_1.default.isRightAddress(address);
                if (address && !isRightAddress.success) {
                    return responseService_1.default.send(res, const_1.HTTPCode.notAccept, {
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.companiesIsWrongAddress,
                                field: "address",
                            },
                        ],
                    });
                }
                req.body.address = Object.assign(Object.assign({}, req.body.address), { cityText: isRightAddress.address.city.fullName, districtText: isRightAddress.address.district.fullName, wardText: isRightAddress.address.ward.fullName });
                next();
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    checkAccountNumberIdExisted(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { accountNumberId } = req.body;
                if (accountNumberId) {
                    const accountNumberExisted = yield accountNumber_service_1.default.getById(accountNumberId);
                    if (!accountNumberExisted) {
                        return baseResponse.notAccepted({
                            message: "account number not found",
                            res,
                            errors: [
                                {
                                    code: errorsCode_1.ErrorsCode.accountNumberNotExisted,
                                    field: "accountNumberId",
                                },
                            ],
                        });
                    }
                }
                next();
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    userCreateCompanies(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { name, taxCode } = req.body;
                const { userId } = req.cookies;
                const [isNameExisted, isTaxCodeExisted, systemConst] = yield Promise.all([
                    companies_service_1.default.getOne({
                        name,
                        status: const_1.CompaniesStatus.normal,
                        userCreateId: userId,
                    }),
                    companies_service_1.default.getOne({
                        taxCode,
                        status: const_1.CompaniesStatus.normal,
                        userCreateId: userId,
                    }),
                    systemConst_service_1.default.getOne({}),
                ]);
                if (isNameExisted) {
                    return responseService_1.default.send(res, const_1.HTTPCode.notAccept, {
                        errors: [
                            {
                                field: "name",
                                error: errorsCode_1.ErrorsCode.companiesNameExisted,
                            },
                        ],
                    });
                }
                if (isTaxCodeExisted) {
                    return responseService_1.default.send(res, const_1.HTTPCode.notAccept, {
                        errors: [
                            {
                                field: "taxCode",
                                error: errorsCode_1.ErrorsCode.companiesTaxCodeExisted,
                            },
                        ],
                    });
                }
                next();
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    userUpdateCompanies(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { id } = req.params;
                const { name, taxCode } = req.body;
                const { userId } = req.cookies;
                const [isNameExisted, isTaxCodeExisted] = yield Promise.all([
                    companies_service_1.default.getOne({
                        _id: { $ne: id },
                        name,
                        status: const_1.CompaniesStatus.normal,
                        userCreateId: userId,
                    }),
                    companies_service_1.default.getOne({
                        _id: { $ne: id },
                        taxCode,
                        status: const_1.CompaniesStatus.normal,
                        userCreateId: userId,
                    }),
                ]);
                if (isNameExisted) {
                    return responseService_1.default.send(res, const_1.HTTPCode.notAccept, {
                        errors: [
                            {
                                field: "name",
                                error: errorsCode_1.ErrorsCode.companiesNameExisted,
                            },
                        ],
                    });
                }
                if (isTaxCodeExisted) {
                    return responseService_1.default.send(res, const_1.HTTPCode.notAccept, {
                        errors: [
                            {
                                field: "taxCode",
                                error: errorsCode_1.ErrorsCode.companiesTaxCodeExisted,
                            },
                        ],
                    });
                }
                next();
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    userHaveCompanies(req, res, next) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { id } = req.params;
                const { userId } = req.cookies;
                const checkUsedHaveCompany = yield companies_service_1.default.getOne({
                    userCreateId: userId,
                    _id: id,
                    status: const_1.CompaniesStatus.normal,
                });
                if (!checkUsedHaveCompany) {
                    return responseService_1.default.send(res, const_1.HTTPCode.notAccept, {
                        errors: [
                            {
                                code: errorsCode_1.ErrorsCode.companiesNotFound,
                                field: "id",
                            },
                        ],
                    });
                }
                next();
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
}
exports.default = new CompaniesMiddleware();
//# sourceMappingURL=companies.middleware.js.map