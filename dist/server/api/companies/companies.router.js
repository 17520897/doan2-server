"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const const_1 = require("../../const");
const Valid_1 = require("../../middlewares/Valid");
const validate_1 = require("../../middlewares/validate");
const companies_controller_1 = __importDefault(require("./companies.controller"));
const companies_middleware_1 = __importDefault(require("./companies.middleware"));
const createCompanies_dto_1 = __importDefault(require("./dto/createCompanies.dto"));
const getByIdCompanies_dto_1 = __importDefault(require("./dto/getByIdCompanies.dto"));
const companiesRouter = express_1.default.Router();
companiesRouter.get("/", validate_1.validateRole(null, const_1.AccountType.users), companies_controller_1.default.userGetListCompanies);
companiesRouter.get("/:id", Valid_1.Valid(getByIdCompanies_dto_1.default, 2 /* params */), validate_1.validateRole(null, const_1.AccountType.users), companies_controller_1.default.userGetByIdCompanies);
companiesRouter.post("/", Valid_1.Valid(createCompanies_dto_1.default, 1 /* body */), validate_1.validateRole(null, const_1.AccountType.users), companies_middleware_1.default.checkAccountNumberIdExisted, companies_middleware_1.default.checkRightAddressCompanies, companies_middleware_1.default.userCreateCompanies, companies_controller_1.default.userCreateCompanies);
companiesRouter.put("/:id", validate_1.validateRole(null, const_1.AccountType.users), companies_middleware_1.default.userHaveCompanies, companies_middleware_1.default.checkRightAddressCompanies, companies_middleware_1.default.userUpdateCompanies, companies_controller_1.default.userUpdateCompanies);
companiesRouter.delete("/:id", validate_1.validateRole(null, const_1.AccountType.users), companies_middleware_1.default.userHaveCompanies, companies_controller_1.default.userDeleteCompanies);
companiesRouter.put("/used/:id", validate_1.validateRole(null, const_1.AccountType.users), companies_middleware_1.default.userHaveCompanies, companies_controller_1.default.userCheckIsUsedCompanies);
exports.default = companiesRouter;
//# sourceMappingURL=companies.router.js.map