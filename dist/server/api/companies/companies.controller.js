"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const const_1 = require("../../const");
const responseService_1 = __importDefault(require("../../services/responseService"));
const companies_service_1 = __importDefault(require("./companies.service"));
const mongoose_1 = __importDefault(require("mongoose"));
const companyType_service_1 = __importDefault(require("../companyType/companyType.service"));
const baseResponse_1 = __importDefault(require("../../services/baseResponse"));
const baseResponse = new baseResponse_1.default("CompaniesController");
class CompaniesController {
    userCreateCompanies(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const session = yield mongoose_1.default.startSession();
            session.startTransaction();
            try {
                const options = { session };
                const { userId } = req.cookies;
                const { companyTypeId } = req.body;
                const companies = yield companies_service_1.default.create(Object.assign(Object.assign({}, req.body), { userCreateId: userId }), options);
                yield companyType_service_1.default.updateById(companyTypeId, {
                    $inc: {
                        numberOfCompany: 1,
                    },
                }, options);
                yield session.commitTransaction();
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    data: companies,
                });
            }
            catch (error) {
                yield session.abortTransaction();
                return baseResponse.serverError(res, error);
            }
            finally {
                session.endSession();
            }
        });
    }
    userGetListCompanies(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { userId } = req.cookies;
                const { page, limit, search } = req.query;
                let query = {
                    status: const_1.CompaniesStatus.normal,
                    userCreateId: userId,
                };
                if (search) {
                    query = Object.assign(Object.assign({}, query), { $or: [
                            {
                                name: { $regex: search, $options: "i" },
                            },
                            {
                                taxCode: { $regex: search, $options: "i" },
                            },
                            {
                                phone: { $regex: search, $options: "i" },
                            },
                        ] });
                }
                const listCompanies = yield companies_service_1.default.populate({
                    query,
                    populate: [
                        {
                            path: "userCreateInfo",
                            select: "-password",
                        },
                        {
                            path: "companyTypeInfo",
                            select: "name",
                        },
                    ],
                    sort: {
                        name: 1,
                        usedAt: -1,
                        createAt: -1,
                    },
                    page,
                    limit,
                });
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    data: listCompanies,
                    message: "get list company type success",
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    userGetByIdCompanies(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { id } = req.params;
                const { userId } = req.cookies;
                const companies = yield companies_service_1.default.getOne({
                    _id: id,
                    userCreateId: userId,
                    status: const_1.CompaniesStatus.normal,
                });
                return baseResponse.success({
                    res,
                    data: companies,
                    message: "get companies by id success",
                    success: true,
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    userUpdateCompanies(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { userId } = req.cookies;
                const { id } = req.params;
                const companies = yield companies_service_1.default.updateById(id, Object.assign(Object.assign({}, req.body), { userCreateId: userId, usedAt: Date.now() }));
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    data: companies,
                    message: "update company success",
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    userDeleteCompanies(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { id } = req.params;
                yield companies_service_1.default.updateById(id, {
                    status: const_1.CompaniesStatus.deleted,
                });
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    message: "delete company type success",
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
    userCheckIsUsedCompanies(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const { id } = req.params;
                yield companies_service_1.default.updateById(id, {
                    usedAt: Date.now(),
                });
                return responseService_1.default.send(res, const_1.HTTPCode.success, {
                    message: "check used company success",
                });
            }
            catch (error) {
                return baseResponse.serverError(res, error);
            }
        });
    }
}
exports.default = new CompaniesController();
//# sourceMappingURL=companies.controller.js.map