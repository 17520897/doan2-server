"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Companies = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
const schema = new mongoose_1.default.Schema({
    companyTypeId: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
    },
    name: {
        type: String,
    },
    taxCode: {
        type: String,
    },
    phone: {
        type: String,
    },
    address: {
        home: {
            type: String,
        },
        ward: {
            type: Number,
        },
        district: {
            type: Number,
        },
        city: {
            type: Number,
        },
        wardText: {
            type: String,
        },
        districtText: {
            type: String,
        },
        cityText: {
            type: String,
        },
    },
    firstInput: {
        type: Date,
    },
    accountNumberId: {
        type: String,
        index: "text",
    },
    status: {
        type: String,
        default: "normal",
        enum: ["normal", "deleted"],
    },
    userCreateId: {
        type: mongoose_1.default.SchemaTypes.ObjectId,
    },
    createAt: {
        type: Date,
        default: Date.now,
    },
    usedAt: {
        type: Date,
        default: Date.now,
    },
}, {
    collection: "Companies",
});
schema.virtual("userCreateInfo", {
    ref: "Users",
    localField: "userCreateId",
    foreignField: "_id",
    justOne: true,
});
schema.virtual("companyTypeInfo", {
    ref: "CompanyType",
    localField: "companyTypeId",
    foreignField: "_id",
    justOne: true,
});
schema.set("toObject", { virtuals: true });
schema.set("toJSON", { virtuals: true });
exports.Companies = mongoose_1.default.model("Companies", schema);
//# sourceMappingURL=companies.model.js.map