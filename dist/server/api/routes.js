"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const admin_router_1 = __importDefault(require("./admin/admin.router"));
const companies_router_1 = __importDefault(require("./companies/companies.router"));
const companyType_router_1 = __importDefault(require("./companyType/companyType.router"));
const permissions_router_1 = __importDefault(require("./permissions/permissions.router"));
const roles_router_1 = __importDefault(require("./roles/roles.router"));
const userRoles_router_1 = __importDefault(require("./userRoles/userRoles.router"));
const users_router_1 = __importDefault(require("./users/users.router"));
const verify_router_1 = __importDefault(require("./verify/verify.router"));
const userPermissions_router_1 = __importDefault(require("./userPermissions/userPermissions.router"));
const systemConst_router_1 = __importDefault(require("./systemConst/systemConst.router"));
const userUsedHistory_router_1 = __importDefault(require("./userUsedHistory/userUsedHistory.router"));
const moneyType_router_1 = __importDefault(require("./moneyType/moneyType.router"));
const accountNumber_router_1 = __importDefault(require("./accountNumber/accountNumber.router"));
const location_router_1 = __importDefault(require("./location/location.router"));
const moneyBill_router_1 = __importDefault(require("./moneyBill/moneyBill.router"));
const person_router_1 = __importDefault(require("./person/person.router"));
const settingTax_router_1 = __importDefault(require("./settingTax/settingTax.router"));
const files_router_1 = __importDefault(require("./files/files.router"));
const settingBank_router_1 = __importDefault(require("./settingBank/settingBank.router"));
const companyBanking_router_1 = __importDefault(require("./companyBanking/companyBanking.router"));
const products_router_1 = __importDefault(require("./products/products.router"));
const serviceBill_router_1 = __importDefault(require("./serviceBill/serviceBill.router"));
const stock_router_1 = __importDefault(require("./stock/stock.router"));
const serviceBillTax_router_1 = __importDefault(require("./serviceBillTax/serviceBillTax.router"));
const serviceBillDetail_router_1 = __importDefault(require("./serviceBillDetail/serviceBillDetail.router"));
const bill_router_1 = __importDefault(require("./bill/bill.router"));
const billDetail_router_1 = __importDefault(require("./billDetail/billDetail.router"));
const billTax_router_1 = __importDefault(require("./billTax/billTax.router"));
const forwardTax_router_1 = __importDefault(require("./forwardTax/forwardTax.router"));
const report_router_1 = __importDefault(require("./report/report.router"));
const router = express_1.default.Router();
router.use("/users", users_router_1.default);
router.use("/verify", verify_router_1.default);
router.use("/admin", admin_router_1.default);
router.use("/accountNumber", accountNumber_router_1.default);
router.use("/companyType", companyType_router_1.default);
router.use("/companies", companies_router_1.default);
router.use("/adminPermissions", permissions_router_1.default);
router.use("/adminRoles", roles_router_1.default);
router.use("/userRoles", userRoles_router_1.default);
router.use("/userPermissions", userPermissions_router_1.default);
router.use("/systemConst", systemConst_router_1.default);
router.use("/userUsedHistory", userUsedHistory_router_1.default);
router.use("/moneyType", moneyType_router_1.default);
router.use("/location", location_router_1.default);
router.use("/moneyBill", moneyBill_router_1.default);
router.use("/person", person_router_1.default);
router.use("/settingTax", settingTax_router_1.default);
router.use("/files", files_router_1.default);
router.use("/settingBank", settingBank_router_1.default);
router.use("/companyBanking", companyBanking_router_1.default);
router.use("/products", products_router_1.default);
router.use("/serviceBill", serviceBill_router_1.default);
router.use("/serviceBillDetail", serviceBillDetail_router_1.default);
router.use("/serviceBillTax", serviceBillTax_router_1.default);
router.use("/bill", bill_router_1.default);
router.use("/billDetail", billDetail_router_1.default);
router.use("/billTax", billTax_router_1.default);
router.use("/stock", stock_router_1.default);
router.use("/forwardTax", forwardTax_router_1.default);
router.use("/userUsedHistory", userUsedHistory_router_1.default);
router.use("/reports", report_router_1.default);
router.get("/test", (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    res.send("XYZ2");
}));
exports.default = router;
//# sourceMappingURL=routes.js.map