"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const routes_1 = __importDefault(require("./api/routes"));
const path_1 = __importDefault(require("path"));
const fs_1 = __importDefault(require("fs"));
function routes(app) {
    app.get("/healthCheck", (req, res) => {
        const { url } = req.query;
        const existed = fs_1.default.existsSync(`${process.env.FILE_UPLOAD_FOLDER}/${url}`);
        return existed ? res.send('yay') : res.send('f*ck');
    });
    app.use("/api", routes_1.default);
    app.use("/docs", (req, res) => {
        const renderPath = __dirname.split("/");
        renderPath.pop();
        res.sendFile(path_1.default.join(renderPath.join("/") + "/public/index.html"));
    });
}
exports.default = routes;
//# sourceMappingURL=routes.js.map