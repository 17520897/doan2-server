"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const path_1 = __importDefault(require("path"));
const http_1 = __importDefault(require("http"));
const os_1 = __importDefault(require("os"));
const cookie_parser_1 = __importDefault(require("cookie-parser"));
const openapi_1 = __importDefault(require("./openapi"));
const logger_1 = __importDefault(require("./logger"));
const morgan_1 = __importDefault(require("morgan"));
const helmet_1 = __importDefault(require("helmet"));
const cors_1 = __importDefault(require("cors"));
const cron_1 = __importDefault(require("cron"));
const log4js_1 = __importDefault(require("log4js"));
const app = express_1.default();
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config();
const CronJob = cron_1.default.CronJob;
class ExpressServer {
    constructor() {
        const root = path_1.default.normalize(__dirname + "/../../..");
        const configRoot = path_1.default.normalize(__dirname + "/../..");
        console.log("TEST ENV: ", process.env.NODE_ENV);
        app.set("appPath", root + "client");
        app.use(morgan_1.default("dev"));
        app.use(express_1.default.json({ limit: process.env.REQUEST_LIMIT || "300kb" }));
        app.use(helmet_1.default());
        app.use(express_1.default.urlencoded({
            extended: true,
            limit: process.env.REQUEST_LIMIT || "300kb",
        }));
        app.use(cors_1.default());
        app.use(cookie_parser_1.default(process.env.SESSION_SECRET));
        app.use(express_1.default.static(`public`));
        try {
            require("fs").mkdirSync("./log");
        }
        catch (e) {
            if (e.code != "EEXIST") {
                console.error("Could not set up log directory, error was: ", e);
                process.exit(1);
            }
        }
        log4js_1.default.configure(`${configRoot}/config/log4js.json`);
    }
    router(routes) {
        openapi_1.default(app, routes);
        return this;
    }
    database(db) {
        db.init();
        return this;
    }
    cronJob(time, job = () => {
        console.log("cron jobs");
    }) {
        const task = new CronJob(time, job, null, true, "Asia/Ho_Chi_Minh");
        task.start();
        return this;
    }
    listen(p = process.env.PORT) {
        const welcome = (port) => () => logger_1.default.info(`up and running in ${process.env.NODE_ENV || "development"} @: ${os_1.default.hostname()} on port: ${port}`);
        http_1.default.createServer(app).listen(p, welcome(p));
        return app;
    }
}
exports.default = ExpressServer;
//# sourceMappingURL=server.js.map