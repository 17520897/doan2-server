"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const log4js_1 = __importDefault(require("log4js"));
const log4j = (path) => {
    return log4js_1.default.getLogger(path);
};
exports.default = log4j;
//# sourceMappingURL=log4js.js.map