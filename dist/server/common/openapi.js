"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const error_handler_1 = __importDefault(require("../middlewares/error.handler"));
function openapi(app, routes) {
    // const apiSpec = path.join(__dirname, "api.yml");
    // app.use(process.env.OPENAPI_SPEC || "/spec", express.static(apiSpec));
    // new OpenApiValidator({
    //   apiSpec
    // }).install(app);
    routes(app);
    app.use(error_handler_1.default);
}
exports.default = openapi;
//# sourceMappingURL=openapi.js.map