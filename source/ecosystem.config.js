const REPO = "git@gitlab.com:17520897/doan2-server.git";
module.exports = {
  apps: [
    {
      name: "kltn",
      script: "/root/kltn/dist/server/index.js",
      env: {
        PORT: 3333,
        NODE_ENV: "staging",
      },
    },
  ],
  deploy: {
    staging: {
      user: "root",
      host: "149.28.172.35",
      ref: "origin/master",
      repo: REPO,
      ssh_options: "StrictHostKeyChecking=no",
      path: `/root/kltn`,
      "pre-deploy": "git pull",
      "post-deploy":
        "npm install && npm run setup staging" +
        " && pm2 startOrRestart ecosystem.config.js" +
        " && pm2 save",
    },
  },
};
