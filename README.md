# CODOSA-PAWN

## Quick Start

Copy file `.env` vào thư mục gốc của dự án

Setup cơ bản ban đầu

```shell
# cài đặt admin account, 63 tỉnh thành và các ngân hàng mặc định
npm run setup dev
```

Thực thi code Dev

```shell
# cài đặt run-rs
npm install -g run-rs

# run db
npm run db

# run dev
npm run dev
```

---

## Lưu ý:

Tạo các branch từ branch dev để code chức năng

- Đặt tên branch: <Tên người thực hiện>/<Tên chức năng>
- Pull code từ dev trước khi push code
- Tạo merge request khi hoàn thành chức năng
- Sau khi được team review code sẽ merge vào branch dev

Quy tắc đặt tên biến:

- Đặt tên biến và hàm theo dạng camelCase
- Tất cả tên biến và hàm đều dùng tiếng Anh
